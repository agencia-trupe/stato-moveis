-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 08/08/2013 às 14h07min
-- Versão do Servidor: 5.5.32
-- Versão do PHP: 5.4.15-1~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `stato_server`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastros`
--

CREATE TABLE IF NOT EXISTS `cadastros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `newsletters_nome` varchar(255) NOT NULL,
  `newsletters_email` varchar(255) NOT NULL,
  `newsletters_data_cadastro` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `cadastros`
--

INSERT INTO `cadastros` (`id`, `newsletters_nome`, `newsletters_email`, `newsletters_data_cadastro`) VALUES
(1, 'Nilton', 'niltondefreitas@gmail.com', 1368472241),
(2, 'Teste', 'nilton@trupe.net', 1368475352);

-- --------------------------------------------------------

--
-- Estrutura da tabela `casos`
--

CREATE TABLE IF NOT EXISTS `casos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordem` int(11) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` text,
  `data` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=89 ;

--
-- Extraindo dados da tabela `casos`
--

INSERT INTO `casos` (`id`, `ordem`, `titulo`, `slug`, `imagem`, `descricao`, `data`) VALUES
(23, 8, 'Anixter', '', 'wok7356_copy.jpg', '<p>Anixter</p>\r\n<p><object id="b84b9a78-cb7a-9587-5b79-daa1e9ba96af" type="application/gas-events-abn" width="0" height="0"></object></p>', NULL),
(26, 14, 'Atilio', '', 'wok2287_copy.jpg', '<p>Atilio</p>', NULL),
(27, 10, 'Banco de Alimentos', '', 'ok4929.jpg', '<p>Banco de Alimentos</p>\r\n<p><object id="ca596a59-e96b-bead-eab9-5ba2ba978da4" type="application/gas-events-abn" width="0" height="0"></object></p>', NULL),
(29, 3, 'BRX', '', 'STATO01.jpg', '<p>BRX</p>', NULL),
(30, 13, 'Câmara Municipal de Pouso Alegre', '', 'DSC05431.jpg', '<p>C&acirc;mara Municipal de Pouso Alegre</p>\r\n<p><object id="a85a2bcb-4a99-82b3-e8e9-dbb13897a1a2" type="application/gas-events-abn" width="0" height="0"></object></p>', NULL),
(31, 17, 'Cartório Ribeirão Pires', '', 'ok4968.jpg', '<p>Cart&oacute;rio Ribeir&atilde;o Pires</p>\r\n<p><object id="39e908aa-3bea-acaa-f9a8-68bf7b91bb9c" type="application/gas-events-abn" width="0" height="0"></object></p>', NULL),
(32, 2, 'Centro de Formação e Integração Social', '', 'a_(1).jpg', '<p>Centro de Forma&ccedil;&atilde;o e Integra&ccedil;&atilde;o Social</p>', NULL),
(33, 9, 'Cethel', '', 'ok7635.jpg', '<p>Cethel</p>', NULL),
(34, 5, 'Colégio Agostiniano', '', 'DSC08727_copy.jpg', '<p>Col&eacute;gio Agostiniano</p>', NULL),
(35, 4, 'Condomínio Edifício Denver', '', '1048.jpg', '<p>Condom&iacute;nio Edif&iacute;cio Denver</p>', NULL),
(38, 16, 'Donnelley', '', 'ook2886_copy.jpg', '<p>Donnelley</p>', NULL),
(39, 12, 'Akiyama Advogados Associados', '', 'IMG_0056.jpg', '<p>Akiyama Advogados Associados</p>\r\n<p><object id="aaa98918-2a29-b489-59d8-088c5886ab8f" type="application/gas-events-abn" width="0" height="0"></object></p>', NULL),
(40, 6, 'Engemon', '', 'engemon.jpg', '<p>Engemon</p>\r\n<p><object id="5b59b858-0b1b-868c-69ab-babd599787aa" type="application/gas-events-abn" width="0" height="0"></object></p>', NULL),
(41, 21, 'Enthae', '', 'ok9872.jpg', '<p>Enthae</p>', NULL),
(42, 11, 'EPC', '', 'DSC07902_compactado.jpg', '<p>EPC</p>', NULL),
(52, 25, 'FDO', '', 'wok8955_copy.jpg', '<p>FDO</p>', NULL),
(53, 29, 'Filipov', '', 'filipov_01.jpg', '<p>Filipov</p>', NULL),
(55, 34, 'GFI', '', 'wok7560_copy.jpg', '<p>GFI</p>', NULL),
(56, 20, 'Global', '', 'wok3252_copy.jpg', '<p>Global</p>', NULL),
(60, 30, 'Kitani', '', 'kitani-02.jpg', '<p>Kitani</p>', NULL),
(61, 18, 'Leone', '', 'wok2107_copy.jpg', '<p>Leone</p>\r\n<p><object id="08ebab68-4bd9-859e-2aba-f8880aa1ac83" type="application/gas-events-abn" width="0" height="0"></object></p>', NULL),
(62, 26, 'LSI', '', 'wok3325_copy.jpg', '<p>LSI</p>', NULL),
(63, 32, 'Luna Import', '', 'wok9799_copy.jpg', '<p>Luna Import</p>', NULL),
(64, 15, 'Nelson Wong', '', 'wok9278_copy.jpg', '<p>Nelson Wong</p>\r\n<p><object id="1adb49fb-2a38-a9a9-293b-aa9c09a7a3ae" type="application/gas-events-abn" width="0" height="0"></object></p>', NULL),
(65, 27, 'Amil', '', 'wok5951_copy.jpg', '<p>Amil</p>\r\n<p><object id="6a880a5b-f8c9-acb3-18eb-5aa20a8d93a1" type="application/gas-events-abn" width="0" height="0"></object></p>', NULL),
(66, 23, 'Medial', '', 'wok3152_copy.jpg', '<p>Medial</p>\r\n<p><object id="98694bd9-dbb8-bb89-1bab-d984c9a49483" type="application/gas-events-abn" width="0" height="0"></object></p>', NULL),
(68, 33, 'Marcos', '', 'DSC04208.jpg', '<p>Marcos</p>', NULL),
(69, 24, 'Medial Recife', '', 'moveis-(2)1.jpg', '<p>Medial Recife</p>', NULL),
(71, 36, 'Ministério Siga me', '', 'wok5825.jpg', '<p>Minist&eacute;rio Siga me</p>', NULL),
(72, 22, 'Open Link', '', 'wok7604_copy.jpg', '<p>Open Link</p>', NULL),
(74, 37, 'Penske', '', 'wok2349_copy.jpg', '<p>Penske</p>', NULL),
(75, 38, 'Polícia Militar SP', '', 'pm_01.jpg', '<p>Pol&iacute;cia Militar SP</p>\r\n<p><object id="d889d91a-6a5b-8e81-6b6b-9b8dd8938287" type="application/gas-events-abn" width="0" height="0"></object></p>', NULL),
(76, 0, 'Qualifertil', '', 'foto.JPG', '<p>Qualifertil</p>', NULL),
(77, 31, 'RDO Diagnósticos', '', 'wok2140_copy.jpg', '<p>RDO Diagn&oacute;sticos</p>', NULL),
(78, 39, 'Reis Office', '', 'wok4428_copy.jpg', '<p>Reis Office</p>', NULL),
(79, 1, 'Sercom', '', 'stato011.jpg', '<p>Sercom</p>', NULL),
(83, 40, 'Triunfo', '', 'ok6066.jpg', '<p>Triunfo</p>', NULL),
(87, 19, 'Weir', '', 'weir_03.jpg', '<p>Weir</p>', NULL),
(88, 7, 'YKP', '', 'auditorio-(6).jpg', '<p>YKP</p>', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `titulo`, `slug`, `ordem`) VALUES
(1, 'Assentos', 'assentos', 0),
(11, 'Call Center', 'call-center', 4),
(12, 'Divisórias Piso Teto', 'divisorias-piso-teto', 5),
(14, 'Operacional', 'operacional', 2),
(15, 'Executiva', 'executiva', 1),
(17, 'Outros Ambientes', 'outros-ambientes', 3),
(18, 'Teste', 'teste', 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cfotos`
--

CREATE TABLE IF NOT EXISTS `cfotos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `caso_id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=563 ;

--
-- Extraindo dados da tabela `cfotos`
--

INSERT INTO `cfotos` (`id`, `caso_id`, `titulo`, `imagem`, `ordem`, `created`, `updated`) VALUES
(1, 0, 'Teste', '1600x9002.jpg', 0, 1368127907, NULL),
(2, 0, 'Teste', '1366x7681.jpg', 0, 1368127965, NULL),
(3, 0, 'Teste', '1280x1024.jpg', 0, 1368128399, NULL),
(4, 0, 'Teste', '4.jpg', 0, 1368128413, NULL),
(5, 0, 'Teste', 'foto-empresa3.jpg', 0, 1368479680, NULL),
(6, 0, 'Teste', 'foto-empresa4.jpg', 0, 1368479684, NULL),
(7, 0, 'Teste', 'foto-empresa5.jpg', 0, 1368479686, NULL),
(8, 0, 'Teste', 'foto-empresa7.jpg', 0, 1368479702, NULL),
(9, 0, 'Teste', 'foto-empresa8.jpg', 0, 1368479705, NULL),
(10, 0, 'Teste', 'foto-empresa9.jpg', 0, 1368479707, NULL),
(11, 0, 'Teste', 'foto-empresa10.jpg', 0, 1368479716, NULL),
(12, 0, 'Teste', 'foto-empresa11.jpg', 0, 1368479718, NULL),
(13, 0, 'Teste', 'foto-empresa12.jpg', 0, 1368479720, NULL),
(14, 0, 'Teste', 'DL254490.jpg', 0, 1368531754, NULL),
(15, 0, 'Teste', 'DL254527.jpg', 0, 1368531762, NULL),
(16, 0, 'Teste', 'PB190360.jpg', 0, 1368531766, NULL),
(17, 0, 'Teste', 'PB190361.jpg', 0, 1368531770, NULL),
(18, 0, 'Teste', 'PB190369.jpg', 0, 1368531773, NULL),
(19, 0, 'Teste', 'PB190372.jpg', 0, 1368531779, NULL),
(20, 0, 'Teste', 'PB190373.jpg', 0, 1368531783, NULL),
(21, 0, 'Teste', 'PB190377.jpg', 0, 1368531787, NULL),
(22, 0, 'Teste', 'PB190391.jpg', 0, 1368531791, NULL),
(23, 0, 'Teste', 'PB210007.jpg', 0, 1368531796, NULL),
(38, 23, 'Teste', 'wok7356_copy.jpg', 0, 1368532449, NULL),
(43, 0, 'Teste', 'DSC07662_copy.jpg', 0, 1368532559, NULL),
(49, 0, 'Teste', 'associacao_03.jpg', 0, 1368532730, NULL),
(51, 26, 'Teste', 'wok2287_copy.jpg', 0, 1368532786, NULL),
(56, 0, 'Teste', 'DL254527.jpg', 0, 1368735299, NULL),
(59, 0, 'Teste', 'PB190369.jpg', 0, 1368735322, NULL),
(60, 0, 'Teste', 'PB190372.jpg', 0, 1368735326, NULL),
(62, 0, 'Teste', 'PB190377.jpg', 0, 1368735335, NULL),
(65, 0, 'Teste', 'amesp-011.jpg', 0, 1368735576, NULL),
(68, 23, 'Teste', 'wok7351_copy.jpg', 0, 1368791808, NULL),
(70, 23, 'Teste', 'wok7364_copy.jpg', 0, 1368791818, NULL),
(71, 23, 'Teste', 'wok7401_copy.jpg', 0, 1368791824, NULL),
(72, 0, 'Teste', 'DSC07651_copy.jpg', 0, 1368791881, NULL),
(73, 0, 'Teste', 'DSC07654.jpg', 0, 1368791901, NULL),
(74, 0, 'Teste', 'DSC07675_copy.jpg', 0, 1368791918, NULL),
(75, 0, 'Teste', 'DSC07676_copy.jpg', 0, 1368791933, NULL),
(76, 0, 'Teste', 'DSC07677.jpg', 0, 1368791951, NULL),
(77, 0, 'Teste', 'DSC07691_copy.jpg', 0, 1368791963, NULL),
(78, 0, 'Teste', 'associacao_02.jpg', 0, 1368791998, NULL),
(79, 0, 'Teste', 'associacao_01.jpg', 0, 1368792003, NULL),
(80, 26, 'Teste', 'wok2275_copy.jpg', 0, 1368792048, NULL),
(87, 0, 'Teste', 'PICT0036.jpg', 0, 1368810374, NULL),
(89, 0, 'Teste', 'PICT0038.jpg', 0, 1368810388, NULL),
(94, 0, 'Teste', 'PICT0046.jpg', 0, 1368810454, NULL),
(96, 0, 'Teste', 'PICT00511.jpg', 0, 1368810475, NULL),
(97, 29, 'Teste', 'STATO02.jpg', 0, 1368810598, NULL),
(98, 29, 'Teste', 'STATO03-2.jpg', 0, 1368810608, NULL),
(99, 29, 'Teste', 'STATO04.jpg', 0, 1368810615, NULL),
(100, 29, 'Teste', 'STATO05-2.jpg', 0, 1368810623, NULL),
(101, 29, 'Teste', 'STATO06-2.jpg', 0, 1368810632, NULL),
(102, 29, 'Teste', 'STATO07-2.jpg', 0, 1368810640, NULL),
(104, 29, 'Teste', 'STATO09.jpg', 0, 1368810654, NULL),
(105, 29, 'Teste', 'STATO10.jpg', 0, 1368810663, NULL),
(133, 30, 'Teste', 'DSC04767.jpg', 0, 1368811135, NULL),
(136, 30, 'Teste', 'DSC05414.jpg', 0, 1368811166, NULL),
(137, 30, 'Teste', 'DSC05418.jpg', 0, 1368811176, NULL),
(138, 30, 'Teste', 'DSC05421.jpg', 0, 1368811186, NULL),
(139, 30, 'Teste', 'DSC05423.jpg', 0, 1368811196, NULL),
(140, 30, 'Teste', 'DSC05428.jpg', 0, 1368811209, NULL),
(147, 31, 'Teste', 'ok4970.jpg', 0, 1368811358, NULL),
(148, 31, 'Teste', 'ok4971.jpg', 0, 1368811391, NULL),
(149, 31, 'Teste', 'ok4978.jpg', 0, 1368811401, NULL),
(150, 31, 'Teste', 'ok4992.jpg', 0, 1368811413, NULL),
(151, 31, 'Teste', 'wok4968_copy.jpg', 0, 1368811421, NULL),
(156, 32, 'Teste', 'a_(2).jpg', 0, 1368811613, NULL),
(157, 32, 'Teste', 'a_(3).jpg', 0, 1368811621, NULL),
(158, 32, 'Teste', 'a_(4).jpg', 0, 1368811630, NULL),
(159, 32, 'Teste', 'a_(5).jpg', 0, 1368811637, NULL),
(160, 32, 'Teste', 'a_(6).jpg', 0, 1368811645, NULL),
(161, 32, 'Teste', 'a_(7).jpg', 0, 1368811652, NULL),
(162, 32, 'Teste', 'a_(8).jpg', 0, 1368811682, NULL),
(163, 32, 'Teste', 'a_(9).jpg', 0, 1368811691, NULL),
(164, 32, 'Teste', 'a_(10).jpg', 0, 1368811701, NULL),
(165, 32, 'Teste', 'a_(11).jpg', 0, 1368811707, NULL),
(166, 33, 'Teste', 'ok7656.jpg', 0, 1368811787, NULL),
(167, 33, 'Teste', 'ok7681.jpg', 0, 1368811795, NULL),
(168, 33, 'Teste', 'ok7693.jpg', 0, 1368811803, NULL),
(169, 33, 'Teste', 'ok7696.jpg', 0, 1368811810, NULL),
(170, 34, 'Teste', 'DSC08729_copy01.jpg', 0, 1368811923, NULL),
(171, 34, 'Teste', 'DSC08737_copy.jpg', 0, 1368811931, NULL),
(172, 34, 'Teste', 'DSC08740_copy.jpg', 0, 1368811945, NULL),
(174, 35, 'Teste', '1062.jpg', 0, 1368812331, NULL),
(175, 35, 'Teste', '1067.jpg', 0, 1368812340, NULL),
(176, 35, 'Teste', '1095.jpg', 0, 1368812346, NULL),
(177, 0, 'Teste', 'credcerto_02.jpg', 0, 1368812450, NULL),
(178, 0, 'Teste', 'credcerto_03.jpg', 0, 1368812456, NULL),
(179, 0, 'Teste', 'credcerto_04.jpg', 0, 1368812463, NULL),
(180, 0, 'Teste', 'credcerto_05.jpg', 0, 1368812469, NULL),
(183, 0, 'Teste', 'Foto2.jpg', 0, 1368812774, NULL),
(184, 0, 'Teste', 'Foto3.jpg', 0, 1368812779, NULL),
(185, 0, 'Teste', 'Foto4.jpg', 0, 1368812788, NULL),
(186, 0, 'Teste', 'Foto5.jpg', 0, 1368812802, NULL),
(187, 0, 'Teste', 'Foto6.jpg', 0, 1368812834, NULL),
(188, 0, 'Teste', 'Foto7.jpg', 0, 1368812841, NULL),
(189, 0, 'Teste', 'Foto8.jpg', 0, 1368812851, NULL),
(190, 0, 'Teste', 'Foto9.jpg', 0, 1368812860, NULL),
(191, 0, 'Teste', 'Foto10.jpg', 0, 1368812865, NULL),
(192, 0, 'Teste', 'Foto11.jpg', 0, 1368812871, NULL),
(193, 0, 'Teste', 'Foto12.jpg', 0, 1368812877, NULL),
(194, 0, 'Teste', 'Foto13.jpg', 0, 1368812885, NULL),
(195, 0, 'Teste', 'Foto14.jpg', 0, 1368812893, NULL),
(196, 0, 'Teste', 'Foto15.jpg', 0, 1368812907, NULL),
(197, 0, 'Teste', 'Foto16.jpg', 0, 1368812915, NULL),
(198, 0, 'Teste', 'Foto18.jpg', 0, 1368812925, NULL),
(199, 0, 'Teste', 'Foto19.jpg', 0, 1368812938, NULL),
(200, 0, 'Teste', 'Foto20.jpg', 0, 1368812950, NULL),
(201, 0, 'Teste', 'Foto21.jpg', 0, 1368812959, NULL),
(202, 0, 'Teste', 'Foto22.jpg', 0, 1368812965, NULL),
(203, 0, 'Teste', 'Foto23.jpg', 0, 1368812972, NULL),
(204, 0, 'Teste', 'Foto24.jpg', 0, 1368812985, NULL),
(205, 0, 'Teste', 'Foto25.jpg', 0, 1368812991, NULL),
(206, 0, 'Teste', 'Foto26.jpg', 0, 1368813003, NULL),
(207, 0, 'Teste', 'Foto27.jpg', 0, 1368813009, NULL),
(208, 0, 'Teste', 'Foto28.jpg', 0, 1368813015, NULL),
(209, 0, 'Teste', 'Foto29.jpg', 0, 1368813023, NULL),
(210, 0, 'Teste', 'Foto30.jpg', 0, 1368813030, NULL),
(211, 38, 'Teste', 'ook2890_copy.jpg', 0, 1368813111, NULL),
(212, 38, 'Teste', 'ook2899_copy.jpg', 0, 1368813122, NULL),
(213, 38, 'Teste', 'ook2900_copy.jpg', 0, 1368813129, NULL),
(214, 38, 'Teste', 'ook2912_copy.jpg', 0, 1368813137, NULL),
(215, 38, 'Teste', 'ook2914_copy.jpg', 0, 1368813145, NULL),
(216, 38, 'Teste', 'ook2933_copy.jpg', 0, 1368813156, NULL),
(233, 39, 'Teste', 'IMG_0027.jpg', 0, 1368813556, NULL),
(235, 39, 'Teste', 'IMG_0037.jpg', 0, 1368813578, NULL),
(239, 39, 'Teste', 'IMG_0052.jpg', 0, 1368813652, NULL),
(240, 39, 'Teste', 'IMG_0069.jpg', 0, 1368813701, NULL),
(241, 39, 'Teste', 'IMG_0071.jpg', 0, 1368813714, NULL),
(243, 39, 'Teste', 'IMG_0073.jpg', 0, 1368813742, NULL),
(245, 39, 'Teste', 'IMG_0076.jpg', 0, 1368813761, NULL),
(247, 39, 'Teste', 'IMG_0082.jpg', 0, 1368813790, NULL),
(248, 39, 'Teste', 'IMG_0089.jpg', 0, 1368813805, NULL),
(251, 39, 'Teste', 'IMG_0101.jpg', 0, 1368813835, NULL),
(255, 39, 'Teste', 'IMG_0131.jpg', 0, 1368813895, NULL),
(256, 39, 'Teste', 'IMG_0141.jpg', 0, 1368813905, NULL),
(264, 40, 'Teste', 'estacoes_(2).jpg', 0, 1368814259, NULL),
(265, 40, 'Teste', 'estacoes_(3).jpg', 0, 1368814268, NULL),
(266, 40, 'Teste', 'estacoes_(4).jpg', 0, 1368814274, NULL),
(267, 40, 'Teste', 'estacoes_(5).jpg', 0, 1368814297, NULL),
(268, 40, 'Teste', 'estacoes_(6).jpg', 0, 1368814305, NULL),
(269, 40, 'Teste', 'estacoes_(7).jpg', 0, 1368814314, NULL),
(270, 40, 'Teste', 'estacoes_(8).jpg', 0, 1368814325, NULL),
(271, 40, 'Teste', 'estacoes_(9).jpg', 0, 1368814333, NULL),
(272, 40, 'Teste', 'estacoes_(10).jpg', 0, 1368814342, NULL),
(273, 40, 'Teste', 'estacoes_(11).jpg', 0, 1368814349, NULL),
(276, 41, 'Teste', 'ok9876.jpg', 0, 1368814484, NULL),
(277, 41, 'Teste', 'ok9878.jpg', 0, 1368814492, NULL),
(278, 41, 'Teste', 'ok9879.jpg', 0, 1368814504, NULL),
(279, 41, 'Teste', 'ok9881.jpg', 0, 1368814529, NULL),
(280, 41, 'Teste', 'ok9901.jpg', 0, 1368814537, NULL),
(281, 41, 'Teste', 'ok9905.jpg', 0, 1368814546, NULL),
(282, 41, 'Teste', 'ok9920.jpg', 0, 1368814556, NULL),
(283, 41, 'Teste', 'ok9928.jpg', 0, 1368814568, NULL),
(284, 42, 'Teste', 'DSC07902_compactado1.jpg', 0, 1372169882, NULL),
(285, 42, 'Teste', 'DSC07920_compactado1.jpg', 0, 1372169901, NULL),
(286, 42, 'Teste', 'isometrica.jpg', 0, 1372169936, NULL),
(287, 42, 'Teste', 'tras.jpg', 0, 1372169959, NULL),
(288, 0, 'Teste', '2ok5295.jpg', 0, 1372251544, NULL),
(289, 0, 'Teste', 'wok4831_copy.jpg', 0, 1372251707, NULL),
(290, 0, 'Teste', 'wok4837_copy.jpg', 0, 1372251711, NULL),
(291, 0, 'Teste', 'wok4854_copy.jpg', 0, 1372251716, NULL),
(292, 0, 'Teste', 'wok4864_copy.jpg', 0, 1372251720, NULL),
(293, 52, 'Teste', 'wok8958_copy.jpg', 0, 1372251814, NULL),
(294, 52, 'Teste', 'wok8966_copy.jpg', 0, 1372251819, NULL),
(295, 52, 'Teste', 'wok8972_copy.jpg', 0, 1372251824, NULL),
(296, 52, 'Teste', 'wok8973_copy.jpg', 0, 1372251829, NULL),
(297, 55, 'Teste', 'wok7576_copy.jpg', 0, 1372252901, NULL),
(298, 55, 'Teste', 'wok7583_copy.jpg', 0, 1372252906, NULL),
(299, 55, 'Teste', 'wok7600_copy.jpg', 0, 1372252910, NULL),
(300, 55, 'Teste', 'wok7614_copy.jpg', 0, 1372252916, NULL),
(301, 53, 'Teste', 'filipov_02.jpg', 0, 1372252930, NULL),
(302, 53, 'Teste', 'filipov_03.jpg', 0, 1372252937, NULL),
(303, 53, 'Teste', 'filipov_04.jpg', 0, 1372252941, NULL),
(304, 53, 'Teste', 'filipov_05.jpg', 0, 1372252947, NULL),
(305, 53, 'Teste', 'filipov_06.jpg', 0, 1372252954, NULL),
(306, 56, 'Teste', 'wok3255.jpg', 0, 1372617056, NULL),
(307, 56, 'Teste', 'wok3259_copy.jpg', 0, 1372617060, NULL),
(308, 56, 'Teste', 'wok3266_copy.jpg', 0, 1372617063, NULL),
(309, 56, 'Teste', 'wok3274_copy.jpg', 0, 1372617067, NULL),
(310, 56, 'Teste', 'wok3280.jpg', 0, 1372617070, NULL),
(311, 56, 'Teste', 'wok3309.jpg', 0, 1372617073, NULL),
(312, 56, 'Teste', 'wok3314_copy.jpg', 0, 1372617076, NULL),
(313, 56, 'Teste', 'wok3321.jpg', 0, 1372617080, NULL),
(314, 56, 'Teste', 'wok3323_copy.jpg', 0, 1372617085, NULL),
(315, 0, 'Teste', 'DSC07716-copy.jpg', 0, 1372617325, NULL),
(316, 0, 'Teste', 'DSC07717-copy.jpg', 0, 1372617331, NULL),
(317, 0, 'Teste', 'DSC07734-copy.jpg', 0, 1372617339, NULL),
(318, 0, 'Teste', 'DSC07745-copy.jpg', 0, 1372617348, NULL),
(319, 0, 'Teste', '24.jpg', 0, 1372617362, NULL),
(320, 0, 'Teste', 'DSC07853.jpg', 0, 1372617645, NULL),
(321, 0, 'Teste', 'DSC07854.jpg', 0, 1372617650, NULL),
(322, 0, 'Teste', 'DSC07855.jpg', 0, 1372617654, NULL),
(323, 0, 'Teste', 'DSC07856.jpg', 0, 1372617659, NULL),
(324, 0, 'Teste', 'DSC07857.jpg', 0, 1372617664, NULL),
(325, 0, 'Teste', 'DSC07858.jpg', 0, 1372617671, NULL),
(326, 0, 'Teste', 'DSC07860.jpg', 0, 1372617676, NULL),
(327, 0, 'Teste', 'DSC07861.jpg', 0, 1372617681, NULL),
(328, 0, 'Teste', 'DSC07863.jpg', 0, 1372617686, NULL),
(329, 0, 'Teste', 'DSC078631.jpg', 0, 1372617692, NULL),
(330, 0, 'Teste', 'DSC07864.jpg', 0, 1372617696, NULL),
(331, 0, 'Teste', 'DSC07865.jpg', 0, 1372617701, NULL),
(332, 0, 'Teste', 'DSC07866.jpg', 0, 1372617707, NULL),
(333, 0, 'Teste', 'DSC07867.jpg', 0, 1372617712, NULL),
(334, 0, 'Teste', 'DSC07868.jpg', 0, 1372617721, NULL),
(335, 0, 'Teste', 'DSC07869.jpg', 0, 1372617727, NULL),
(336, 0, 'Teste', 'DSC07870.jpg', 0, 1372617731, NULL),
(337, 0, 'Teste', 'DSC07871.jpg', 0, 1372617736, NULL),
(338, 0, 'Teste', 'DSC07872.jpg', 0, 1372617741, NULL),
(339, 0, 'Teste', 'DSC07873.JPG', 0, 1372617746, NULL),
(340, 0, 'Teste', 'DSC07874.jpg', 0, 1372617750, NULL),
(341, 0, 'Teste', 'wok2551_copy.jpg', 0, 1372617811, NULL),
(344, 0, 'Teste', 'wok2587_copy.jpg', 0, 1372617820, NULL),
(346, 61, 'Teste', 'w2ok2091_copy.jpg', 0, 1372617930, NULL),
(347, 61, 'Teste', 'w2ok2107_copy.jpg', 0, 1372617934, NULL),
(348, 61, 'Teste', 'w2ok2112_copy.jpg', 0, 1372617937, NULL),
(349, 61, 'Teste', 'wok2082_copy.jpg', 0, 1372617940, NULL),
(350, 61, 'Teste', 'wok2091_copy.jpg', 0, 1372617943, NULL),
(351, 61, 'Teste', 'wok2102_copy.jpg', 0, 1372617946, NULL),
(352, 61, 'Teste', 'wok2112_copy.jpg', 0, 1372617949, NULL),
(353, 62, 'Teste', 'wok3330_copy.jpg', 0, 1372618388, NULL),
(354, 62, 'Teste', 'wok3331_copy.jpg', 0, 1372618412, NULL),
(355, 62, 'Teste', 'wok3341_copy.jpg', 0, 1372618426, NULL),
(356, 62, 'Teste', 'wok3347_copy.jpg', 0, 1372618441, NULL),
(357, 63, 'Teste', 'wok9811_copy.jpg', 0, 1372618514, NULL),
(358, 63, 'Teste', 'wok9816_copy.jpg', 0, 1372618542, NULL),
(359, 63, 'Teste', 'wok9822_copy.jpg', 0, 1372618558, NULL),
(360, 63, 'Teste', 'wok9824_copy.jpg', 0, 1372618577, NULL),
(361, 64, 'Teste', 'wok9285_copy.jpg', 0, 1372618726, NULL),
(362, 64, 'Teste', 'wok9313_copy.jpg', 0, 1372618733, NULL),
(363, 64, 'Teste', 'wok9322_copy.jpg', 0, 1372618738, NULL),
(364, 64, 'Teste', 'wok9346_copy.jpg', 0, 1372618747, NULL),
(366, 64, 'Teste', 'wok9350_copy.jpg', 0, 1372618759, NULL),
(367, 64, 'Teste', 'wok9353_copy.jpg', 0, 1372618764, NULL),
(368, 65, 'Teste', 'wok5964_copy.jpg', 0, 1372619113, NULL),
(369, 66, 'Teste', 'wok3062_copy.jpg', 0, 1372619151, NULL),
(370, 66, 'Teste', 'wok3070_copy.jpg', 0, 1372619156, NULL),
(371, 66, 'Teste', 'wok3074_copy.jpg', 0, 1372619161, NULL),
(372, 66, 'Teste', 'wok3082_copy.jpg', 0, 1372619165, NULL),
(373, 66, 'Teste', 'wok3085_copy.jpg', 0, 1372619172, NULL),
(374, 66, 'Teste', 'wok3099_copy.jpg', 0, 1372619177, NULL),
(375, 66, 'Teste', 'wok3109_copy.jpg', 0, 1372619183, NULL),
(376, 66, 'Teste', 'wok3116_copy.jpg', 0, 1372619188, NULL),
(377, 66, 'Teste', 'wok3124_copy.jpg', 0, 1372619193, NULL),
(378, 66, 'Teste', 'wok3152_copy1.jpg', 0, 1372619201, NULL),
(379, 0, 'Teste', 'mercadao_01.jpg', 0, 1372619295, NULL),
(380, 0, 'Teste', 'mercadao_02.jpg', 0, 1372619299, NULL),
(381, 69, 'Teste', 'moveis-(46).jpg', 0, 1372621085, NULL),
(387, 69, 'Teste', 'moveis-(50).jpg', 0, 1372621174, NULL),
(388, 69, 'Teste', 'moveis-(51).jpg', 0, 1372621182, NULL),
(401, 69, 'Teste', 'moveis-(65).jpg', 0, 1372621287, NULL),
(404, 69, 'Teste', 'moveis-(70).jpg', 0, 1372621313, NULL),
(407, 69, 'Teste', 'moveis-(43).jpg', 0, 1372621432, NULL),
(415, 69, 'Teste', 'moveis-(32).jpg', 0, 1372621497, NULL),
(420, 69, 'Teste', 'moveis-(26).jpg', 0, 1372621533, NULL),
(428, 69, 'Teste', 'moveis-(13).jpg', 0, 1372621813, NULL),
(431, 69, 'Teste', 'moveis-(9).jpg', 0, 1372621837, NULL),
(434, 69, 'Teste', 'moveis-(6).jpg', 0, 1372621858, NULL),
(435, 69, 'Teste', 'moveis-(5).jpg', 0, 1372621865, NULL),
(440, 0, 'Teste', 'ministerio_da_agricultura_03.jpg', 0, 1372622057, NULL),
(455, 71, 'Teste', 'wok5829.jpg', 0, 1372622384, NULL),
(456, 71, 'Teste', 'wok5846.jpg', 0, 1372622391, NULL),
(457, 71, 'Teste', 'wok5809.jpg', 0, 1372622396, NULL),
(458, 71, 'Teste', 'wok5807.jpg', 0, 1372622401, NULL),
(459, 72, 'Teste', 'wok7611_copy.jpg', 0, 1372622457, NULL),
(460, 72, 'Teste', 'wok7618_copy.jpg', 0, 1372622463, NULL),
(461, 72, 'Teste', 'wok7580_copy.jpg', 0, 1372622470, NULL),
(462, 72, 'Teste', 'wok7630_copy.jpg', 0, 1372622476, NULL),
(463, 72, 'Teste', 'wok7640_copy.jpg', 0, 1372622482, NULL),
(464, 72, 'Teste', 'wok7579_copy.jpg', 0, 1372622495, NULL),
(465, 0, 'Teste', 'ok0030.jpg', 0, 1372622554, NULL),
(466, 0, 'Teste', 'ok0036.jpg', 0, 1372622559, NULL),
(467, 0, 'Teste', 'ok0043.jpg', 0, 1372622563, NULL),
(468, 0, 'Teste', 'ok0049.jpg', 0, 1372622568, NULL),
(469, 0, 'Teste', 'ok0063.jpg', 0, 1372622573, NULL),
(470, 0, 'Teste', 'ok00631.jpg', 0, 1372622580, NULL),
(471, 0, 'Teste', 'ok00691.jpg', 0, 1372622589, NULL),
(472, 0, 'Teste', 'ok0071.jpg', 0, 1372622601, NULL),
(473, 0, 'Teste', 'ok0087.jpg', 0, 1372622606, NULL),
(474, 0, 'Teste', 'ok0123.jpg', 0, 1372622611, NULL),
(475, 0, 'Teste', 'ok0132.jpg', 0, 1372622617, NULL),
(476, 0, 'Teste', 'ok0142.jpg', 0, 1372622622, NULL),
(477, 74, 'Teste', 'wok0677_copy.jpg', 0, 1372622669, NULL),
(478, 74, 'Teste', 'wok0679_copy.jpg', 0, 1372622676, NULL),
(479, 74, 'Teste', 'wok2352_copy.jpg', 0, 1372622680, NULL),
(480, 74, 'Teste', 'wok2370_copy.jpg', 0, 1372622686, NULL),
(481, 74, 'Teste', 'wok2371_copy.jpg', 0, 1372622691, NULL),
(482, 74, 'Teste', 'wok2390_copy.jpg', 0, 1372622698, NULL),
(483, 74, 'Teste', 'wok2395_copy.jpg', 0, 1372622703, NULL),
(484, 76, 'Teste', 'foto210.JPG', 0, 1372622847, NULL),
(486, 76, 'Teste', 'foto31.JPG', 0, 1372622868, NULL),
(487, 76, 'Teste', 'foto41.JPG', 0, 1372622876, NULL),
(488, 76, 'Teste', 'foto51.JPG', 0, 1372622884, NULL),
(489, 76, 'Teste', 'foto61.JPG', 0, 1372622892, NULL),
(490, 76, 'Teste', 'foto71.JPG', 0, 1372622900, NULL),
(491, 76, 'Teste', 'foto81.JPG', 0, 1372622914, NULL),
(492, 76, 'Teste', 'foto91.JPG', 0, 1372622925, NULL),
(493, 77, 'Teste', 'wok2132_copy.jpg', 0, 1372622975, NULL),
(495, 77, 'Teste', 'wok2147_copy.jpg', 0, 1372622986, NULL),
(498, 78, 'Teste', 'wok4439_copy.jpg', 0, 1372623037, NULL),
(499, 78, 'Teste', 'wok4441_copy.jpg', 0, 1372623041, NULL),
(500, 78, 'Teste', 'wok4448_copy.jpg', 0, 1372623045, NULL),
(501, 79, 'Teste', 'stato021.jpg', 0, 1372623172, NULL),
(502, 79, 'Teste', 'stato03.jpg', 0, 1372623183, NULL),
(503, 79, 'Teste', 'stato041.jpg', 0, 1372623191, NULL),
(504, 79, 'Teste', 'stato05.jpg', 0, 1372623197, NULL),
(505, 79, 'Teste', 'stato06.jpg', 0, 1372623203, NULL),
(506, 79, 'Teste', 'stato07.jpg', 0, 1372623210, NULL),
(507, 79, 'Teste', 'stato08.jpg', 0, 1372623216, NULL),
(508, 0, 'Teste', 'ok0156.jpg', 0, 1372623270, NULL),
(509, 0, 'Teste', 'ok0158.jpg', 0, 1372623274, NULL),
(510, 0, 'Teste', 'ok0176.jpg', 0, 1372623279, NULL),
(511, 0, 'Teste', 'ok0177.jpg', 0, 1372623284, NULL),
(512, 0, 'Teste', 'wok0149_copy.jpg', 0, 1372623289, NULL),
(513, 0, 'Teste', 'DSC00168.jpg', 0, 1372623345, NULL),
(514, 0, 'Teste', 'ook5556.jpg', 0, 1372623385, NULL),
(515, 0, 'Teste', 'ook5558.jpg', 0, 1372623389, NULL),
(516, 0, 'Teste', 'ook5571.jpg', 0, 1372623393, NULL),
(518, 83, 'Teste', 'ok6069.jpg', 0, 1372623493, NULL),
(519, 83, 'Teste', 'ok6051.jpg', 0, 1372623503, NULL),
(520, 83, 'Teste', 'ok6047.jpg', 0, 1372623581, NULL),
(521, 0, 'Teste', 'unilearn-01.jpg', 0, 1372623640, NULL),
(522, 0, 'Teste', 'unilearn-02.jpg', 0, 1372623648, NULL),
(523, 0, 'Teste', 'unilearn-03.jpg', 0, 1372623659, NULL),
(524, 0, 'Teste', 'unilearn-05.jpg', 0, 1372623668, NULL),
(525, 0, 'Teste', 'unilearn-06.jpg', 0, 1372623677, NULL),
(526, 0, 'Teste', 'evento_universo_qualidade_02.jpg', 0, 1372623715, NULL),
(527, 0, 'Teste', 'evento_universo_qualidade_03.jpg', 0, 1372623719, NULL),
(528, 0, 'Teste', 'voice-01.jpg', 0, 1372623766, NULL),
(529, 87, 'Teste', 'weir_01.jpg', 0, 1372623805, NULL),
(530, 87, 'Teste', 'weir_02.jpg', 0, 1372623810, NULL),
(531, 87, 'Teste', 'weir_04.jpg', 0, 1372623814, NULL),
(532, 87, 'Teste', 'weir_05.jpg', 0, 1372623819, NULL),
(533, 87, 'Teste', 'weir_06.jpg', 0, 1372623824, NULL),
(534, 88, 'Teste', 'auditrio-(1).jpg', 0, 1372623907, NULL),
(535, 88, 'Teste', 'auditrio-(2).jpg', 0, 1372623919, NULL),
(536, 88, 'Teste', 'auditrio-(3).jpg', 0, 1372623934, NULL),
(537, 88, 'Teste', 'auditrio-(4).jpg', 0, 1372623948, NULL),
(538, 88, 'Teste', 'auditrio-(5).jpg', 0, 1372623963, NULL),
(542, 68, 'Teste', 'DSC04203.jpg', 0, 1372940310, NULL),
(543, 68, 'Teste', 'DSC04204.jpg', 0, 1372940317, NULL),
(544, 68, 'Teste', 'DSC04206.jpg', 0, 1372940323, NULL),
(545, 68, 'Teste', 'DSC04207.jpg', 0, 1372940328, NULL),
(546, 68, 'Teste', 'DSC042081.jpg', 0, 1372940334, NULL),
(547, 68, 'Teste', 'DSC04212.jpg', 0, 1372940341, NULL),
(549, 68, 'Teste', 'DSC04215.jpg', 0, 1372940354, NULL),
(553, 68, 'Teste', 'DSC04220.jpg', 0, 1372940387, NULL),
(554, 68, 'Teste', 'DSC04221.jpg', 0, 1372940398, NULL),
(556, 68, 'Teste', 'DSC04223.jpg', 0, 1372940412, NULL),
(557, 68, 'Teste', 'DSC04224.jpg', 0, 1372940420, NULL),
(559, 68, 'Teste', 'DSC04227.jpg', 0, 1372940434, NULL),
(560, 68, 'Teste', 'DSC04229.jpg', 0, 1372940442, NULL),
(561, 27, 'Teste', 'ok4919.jpg', 0, 1373047237, NULL),
(562, 27, 'Teste', 'ok4922.jpg', 0, 1373047249, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos`
--

CREATE TABLE IF NOT EXISTS `contatos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `endereco` varchar(400) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telefone` varchar(12) NOT NULL,
  `cep` varchar(12) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contatos`
--

INSERT INTO `contatos` (`id`, `endereco`, `bairro`, `cidade`, `uf`, `email`, `telefone`, `cep`, `twitter`, `facebook`) VALUES
(1, 'Rua Marques de Olinda, 316, Casa 05', 'Alterado', 'São Paulo', 'SP', 'atendimento@concept.arq.br', '11 7828 5696', '04277-000', 'teste2', 'http://fb.com/teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `dicas`
--

CREATE TABLE IF NOT EXISTS `dicas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo_nav` varchar(255) DEFAULT NULL,
  `titulo_conteudo` varchar(255) DEFAULT NULL,
  `texto` text,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `dicas`
--

INSERT INTO `dicas` (`id`, `titulo_nav`, `titulo_conteudo`, `texto`, `created`, `updated`) VALUES
(1, 'Por que contratar um profissional', 'Para o desenvolvimento de um bom projeto, sempre é bom escolher profissionais capacitados', '<p>Dica Concept Arquitetura de Interiores</p>', 1363372108, 0),
(2, 'Dica 11', 'Dica 11', '<p>Dica Concept Arquitetura de Interiores3wdqwdxczadc</p>', 1363178293, 0),
(3, 'Dica 2', 'Dica 2', '<p>Dica Concept Arquitetura de Interiores', 1362678872, NULL),
(4, 'Teste cadastro', 'Teste de cadastro de dica', '<p>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>Lorem ipsum dolor</p>', 1363782263, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `fotos`
--

CREATE TABLE IF NOT EXISTS `fotos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produto_id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=587 ;

--
-- Extraindo dados da tabela `fotos`
--

INSERT INTO `fotos` (`id`, `produto_id`, `titulo`, `imagem`, `ordem`, `created`, `updated`) VALUES
(1, 0, 'Foto 0', 'foto_exemplo2.jpg', 3, 1362676238, 0),
(2, 0, 'Foto 1', 'foto_exemplo3.jpg', 7, 1362676238, 0),
(3, 0, 'Foto 2', 'foto_exemplo.jpg', 8, 1362676238, 0),
(4, 0, 'Foto 3', 'foto_exemplo.jpg', 4, 1362676238, 0),
(8, 0, 'Foto 7', 'foto_exemplo4.jpg', 1, 1362676238, 0),
(31, 0, 'Teste', 'imagem-atuacao.jpg', 0, 1363280550, NULL),
(32, 0, 'Teste', '112.jpg', 0, 1363280606, NULL),
(33, 0, 'Teste', '113.jpg', 0, 1363280650, NULL),
(35, 0, 'Teste', '4.jpg', 0, 1363281524, NULL),
(36, 0, 'Teste', '41.jpg', 0, 1363281585, NULL),
(37, 0, 'Teste', '3.jpg', 0, 1363281829, NULL),
(62, 0, 'Teste', '48.jpg', 0, 1363289111, 0),
(77, 0, 'Teste', '640x9601.jpg', 2, 1363291729, 0),
(78, 0, 'Teste', '480x8004.jpg', 1, 1363360280, 0),
(79, 0, 'Teste', '1280x800.jpg', 0, 1363360378, 0),
(80, 0, 'Teste', '1920x1080.jpg', 0, 1363364415, NULL),
(88, 0, 'Teste', '1152189297_13e08144b7_z.jpg', 0, 1363703226, NULL),
(89, 0, 'Teste', '2719386997_d59889f882.jpg', 0, 1363703230, NULL),
(90, 0, 'Teste', '1152189297_13e08144b7_z1.jpg', 3, 1363703271, 0),
(91, 0, 'Teste', '2719386997_d59889f8821.jpg', 4, 1363703278, 0),
(92, 0, 'Teste', 'Modern_decoration_at_Larnaca_International_Airport_Republic_of_Cyprus.JPG', 2, 1363703281, 0),
(93, 0, 'Teste', '2719386997_d59889f8822.jpg', 1, 1363780250, 0),
(94, 0, 'Teste', '1152189297_13e08144b7_z2.jpg', 0, 1363780254, 0),
(95, 0, 'Teste', '2719386997_d59889f8823.jpg', 0, 1363782041, 0),
(96, 0, 'Teste', '1152189297_13e08144b7_z3.jpg', 1, 1363782127, 0),
(97, 7, 'Teste', '38.jpg', 0, 1364495839, 0),
(98, 7, 'Teste', '39.jpg', 1, 1364495954, 0),
(99, 7, 'Teste', 'projeto_2.jpg', 0, 1365616407, NULL),
(100, 0, 'Teste', 'Concept_(40).JPG', 0, 1368111984, NULL),
(101, 0, 'Teste', 'Concept_(40)1.JPG', 0, 1368112023, NULL),
(102, 0, 'Teste', 'Concept_(40)2.JPG', 0, 1368112083, NULL),
(103, 0, 'Teste', 'Concept_(40)3.JPG', 0, 1368116046, NULL),
(115, 0, 'Teste', '11.jpg', 0, 1368117825, 0),
(116, 0, 'Teste', '4.jpg', 2, 1368117829, 0),
(118, 0, 'Teste', '23.jpg', 1, 1368120721, 0),
(119, 0, 'Teste', '12.jpg', 0, 1368122820, NULL),
(120, 0, 'Teste', '24.jpg', 0, 1368122823, NULL),
(121, 0, 'Teste', '34.jpg', 0, 1368122826, NULL),
(122, 0, 'Teste', 'foto-empresa3.jpg', 0, 1368479402, NULL),
(123, 0, 'Teste', 'foto-empresa4.jpg', 0, 1368479413, NULL),
(124, 0, 'Teste', 'foto-empresa5.jpg', 0, 1368479419, NULL),
(125, 0, 'Teste', 'foto-empresa6.jpg', 0, 1368479467, NULL),
(126, 0, 'Teste', 'foto-empresa7.jpg', 0, 1368479477, NULL),
(127, 0, 'Teste', 'foto-empresa8.jpg', 0, 1368479482, NULL),
(128, 0, 'Teste', 'foto-empresa9.jpg', 0, 1368479497, NULL),
(129, 0, 'Teste', 'foto-empresa10.jpg', 0, 1368479501, NULL),
(130, 0, 'Teste', 'foto-empresa11.jpg', 0, 1368479520, NULL),
(131, 0, 'Teste', 'anconacostas2.jpg', 0, 1368531306, NULL),
(132, 0, 'Teste', 'catania_dupla.png', 0, 1368792496, NULL),
(133, 0, 'Teste', 'catania_frontal.png', 0, 1368792515, NULL),
(134, 0, 'Teste', 'catania_lateral.jpg', 0, 1368792533, NULL),
(135, 0, 'Teste', 'catania_Perspectiva.jpg', 0, 1368792538, NULL),
(136, 0, 'Teste', 'gioia_frontal.png', 0, 1368792616, NULL),
(137, 0, 'Teste', 'gioia_frontal1.png', 0, 1368792628, NULL),
(138, 0, 'Teste', 'gioia_lateral.png', 0, 1368792637, NULL),
(139, 0, 'Teste', 'gioia_perspectiva.png', 0, 1368792656, NULL),
(140, 0, 'Teste', 'riesi_sem_piso.jpg', 0, 1368793398, NULL),
(141, 0, 'Teste', 'sofia-azul-frontal.jpg', 0, 1368793834, NULL),
(142, 0, 'Teste', 'sofia-azul-lateral.jpg', 0, 1368793843, NULL),
(143, 0, 'Teste', 'Sofia_Dupla.jpg', 0, 1368793849, NULL),
(144, 0, 'Teste', 'Sofia_Frontal_dupla.jpg', 0, 1368793855, NULL),
(145, 0, 'Teste', 'Sofia_Frontal.jpg', 0, 1368793861, NULL),
(146, 0, 'Teste', 'Sofia_Lateral.jpg', 0, 1368793868, NULL),
(147, 0, 'Teste', 'Sofia_Perspectiva.jpg', 0, 1368793874, NULL),
(148, 0, 'Teste', 'treviso_dupla.jpg', 0, 1368793956, NULL),
(149, 0, 'Teste', 'Treviso_frente_2.jpg', 0, 1368793961, NULL),
(150, 0, 'Teste', 'Treviso_frente.jpg', 0, 1368793966, NULL),
(151, 0, 'Teste', 'Treviso_lateral_2.jpg', 0, 1368793972, NULL),
(152, 0, 'Teste', 'Treviso_lateral_rebatida.jpg', 0, 1368793975, NULL),
(153, 0, 'Teste', 'Treviso_lateral.jpg', 0, 1368793979, NULL),
(154, 0, 'Teste', 'Treviso_perspectiva_2.jpg', 0, 1368793984, NULL),
(155, 0, 'Teste', 'Treviso_perspectiva.jpg', 0, 1368793989, NULL),
(156, 0, 'Teste', 'call_center_03a.jpg', 0, 1368794243, NULL),
(157, 0, 'Teste', 'call_center_03b.jpg', 0, 1368794248, NULL),
(158, 0, 'Teste', 'call_center02_copy.jpg', 0, 1368794253, NULL),
(159, 0, 'Teste', 'call_center03.jpg', 0, 1368794259, NULL),
(160, 0, 'Teste', 'call_center06.jpg', 0, 1368794265, NULL),
(161, 0, 'Teste', 'call_center07.jpg', 0, 1368794272, NULL),
(162, 0, 'Teste', 'callcenter.jpg', 0, 1368794280, NULL),
(163, 0, 'Teste', 'callcentera.jpg', 0, 1368794286, NULL),
(164, 0, 'Teste', 'callcenterb.jpg', 0, 1368794291, NULL),
(165, 0, 'Teste', 'call_center_04a.jpg', 0, 1368794351, NULL),
(166, 0, 'Teste', 'call_center_04b.jpg', 0, 1368794358, NULL),
(167, 0, 'Teste', 'call_center_painel_com_madeira.jpg', 0, 1368794367, NULL),
(168, 0, 'Teste', 'call_center_painel_com_madeira03.jpg', 0, 1368794375, NULL),
(169, 0, 'Teste', 'call_center_painel_com_madeira031.jpg', 0, 1368794397, NULL),
(170, 0, 'Teste', 'call_center_painel_com_madeira04.jpg', 0, 1368794404, NULL),
(195, 82, 'Teste', 'balco_041.jpg', 0, 1368820368, NULL),
(196, 0, 'Teste', 'balco_042.jpg', 0, 1368820404, NULL),
(197, 0, 'Teste', 'balco_tivoli_031.jpg', 0, 1368820447, NULL),
(198, 0, 'Teste', 'balcotivoli4.jpg', 0, 1368820452, NULL),
(199, 0, 'Teste', 'balco_tivoli02_jpg1.jpg', 0, 1368820549, NULL),
(200, 0, 'Teste', 'balco_tivoli03_copy.jpg', 0, 1368820554, NULL),
(201, 0, 'Teste', 'balco_tivoli04_.jpg', 0, 1368820561, NULL),
(202, 0, 'Teste', 'balco_tivoli05.jpg', 0, 1368820569, NULL),
(203, 0, 'Teste', 'balcoreto.jpg', 0, 1368820575, NULL),
(204, 0, 'Teste', 'comp_(2).jpg', 0, 1368820744, NULL),
(205, 0, 'Teste', 'comp_(3).jpg', 0, 1368820748, NULL),
(206, 0, 'Teste', 'comp_(4).jpg', 0, 1368820753, NULL),
(207, 0, 'Teste', 'comp_(6).jpg', 0, 1368820851, NULL),
(208, 0, 'Teste', 'comp_(7).jpg', 0, 1368820856, NULL),
(209, 0, 'Teste', 'comp_(8).jpg', 0, 1368820861, NULL),
(210, 0, 'Teste', 'comp_(9).jpg', 0, 1368820865, NULL),
(211, 0, 'Teste', 'comp_(10).jpg', 0, 1368820871, NULL),
(212, 0, 'Teste', 'comp_(11).jpg', 0, 1368820876, NULL),
(213, 0, 'Teste', 'comp_(12).jpg', 0, 1368820882, NULL),
(214, 0, 'Teste', 'comp_(13).jpg', 0, 1368820887, NULL),
(215, 0, 'Teste', 'comp_(14).jpg', 0, 1368820890, NULL),
(219, 0, 'Teste', 'gioia_frontal.jpg', 0, 1368821394, NULL),
(220, 0, 'Teste', 'gioia_lateral.jpg', 0, 1368821400, NULL),
(221, 0, 'Teste', 'gioia_perspectiva.jpg', 0, 1368821404, NULL),
(222, 0, 'Teste', 'riesi1.jpg', 0, 1368821445, NULL),
(223, 0, 'Teste', 'sofia_(2).jpg', 0, 1368821529, NULL),
(224, 0, 'Teste', 'sofia_(3).jpg', 0, 1368821540, NULL),
(225, 0, 'Teste', 'sofia_(4).jpg', 0, 1368821544, NULL),
(226, 0, 'Teste', 'sofia_(5).jpg', 0, 1368821549, NULL),
(227, 0, 'Teste', 'sofia_(6).jpg', 0, 1368821553, NULL),
(228, 0, 'Teste', 'sofia_(7).jpg', 0, 1368821557, NULL),
(229, 0, 'Teste', 'sofia_(8).jpg', 0, 1368821562, NULL),
(230, 0, 'Teste', 'sofia_(9).jpg', 0, 1368821567, NULL),
(231, 0, 'Teste', 'treviso_dupla1.jpg', 0, 1368821603, NULL),
(232, 0, 'Teste', 'Treviso_frente_21.jpg', 0, 1368821609, NULL),
(233, 0, 'Teste', 'Treviso_lateral_21.jpg', 0, 1368821618, NULL),
(234, 0, 'Teste', 'Treviso_lateral_rebatida1.jpg', 0, 1368821626, NULL),
(235, 0, 'Teste', 'Treviso_lateral1.jpg', 0, 1368821635, NULL),
(236, 0, 'Teste', 'Treviso_perspectiva1.jpg', 0, 1368821642, NULL),
(237, 0, 'Teste', 'treviso_sem_piso1.jpg', 0, 1368821653, NULL),
(238, 0, 'Teste', 'treviso1.jpg', 0, 1368821660, NULL),
(239, 0, 'Teste', 'call_center_top02.jpg', 0, 1368821898, NULL),
(240, 0, 'Teste', 'call_center_top03.jpg', 0, 1368821903, NULL),
(241, 0, 'Teste', 'call_center_03b1.jpg', 0, 1368821959, NULL),
(242, 0, 'Teste', 'call_center02_copy1.jpg', 0, 1368822067, NULL),
(243, 0, 'Teste', 'call_center031.jpg', 0, 1368822075, NULL),
(244, 0, 'Teste', 'call_center061.jpg', 0, 1368822080, NULL),
(245, 0, 'Teste', 'call_center071.jpg', 0, 1368822086, NULL),
(246, 0, 'Teste', 'callcenter1.jpg', 0, 1368822090, NULL),
(247, 0, 'Teste', 'callcentera1.jpg', 0, 1368822096, NULL),
(248, 0, 'Teste', 'callcenterb1.jpg', 0, 1368822102, NULL),
(249, 0, 'Teste', 'call_center_04b1.jpg', 0, 1368822313, NULL),
(250, 0, 'Teste', 'call_center_painel_com_madeira1.jpg', 0, 1368822365, NULL),
(251, 0, 'Teste', 'call_center_painel_com_madeira02.jpg', 0, 1368822376, NULL),
(281, 0, 'Teste', '002.jpg', 0, 1368823106, NULL),
(282, 0, 'Teste', '003.jpg', 0, 1368823110, NULL),
(283, 0, 'Teste', '004.jpg', 0, 1368823115, NULL),
(284, 0, 'Teste', '005.jpg', 0, 1368823120, NULL),
(285, 0, 'Teste', '006.jpg', 0, 1368823128, NULL),
(286, 0, 'Teste', '007.jpg', 0, 1368823133, NULL),
(287, 0, 'Teste', '008.jpg', 0, 1368823138, NULL),
(288, 0, 'Teste', '009.jpg', 0, 1368823153, NULL),
(289, 0, 'Teste', '010.jpg', 0, 1368823158, NULL),
(290, 0, 'Teste', '011.jpg', 0, 1368823161, NULL),
(291, 0, 'Teste', '012.jpg', 0, 1368823165, NULL),
(292, 0, 'Teste', '013.jpg', 0, 1368823171, NULL),
(293, 0, 'Teste', '014.jpg', 0, 1368823175, NULL),
(294, 0, 'Teste', 'a_(2).jpg', 0, 1368823225, NULL),
(295, 0, 'Teste', 'a_(3).jpg', 0, 1368823230, NULL),
(296, 0, 'Teste', 'a_(4).jpg', 0, 1368823234, NULL),
(297, 0, 'Teste', 'a_(5).jpg', 0, 1368823238, NULL),
(298, 0, 'Teste', 'a_(6).jpg', 0, 1368823242, NULL),
(299, 0, 'Teste', 'a_(7).jpg', 0, 1368823246, NULL),
(300, 0, 'Teste', 'a_(8).jpg', 0, 1368823251, NULL),
(301, 0, 'Teste', 'a_(9).jpg', 0, 1368823257, NULL),
(302, 0, 'Teste', 'a_(10).jpg', 0, 1368823267, NULL),
(303, 0, 'Teste', 'a_(11).jpg', 0, 1368823272, NULL),
(304, 0, 'Teste', 'a_(2)1.jpg', 0, 1368823336, NULL),
(305, 0, 'Teste', 'a_(3)1.jpg', 0, 1368823343, NULL),
(306, 0, 'Teste', 'a_(4)1.jpg', 0, 1368823350, NULL),
(307, 0, 'Teste', 'a_(5)1.jpg', 0, 1368823355, NULL),
(308, 0, 'Teste', 'a_(6)1.jpg', 0, 1368823359, NULL),
(309, 0, 'Teste', 'a_(7)1.jpg', 0, 1368823363, NULL),
(310, 0, 'Teste', 'a_(8)1.jpg', 0, 1368823367, NULL),
(311, 0, 'Teste', 'a_(2)2.jpg', 0, 1368823405, NULL),
(312, 0, 'Teste', 'a_(3)2.jpg', 0, 1368823410, NULL),
(313, 0, 'Teste', 'a_(4)2.jpg', 0, 1368823414, NULL),
(314, 0, 'Teste', 'a_(5)2.jpg', 0, 1368823418, NULL),
(315, 0, 'Teste', 'a_(6)2.jpg', 0, 1368823423, NULL),
(316, 0, 'Teste', 'a_(7)2.jpg', 0, 1368823427, NULL),
(317, 0, 'Teste', 'a_(2)3.jpg', 0, 1368823464, NULL),
(318, 0, 'Teste', 'a_(3)3.jpg', 0, 1368823469, NULL),
(319, 0, 'Teste', 'a_(4)3.jpg', 0, 1368823473, NULL),
(320, 0, 'Teste', 'a_(5)3.jpg', 0, 1368823477, NULL),
(321, 0, 'Teste', 'a_(6)3.jpg', 0, 1368823482, NULL),
(322, 0, 'Teste', 'a_(7)3.jpg', 0, 1368823486, NULL),
(323, 109, 'Teste', 'arquivos_copy.jpg', 0, 1368823633, NULL),
(324, 110, 'Teste', 'gaveteiro_fixo02.jpg', 0, 1368823754, NULL),
(325, 110, 'Teste', 'gaveteiro_fixo03.jpg', 0, 1368823759, NULL),
(326, 110, 'Teste', 'gaveteiro_fixo04.jpg', 0, 1368823763, NULL),
(327, 111, 'Teste', 'junc_(2).jpg', 0, 1368823772, NULL),
(328, 111, 'Teste', 'junc_(3).jpg', 0, 1368823776, NULL),
(329, 112, 'Teste', 'gaveteiro_pedestal_02.jpg', 0, 1368823786, NULL),
(330, 112, 'Teste', 'gaveteiro_pedestal_03.jpg', 0, 1368823789, NULL),
(331, 113, 'Teste', 'gaveteiro020000.jpg', 0, 1368823800, NULL),
(332, 113, 'Teste', 'gaveteiro030000.jpg', 0, 1368823807, NULL),
(333, 113, 'Teste', 'gaveteiro040000.jpg', 0, 1368823811, NULL),
(334, 114, 'Teste', 'a_(2)4.jpg', 0, 1368823926, NULL),
(335, 114, 'Teste', 'a_(3)4.jpg', 0, 1368823930, NULL),
(336, 114, 'Teste', 'a_(4)4.jpg', 0, 1368823934, NULL),
(337, 114, 'Teste', 'a_(5)4.jpg', 0, 1368823938, NULL),
(338, 114, 'Teste', 'a_(6)4.jpg', 0, 1368823942, NULL),
(339, 114, 'Teste', 'a_(7)4.jpg', 0, 1368823946, NULL),
(340, 114, 'Teste', 'a_(8)2.jpg', 0, 1368823952, NULL),
(341, 114, 'Teste', 'a_(9)1.jpg', 0, 1368823957, NULL),
(342, 114, 'Teste', 'a_(10)1.jpg', 0, 1368823962, NULL),
(348, 0, 'Teste', 'ancona22.jpg', 0, 1369075781, NULL),
(349, 0, 'Teste', 'anconacostas22.jpg', 0, 1369075786, NULL),
(350, 58, 'Teste', 'cetara-2.jpg', 0, 1369314477, NULL),
(351, 59, 'Teste', 'colore-2.jpg', 0, 1369314938, NULL),
(352, 60, 'Teste', 'diva-2.jpg', 0, 1369315083, NULL),
(354, 0, 'Teste', 'loop-2.jpg', 0, 1369315475, NULL),
(355, 0, 'Teste', 'loop-21.jpg', 0, 1369315583, NULL),
(356, 63, 'Teste', 'luca-2.jpg', 0, 1369315751, NULL),
(357, 63, 'Teste', 'luca-3.jpg', 0, 1369315760, NULL),
(358, 64, 'Teste', 'poli-2.jpg', 0, 1369315868, NULL),
(359, 65, 'Teste', 'scala-2.jpg', 0, 1369315980, NULL),
(366, 67, 'Teste', 'trevi-5.jpg', 0, 1369316459, NULL),
(367, 68, 'Teste', 'vitara-2.jpg', 0, 1369316629, NULL),
(372, 118, 'Teste', 'fraqueposterior.jpg', 0, 1371054872, NULL),
(373, 121, 'Teste', 'nr17reg.jpg', 0, 1371055174, NULL),
(374, 121, 'Teste', 'tampofixo.jpg', 0, 1371055180, NULL),
(375, 103, 'Teste', '15.jpg', 0, 1371056224, NULL),
(376, 103, 'Teste', 'paginacao_vidros.JPG', 0, 1371056238, NULL),
(377, 103, 'Teste', 'paineis_cegos.JPG', 0, 1371056261, NULL),
(378, 103, 'Teste', 'porta_de_correr_cega.jpg', 0, 1371056268, NULL),
(379, 103, 'Teste', 'porta_de_correr_de_vidro.jpg', 0, 1371056273, NULL),
(380, 103, 'Teste', 'vidro_duplo_com_persiana_embutida_e_porta_cega_02.JPG', 0, 1371056316, NULL),
(382, 103, 'Teste', 'vidro_duplo_com_persiana_embutida_e_porta_de_vidro.JPG', 0, 1371056339, NULL),
(383, 103, 'Teste', 'vidro_transparente_e_porta_cega.jpg', 0, 1371056369, NULL),
(384, 121, 'Teste', '1_(2).jpg', 0, 1372171015, NULL),
(388, 0, 'Teste', 'armário_aberto1.jpg', 0, 1372264237, NULL),
(389, 0, 'Teste', 'armário_com_trilho_e_aberto.jpg', 0, 1372264269, NULL),
(390, 0, 'Teste', 'armário_com_trilho.jpg', 0, 1372264293, NULL),
(391, 212, 'Teste', 'Ancona_posterior_2.jpg', 0, 1372440875, NULL),
(392, 61, 'Teste', 'Fraque_fixa.jpg', 0, 1372441054, NULL),
(393, 213, 'Teste', 'Lateral.jpg', 0, 1372441822, NULL),
(394, 57, 'Teste', 'Perspectiva1.jpg', 3, 1372441998, 0),
(395, 57, 'Teste', 'Posterior_fixa.jpg', 4, 1372442007, 0),
(396, 67, 'Teste', 'Perspectiva_fixa.jpg', 0, 1372442114, NULL),
(403, 215, 'Teste', 'Perspectiva_preta1.jpg', 0, 1372445360, NULL),
(406, 214, 'Teste', 'WEB_0052.jpg', 0, 1372446276, NULL),
(407, 214, 'Teste', 'WEB_0071.jpg', 0, 1372446290, NULL),
(408, 214, 'Teste', 'WEB_0101.jpg', 0, 1372446302, NULL),
(409, 214, 'Teste', 'WEB_0111.jpg', 0, 1372446313, NULL),
(410, 66, 'Teste', 'SkynetIT_02_sc.jpg', 2, 1372447056, 0),
(411, 66, 'Teste', 'SkynetIT_04.jpg', 3, 1372447065, 0),
(421, 117, 'Teste', 'catania_preta.jpg', 5, 1372452991, 0),
(428, 222, 'Teste', 'frente.jpg', 0, 1372710512, 0),
(429, 222, 'Teste', 'tras.jpg', 1, 1372710527, 0),
(436, 219, 'Teste', 'riesi_sem_piso2.jpg', 0, 1372713384, NULL),
(437, 220, 'Teste', 'auditorio2.jpg', 0, 1372794064, NULL),
(438, 220, 'Teste', 'auditorio3.jpg', 0, 1372794079, NULL),
(442, 216, 'Teste', 'divagir4.jpg', 0, 1372798371, NULL),
(443, 220, 'Teste', 'diva42.jpg', 0, 1372798399, NULL),
(448, 117, 'Teste', 'pulpito.jpg', 2, 1372863394, 0),
(449, 117, 'Teste', 'contra_encosto.jpg', 1, 1372863406, 0),
(450, 117, 'Teste', 'detalhe_catania.jpg', 3, 1372863418, 0),
(451, 119, 'Teste', 'ok4922.jpg', 0, 1372863700, 0),
(452, 119, 'Teste', 'ok4929.jpg', 1, 1372863792, 0),
(453, 120, 'Teste', 'DSC08729_copy.jpg', 0, 1372864030, NULL),
(454, 120, 'Teste', 'DSC08737_copy.jpg', 0, 1372864040, NULL),
(455, 120, 'Teste', 'DSC08740_copy.jpg', 0, 1372864049, NULL),
(456, 120, 'Teste', 'DSC08769_copy.jpg', 0, 1372864062, NULL),
(457, 120, 'Teste', 'treviso_sem_piso4.jpg', 0, 1372864072, NULL),
(459, 226, 'Teste', 'sdaasa.jpg', 0, 1372865237, NULL),
(460, 226, 'Teste', 'nuova_longarina.jpg', 0, 1372865248, NULL),
(461, 226, 'Teste', 'nuova_fixa_3.jpg', 0, 1372865255, NULL),
(462, 226, 'Teste', 'nuova_empilhada.jpg', 0, 1372865264, NULL),
(463, 226, 'Teste', 'nuova_com_braco.jpg', 0, 1372865271, NULL),
(464, 223, 'Teste', 'NOW1.jpg', 0, 1372865367, NULL),
(465, 223, 'Teste', 'Now_131.jpg', 0, 1372865376, NULL),
(466, 223, 'Teste', 'Now_141.jpg', 0, 1372865386, NULL),
(467, 217, 'Teste', 'PLOP_0101.jpg', 0, 1372865455, NULL),
(468, 228, 'Teste', 'dani_fixa.jpg', 0, 1372871404, NULL),
(469, 228, 'Teste', 'Foto-12.jpg', 0, 1372871420, NULL),
(470, 228, 'Teste', 'Foto-13.jpg', 0, 1372871431, NULL),
(471, 228, 'Teste', 'Foto-20.jpg', 0, 1372871446, NULL),
(472, 228, 'Teste', 'Foto-15.jpg', 0, 1372871465, NULL),
(473, 65, 'Teste', 'Skyline_cantsb.jpg', 0, 1372871682, NULL),
(474, 65, 'Teste', 'Skyline_gir_baixasb.jpg', 0, 1372871693, NULL),
(475, 66, 'Teste', 'Posterior_sotile_baixa.jpg', 0, 1372872150, 0),
(476, 66, 'Teste', 'Perspectiva_sotile_baixa.jpg', 1, 1372872250, 0),
(477, 118, 'Teste', 'Fraque_fixa1.jpg', 0, 1372872994, NULL),
(478, 229, 'Teste', 'Turim_Frontal1.jpg', 0, 1372873283, NULL),
(479, 229, 'Teste', 'Turim_Lateral1.jpg', 0, 1372873292, NULL),
(480, 128, 'Teste', 'totti_05.jpg', 0, 1372874301, NULL),
(481, 129, 'Teste', 'totti_02.jpg', 0, 1372874338, NULL),
(482, 129, 'Teste', 'totti_03.jpg', 0, 1372874347, NULL),
(484, 231, 'Teste', '210.jpg', 0, 1372874841, 0),
(485, 231, 'Teste', '39.jpg', 5, 1372875046, 0),
(486, 231, 'Teste', '48.jpg', 1, 1372875056, 0),
(487, 231, 'Teste', '52.jpg', 4, 1372875065, 0),
(488, 231, 'Teste', '63.jpg', 2, 1372875073, 0),
(489, 231, 'Teste', '72.jpg', 3, 1372875085, 0),
(490, 231, 'Teste', '82.jpg', 6, 1372875094, 0),
(492, 233, 'Teste', '19.jpg', 0, 1372875402, NULL),
(493, 233, 'Teste', '212.jpg', 0, 1372875415, NULL),
(494, 233, 'Teste', '310.jpg', 0, 1372875427, NULL),
(495, 234, 'Teste', 'gaveteiro0300001.jpg', 0, 1372875526, NULL),
(496, 234, 'Teste', 'gaveteiro0100001.jpg', 0, 1372875536, NULL),
(497, 234, 'Teste', 'gaveteiro0400001.jpg', 0, 1372875548, NULL),
(498, 235, 'Teste', '213.jpg', 0, 1372875743, NULL),
(499, 235, 'Teste', '312.jpg', 0, 1372875752, NULL),
(500, 236, 'Teste', '214.jpg', 0, 1372875916, NULL),
(501, 236, 'Teste', '313.jpg', 0, 1372875925, NULL),
(503, 181, 'Teste', 'painel_bp_com_vidro.jpg', 2, 1372876234, 0),
(504, 181, 'Teste', 'Painel_de_vidro.jpg', 3, 1372876243, 0),
(505, 181, 'Teste', 'painel_tecido_com_vidro.jpg', 4, 1372876252, 0),
(506, 181, 'Teste', 'painel_tecido.jpg', 5, 1372876260, 0),
(510, 181, 'Teste', 'capa2.jpg', 1, 1372876501, 0),
(515, 210, 'Teste', '113.jpg', 0, 1372876783, NULL),
(516, 210, 'Teste', '215.jpg', 0, 1372876790, NULL),
(517, 210, 'Teste', '314.jpg', 0, 1372876797, NULL),
(518, 210, 'Teste', '49.jpg', 0, 1372876804, NULL),
(520, 197, 'Teste', '114.jpg', 0, 1372877333, NULL),
(521, 197, 'Teste', '216.jpg', 0, 1372877344, NULL),
(522, 197, 'Teste', '315.jpg', 0, 1372877353, NULL),
(523, 197, 'Teste', '410.jpg', 0, 1372877363, NULL),
(524, 197, 'Teste', '53.jpg', 0, 1372877373, NULL),
(525, 197, 'Teste', '64.jpg', 0, 1372877383, NULL),
(526, 197, 'Teste', '73.jpg', 0, 1372877394, NULL),
(527, 182, 'Teste', '115.jpg', 0, 1372877646, NULL),
(528, 182, 'Teste', '217.jpg', 0, 1372877663, NULL),
(529, 182, 'Teste', '316.jpg', 0, 1372877673, NULL),
(530, 182, 'Teste', '411.jpg', 0, 1372877683, NULL),
(531, 182, 'Teste', '54.jpg', 0, 1372877692, NULL),
(532, 182, 'Teste', '65.jpg', 0, 1372877702, NULL),
(533, 182, 'Teste', '74.jpg', 0, 1372877716, NULL),
(534, 237, 'Teste', '218.jpg', 0, 1372877982, NULL),
(535, 237, 'Teste', '317.jpg', 0, 1372877991, NULL),
(536, 135, 'Teste', '117.jpg', 0, 1372878259, NULL),
(537, 135, 'Teste', '219.jpg', 0, 1372878267, NULL),
(538, 135, 'Teste', '318.jpg', 0, 1372878281, NULL),
(539, 135, 'Teste', '412.jpg', 0, 1372878289, NULL),
(540, 135, 'Teste', '55.jpg', 0, 1372878297, NULL),
(541, 141, 'Teste', '118.jpg', 0, 1372878618, NULL),
(542, 141, 'Teste', '220.jpg', 0, 1372878628, NULL),
(543, 141, 'Teste', '319.jpg', 0, 1372878637, NULL),
(548, 151, 'Teste', '551.jpg', 0, 1372879100, NULL),
(549, 155, 'Teste', '120.jpg', 0, 1372879231, NULL),
(550, 155, 'Teste', '1110.jpg', 0, 1372879239, NULL),
(551, 238, 'Teste', 'w2ok2091_copy.jpg', 0, 1372879461, NULL),
(552, 238, 'Teste', 'w2ok2112_copy.jpg', 0, 1372879471, NULL),
(553, 238, 'Teste', 'wok2082_copy.jpg', 0, 1372879480, NULL),
(554, 238, 'Teste', 'wok2091_copy.jpg', 0, 1372879491, NULL),
(555, 238, 'Teste', 'wok2102_copy.jpg', 0, 1372879498, NULL),
(556, 238, 'Teste', 'wok2112_copy.jpg', 0, 1372879521, NULL),
(557, 156, 'Teste', 'mesa_delta_peninsular_01_copy.jpg', 0, 1372879646, NULL),
(558, 156, 'Teste', 'mesa_delta_peninsular_02.jpg', 0, 1372879656, NULL),
(559, 156, 'Teste', 'mesa_delta_peninsular_03.jpg', 0, 1372879664, NULL),
(560, 156, 'Teste', 'mesa_delta_peninsular_04.jpg', 0, 1372879673, NULL),
(561, 239, 'Teste', '222.jpg', 0, 1372880006, NULL),
(562, 239, 'Teste', '320.jpg', 0, 1372880015, NULL),
(563, 240, 'Teste', '322.jpg', 0, 1372880241, NULL),
(564, 240, 'Teste', '223.jpg', 0, 1372880249, NULL),
(565, 240, 'Teste', '122.jpg', 0, 1372880258, NULL),
(566, 241, 'Teste', '224.jpg', 0, 1372880448, NULL),
(567, 241, 'Teste', '323.jpg', 0, 1372880456, NULL),
(568, 242, 'Teste', '225.jpg', 0, 1372881172, NULL),
(569, 242, 'Teste', '324.jpg', 0, 1372881181, NULL),
(570, 242, 'Teste', '415.jpg', 0, 1372881187, NULL),
(571, 243, 'Teste', 'wok5807.jpg', 0, 1373046804, 0),
(572, 243, 'Teste', 'pouso_alegre_02.jpg', 2, 1373046821, 0),
(573, 243, 'Teste', 'pouso_alegre_01.jpg', 3, 1373046830, 0),
(574, 243, 'Teste', 'wok5809.jpg', 1, 1373046856, 0),
(575, 122, 'Teste', 'stato01.jpg', 0, 1373048524, NULL),
(576, 122, 'Teste', 'stato02.jpg', 0, 1373048534, NULL),
(577, 122, 'Teste', 'stato06.jpg', 0, 1373048543, NULL),
(578, 186, 'Teste', 'fl2009_5.jpg', 0, 1373117531, NULL),
(579, 186, 'Teste', 'fl2009_7.jpg', 0, 1373117551, NULL),
(580, 186, 'Teste', 'fl2009_2.jpg', 0, 1373117567, NULL),
(581, 186, 'Teste', 'fl2009_3.jpg', 0, 1373117585, NULL),
(582, 186, 'Teste', 'fl2009_4.jpg', 0, 1373117597, NULL),
(583, 186, 'Teste', 'fl2009_6.jpg', 0, 1373117657, NULL),
(584, 186, 'Teste', 'plataforma02.jpg', 0, 1373117667, NULL),
(585, 186, 'Teste', 'plataforma31_copy.jpg', 0, 1373117678, NULL),
(586, 247, 'Teste', 'lc1.jpg', 0, 1373128612, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `midia`
--

CREATE TABLE IF NOT EXISTS `midia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `midia`
--

INSERT INTO `midia` (`id`, `titulo`, `imagem`, `ordem`, `created`, `updated`) VALUES
(4, 'Teste Mídia', 'imagem-atuacao.jpg', 0, 1363703745, 1363703745),
(5, 'Teste Mídia 2', 'imagem-atuacao1.jpg', 0, 1363703755, 0),
(7, 'Teste', 'imagem-atuacao2.jpg', 0, 1363704128, 1363704128),
(8, 'Teste Cadastro', '2719386997_d59889f882.jpg', 0, 1363782206, 1363782206);

-- --------------------------------------------------------

--
-- Estrutura da tabela `newsletters`
--

CREATE TABLE IF NOT EXISTS `newsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `data` int(10) DEFAULT NULL,
  `html` text,
  `imagem` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `newsletters`
--

INSERT INTO `newsletters` (`id`, `titulo`, `data`, `html`, `imagem`) VALUES
(1, NULL, 1354327200, '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\r\n<html>\r\n    <head>\r\n        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\r\n        \r\n        <!-- Facebook sharing information tags -->\r\n        <meta property="og:title" content="*|MC:SUBJECT|*" />\r\n        \r\n        <title>*|MC:SUBJECT|*</title>\r\n		<style type="text/css">\r\n			/* Client-specific Styles */\r\n			#outlook a{padding:0;} /* Force Outlook to provide a "view in browser" button. */\r\n			html{width:800px;}\r\n			body{width:800px !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */\r\n			body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */\r\n\r\n			/* Reset Styles */\r\n			body{margin:0; padding:0;}\r\n			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}\r\n			table td{border-collapse:collapse;}\r\n			#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}\r\n\r\n			/* Template Styles */\r\n\r\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: COMMON PAGE ELEMENTS /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section background color\r\n			* @tip Set the background color for your email. You may want to choose one that matches your company''s branding.\r\n			* @theme page\r\n			*/\r\n			body, #backgroundTable{\r\n				/*@editable*/ background-color:#FAFAFA;\r\n			}\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section email border\r\n			* @tip Set the border for your email.\r\n			*/\r\n			#templateContainer{\r\n				/*@editable*/ border: 1px solid #DDDDDD;\r\n			}\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section heading 1\r\n			* @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.\r\n			* @style heading 1\r\n			*/\r\n			h1, .h1{\r\n				/*@editable*/ color:#202020;\r\n				display:block;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:34px;\r\n				/*@editable*/ font-weight:bold;\r\n				/*@editable*/ line-height:100%;\r\n				margin-top:0;\r\n				margin-right:0;\r\n				margin-bottom:10px;\r\n				margin-left:0;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section heading 2\r\n			* @tip Set the styling for all second-level headings in your emails.\r\n			* @style heading 2\r\n			*/\r\n			h2, .h2{\r\n				/*@editable*/ color:#202020;\r\n				display:block;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:30px;\r\n				/*@editable*/ font-weight:bold;\r\n				/*@editable*/ line-height:100%;\r\n				margin-top:0;\r\n				margin-right:0;\r\n				margin-bottom:10px;\r\n				margin-left:0;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section heading 3\r\n			* @tip Set the styling for all third-level headings in your emails.\r\n			* @style heading 3\r\n			*/\r\n			h3, .h3{\r\n				/*@editable*/ color:#202020;\r\n				display:block;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:26px;\r\n				/*@editable*/ font-weight:bold;\r\n				/*@editable*/ line-height:100%;\r\n				margin-top:0;\r\n				margin-right:0;\r\n				margin-bottom:10px;\r\n				margin-left:0;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section heading 4\r\n			* @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.\r\n			* @style heading 4\r\n			*/\r\n			h4, .h4{\r\n				/*@editable*/ color:#202020;\r\n				display:block;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:22px;\r\n				/*@editable*/ font-weight:bold;\r\n				/*@editable*/ line-height:100%;\r\n				margin-top:0;\r\n				margin-right:0;\r\n				margin-bottom:10px;\r\n				margin-left:0;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: PREHEADER /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section preheader style\r\n			* @tip Set the background color for your email''s preheader area.\r\n			* @theme page\r\n			*/\r\n			#templatePreheader{\r\n				/*@editable*/ background-color:#FAFAFA;\r\n			}\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section preheader text\r\n			* @tip Set the styling for your email''s preheader text. Choose a size and color that is easy to read.\r\n			*/\r\n			.preheaderContent div{\r\n				/*@editable*/ color:#505050;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:10px;\r\n				/*@editable*/ line-height:100%;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section preheader link\r\n			* @tip Set the styling for your email''s preheader links. Choose a color that helps them stand out from your text.\r\n			*/\r\n			.preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{\r\n				/*@editable*/ color:#336699;\r\n				/*@editable*/ font-weight:normal;\r\n				/*@editable*/ text-decoration:underline;\r\n			}\r\n\r\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: HEADER /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section header style\r\n			* @tip Set the background color and border for your email''s header area.\r\n			* @theme header\r\n			*/\r\n			#templateHeader{\r\n				/*@editable*/ background-color:#FFFFFF;\r\n				/*@editable*/ border-bottom:0;\r\n			}\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section header text\r\n			* @tip Set the styling for your email''s header text. Choose a size and color that is easy to read.\r\n			*/\r\n			.headerContent{\r\n				/*@editable*/ color:#202020;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:34px;\r\n				/*@editable*/ font-weight:bold;\r\n				/*@editable*/ line-height:100%;\r\n				/*@editable*/ padding:0;\r\n				/*@editable*/ text-align:center;\r\n				/*@editable*/ vertical-align:middle;\r\n			}\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section header link\r\n			* @tip Set the styling for your email''s header links. Choose a color that helps them stand out from your text.\r\n			*/\r\n			.headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{\r\n				/*@editable*/ color:#336699;\r\n				/*@editable*/ font-weight:normal;\r\n				/*@editable*/ text-decoration:underline;\r\n			}\r\n\r\n			#headerImage{\r\n				height:auto;\r\n				max-width:600px !important;\r\n			}\r\n\r\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: MAIN BODY /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\r\n\r\n			/**\r\n			* @tab Body\r\n			* @section body style\r\n			* @tip Set the background color for your email''s body area.\r\n			*/\r\n			#templateContainer, .bodyContent{\r\n				/*@editable*/ background-color:#FFFFFF;\r\n			}\r\n\r\n			/**\r\n			* @tab Body\r\n			* @section body text\r\n			* @tip Set the styling for your email''s main content text. Choose a size and color that is easy to read.\r\n			* @theme main\r\n			*/\r\n			.bodyContent div{\r\n				/*@editable*/ color:#505050;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:14px;\r\n				/*@editable*/ line-height:150%;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Body\r\n			* @section body link\r\n			* @tip Set the styling for your email''s main content links. Choose a color that helps them stand out from your text.\r\n			*/\r\n			.bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{\r\n				/*@editable*/ color:#336699;\r\n				/*@editable*/ font-weight:normal;\r\n				/*@editable*/ text-decoration:underline;\r\n			}\r\n\r\n			.bodyContent img{\r\n				display:inline;\r\n				height:auto;\r\n			}\r\n\r\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: FOOTER /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section footer style\r\n			* @tip Set the background color and top border for your email''s footer area.\r\n			* @theme footer\r\n			*/\r\n			#templateFooter{\r\n				/*@editable*/ background-color:#FFFFFF;\r\n				/*@editable*/ border-top:0;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section footer text\r\n			* @tip Set the styling for your email''s footer text. Choose a size and color that is easy to read.\r\n			* @theme footer\r\n			*/\r\n			.footerContent div{\r\n				/*@editable*/ color:#707070;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:12px;\r\n				/*@editable*/ line-height:125%;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section footer link\r\n			* @tip Set the styling for your email''s footer links. Choose a color that helps them stand out from your text.\r\n			*/\r\n			.footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{\r\n				/*@editable*/ color:#336699;\r\n				/*@editable*/ font-weight:normal;\r\n				/*@editable*/ text-decoration:underline;\r\n			}\r\n\r\n			.footerContent img{\r\n				display:inline;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section social bar style\r\n			* @tip Set the background color and border for your email''s footer social bar.\r\n			* @theme footer\r\n			*/\r\n			#social{\r\n				/*@editable*/ background-color:#FAFAFA;\r\n				/*@editable*/ border:0;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section social bar style\r\n			* @tip Set the background color and border for your email''s footer social bar.\r\n			*/\r\n			#social div{\r\n				/*@editable*/ text-align:center;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section utility bar style\r\n			* @tip Set the background color and border for your email''s footer utility bar.\r\n			* @theme footer\r\n			*/\r\n			#utility{\r\n				/*@editable*/ background-color:#FFFFFF;\r\n				/*@editable*/ border:0;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section utility bar style\r\n			* @tip Set the background color and border for your email''s footer utility bar.\r\n			*/\r\n			#utility div{\r\n				/*@editable*/ text-align:center;\r\n			}\r\n\r\n			#monkeyRewards img{\r\n				max-width:190px;\r\n			}\r\n		</style>\r\n	</head>\r\n    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">\r\n    	<center>\r\n        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">\r\n            	<tr>\r\n                	<td align="center" valign="top">\r\n                        <!-- // Begin Template Preheader \\\\ -->\r\n                        <table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader">\r\n                            <tr>\r\n                                <td valign="top" class="preheaderContent">\r\n                                \r\n                                	<!-- // Begin Module: Standard Preheader \\ -->\r\n                                    <table border="0" cellpadding="10" cellspacing="0" width="100%">\r\n                                    	<tr>\r\n                                        	<td valign="top">\r\n                                            	<div mc:edit="std_preheader_content">\r\n                                                	 Use this area to offer a short teaser of your email''s content. Text here will show in the preview area of some email clients.\r\n                                                </div>\r\n                                            </td>\r\n                                            <!-- *|IFNOT:ARCHIVE_PAGE|* -->\r\n											<td valign="top" width="190">\r\n                                            	<div mc:edit="std_preheader_links">\r\n                                                	Is this email not displaying correctly?<br /><a href="*|ARCHIVE|*" target="_blank">View it in your browser</a>.\r\n                                                </div>\r\n                                            </td>\r\n											<!-- *|END:IF|* -->\r\n                                        </tr>\r\n                                    </table>\r\n                                	<!-- // End Module: Standard Preheader \\ -->\r\n                                \r\n                                </td>\r\n                            </tr>\r\n                        </table>\r\n                        <!-- // End Template Preheader \\\\ -->\r\n                    	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">\r\n                        	<tr>\r\n                            	<td align="center" valign="top">\r\n                                    <!-- // Begin Template Header \\\\ -->\r\n                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">\r\n                                        <tr>\r\n                                            <td class="headerContent">\r\n                                            \r\n                                            	<!-- // Begin Module: Standard Header Image \\\\ -->\r\n                                            	<img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/placeholder_600.gif" style="max-width:600px;" id="headerImage campaign-icon" mc:label="header_image" mc:edit="header_image" mc:allowdesigner mc:allowtext />\r\n                                            	<!-- // End Module: Standard Header Image \\\\ -->\r\n                                            \r\n                                            </td>\r\n                                        </tr>\r\n                                    </table>\r\n                                    <!-- // End Template Header \\\\ -->\r\n                                </td>\r\n                            </tr>\r\n                        	<tr>\r\n                            	<td align="center" valign="top">\r\n                                    <!-- // Begin Template Body \\\\ -->\r\n                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">\r\n                                    	<tr>\r\n                                            <td valign="top" class="bodyContent">\r\n                                \r\n                                                <!-- // Begin Module: Standard Content \\\\ -->\r\n                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">\r\n                                                    <tr>\r\n                                                        <td valign="top">\r\n                                                            <div mc:edit="std_content00">\r\n                                                                <h1 class="h1">Heading 1</h1>\r\n                                                                <h2 class="h2">Heading 2</h2>\r\n                                                                <h3 class="h3">Heading 3</h3>\r\n                                                                <h4 class="h4">Heading 4</h4>\r\n                                                                <strong>Getting started:</strong> Customize your template by clicking on the style editor tabs up above. Set your fonts, colors, and styles. After setting your styling is all done you can click here in this area, delete the text, and start adding your own awesome content!\r\n                                                                <br />\r\n                                                                <br />\r\n                                                                After you enter your content, highlight the text you want to style and select the options you set in the style editor in the "styles" drop down box. Want to <a href="http://www.mailchimp.com/kb/article/im-using-the-style-designer-and-i-cant-get-my-formatting-to-change" target="_blank">get rid of styling on a bit of text</a>, but having trouble doing it? Just use the "remove formatting" button to strip the text of any formatting and reset your style.\r\n                                                            </div>\r\n														</td>\r\n                                                    </tr>\r\n                                                </table>\r\n                                                <!-- // End Module: Standard Content \\\\ -->\r\n                                                \r\n                                            </td>\r\n                                        </tr>\r\n                                    </table>\r\n                                    <!-- // End Template Body \\\\ -->\r\n                                </td>\r\n                            </tr>\r\n                        	<tr>\r\n                            	<td align="center" valign="top">\r\n                                    <!-- // Begin Template Footer \\\\ -->\r\n                                	<table border="0" cellpadding="10" cellspacing="0" width="600" id="templateFooter">\r\n                                    	<tr>\r\n                                        	<td valign="top" class="footerContent">\r\n                                            \r\n                                                <!-- // Begin Module: Standard Footer \\\\ -->\r\n                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">\r\n                                                    <tr>\r\n                                                        <td colspan="2" valign="middle" id="social">\r\n                                                            <div mc:edit="std_social">\r\n                                                                &nbsp;<a href="*|TWITTER:PROFILEURL|*">follow on Twitter</a> | <a href="*|FACEBOOK:PROFILEURL|*">friend on Facebook</a> | <a href="*|FORWARD|*">forward to a friend</a>&nbsp;\r\n                                                            </div>\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    <tr>\r\n                                                        <td valign="top" width="350">\r\n                                                            <div mc:edit="std_footer">\r\n																<em>Copyright &copy; *|CURRENT_YEAR|* *|LIST:COMPANY|*, All rights reserved.</em>\r\n																<br />\r\n																*|IFNOT:ARCHIVE_PAGE|* *|LIST:DESCRIPTION|*\r\n																<br />\r\n																<strong>Our mailing address is:</strong>\r\n																<br />\r\n																*|HTML:LIST_ADDRESS_HTML|**|END:IF|* \r\n                                                            </div>\r\n                                                        </td>\r\n                                                        <td valign="top" width="190" id="monkeyRewards">\r\n                                                            <div mc:edit="monkeyrewards">\r\n                                                                *|IF:REWARDS|* *|HTML:REWARDS|* *|END:IF|*\r\n                                                            </div>\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    <tr>\r\n                                                        <td colspan="2" valign="middle" id="utility">\r\n                                                            <div mc:edit="std_utility">\r\n                                                                &nbsp;<a href="*|UNSUB|*">unsubscribe from this list</a> | <a href="*|UPDATE_PROFILE|*">update subscription preferences</a>&nbsp;\r\n                                                            </div>\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                </table>\r\n                                                <!-- // End Module: Standard Footer \\\\ -->\r\n                                            \r\n                                            </td>\r\n                                        </tr>\r\n                                    </table>\r\n                                    <!-- // End Template Footer \\\\ -->\r\n                                </td>\r\n                            </tr>\r\n                        </table>\r\n                        <br />\r\n                    </td>\r\n                </tr>\r\n            </table>\r\n        </center>\r\n    </body>\r\n</html>', 'dez.jpg'),
(2, NULL, 1357005600, '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\r\n<html>\r\n    <head>\r\n        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\r\n        \r\n        <!-- Facebook sharing information tags -->\r\n        <meta property="og:title" content="*|MC:SUBJECT|*" />\r\n        \r\n        <title>*|MC:SUBJECT|*</title>\r\n		<style type="text/css">\r\n			/* Client-specific Styles */\r\n			#outlook a{padding:0;} /* Force Outlook to provide a "view in browser" button. */\r\n			html{width:800px;}\r\n			body{width:800px !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */\r\n			body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */\r\n\r\n			/* Reset Styles */\r\n			body{margin:0; padding:0;}\r\n			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}\r\n			table td{border-collapse:collapse;}\r\n			#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}\r\n\r\n			/* Template Styles */\r\n\r\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: COMMON PAGE ELEMENTS /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section background color\r\n			* @tip Set the background color for your email. You may want to choose one that matches your company''s branding.\r\n			* @theme page\r\n			*/\r\n			body, #backgroundTable{\r\n				/*@editable*/ background-color:#FAFAFA;\r\n			}\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section email border\r\n			* @tip Set the border for your email.\r\n			*/\r\n			#templateContainer{\r\n				/*@editable*/ border: 1px solid #DDDDDD;\r\n			}\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section heading 1\r\n			* @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.\r\n			* @style heading 1\r\n			*/\r\n			h1, .h1{\r\n				/*@editable*/ color:#202020;\r\n				display:block;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:34px;\r\n				/*@editable*/ font-weight:bold;\r\n				/*@editable*/ line-height:100%;\r\n				margin-top:0;\r\n				margin-right:0;\r\n				margin-bottom:10px;\r\n				margin-left:0;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section heading 2\r\n			* @tip Set the styling for all second-level headings in your emails.\r\n			* @style heading 2\r\n			*/\r\n			h2, .h2{\r\n				/*@editable*/ color:#202020;\r\n				display:block;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:30px;\r\n				/*@editable*/ font-weight:bold;\r\n				/*@editable*/ line-height:100%;\r\n				margin-top:0;\r\n				margin-right:0;\r\n				margin-bottom:10px;\r\n				margin-left:0;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section heading 3\r\n			* @tip Set the styling for all third-level headings in your emails.\r\n			* @style heading 3\r\n			*/\r\n			h3, .h3{\r\n				/*@editable*/ color:#202020;\r\n				display:block;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:26px;\r\n				/*@editable*/ font-weight:bold;\r\n				/*@editable*/ line-height:100%;\r\n				margin-top:0;\r\n				margin-right:0;\r\n				margin-bottom:10px;\r\n				margin-left:0;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section heading 4\r\n			* @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.\r\n			* @style heading 4\r\n			*/\r\n			h4, .h4{\r\n				/*@editable*/ color:#202020;\r\n				display:block;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:22px;\r\n				/*@editable*/ font-weight:bold;\r\n				/*@editable*/ line-height:100%;\r\n				margin-top:0;\r\n				margin-right:0;\r\n				margin-bottom:10px;\r\n				margin-left:0;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: PREHEADER /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section preheader style\r\n			* @tip Set the background color for your email''s preheader area.\r\n			* @theme page\r\n			*/\r\n			#templatePreheader{\r\n				/*@editable*/ background-color:#FAFAFA;\r\n			}\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section preheader text\r\n			* @tip Set the styling for your email''s preheader text. Choose a size and color that is easy to read.\r\n			*/\r\n			.preheaderContent div{\r\n				/*@editable*/ color:#505050;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:10px;\r\n				/*@editable*/ line-height:100%;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section preheader link\r\n			* @tip Set the styling for your email''s preheader links. Choose a color that helps them stand out from your text.\r\n			*/\r\n			.preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{\r\n				/*@editable*/ color:#336699;\r\n				/*@editable*/ font-weight:normal;\r\n				/*@editable*/ text-decoration:underline;\r\n			}\r\n\r\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: HEADER /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section header style\r\n			* @tip Set the background color and border for your email''s header area.\r\n			* @theme header\r\n			*/\r\n			#templateHeader{\r\n				/*@editable*/ background-color:#FFFFFF;\r\n				/*@editable*/ border-bottom:0;\r\n			}\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section header text\r\n			* @tip Set the styling for your email''s header text. Choose a size and color that is easy to read.\r\n			*/\r\n			.headerContent{\r\n				/*@editable*/ color:#202020;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:34px;\r\n				/*@editable*/ font-weight:bold;\r\n				/*@editable*/ line-height:100%;\r\n				/*@editable*/ padding:0;\r\n				/*@editable*/ text-align:center;\r\n				/*@editable*/ vertical-align:middle;\r\n			}\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section header link\r\n			* @tip Set the styling for your email''s header links. Choose a color that helps them stand out from your text.\r\n			*/\r\n			.headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{\r\n				/*@editable*/ color:#336699;\r\n				/*@editable*/ font-weight:normal;\r\n				/*@editable*/ text-decoration:underline;\r\n			}\r\n\r\n			#headerImage{\r\n				height:auto;\r\n				max-width:600px !important;\r\n			}\r\n\r\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: MAIN BODY /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\r\n\r\n			/**\r\n			* @tab Body\r\n			* @section body style\r\n			* @tip Set the background color for your email''s body area.\r\n			*/\r\n			#templateContainer, .bodyContent{\r\n				/*@editable*/ background-color:#FFFFFF;\r\n			}\r\n\r\n			/**\r\n			* @tab Body\r\n			* @section body text\r\n			* @tip Set the styling for your email''s main content text. Choose a size and color that is easy to read.\r\n			* @theme main\r\n			*/\r\n			.bodyContent div{\r\n				/*@editable*/ color:#505050;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:14px;\r\n				/*@editable*/ line-height:150%;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Body\r\n			* @section body link\r\n			* @tip Set the styling for your email''s main content links. Choose a color that helps them stand out from your text.\r\n			*/\r\n			.bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{\r\n				/*@editable*/ color:#336699;\r\n				/*@editable*/ font-weight:normal;\r\n				/*@editable*/ text-decoration:underline;\r\n			}\r\n\r\n			.bodyContent img{\r\n				display:inline;\r\n				height:auto;\r\n			}\r\n\r\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: FOOTER /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section footer style\r\n			* @tip Set the background color and top border for your email''s footer area.\r\n			* @theme footer\r\n			*/\r\n			#templateFooter{\r\n				/*@editable*/ background-color:#FFFFFF;\r\n				/*@editable*/ border-top:0;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section footer text\r\n			* @tip Set the styling for your email''s footer text. Choose a size and color that is easy to read.\r\n			* @theme footer\r\n			*/\r\n			.footerContent div{\r\n				/*@editable*/ color:#707070;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:12px;\r\n				/*@editable*/ line-height:125%;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section footer link\r\n			* @tip Set the styling for your email''s footer links. Choose a color that helps them stand out from your text.\r\n			*/\r\n			.footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{\r\n				/*@editable*/ color:#336699;\r\n				/*@editable*/ font-weight:normal;\r\n				/*@editable*/ text-decoration:underline;\r\n			}\r\n\r\n			.footerContent img{\r\n				display:inline;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section social bar style\r\n			* @tip Set the background color and border for your email''s footer social bar.\r\n			* @theme footer\r\n			*/\r\n			#social{\r\n				/*@editable*/ background-color:#FAFAFA;\r\n				/*@editable*/ border:0;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section social bar style\r\n			* @tip Set the background color and border for your email''s footer social bar.\r\n			*/\r\n			#social div{\r\n				/*@editable*/ text-align:center;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section utility bar style\r\n			* @tip Set the background color and border for your email''s footer utility bar.\r\n			* @theme footer\r\n			*/\r\n			#utility{\r\n				/*@editable*/ background-color:#FFFFFF;\r\n				/*@editable*/ border:0;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section utility bar style\r\n			* @tip Set the background color and border for your email''s footer utility bar.\r\n			*/\r\n			#utility div{\r\n				/*@editable*/ text-align:center;\r\n			}\r\n\r\n			#monkeyRewards img{\r\n				max-width:190px;\r\n			}\r\n		</style>\r\n	</head>\r\n    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">\r\n    	<center>\r\n        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">\r\n            	<tr>\r\n                	<td align="center" valign="top">\r\n                        <!-- // Begin Template Preheader \\\\ -->\r\n                        <table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader">\r\n                            <tr>\r\n                                <td valign="top" class="preheaderContent">\r\n                                \r\n                                	<!-- // Begin Module: Standard Preheader \\ -->\r\n                                    <table border="0" cellpadding="10" cellspacing="0" width="100%">\r\n                                    	<tr>\r\n                                        	<td valign="top">\r\n                                            	<div mc:edit="std_preheader_content">\r\n                                                	 Use this area to offer a short teaser of your email''s content. Text here will show in the preview area of some email clients.\r\n                                                </div>\r\n                                            </td>\r\n                                            <!-- *|IFNOT:ARCHIVE_PAGE|* -->\r\n											<td valign="top" width="190">\r\n                                            	<div mc:edit="std_preheader_links">\r\n                                                	Is this email not displaying correctly?<br /><a href="*|ARCHIVE|*" target="_blank">View it in your browser</a>.\r\n                                                </div>\r\n                                            </td>\r\n											<!-- *|END:IF|* -->\r\n                                        </tr>\r\n                                    </table>\r\n                                	<!-- // End Module: Standard Preheader \\ -->\r\n                                \r\n                                </td>\r\n                            </tr>\r\n                        </table>\r\n                        <!-- // End Template Preheader \\\\ -->\r\n                    	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">\r\n                        	<tr>\r\n                            	<td align="center" valign="top">\r\n                                    <!-- // Begin Template Header \\\\ -->\r\n                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">\r\n                                        <tr>\r\n                                            <td class="headerContent">\r\n                                            \r\n                                            	<!-- // Begin Module: Standard Header Image \\\\ -->\r\n                                            	<img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/placeholder_600.gif" style="max-width:600px;" id="headerImage campaign-icon" mc:label="header_image" mc:edit="header_image" mc:allowdesigner mc:allowtext />\r\n                                            	<!-- // End Module: Standard Header Image \\\\ -->\r\n                                            \r\n                                            </td>\r\n                                        </tr>\r\n                                    </table>\r\n                                    <!-- // End Template Header \\\\ -->\r\n                                </td>\r\n                            </tr>\r\n                        	<tr>\r\n                            	<td align="center" valign="top">\r\n                                    <!-- // Begin Template Body \\\\ -->\r\n                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">\r\n                                    	<tr>\r\n                                            <td valign="top" class="bodyContent">\r\n                                \r\n                                                <!-- // Begin Module: Standard Content \\\\ -->\r\n                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">\r\n                                                    <tr>\r\n                                                        <td valign="top">\r\n                                                            <div mc:edit="std_content00">\r\n                                                                <h1 class="h1">Heading 1</h1>\r\n                                                                <h2 class="h2">Heading 2</h2>\r\n                                                                <h3 class="h3">Heading 3</h3>\r\n                                                                <h4 class="h4">Heading 4</h4>\r\n                                                                <strong>Getting started:</strong> Customize your template by clicking on the style editor tabs up above. Set your fonts, colors, and styles. After setting your styling is all done you can click here in this area, delete the text, and start adding your own awesome content!\r\n                                                                <br />\r\n                                                                <br />\r\n                                                                After you enter your content, highlight the text you want to style and select the options you set in the style editor in the "styles" drop down box. Want to <a href="http://www.mailchimp.com/kb/article/im-using-the-style-designer-and-i-cant-get-my-formatting-to-change" target="_blank">get rid of styling on a bit of text</a>, but having trouble doing it? Just use the "remove formatting" button to strip the text of any formatting and reset your style.\r\n                                                            </div>\r\n														</td>\r\n                                                    </tr>\r\n                                                </table>\r\n                                                <!-- // End Module: Standard Content \\\\ -->\r\n                                                \r\n                                            </td>\r\n                                        </tr>\r\n                                    </table>\r\n                                    <!-- // End Template Body \\\\ -->\r\n                                </td>\r\n                            </tr>\r\n                        	<tr>\r\n                            	<td align="center" valign="top">\r\n                                    <!-- // Begin Template Footer \\\\ -->\r\n                                	<table border="0" cellpadding="10" cellspacing="0" width="600" id="templateFooter">\r\n                                    	<tr>\r\n                                        	<td valign="top" class="footerContent">\r\n                                            \r\n                                                <!-- // Begin Module: Standard Footer \\\\ -->\r\n                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">\r\n                                                    <tr>\r\n                                                        <td colspan="2" valign="middle" id="social">\r\n                                                            <div mc:edit="std_social">\r\n                                                                &nbsp;<a href="*|TWITTER:PROFILEURL|*">follow on Twitter</a> | <a href="*|FACEBOOK:PROFILEURL|*">friend on Facebook</a> | <a href="*|FORWARD|*">forward to a friend</a>&nbsp;\r\n                                                            </div>\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    <tr>\r\n                                                        <td valign="top" width="350">\r\n                                                            <div mc:edit="std_footer">\r\n																<em>Copyright &copy; *|CURRENT_YEAR|* *|LIST:COMPANY|*, All rights reserved.</em>\r\n																<br />\r\n																*|IFNOT:ARCHIVE_PAGE|* *|LIST:DESCRIPTION|*\r\n																<br />\r\n																<strong>Our mailing address is:</strong>\r\n																<br />\r\n																*|HTML:LIST_ADDRESS_HTML|**|END:IF|* \r\n                                                            </div>\r\n                                                        </td>\r\n                                                        <td valign="top" width="190" id="monkeyRewards">\r\n                                                            <div mc:edit="monkeyrewards">\r\n                                                                *|IF:REWARDS|* *|HTML:REWARDS|* *|END:IF|*\r\n                                                            </div>\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    <tr>\r\n                                                        <td colspan="2" valign="middle" id="utility">\r\n                                                            <div mc:edit="std_utility">\r\n                                                                &nbsp;<a href="*|UNSUB|*">unsubscribe from this list</a> | <a href="*|UPDATE_PROFILE|*">update subscription preferences</a>&nbsp;\r\n                                                            </div>\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                </table>\r\n                                                <!-- // End Module: Standard Footer \\\\ -->\r\n                                            \r\n                                            </td>\r\n                                        </tr>\r\n                                    </table>\r\n                                    <!-- // End Template Footer \\\\ -->\r\n                                </td>\r\n                            </tr>\r\n                        </table>\r\n                        <br />\r\n                    </td>\r\n                </tr>\r\n            </table>\r\n        </center>\r\n    </body>\r\n</html>', 'ja.jpg');
INSERT INTO `newsletters` (`id`, `titulo`, `data`, `html`, `imagem`) VALUES
(3, NULL, 1359684000, '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\r\n<html>\r\n    <head>\r\n        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\r\n        \r\n        <!-- Facebook sharing information tags -->\r\n        <meta property="og:title" content="*|MC:SUBJECT|*" />\r\n        \r\n        <title>*|MC:SUBJECT|*</title>\r\n		<style type="text/css">\r\n			/* Client-specific Styles */\r\n			#outlook a{padding:0;} /* Force Outlook to provide a "view in browser" button. */\r\n			html{width:800px;}\r\n			body{width:800px !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */\r\n			body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */\r\n\r\n			/* Reset Styles */\r\n			body{margin:0; padding:0;}\r\n			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}\r\n			table td{border-collapse:collapse;}\r\n			#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}\r\n\r\n			/* Template Styles */\r\n\r\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: COMMON PAGE ELEMENTS /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section background color\r\n			* @tip Set the background color for your email. You may want to choose one that matches your company''s branding.\r\n			* @theme page\r\n			*/\r\n			body, #backgroundTable{\r\n				/*@editable*/ background-color:#FAFAFA;\r\n			}\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section email border\r\n			* @tip Set the border for your email.\r\n			*/\r\n			#templateContainer{\r\n				/*@editable*/ border: 1px solid #DDDDDD;\r\n			}\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section heading 1\r\n			* @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.\r\n			* @style heading 1\r\n			*/\r\n			h1, .h1{\r\n				/*@editable*/ color:#202020;\r\n				display:block;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:34px;\r\n				/*@editable*/ font-weight:bold;\r\n				/*@editable*/ line-height:100%;\r\n				margin-top:0;\r\n				margin-right:0;\r\n				margin-bottom:10px;\r\n				margin-left:0;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section heading 2\r\n			* @tip Set the styling for all second-level headings in your emails.\r\n			* @style heading 2\r\n			*/\r\n			h2, .h2{\r\n				/*@editable*/ color:#202020;\r\n				display:block;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:30px;\r\n				/*@editable*/ font-weight:bold;\r\n				/*@editable*/ line-height:100%;\r\n				margin-top:0;\r\n				margin-right:0;\r\n				margin-bottom:10px;\r\n				margin-left:0;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section heading 3\r\n			* @tip Set the styling for all third-level headings in your emails.\r\n			* @style heading 3\r\n			*/\r\n			h3, .h3{\r\n				/*@editable*/ color:#202020;\r\n				display:block;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:26px;\r\n				/*@editable*/ font-weight:bold;\r\n				/*@editable*/ line-height:100%;\r\n				margin-top:0;\r\n				margin-right:0;\r\n				margin-bottom:10px;\r\n				margin-left:0;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Page\r\n			* @section heading 4\r\n			* @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.\r\n			* @style heading 4\r\n			*/\r\n			h4, .h4{\r\n				/*@editable*/ color:#202020;\r\n				display:block;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:22px;\r\n				/*@editable*/ font-weight:bold;\r\n				/*@editable*/ line-height:100%;\r\n				margin-top:0;\r\n				margin-right:0;\r\n				margin-bottom:10px;\r\n				margin-left:0;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: PREHEADER /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section preheader style\r\n			* @tip Set the background color for your email''s preheader area.\r\n			* @theme page\r\n			*/\r\n			#templatePreheader{\r\n				/*@editable*/ background-color:#FAFAFA;\r\n			}\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section preheader text\r\n			* @tip Set the styling for your email''s preheader text. Choose a size and color that is easy to read.\r\n			*/\r\n			.preheaderContent div{\r\n				/*@editable*/ color:#505050;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:10px;\r\n				/*@editable*/ line-height:100%;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section preheader link\r\n			* @tip Set the styling for your email''s preheader links. Choose a color that helps them stand out from your text.\r\n			*/\r\n			.preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{\r\n				/*@editable*/ color:#336699;\r\n				/*@editable*/ font-weight:normal;\r\n				/*@editable*/ text-decoration:underline;\r\n			}\r\n\r\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: HEADER /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section header style\r\n			* @tip Set the background color and border for your email''s header area.\r\n			* @theme header\r\n			*/\r\n			#templateHeader{\r\n				/*@editable*/ background-color:#FFFFFF;\r\n				/*@editable*/ border-bottom:0;\r\n			}\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section header text\r\n			* @tip Set the styling for your email''s header text. Choose a size and color that is easy to read.\r\n			*/\r\n			.headerContent{\r\n				/*@editable*/ color:#202020;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:34px;\r\n				/*@editable*/ font-weight:bold;\r\n				/*@editable*/ line-height:100%;\r\n				/*@editable*/ padding:0;\r\n				/*@editable*/ text-align:center;\r\n				/*@editable*/ vertical-align:middle;\r\n			}\r\n\r\n			/**\r\n			* @tab Header\r\n			* @section header link\r\n			* @tip Set the styling for your email''s header links. Choose a color that helps them stand out from your text.\r\n			*/\r\n			.headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{\r\n				/*@editable*/ color:#336699;\r\n				/*@editable*/ font-weight:normal;\r\n				/*@editable*/ text-decoration:underline;\r\n			}\r\n\r\n			#headerImage{\r\n				height:auto;\r\n				max-width:600px !important;\r\n			}\r\n\r\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: MAIN BODY /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\r\n\r\n			/**\r\n			* @tab Body\r\n			* @section body style\r\n			* @tip Set the background color for your email''s body area.\r\n			*/\r\n			#templateContainer, .bodyContent{\r\n				/*@editable*/ background-color:#FFFFFF;\r\n			}\r\n\r\n			/**\r\n			* @tab Body\r\n			* @section body text\r\n			* @tip Set the styling for your email''s main content text. Choose a size and color that is easy to read.\r\n			* @theme main\r\n			*/\r\n			.bodyContent div{\r\n				/*@editable*/ color:#505050;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:14px;\r\n				/*@editable*/ line-height:150%;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Body\r\n			* @section body link\r\n			* @tip Set the styling for your email''s main content links. Choose a color that helps them stand out from your text.\r\n			*/\r\n			.bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{\r\n				/*@editable*/ color:#336699;\r\n				/*@editable*/ font-weight:normal;\r\n				/*@editable*/ text-decoration:underline;\r\n			}\r\n\r\n			.bodyContent img{\r\n				display:inline;\r\n				height:auto;\r\n			}\r\n\r\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: FOOTER /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section footer style\r\n			* @tip Set the background color and top border for your email''s footer area.\r\n			* @theme footer\r\n			*/\r\n			#templateFooter{\r\n				/*@editable*/ background-color:#FFFFFF;\r\n				/*@editable*/ border-top:0;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section footer text\r\n			* @tip Set the styling for your email''s footer text. Choose a size and color that is easy to read.\r\n			* @theme footer\r\n			*/\r\n			.footerContent div{\r\n				/*@editable*/ color:#707070;\r\n				/*@editable*/ font-family:Arial;\r\n				/*@editable*/ font-size:12px;\r\n				/*@editable*/ line-height:125%;\r\n				/*@editable*/ text-align:left;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section footer link\r\n			* @tip Set the styling for your email''s footer links. Choose a color that helps them stand out from your text.\r\n			*/\r\n			.footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{\r\n				/*@editable*/ color:#336699;\r\n				/*@editable*/ font-weight:normal;\r\n				/*@editable*/ text-decoration:underline;\r\n			}\r\n\r\n			.footerContent img{\r\n				display:inline;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section social bar style\r\n			* @tip Set the background color and border for your email''s footer social bar.\r\n			* @theme footer\r\n			*/\r\n			#social{\r\n				/*@editable*/ background-color:#FAFAFA;\r\n				/*@editable*/ border:0;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section social bar style\r\n			* @tip Set the background color and border for your email''s footer social bar.\r\n			*/\r\n			#social div{\r\n				/*@editable*/ text-align:center;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section utility bar style\r\n			* @tip Set the background color and border for your email''s footer utility bar.\r\n			* @theme footer\r\n			*/\r\n			#utility{\r\n				/*@editable*/ background-color:#FFFFFF;\r\n				/*@editable*/ border:0;\r\n			}\r\n\r\n			/**\r\n			* @tab Footer\r\n			* @section utility bar style\r\n			* @tip Set the background color and border for your email''s footer utility bar.\r\n			*/\r\n			#utility div{\r\n				/*@editable*/ text-align:center;\r\n			}\r\n\r\n			#monkeyRewards img{\r\n				max-width:190px;\r\n			}\r\n		</style>\r\n	</head>\r\n    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">\r\n    	<center>\r\n        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">\r\n            	<tr>\r\n                	<td align="center" valign="top">\r\n                        <!-- // Begin Template Preheader \\\\ -->\r\n                        <table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader">\r\n                            <tr>\r\n                                <td valign="top" class="preheaderContent">\r\n                                \r\n                                	<!-- // Begin Module: Standard Preheader \\ -->\r\n                                    <table border="0" cellpadding="10" cellspacing="0" width="100%">\r\n                                    	<tr>\r\n                                        	<td valign="top">\r\n                                            	<div mc:edit="std_preheader_content">\r\n                                                	 Use this area to offer a short teaser of your email''s content. Text here will show in the preview area of some email clients.\r\n                                                </div>\r\n                                            </td>\r\n                                            <!-- *|IFNOT:ARCHIVE_PAGE|* -->\r\n											<td valign="top" width="190">\r\n                                            	<div mc:edit="std_preheader_links">\r\n                                                	Is this email not displaying correctly?<br /><a href="*|ARCHIVE|*" target="_blank">View it in your browser</a>.\r\n                                                </div>\r\n                                            </td>\r\n											<!-- *|END:IF|* -->\r\n                                        </tr>\r\n                                    </table>\r\n                                	<!-- // End Module: Standard Preheader \\ -->\r\n                                \r\n                                </td>\r\n                            </tr>\r\n                        </table>\r\n                        <!-- // End Template Preheader \\\\ -->\r\n                    	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">\r\n                        	<tr>\r\n                            	<td align="center" valign="top">\r\n                                    <!-- // Begin Template Header \\\\ -->\r\n                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">\r\n                                        <tr>\r\n                                            <td class="headerContent">\r\n                                            \r\n                                            	<!-- // Begin Module: Standard Header Image \\\\ -->\r\n                                            	<img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/placeholder_600.gif" style="max-width:600px;" id="headerImage campaign-icon" mc:label="header_image" mc:edit="header_image" mc:allowdesigner mc:allowtext />\r\n                                            	<!-- // End Module: Standard Header Image \\\\ -->\r\n                                            \r\n                                            </td>\r\n                                        </tr>\r\n                                    </table>\r\n                                    <!-- // End Template Header \\\\ -->\r\n                                </td>\r\n                            </tr>\r\n                        	<tr>\r\n                            	<td align="center" valign="top">\r\n                                    <!-- // Begin Template Body \\\\ -->\r\n                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">\r\n                                    	<tr>\r\n                                            <td valign="top" class="bodyContent">\r\n                                \r\n                                                <!-- // Begin Module: Standard Content \\\\ -->\r\n                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">\r\n                                                    <tr>\r\n                                                        <td valign="top">\r\n                                                            <div mc:edit="std_content00">\r\n                                                                <h1 class="h1">Heading 1</h1>\r\n                                                                <h2 class="h2">Heading 2</h2>\r\n                                                                <h3 class="h3">Heading 3</h3>\r\n                                                                <h4 class="h4">Heading 4</h4>\r\n                                                                <strong>Getting started:</strong> Customize your template by clicking on the style editor tabs up above. Set your fonts, colors, and styles. After setting your styling is all done you can click here in this area, delete the text, and start adding your own awesome content!\r\n                                                                <br />\r\n                                                                <br />\r\n                                                                After you enter your content, highlight the text you want to style and select the options you set in the style editor in the "styles" drop down box. Want to <a href="http://www.mailchimp.com/kb/article/im-using-the-style-designer-and-i-cant-get-my-formatting-to-change" target="_blank">get rid of styling on a bit of text</a>, but having trouble doing it? Just use the "remove formatting" button to strip the text of any formatting and reset your style.\r\n                                                            </div>\r\n														</td>\r\n                                                    </tr>\r\n                                                </table>\r\n                                                <!-- // End Module: Standard Content \\\\ -->\r\n                                                \r\n                                            </td>\r\n                                        </tr>\r\n                                    </table>\r\n                                    <!-- // End Template Body \\\\ -->\r\n                                </td>\r\n                            </tr>\r\n                        	<tr>\r\n                            	<td align="center" valign="top">\r\n                                    <!-- // Begin Template Footer \\\\ -->\r\n                                	<table border="0" cellpadding="10" cellspacing="0" width="600" id="templateFooter">\r\n                                    	<tr>\r\n                                        	<td valign="top" class="footerContent">\r\n                                            \r\n                                                <!-- // Begin Module: Standard Footer \\\\ -->\r\n                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">\r\n                                                    <tr>\r\n                                                        <td colspan="2" valign="middle" id="social">\r\n                                                            <div mc:edit="std_social">\r\n                                                                &nbsp;<a href="*|TWITTER:PROFILEURL|*">follow on Twitter</a> | <a href="*|FACEBOOK:PROFILEURL|*">friend on Facebook</a> | <a href="*|FORWARD|*">forward to a friend</a>&nbsp;\r\n                                                            </div>\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    <tr>\r\n                                                        <td valign="top" width="350">\r\n                                                            <div mc:edit="std_footer">\r\n																<em>Copyright &copy; *|CURRENT_YEAR|* *|LIST:COMPANY|*, All rights reserved.</em>\r\n																<br />\r\n																*|IFNOT:ARCHIVE_PAGE|* *|LIST:DESCRIPTION|*\r\n																<br />\r\n																<strong>Our mailing address is:</strong>\r\n																<br />\r\n																*|HTML:LIST_ADDRESS_HTML|**|END:IF|* \r\n                                                            </div>\r\n                                                        </td>\r\n                                                        <td valign="top" width="190" id="monkeyRewards">\r\n                                                            <div mc:edit="monkeyrewards">\r\n                                                                *|IF:REWARDS|* *|HTML:REWARDS|* *|END:IF|*\r\n                                                            </div>\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    <tr>\r\n                                                        <td colspan="2" valign="middle" id="utility">\r\n                                                            <div mc:edit="std_utility">\r\n                                                                &nbsp;<a href="*|UNSUB|*">unsubscribe from this list</a> | <a href="*|UPDATE_PROFILE|*">update subscription preferences</a>&nbsp;\r\n                                                            </div>\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                </table>\r\n                                                <!-- // End Module: Standard Footer \\\\ -->\r\n                                            \r\n                                            </td>\r\n                                        </tr>\r\n                                    </table>\r\n                                    <!-- // End Template Footer \\\\ -->\r\n                                </td>\r\n                            </tr>\r\n                        </table>\r\n                        <br />\r\n                    </td>\r\n                </tr>\r\n            </table>\r\n        </center>\r\n    </body>\r\n</html>', 'fe.jpg'),
(7, NULL, 1362106800, 'http://trupe.net/previa-stato/produtos/detalhe/41', 'mar.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias`
--

CREATE TABLE IF NOT EXISTS `noticias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `noticia_titulo` varchar(255) NOT NULL,
  `noticia_conteudo` text NOT NULL,
  `noticia_imagem` varchar(255) NOT NULL,
  `noticia_data_publicacao` int(10) NOT NULL,
  `noticia_resumo` varchar(400) NOT NULL,
  `noticia_categoria` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Extraindo dados da tabela `noticias`
--

INSERT INTO `noticias` (`id`, `noticia_titulo`, `noticia_conteudo`, `noticia_imagem`, `noticia_data_publicacao`, `noticia_resumo`, `noticia_categoria`) VALUES
(24, 'Projetos sustentáveis', '<p><span>A cada dia, a preocupa&ccedil;&atilde;o com a sustentabilidade ganha espa&ccedil;o em projetos arquitet&ocirc;nicos para escrit&oacute;rios, tanto na fase de constru&ccedil;&atilde;o at&eacute; na decora&ccedil;&atilde;o de interiores. Um de seus pilares &eacute; investir na economia de tempo e dinheiro, bem como em recursos naturais, minimizando os impactos ambientais.</span></p>\r\n<p><span>Uma das maneiras de melhorar o uso de seus espa&ccedil;os &eacute; tirar proveito do relevo, por exemplo. Assim, ser&aacute; poss&iacute;vel criar espa&ccedil;os diferentes. Analisar a ilumina&ccedil;&atilde;o e ventila&ccedil;&atilde;o natural tamb&eacute;m ajuda no momento de posicionar as janelas e portas, para aproveit&aacute;-las de forma a poupar o uso de l&acirc;mpadas, lumin&aacute;rias, ventiladores e ar-condicionado.</span></p>\r\n<p><span>Outra possibilidade &eacute; criar ambientes interativos e conjugados. Nesse sentido, n&atilde;o &eacute; necess&aacute;rio arrancar uma &aacute;rvore j&aacute; existente, que tal us&aacute;-la como ponto inicial de um jardim ou &aacute;rea verde para sua empresa? Um espa&ccedil;o assim humaniza qualquer local.</span></p>\r\n<p><span>O uso de materiais sustent&aacute;veis tamb&eacute;m deve ser considerado. D&ecirc; prefer&ecirc;ncia aos que sejam naturais e que possam ser reciclados, como madeira, sap&ecirc;, pedra e afins. Bambu, tijolo e cer&acirc;mica de demoli&ccedil;&atilde;o s&atilde;o boas alternativas que, al&eacute;m de tudo, s&atilde;o tend&ecirc;ncia na decora&ccedil;&atilde;o de interiores. Do mesmo modo, vale optar por tubula&ccedil;&atilde;o pain&eacute;is, telhado e outros itens confeccionados com produtos reciclados.</span></p>\r\n<p><span>Na compra dos m&oacute;veis tamb&eacute;m &eacute; poss&iacute;vel pensar &ldquo;verde&rdquo;, ou seja, comprar apenas produtos certificados.</span></p>\r\n<p><span>Para finalizar, vale lembrar que criar espa&ccedil;os sustent&aacute;veis significa pensar em um ambiente mais humano e aconchegante, que promova intera&ccedil;&atilde;o entre as pessoas e o espa&ccedil;o que ocupam. Assim, a sustentabilidade ajuda a criar ambientes mais produtivos, reduzindo custos e promovendo bem-estar.</span></p>', '', 1368500400, '', ''),
(27, 'O poder das cores em escritórios', '<div>As cores, al&eacute;m de criarem a identidade visual de sua empresa, s&atilde;o capazes de influenciar o humor dos colaboradores. Prova disso s&atilde;o as diversas pesquisas que comprovam que as tonalidades escolhidas para colorir locais de trabalho garantem efeitos emocionais sobre as pessoas que usam e circulam no espa&ccedil;o.</div>\r\n<div>&nbsp;</div>\r\n<div>Nesse sentido, por exemplo, as cores quentes tendem a favorecer ambientes mais ativos e criativos, enquanto as frias transmitem maior tranquilidade. Por isso, considerar a &aacute;rea de atua&ccedil;&atilde;o da sua empresa &eacute; fundamental. Sendo assim, diferentes ramos pedem diferentes cores e tipos de decora&ccedil;&atilde;o.</div>\r\n<div>&nbsp;</div>\r\n<div>No entanto, independente das tonalidades escolhidas, &eacute; fundamental estudar as propor&ccedil;&otilde;es para que o efeito obtido seja adequado e o esperado, lembrando que o exagero de uma cor ou outra pode causar irrita&ccedil;&atilde;o e deixar o ambiente carregado.</div>\r\n<div>&nbsp;</div>\r\n<div>E al&eacute;m de colorir as paredes, as cores podem estar presentes tamb&eacute;m no&nbsp;<span><a title="Compre m&oacute;veis e acess&oacute;rios para o seu escrit&oacute;rio em nosso site" href="http://www.stato-moveisparaescritorio.com.br/">mobili&aacute;rio e nos acess&oacute;rios de seu escrit&oacute;rio</a></span>, deixando o ambiente ainda mais alegre e din&acirc;mico.</div>\r\n<div>&nbsp;</div>\r\n<div>Para escolher a cor certa, confira as sensa&ccedil;&otilde;es que algumas delas transitem quando utilizadas em locais de trabalho:</div>\r\n<ul type="disc">\r\n<li><span>O vermelho est&aacute; associado ao sucesso, criatividade e for&ccedil;a. Por ser uma cor muito forte, deve aparecer apenas em detalhes ou em pequenas &aacute;reas, para evitar cansa&ccedil;o visual;</span></li>\r\n</ul>\r\n<ul type="disc">\r\n<li><span>O azul &eacute; respons&aacute;vel por deixar qualquer ambiente mais calmo e tranquilo. Por isso, deve ser usado em espa&ccedil;os para descanso. Para evitar seu efeito depressivo, o ideal &eacute; combin&aacute;-lo com outras cores;</span></li>\r\n</ul>\r\n<ul type="disc">\r\n<li><span>O verde, assim como azul, transmite tranquilidade. Seu grande diferencial &eacute; evitar o cansa&ccedil;o visual e, por isso, a cor &eacute; muito usada em quadros negros (em escolas, por exemplo) e em mesas de jogos;</span></li>\r\n</ul>\r\n<ul type="disc">\r\n<li><span>O branco, quando domina o ambiente, acaba deixando o espa&ccedil;o frio e impessoal. Por isso, a cor deve ser usada para contrastar cores pr&oacute;ximas;</span></li>\r\n</ul>\r\n<ul type="disc">\r\n<li><span>O preto e o cinza indicam seriedade, mas, em excesso, deixam o ambiente carregado. O cinza &eacute; o mais indicado para contrastar cores intensas, enquanto o preto garante sofistica&ccedil;&atilde;o se usado em detalhes;</span></li>\r\n</ul>\r\n<ul type="disc">\r\n<li><span>O amarelo incita e estimula a criatividade. Deve estar presente em pequenas &aacute;reas e pontos do escrit&oacute;rio, para n&atilde;o causar irrita&ccedil;&atilde;o.</span><span>Conhecendo o poder das cores, &eacute; poss&iacute;vel fazer a escolha certa para deixar seu escrit&oacute;rio mais colorido e, sobretudo, mais estimulante aos olhos dos colaboradores.</span></li>\r\n</ul>', '', 1331348400, '', ''),
(28, 'Estrutura de um Call Center', '<p><strong>O que um call center deve ter?</strong></p>\r\n<div class="content clear-block">\r\n<div>&nbsp;</div>\r\n<div><span>Criar um departamento respons&aacute;vel por a&ccedil;&otilde;es de telemarketing pode ser uma estrat&eacute;gia importante de aproxima&ccedil;&atilde;o com o p&uacute;blico alvo. E para que as atividades alcancem os objetivos, &eacute; preciso, al&eacute;m do planejamento de opera&ccedil;&atilde;o, que a infraestrutura e o mobili&aacute;rio sejam os melhores, assim como a equipe.</span></div>\r\n<div><span>Dessa forma, &eacute; imprescind&iacute;vel oferecer condi&ccedil;&otilde;es apropriadas aos trabalhadores. Confira algumas orienta&ccedil;&otilde;es para&nbsp;</span><span><a title="call center" href="http://www.stato-moveisparaescritorio.com.br/category/categorias-mobiliario-em-madeira/telemarketing"><span>montar um call center</span></a></span><span>&nbsp;adequado.</span></div>\r\n<div>&nbsp;</div>\r\n<div><strong><span>Infraestrutura</span></strong><span><br />O espa&ccedil;o deve comportar confortavelmente sua equipe, nada de salas improvisadas. Os isolamentos, t&eacute;rmico e ac&uacute;stico, devem ser observados com cautela. Nesse sentido, evite ambientes muito pr&oacute;ximos &agrave; rua e pouco ventilados.</span></div>\r\n<div>&nbsp;</div>\r\n<div><span>Verifique a parte da conex&atilde;o em busca de problemas, j&aacute; que este item ser&aacute; o principal facilitador do trabalho di&aacute;rio. Para refor&ccedil;ar a efici&ecirc;ncia de seus servi&ccedil;os, opte por servi&ccedil;os de telefonia compat&iacute;veis com a demanda de servi&ccedil;os realizados e, do mesmo modo, escolha com cuidado o software utilizado, pois ele ser&aacute; a base das suas opera&ccedil;&otilde;es.</span></div>\r\n<div>&nbsp;</div>\r\n<div><span>Al&eacute;m disso, verifique se h&aacute; suporte t&eacute;cnico especializado, quais as refer&ecirc;ncias do fornecedor e se h&aacute; atualiza&ccedil;&otilde;es do produto. Em resumo, cerque-se de todas as garantias de que a infraestrutura t&eacute;cnica &eacute; suficiente para atender seus clientes e funcion&aacute;rios com satisfa&ccedil;&atilde;o.</span></div>\r\n<div>&nbsp;</div>\r\n<div><strong><span>Mobili&aacute;rio</span></strong><span><br />Depois de escolher a melhor tecnologia, &eacute; preciso pensar no conforto f&iacute;sico de seus colaboradores. Isso porque a maior parte do trabalho &eacute; realizada enquanto eles estiverem sentados, o que pode causar problemas devido &agrave; m&aacute; postura ou mobili&aacute;rio inadequado.</span></div>\r\n<div>&nbsp;</div>\r\n<div><span>Por isso, &eacute; importante que os m&oacute;veis sejam escolhidos de acordo com a&nbsp;</span><span><a title="Acesse a norma na &iacute;ntegra" href="http://www.guiatrabalhista.com.br/legislacao/nr/nr17_anexoII.htm"><span>NR-17</span></a></span><span>&nbsp;que indica quais as caracter&iacute;sticas necess&aacute;rias &agrave; mob&iacute;lia para evitar danos aos funcion&aacute;rios &ndash; lembrando que empresas confi&aacute;veis, como a stato, trabalham apenas em cumprimento de todas as exig&ecirc;ncias e normas previstas.</span></div>\r\n<div>&nbsp;</div>\r\n<div><span>Segundo a norma, a&nbsp;<strong>esta&ccedil;&atilde;o de trabalho</strong>&nbsp;deve ter espa&ccedil;o para acomodar todos os itens necess&aacute;rios ao trabalho do operador. O monitor e o teclado devem estar apoiados em superf&iacute;cies com mecanismos de regulagem independentes, que possam ser ajustados sempre que o colaborar sentir necessidade.</span></div>\r\n<div>&nbsp;</div>\r\n<div><span>A&nbsp;<strong>mesa</strong>&nbsp;deve ter espa&ccedil;o para o teclado, mouse, telefone e demais equipamentos necess&aacute;rios para o cumprimento das fun&ccedil;&otilde;es di&aacute;rias.</span></div>\r\n<p><span>A&nbsp;</span><strong>cadeira</strong><span>&nbsp;deve ter tamanho ajust&aacute;vel, com apoio para a cabe&ccedil;a e bra&ccedil;os, preferencialmente. E quando n&atilde;o for poss&iacute;vel manter os p&eacute;s no ch&atilde;o, deve ser oferecido um apoio que permita que ele se acomode confortavelmente. Al&eacute;m disso, a cadeira deve ser estofada e com 05 rod&iacute;zios fixos.</span></p>\r\n</div>', 'cc.jpg', 1350871200, '', ''),
(29, 'Teste', '<p><span>A cada dia, a preocupa&ccedil;&atilde;o com a sustentabilidade ganha espa&ccedil;o em projetos arquitet&ocirc;nicos para escrit&oacute;rios, tanto na fase de constru&ccedil;&atilde;o at&eacute; na decora&ccedil;&atilde;o de interiores. Um de seus pilares &eacute; investir na economia de tempo e dinheiro, bem como em recursos naturais, minimizando os impactos ambientais.</span></p>\r\n<p><span>Uma das maneiras de melhorar o uso de seus espa&ccedil;os &eacute; tirar proveito do relevo, por exemplo. Assim, ser&aacute; poss&iacute;vel criar espa&ccedil;os diferentes. Analisar a ilumina&ccedil;&atilde;o e ventila&ccedil;&atilde;o natural tamb&eacute;m ajuda no momento de posicionar as janelas e portas, para aproveit&aacute;-las de forma a poupar o uso de l&acirc;mpadas, lumin&aacute;rias, ventiladores e ar-condicionado.</span></p>\r\n<p><span>Outra possibilidade &eacute; criar ambientes interativos e conjugados. Nesse sentido, n&atilde;o &eacute; necess&aacute;rio arrancar uma &aacute;rvore j&aacute; existente, que tal us&aacute;-la como ponto inicial de um jardim ou &aacute;rea verde para sua empresa? Um espa&ccedil;o assim humaniza qualquer local.</span></p>\r\n<p><span>O uso de materiais sustent&aacute;veis tamb&eacute;m deve ser considerado. D&ecirc; prefer&ecirc;ncia aos que sejam naturais e que possam ser reciclados, como madeira, sap&ecirc;, pedra e afins. Bambu, tijolo e cer&acirc;mica de demoli&ccedil;&atilde;o s&atilde;o boas alternativas que, al&eacute;m de tudo, s&atilde;o tend&ecirc;ncia na decora&ccedil;&atilde;o de interiores. Do mesmo modo, vale optar por tubula&ccedil;&atilde;o pain&eacute;is, telhado e outros itens confeccionados com produtos reciclados.</span></p>\r\n<p><span>Na compra dos m&oacute;veis tamb&eacute;m &eacute; poss&iacute;vel pensar &ldquo;verde&rdquo;, ou seja, comprar apenas produtos certificados.</span></p>\r\n<p><span>Para finalizar, vale lembrar que criar espa&ccedil;os sustent&aacute;veis significa pensar em um ambiente mais humano e aconchegante, que promova intera&ccedil;&atilde;o entre as pessoas e o espa&ccedil;o que ocupam. Assim, a sustentabilidade ajuda a criar ambientes mais produtivos, reduzindo custos e promovendo bem-estar.</span></p>', '', 1320883200, '', ''),
(30, 'Teste', '<p><span>A cada dia, a preocupa&ccedil;&atilde;o com a sustentabilidade ganha espa&ccedil;o em projetos arquitet&ocirc;nicos para escrit&oacute;rios, tanto na fase de constru&ccedil;&atilde;o at&eacute; na decora&ccedil;&atilde;o de interiores. Um de seus pilares &eacute; investir na economia de tempo e dinheiro, bem como em recursos naturais, minimizando os impactos ambientais.</span></p>\r\n<p><span>Uma das maneiras de melhorar o uso de seus espa&ccedil;os &eacute; tirar proveito do relevo, por exemplo. Assim, ser&aacute; poss&iacute;vel criar espa&ccedil;os diferentes. Analisar a ilumina&ccedil;&atilde;o e ventila&ccedil;&atilde;o natural tamb&eacute;m ajuda no momento de posicionar as janelas e portas, para aproveit&aacute;-las de forma a poupar o uso de l&acirc;mpadas, lumin&aacute;rias, ventiladores e ar-condicionado.</span></p>\r\n<p><span>Outra possibilidade &eacute; criar ambientes interativos e conjugados. Nesse sentido, n&atilde;o &eacute; necess&aacute;rio arrancar uma &aacute;rvore j&aacute; existente, que tal us&aacute;-la como ponto inicial de um jardim ou &aacute;rea verde para sua empresa? Um espa&ccedil;o assim humaniza qualquer local.</span></p>\r\n<p><span>O uso de materiais sustent&aacute;veis tamb&eacute;m deve ser considerado. D&ecirc; prefer&ecirc;ncia aos que sejam naturais e que possam ser reciclados, como madeira, sap&ecirc;, pedra e afins. Bambu, tijolo e cer&acirc;mica de demoli&ccedil;&atilde;o s&atilde;o boas alternativas que, al&eacute;m de tudo, s&atilde;o tend&ecirc;ncia na decora&ccedil;&atilde;o de interiores. Do mesmo modo, vale optar por tubula&ccedil;&atilde;o pain&eacute;is, telhado e outros itens confeccionados com produtos reciclados.</span></p>\r\n<p><span>Na compra dos m&oacute;veis tamb&eacute;m &eacute; poss&iacute;vel pensar &ldquo;verde&rdquo;, ou seja, comprar apenas produtos certificados.</span></p>\r\n<p><span>Para finalizar, vale lembrar que criar espa&ccedil;os sustent&aacute;veis significa pensar em um ambiente mais humano e aconchegante, que promova intera&ccedil;&atilde;o entre as pessoas e o espa&ccedil;o que ocupam. Assim, a sustentabilidade ajuda a criar ambientes mais produtivos, reduzindo custos e promovendo bem-estar.</span></p>', '', 1320883200, '', ''),
(31, 'Teste', '<p><span>A cada dia, a preocupa&ccedil;&atilde;o com a sustentabilidade ganha espa&ccedil;o em projetos arquitet&ocirc;nicos para escrit&oacute;rios, tanto na fase de constru&ccedil;&atilde;o at&eacute; na decora&ccedil;&atilde;o de interiores. Um de seus pilares &eacute; investir na economia de tempo e dinheiro, bem como em recursos naturais, minimizando os impactos ambientais.</span></p>\r\n<p><span>Uma das maneiras de melhorar o uso de seus espa&ccedil;os &eacute; tirar proveito do relevo, por exemplo. Assim, ser&aacute; poss&iacute;vel criar espa&ccedil;os diferentes. Analisar a ilumina&ccedil;&atilde;o e ventila&ccedil;&atilde;o natural tamb&eacute;m ajuda no momento de posicionar as janelas e portas, para aproveit&aacute;-las de forma a poupar o uso de l&acirc;mpadas, lumin&aacute;rias, ventiladores e ar-condicionado.</span></p>\r\n<p><span>Outra possibilidade &eacute; criar ambientes interativos e conjugados. Nesse sentido, n&atilde;o &eacute; necess&aacute;rio arrancar uma &aacute;rvore j&aacute; existente, que tal us&aacute;-la como ponto inicial de um jardim ou &aacute;rea verde para sua empresa? Um espa&ccedil;o assim humaniza qualquer local.</span></p>\r\n<p><span>O uso de materiais sustent&aacute;veis tamb&eacute;m deve ser considerado. D&ecirc; prefer&ecirc;ncia aos que sejam naturais e que possam ser reciclados, como madeira, sap&ecirc;, pedra e afins. Bambu, tijolo e cer&acirc;mica de demoli&ccedil;&atilde;o s&atilde;o boas alternativas que, al&eacute;m de tudo, s&atilde;o tend&ecirc;ncia na decora&ccedil;&atilde;o de interiores. Do mesmo modo, vale optar por tubula&ccedil;&atilde;o pain&eacute;is, telhado e outros itens confeccionados com produtos reciclados.</span></p>\r\n<p><span>Na compra dos m&oacute;veis tamb&eacute;m &eacute; poss&iacute;vel pensar &ldquo;verde&rdquo;, ou seja, comprar apenas produtos certificados.</span></p>\r\n<p><span>Para finalizar, vale lembrar que criar espa&ccedil;os sustent&aacute;veis significa pensar em um ambiente mais humano e aconchegante, que promova intera&ccedil;&atilde;o entre as pessoas e o espa&ccedil;o que ocupam. Assim, a sustentabilidade ajuda a criar ambientes mais produtivos, reduzindo custos e promovendo bem-estar.</span></p>', '', 1320883200, '', ''),
(32, 'Teste', '<p><span>A cada dia, a preocupa&ccedil;&atilde;o com a sustentabilidade ganha espa&ccedil;o em projetos arquitet&ocirc;nicos para escrit&oacute;rios, tanto na fase de constru&ccedil;&atilde;o at&eacute; na decora&ccedil;&atilde;o de interiores. Um de seus pilares &eacute; investir na economia de tempo e dinheiro, bem como em recursos naturais, minimizando os impactos ambientais.</span></p>\r\n<p><span>Uma das maneiras de melhorar o uso de seus espa&ccedil;os &eacute; tirar proveito do relevo, por exemplo. Assim, ser&aacute; poss&iacute;vel criar espa&ccedil;os diferentes. Analisar a ilumina&ccedil;&atilde;o e ventila&ccedil;&atilde;o natural tamb&eacute;m ajuda no momento de posicionar as janelas e portas, para aproveit&aacute;-las de forma a poupar o uso de l&acirc;mpadas, lumin&aacute;rias, ventiladores e ar-condicionado.</span></p>\r\n<p><span>Outra possibilidade &eacute; criar ambientes interativos e conjugados. Nesse sentido, n&atilde;o &eacute; necess&aacute;rio arrancar uma &aacute;rvore j&aacute; existente, que tal us&aacute;-la como ponto inicial de um jardim ou &aacute;rea verde para sua empresa? Um espa&ccedil;o assim humaniza qualquer local.</span></p>\r\n<p><span>O uso de materiais sustent&aacute;veis tamb&eacute;m deve ser considerado. D&ecirc; prefer&ecirc;ncia aos que sejam naturais e que possam ser reciclados, como madeira, sap&ecirc;, pedra e afins. Bambu, tijolo e cer&acirc;mica de demoli&ccedil;&atilde;o s&atilde;o boas alternativas que, al&eacute;m de tudo, s&atilde;o tend&ecirc;ncia na decora&ccedil;&atilde;o de interiores. Do mesmo modo, vale optar por tubula&ccedil;&atilde;o pain&eacute;is, telhado e outros itens confeccionados com produtos reciclados.</span></p>\r\n<p><span>Na compra dos m&oacute;veis tamb&eacute;m &eacute; poss&iacute;vel pensar &ldquo;verde&rdquo;, ou seja, comprar apenas produtos certificados.</span></p>\r\n<p><span>Para finalizar, vale lembrar que criar espa&ccedil;os sustent&aacute;veis significa pensar em um ambiente mais humano e aconchegante, que promova intera&ccedil;&atilde;o entre as pessoas e o espa&ccedil;o que ocupam. Assim, a sustentabilidade ajuda a criar ambientes mais produtivos, reduzindo custos e promovendo bem-estar.</span></p>', '', 1320883200, '', ''),
(33, 'Teste', '<p><span>A cada dia, a preocupa&ccedil;&atilde;o com a sustentabilidade ganha espa&ccedil;o em projetos arquitet&ocirc;nicos para escrit&oacute;rios, tanto na fase de constru&ccedil;&atilde;o at&eacute; na decora&ccedil;&atilde;o de interiores. Um de seus pilares &eacute; investir na economia de tempo e dinheiro, bem como em recursos naturais, minimizando os impactos ambientais.</span></p>\r\n<p><span>Uma das maneiras de melhorar o uso de seus espa&ccedil;os &eacute; tirar proveito do relevo, por exemplo. Assim, ser&aacute; poss&iacute;vel criar espa&ccedil;os diferentes. Analisar a ilumina&ccedil;&atilde;o e ventila&ccedil;&atilde;o natural tamb&eacute;m ajuda no momento de posicionar as janelas e portas, para aproveit&aacute;-las de forma a poupar o uso de l&acirc;mpadas, lumin&aacute;rias, ventiladores e ar-condicionado.</span></p>\r\n<p><span>Outra possibilidade &eacute; criar ambientes interativos e conjugados. Nesse sentido, n&atilde;o &eacute; necess&aacute;rio arrancar uma &aacute;rvore j&aacute; existente, que tal us&aacute;-la como ponto inicial de um jardim ou &aacute;rea verde para sua empresa? Um espa&ccedil;o assim humaniza qualquer local.</span></p>\r\n<p><span>O uso de materiais sustent&aacute;veis tamb&eacute;m deve ser considerado. D&ecirc; prefer&ecirc;ncia aos que sejam naturais e que possam ser reciclados, como madeira, sap&ecirc;, pedra e afins. Bambu, tijolo e cer&acirc;mica de demoli&ccedil;&atilde;o s&atilde;o boas alternativas que, al&eacute;m de tudo, s&atilde;o tend&ecirc;ncia na decora&ccedil;&atilde;o de interiores. Do mesmo modo, vale optar por tubula&ccedil;&atilde;o pain&eacute;is, telhado e outros itens confeccionados com produtos reciclados.</span></p>\r\n<p><span>Na compra dos m&oacute;veis tamb&eacute;m &eacute; poss&iacute;vel pensar &ldquo;verde&rdquo;, ou seja, comprar apenas produtos certificados.</span></p>\r\n<p><span>Para finalizar, vale lembrar que criar espa&ccedil;os sustent&aacute;veis significa pensar em um ambiente mais humano e aconchegante, que promova intera&ccedil;&atilde;o entre as pessoas e o espa&ccedil;o que ocupam. Assim, a sustentabilidade ajuda a criar ambientes mais produtivos, reduzindo custos e promovendo bem-estar.</span></p>', '', 1320883200, '', ''),
(34, 'Teste', '<p><span>A cada dia, a preocupa&ccedil;&atilde;o com a sustentabilidade ganha espa&ccedil;o em projetos arquitet&ocirc;nicos para escrit&oacute;rios, tanto na fase de constru&ccedil;&atilde;o at&eacute; na decora&ccedil;&atilde;o de interiores. Um de seus pilares &eacute; investir na economia de tempo e dinheiro, bem como em recursos naturais, minimizando os impactos ambientais.</span></p>\r\n<p><span>Uma das maneiras de melhorar o uso de seus espa&ccedil;os &eacute; tirar proveito do relevo, por exemplo. Assim, ser&aacute; poss&iacute;vel criar espa&ccedil;os diferentes. Analisar a ilumina&ccedil;&atilde;o e ventila&ccedil;&atilde;o natural tamb&eacute;m ajuda no momento de posicionar as janelas e portas, para aproveit&aacute;-las de forma a poupar o uso de l&acirc;mpadas, lumin&aacute;rias, ventiladores e ar-condicionado.</span></p>\r\n<p><span>Outra possibilidade &eacute; criar ambientes interativos e conjugados. Nesse sentido, n&atilde;o &eacute; necess&aacute;rio arrancar uma &aacute;rvore j&aacute; existente, que tal us&aacute;-la como ponto inicial de um jardim ou &aacute;rea verde para sua empresa? Um espa&ccedil;o assim humaniza qualquer local.</span></p>\r\n<p><span>O uso de materiais sustent&aacute;veis tamb&eacute;m deve ser considerado. D&ecirc; prefer&ecirc;ncia aos que sejam naturais e que possam ser reciclados, como madeira, sap&ecirc;, pedra e afins. Bambu, tijolo e cer&acirc;mica de demoli&ccedil;&atilde;o s&atilde;o boas alternativas que, al&eacute;m de tudo, s&atilde;o tend&ecirc;ncia na decora&ccedil;&atilde;o de interiores. Do mesmo modo, vale optar por tubula&ccedil;&atilde;o pain&eacute;is, telhado e outros itens confeccionados com produtos reciclados.</span></p>\r\n<p><span>Na compra dos m&oacute;veis tamb&eacute;m &eacute; poss&iacute;vel pensar &ldquo;verde&rdquo;, ou seja, comprar apenas produtos certificados.</span></p>\r\n<p><span>Para finalizar, vale lembrar que criar espa&ccedil;os sustent&aacute;veis significa pensar em um ambiente mais humano e aconchegante, que promova intera&ccedil;&atilde;o entre as pessoas e o espa&ccedil;o que ocupam. Assim, a sustentabilidade ajuda a criar ambientes mais produtivos, reduzindo custos e promovendo bem-estar.</span></p>', '', 1320883200, '', ''),
(35, 'Teste', '<p><span>A cada dia, a preocupa&ccedil;&atilde;o com a sustentabilidade ganha espa&ccedil;o em projetos arquitet&ocirc;nicos para escrit&oacute;rios, tanto na fase de constru&ccedil;&atilde;o at&eacute; na decora&ccedil;&atilde;o de interiores. Um de seus pilares &eacute; investir na economia de tempo e dinheiro, bem como em recursos naturais, minimizando os impactos ambientais.</span></p>\r\n<p><span>Uma das maneiras de melhorar o uso de seus espa&ccedil;os &eacute; tirar proveito do relevo, por exemplo. Assim, ser&aacute; poss&iacute;vel criar espa&ccedil;os diferentes. Analisar a ilumina&ccedil;&atilde;o e ventila&ccedil;&atilde;o natural tamb&eacute;m ajuda no momento de posicionar as janelas e portas, para aproveit&aacute;-las de forma a poupar o uso de l&acirc;mpadas, lumin&aacute;rias, ventiladores e ar-condicionado.</span></p>\r\n<p><span>Outra possibilidade &eacute; criar ambientes interativos e conjugados. Nesse sentido, n&atilde;o &eacute; necess&aacute;rio arrancar uma &aacute;rvore j&aacute; existente, que tal us&aacute;-la como ponto inicial de um jardim ou &aacute;rea verde para sua empresa? Um espa&ccedil;o assim humaniza qualquer local.</span></p>\r\n<p><span>O uso de materiais sustent&aacute;veis tamb&eacute;m deve ser considerado. D&ecirc; prefer&ecirc;ncia aos que sejam naturais e que possam ser reciclados, como madeira, sap&ecirc;, pedra e afins. Bambu, tijolo e cer&acirc;mica de demoli&ccedil;&atilde;o s&atilde;o boas alternativas que, al&eacute;m de tudo, s&atilde;o tend&ecirc;ncia na decora&ccedil;&atilde;o de interiores. Do mesmo modo, vale optar por tubula&ccedil;&atilde;o pain&eacute;is, telhado e outros itens confeccionados com produtos reciclados.</span></p>\r\n<p><span>Na compra dos m&oacute;veis tamb&eacute;m &eacute; poss&iacute;vel pensar &ldquo;verde&rdquo;, ou seja, comprar apenas produtos certificados.</span></p>\r\n<p><span>Para finalizar, vale lembrar que criar espa&ccedil;os sustent&aacute;veis significa pensar em um ambiente mais humano e aconchegante, que promova intera&ccedil;&atilde;o entre as pessoas e o espa&ccedil;o que ocupam. Assim, a sustentabilidade ajuda a criar ambientes mais produtivos, reduzindo custos e promovendo bem-estar.</span></p>', '', 1320883200, '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `paginas`
--

CREATE TABLE IF NOT EXISTS `paginas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `texto` text,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `template` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `paginas`
--

INSERT INTO `paginas` (`id`, `titulo`, `texto`, `imagem`, `ativo`, `created`, `updated`, `slug`, `description`, `template`) VALUES
(1, 'Pefil Teste de Alteração', '<p>P&aacute;gina de perfil teste de<span style="text-decoration: line-through;"><strong> altera&ccedil;&atilde;o de texto</strong></span></p>', 'imagem-perfil2.jpg', 1, 1362513475, 0, 'perfil', '<p>Perfil Concept Arquitetura</p>', ''),
(3, 'Atuação', '<p><strong>Atua&ccedil;&atilde;o&nbsp;</strong></p>\n<p>Lorem ipsum dolor</p>', 'imagem-atuacao3.jpg', 1, 1362659707, 0, 'atuacao', '', 'atuacao');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE IF NOT EXISTS `produtos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `resumo` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `subcategoria_id` int(11) NOT NULL,
  `ordem` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=248 ;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `titulo`, `resumo`, `descricao`, `imagem`, `categoria_id`, `subcategoria_id`, `ordem`) VALUES
(57, 'Catena', 'cadeira fixa/giratória, estofada', '<p>cadeira fixa/girat&oacute;ria, estofada</p>\r\n<p>op&ccedil;&otilde;es: bra&ccedil;os opcionais. base fixa ou girat&oacute;ria.</p>\r\n<p>regulagens: altura do assento, inclina&ccedil;&atilde;o do encosto com bloqueio multiponto atrav&eacute;s de alavanca ou assento interligado ao encosto por meio de l&acirc;mina de a&ccedil;o fixa. altura dos bra&ccedil;os</p>\r\n<p>materiais: assento e encosto blindados na parte traseira com capa de polipropileno, base em nylon injetado/a&ccedil;o cromado/alum&iacute;nio polido, rodizios com faixa de poliuretano&nbsp;</p>', 'catena5.jpg', 1, 103, 11),
(58, 'Cetara', 'poltrona giratória telada', '<p>poltrona girat&oacute;ria telada</p>\r\n<p>op&ccedil;&otilde;es: assento e encosto telados ou assento estofado em couro preto com encosto telado</p>\r\n<p>regulagens: altura do assento, altura e rota&ccedil;&atilde;o do bra&ccedil;o, inclina&ccedil;&atilde;o do assento/encosto com bloqueio multiponto (mecanismo synchron), altura do encosto, ajuste da profundidade do assento, altura do apoio de cabe&ccedil;a, ajuste de tens&atilde;o de inclina&ccedil;&atilde;o. ajustes do mecanismo atrav&eacute;s de borboletas cabeadas</p>\r\n<p>materiais: tela em mesh pvc, base em aluminio, rodizios com faixa de poliuretano&nbsp;</p>', 'cetara1.jpg', 1, 103, 1),
(59, 'Colore', 'poltrona fixa/giratória, telada', '<p>poltrona fixa/girat&oacute;ria, telada</p>\r\n<p>op&ccedil;&otilde;es: espaldar m&eacute;dio (diretor) e espaldar alto (presidente). base fixa ou girat&oacute;ria. bra&ccedil;os opcionais</p>\r\n<p>regulagens: altura do assento, altura do bra&ccedil;o, mecanismo com inclina&ccedil;&atilde;o do encosto com bloqueio multiponto ou com ajuste relax (tens&atilde;o de inclina&ccedil;&atilde;o do assento/encosto).</p>\r\n<p>materiais: encosto em tela, assento estofado, base em nylon injetado/a&ccedil;o cromado/aluminio, rodizios com faixa de poliuretano</p>', 'colore-1.jpg', 1, 103, 10),
(60, 'Diva', 'cadeira fixa/giratória, telada, multiuso', '<p>cadeira fixa/girat&oacute;ria, telada, multiuso</p>\r\n<p>op&ccedil;&otilde;es: base fixa continua cromada/preta ou girat&oacute;ria. bra&ccedil;os opcionais</p>\r\n<p>regulagens: altura do assento</p>\r\n<p>materiais: encosto em tela, assento estofado, base cromada/preta</p>', 'diva-1.jpg', 1, 103, 9),
(61, 'Fraque', 'poltrona giratória/fixa estofada', '<p>poltrona girat&oacute;ria/fixa estofada</p>\r\n<p>op&ccedil;&otilde;es: espaldar m&eacute;dio (diretor) e espaldar alto (presidente). bra&ccedil;os opcionais. base girat&oacute;ria ou fixa. v&aacute;rias op&ccedil;&otilde;es de mecanismos.</p>\r\n<p>regulagens: altura do assento, inclina&ccedil;&atilde;o do encosto com bloqueio multiponto atrav&eacute;s de alavanca, altura dos bra&ccedil;os</p>\r\n<p>materiais: assento e encosto em espuma de poliuretano injetado, base girat&oacute;ria em nylon injetado ou em a&ccedil;o cromado ou em alum&iacute;nio polido, rodizios com faixa de poliuretano. base fixa em a&ccedil;o pintado ou cromado. bra&ccedil;os opcionais estruturados em a&ccedil;o revestido externamente em poliuretano injetado.</p>', 'fraque2.jpg', 1, 103, 7),
(63, 'Luca', 'poltrona giratória telada', '<p>poltrona girat&oacute;ria telada</p>\r\n<p>op&ccedil;&otilde;es: espaldar m&eacute;dio sem apoio de cabe&ccedil;a (diretor) e espaldar m&eacute;dio com apoio de cabe&ccedil;a (presidente)</p>\r\n<p>regulagens: altura do assento, altura do bra&ccedil;o, inclina&ccedil;&atilde;o do assento/encosto com bloqueio multiponto (mecanismo synchron), ajuste de tens&atilde;o de inclina&ccedil;&atilde;o.</p>\r\n<p>materiais: encosto em tela mesh pvc, assento estofado revestido em tecido preto 3d, base em nylon injetado ou em aluminio, rodizios com faixa de poliuretano</p>', 'luca-1.jpg', 1, 103, 5),
(64, 'Poli', 'cadeira giratória telada', '<p>cadeira fixa/girat&oacute;ria, estofada</p>\r\n<p>regulagens: altura do assento, ajuste de tens&atilde;o de inclina&ccedil;&atilde;o (relax) do conjunto assento/encosto com bloqueio na posi&ccedil;&atilde;o reta</p>\r\n<p>materiais: encosto em tela mesh pvc, assento estofado revestido em tecido preto 3d, base em nylon injetado, rodizios com faixa de poliuretano</p>', 'poli-1.jpg', 1, 103, 13),
(65, 'Scala', 'poltrona giratória estofada', '<p>poltrona girat&oacute;ria estofada</p>\r\n<p>op&ccedil;&otilde;es: espaldar m&eacute;dio (diretor) e espaldar alto (presidente). tela branca ou preta. bra&ccedil;os opcionais.</p>\r\n<p>regulagens: altura do assento, tens&atilde;o de inclina&ccedil;&atilde;o do assento/encosto com bloqueio na posi&ccedil;&atilde;o reta (relax)</p>\r\n<p>materiais: assento e encosto em concha &uacute;nica estofada em gomos, base em aluminio, rodizios com faixa de poliuretano, estrutura do assento/encosto e bra&ccedil;os opcionais em alum&iacute;nio</p>', 'scala-1.jpg', 1, 103, 8),
(66, 'Sotile', 'poltrona giratória telada', '<p>poltrona girat&oacute;ria telada</p>\r\n<p>op&ccedil;&otilde;es: espaldar m&eacute;dio (diretor) e espaldar alto (presidente). tela branca ou preta. bra&ccedil;os opcionais.</p>\r\n<p>regulagens: altura do assento, tens&atilde;o de inclina&ccedil;&atilde;o do assento/encosto com bloqueio na posi&ccedil;&atilde;o reta (relax)</p>\r\n<p>materiais: assento e encosto em concha &uacute;nica telada, base em aluminio, rodizios com faixa de poliuretano, estrutura do assento/encosto e bra&ccedil;os opcionais em alum&iacute;nio</p>', 'Perspectiva_sotile_alta.jpg', 1, 103, 3),
(67, 'Trevi', 'cadeira fixa/giratória, estofada', '<p>cadeira fixa/girat&oacute;ria, estofada</p>\r\n<p>op&ccedil;&otilde;es: bra&ccedil;os opcionais. base fixa ou girat&oacute;ria.</p>\r\n<p>regulagens: altura do assento, inclina&ccedil;&atilde;o do encosto com bloqueio multiponto atrav&eacute;s de alavanca ou assento interligado ao encosto por meio de l&acirc;mina de a&ccedil;o fixa. altura dos bra&ccedil;os</p>\r\n<p>materiais: assento e encosto estofados, base em nylon injetado/a&ccedil;o cromado/alum&iacute;nio polido, rodizios com faixa de poliuretano&nbsp;</p>', 'trevi.jpg', 1, 103, 12),
(68, 'Vitara', 'poltrona giratória telada', '<p>poltrona girat&oacute;ria telada</p>\r\n<p>op&ccedil;&otilde;es: espaldar m&eacute;dio sem apoio de cabe&ccedil;a (diretor) e espaldar m&eacute;dio com apoio de cabe&ccedil;a (presidente)</p>\r\n<p>regulagens: altura do assento, altura/rota&ccedil;&atilde;o/profundidade do bra&ccedil;o, inclina&ccedil;&atilde;o do assento/encosto com bloqueio multiponto (mecanismo synchron), ajuste da profundidade do assento, altura do apoio de cabe&ccedil;a, ajuste de tens&atilde;o de inclina&ccedil;&atilde;o.</p>\r\n<p>materiais: encosto em tela mesh pvc, assento estofado revestido em tecido preto 3d, base em aluminio, rodizios com faixa de poliuretano, estrutura interligando mecanismo ao encosto em alum&iacute;nio</p>', 'vitara.jpg', 1, 103, 2),
(103, 'Divisórias Piso Teto', 'Divisórias Piso Teto', '<p>Divis&oacute;rias Piso Teto</p>', 'paginacao_de_vidro_com_placas_inferiores_cegas.JPG', 12, 91, NULL),
(109, 'Arquivos', 'Arquivos', '<p>Arquivos</p>', 'arquivamento_carvalho_prata0000_copy.jpg', 14, 98, NULL),
(110, 'Fixos', 'Fixos', '<p>Fixos</p>', 'gaveteiro_fixo01.jpg', 14, 99, NULL),
(111, 'Junção', 'Junção', '<p>Jun&ccedil;&atilde;o</p>', 'junc_(1).jpg', 14, 99, NULL),
(112, 'Pedestal', 'Pedestal', '<p>Pedestal</p>', 'gaveteiro_pedestal_01.jpg', 14, 99, NULL),
(113, 'Volantes', 'Volantes', '<p>Volantes</p>', 'gaveteiro010000.jpg', 14, 99, NULL),
(114, 'Armários', 'Armários', '<p>Arm&aacute;rios</p>', 'a_(1)4.jpg', 14, 97, NULL),
(117, 'Catânia', 'modular, assento fixo ou retrátil', '<p>modular, assento fixo ou retr&aacute;til</p>\r\n<p>blindagem no contra assento e no contra encosto</p>\r\n<p>prancheta escamote&aacute;vel opcional</p>\r\n<p><object id="4b1b1b79-7909-84bd-f90a-9b93d889b294" type="application/gas-events-abn" width="0" height="0"></object></p>', 'capa_catania.jpg', 1, 105, 0),
(118, 'Fraque', 'poltrona individual solta ou em longarina', '<p>poltrona individual solta ou em longarina</p>\r\n<p>op&ccedil;&otilde;es: prancheta escamote&aacute;vel, espaldar m&eacute;dio (diretor) e espaldar alto (presidente). bra&ccedil;os e/ou porta livros opcional(is)</p>\r\n<p>montagem: poltronas individuais soltas ou montagem em longarinas de multiplos lugares</p>', 'fraque1.jpg', 1, 105, 5),
(119, 'Savuto', 'poltrona individual solta ou longarina com assento fixo ou retrátil', '<p>poltrona individual solta ou longarina com assento fixo ou retr&aacute;til</p>\r\n<p>blindagem opcional com capa de polipropileno</p>\r\n<p>prancheta escamote&aacute;vel e/ou porta livros opcional(is)</p>\r\n<p><object id="db19b978-894b-a693-e8d9-ba82ca8cbd9e" type="application/gas-events-abn" width="0" height="0"></object></p>', 'ok4919.jpg', 1, 105, 4),
(120, 'Treviso', 'modular, assento/encosto retráteis', '<p>modular, assento/encosto retr&aacute;teis</p>\r\n<p>blindagem opcional no contra assento e no contra encosto</p>\r\n<p>prancheta escamote&aacute;vel embutida nos bra&ccedil;os opcional</p>\r\n<p>&nbsp;</p>\r\n<p><object id="8b3bea58-e94b-909d-9878-bb83f8a88fbf" type="application/gas-events-abn" width="0" height="0"></object></p>', 'DSC08727_copy.jpg', 1, 105, 1),
(121, 'Call Center', 'NR 17 - tampos reguláveis e painéis metálicos', '<p>NR 17 - tampos regul&aacute;veis e pain&eacute;is met&aacute;licos</p>', 'nr17.jpg', 11, 110, NULL),
(122, 'Call Center - Ambientes', 'Call Center - Ambientes', '<p>Call Center - Ambientes</p>', 'callcenter012.jpg', 11, 90, NULL),
(123, 'Barcelona', ' Mies van der Rohe', '<p>Poltrona Barcelona</p>\r\n<p><span>Mies van der Rohe</span></p>', 'barcelona.jpg', 1, 106, 19),
(124, 'Egg', 'Arne Jacobsen', '<p>Poltrona Egg</p>\r\n<p>Arne Jacobsen</p>', 'egg.jpg', 1, 106, 20),
(125, 'Knoll fixa', 'Florence Knoll', '<p>Poltrona Knoll fixa</p>\r\n<p>Florence Knoll</p>', 'florence_knoll_base_fixa.jpg', 1, 106, 21),
(126, 'Knoll giratória', 'Florence Knoll', '<p>Knoll girat&oacute;ria</p>\r\n<p>Florence Knoll</p>', 'florence_knoll_base_gira.jpg', 1, 106, 22),
(127, 'Swan', 'Arne Jacobsen', '<p>Poltrona Swan</p>\r\n<p>Arne Jacobsen</p>', 'paulin.jpg', 1, 106, 23),
(128, 'Totti', '01, 02 ou 03 lugares', '<p>Sof&aacute; Totti</p>\r\n<p>01, 02 ou 03 lugares</p>\r\n<p><object id="795a9b09-5b58-a09c-091b-4bbcd98bb4a8" type="application/gas-events-abn" width="0" height="0"></object></p>', 'totti_01_lugar.jpg', 1, 109, 24),
(129, 'Totti reforçado', '01, 02 ou 03 lugares', '<p>Sof&aacute; Totti refor&ccedil;ado com estrutura tubular met&aacute;lica externa</p>\r\n<p>01, 02 ou 03 lugares</p>\r\n<p><object id="38ea296a-78ab-8799-9889-0ab2f9ae879e" type="application/gas-events-abn" width="0" height="0"></object></p>', 'totti_reforcado_01_lugar.jpg', 1, 109, 25),
(132, 'Trento', '01, 02 ou 03 lugares', '<p>sof&aacute; Trento</p>\r\n<p>01, 02 ou 03 lugares</p>', 'trento_02_lugares.jpg', 1, 109, 28),
(134, 'Coffice', 'Coffice', '<p>Coffice</p>', 'coffice.jpg', 17, 111, NULL),
(135, 'Balcão semi circular', 'Balcão semi circular', '<p>Balc&atilde;o semi circular</p>', 'balcao-semi-circular.jpg', 17, 113, 2),
(136, 'Projeto Personalizado', 'Projeto Personalizado', '<p>Projeto Personalizado</p>', 'projeto-personalizado-01.jpg', 17, 113, 0),
(137, 'Projeto Personalizado', 'Projeto Personalizado', '<p>Projeto Personalizado</p>', 'projeto-personalizado-02.jpg', 17, 113, 1),
(138, 'Toilet', 'Toilet', '<p>Toilet</p>\r\n<p><object id="390afa9b-88ca-a783-8939-79b9ebb8ba86" type="application/gas-events-abn" width="0" height="0"></object></p>', 'artesano_banheiro.jpg', 17, 114, NULL),
(140, 'Mesa reunião e conjunto diretor', 'reunião redonda em vidro preto. Conjunto diretor e bancada suspensa em fresno.', '<p>Mesa reuni&atilde;o e conjunto direto.</p>\r\n<p>Mesa de reuni&atilde;o redonda em vidro preto. Conjunto diretor e bancada suspensa em fresno.</p>', 'conjunto-em-fresno-com-reuniao-em-vidro-preto.jpg', 15, 115, 1),
(141, 'Mesa reunião e conjunto diretor', 'reunião em nogal. conjunto diretor com tampo de vidro preto.', '<p>mesa reuni&atilde;o e conjunto diretor.</p>\r\n<p>reuni&atilde;o em nogal. conjunto diretor com tampo de vidro preto.</p>', 'conjunto-em-nogal.jpg', 15, 115, 0),
(142, 'Reunião', 'Mesa em ártico. Portas dos armários em lacca branca brilho. painel de parede em ártico.', '<p>Reuni&atilde;o</p>\r\n<p>Mesa em &aacute;rtico. Bancada de arm&aacute;rios suspensos com corpo em &aacute;rtico e portas em lacca branca brilho. painel de parede em &aacute;rtico.</p>', 'reuniao-em-artico-e-portas-dos-armarios-em-lacca-branca.jpg', 15, 115, 2),
(143, 'Reunião', 'Mesa e bancada de armários em fresno. ', '<p>Reuni&atilde;o</p>\r\n<p>Mesa e bancada de arm&aacute;rios em fresno</p>', 'reuniao-em-fresno.jpg', 15, 115, 3),
(144, 'Armarios suspensos e painel de tv', 'Detlalhes em narita e, lacca branca e preta brilhante', '<p>Armarios suspensos e painel de tv.</p>\r\n<p>Detlalhes em narita e, lacca branca e preta brilhante.</p>', 'armariossuspensosepaineldetv.jpg', 15, 116, 1),
(145, 'Bancada em fresno e armarios suspensos', 'Bancada em fresno e armarios suspensos', '<p>Bancada em fresno e armarios suspensos</p>', 'bancada-em-fresno-e-armarios-suspensos.jpg', 15, 116, 2),
(146, 'Bancada painel de tv e estante e armarios suspensos', 'Bancada painel de tv e estante e armarios suspensos', '<p>Bancada painel de tv e estante e armarios suspensos</p>', 'bancadapainelde-tvestanteearmariossuspensos.jpg', 15, 116, 4),
(147, 'Conjunto diretor e armario piso teto em lacca', 'bancada em fresno. Armário com portas laqueadas.', '<p>Conjunto diretor e armario piso teto.</p>\r\n<p>Bancada em fresno. Arm&aacute;rio com portas laqueadas.</p>', 'conjunto-diretor-e-armario-piso-teto-em-lacca.jpg', 15, 116, 0),
(148, 'Cconjunto diretor e estante piso-teto', 'Cconjunto diretor e estante piso-teto', '<p>Cconjunto diretor e estante piso-teto</p>', 'conjunto-diretor-e-estante-piso-teto.jpg', 15, 116, 5),
(149, 'Conjunto diretor painel de tv e armarios piso teto', 'Conjunto diretor painel de tv e armarios piso teto', '<p>Conjunto diretor painel de tv e armarios piso teto</p>', 'conjunto-diretorpainel-de-tv-e-armarios-piso-teto.jpg', 15, 116, 6),
(150, 'Consultorio com mesa e estante piso teto', 'Consultorio com mesa e estante piso teto', '<p>Consultorio com mesa e estante piso teto</p>', 'consultorio-com-mesa-e-estante-piso-teto.jpg', 15, 116, 3),
(151, 'Banca de armários', 'Portas e detalhes em lacca clay brilho.', '<p>Banca de arm&aacute;rios.</p>\r\n<p>Portas e detalhes em lacca clay brilho.</p>', 'banca-de-armarios.jpg', 15, 117, 4),
(152, 'Conjunto diretor e armários', 'Mobiliario em lino. Armários com portas e detalhes em lacca clay brilho.', '<p>Conjunto diretor e arm&aacute;rios.</p>\r\n<p>Mobiliario em lino. Arm&aacute;rios com portas e detalhes em lacca clay brilho.</p>', 'conjunto-diretor-e-armarios.jpg', 15, 117, 1),
(153, 'Conjunto diretor em "L"', 'Mobiliário em lino. ', '<p>Conjunto diretor em ''L''.</p>\r\n<p>Mobili&aacute;rio em lino.</p>', 'conjunto-diretor-em-L-.jpg', 15, 117, 2),
(154, 'Conjuntos diretoria com aparadores', 'Mobiliario em lino. Portas dos armários em lacca clay brilho.', '<p>Conjuntos diretoria com aparadores.</p>\r\n<p>Mobiliario em lino. Portas dos arm&aacute;rios em lacca clay brilho.</p>', 'conjuntos-diretoria-com-aparadores.jpg', 15, 117, 0),
(155, 'Mesa reta e armários', 'Mobiliario em lino. Armários com portas e detalhes em lacca clay brilho.', '<p>Mesa reta e arm&aacute;rios.</p>\r\n<p>Mobiliario em lino. Arm&aacute;rios com portas e detalhes em lacca clay brilho.</p>', 'mesa-reta-e-armarios.jpg', 15, 117, 3),
(156, 'Estação em -C- em platina', 'Estacao em -C- em platina', '<p>Estacao em -C- em platina</p>', 'estacao-em--C--em-platina.jpg', 15, 118, 17),
(157, 'Estação em -C- padrão madeira', 'Estação em -C- padrão madeira', '<p>Esta&ccedil;&atilde;o em -C- padr&atilde;o madeira</p>', 'estacao-em--C--padrao-madeira.jpg', 15, 118, 18),
(165, 'Arquivos', 'Arquivos', '<p>Arquivos</p>', 'arquivos.jpg', 14, 119, 5),
(181, 'Divisórias em painéis metálicos', 'Divisórias em painéis metálicos', '<p>Divis&oacute;rias em pain&eacute;is met&aacute;licos</p>', 'divisorias-em-paineis-metalicos3.jpg', 14, 120, 0),
(182, 'Painéis suspensos em vidro 01', 'Painéis suspensos em vidro 01', '<p>Pain&eacute;is suspensos em vidro 01</p>', 'paineis-suspensos-em-vidro-01.jpg', 14, 120, 4),
(183, 'Painéis suspensos em vidro 02', 'Painéis suspensos em vidro 02', '<p>Pain&eacute;is suspensos em vidro 02</p>', 'paineis-suspensos-em-vidro-02.jpg', 14, 120, 5),
(184, 'Plataforma com divisória frontal cega', 'Plataforma com divisória frontal cega', '<p>Plataforma com divis&oacute;ria frontal cega</p>', 'plataforma-com-divisoria-frontal-cega.jpg', 14, 121, 1),
(185, 'Plataforma com divisória frontal em vidro e gaveteiro pedestal', 'Plataforma com divisória frontal em vidro e gaveteiro pedestal', '<p>Plataforma com divis&oacute;ria frontal em vidro e gaveteiro pedestal</p>', 'plataforma-com-divisoria-frontal-em-vidro-e-gaveteiro-pedestal.jpg', 14, 121, 3),
(186, 'Plataforma com divisória frontal em vidro e gaveteiro', 'Plataforma com divisória frontal em vidro e gaveteiro', '<p>Plataforma com divis&oacute;ria frontal em vidro e gaveteiro</p>', 'plataforma-com-divisoria-frontal-em-vidro-e-gaveteiro.jpg', 14, 121, 0),
(187, 'Plataforma com divisória frontal em vidro e gaveteiro pedestal', 'Plataforma com divisória frontal em vidro', '<p>Plataforma com divis&oacute;ria frontal em vidro</p>', 'plataforma-com-divisoria-frontal-em-vidro.jpg', 14, 121, 2),
(188, 'Plataforma com gaveteiro divisória frontal em vidro e armário complemento', 'Plataforma com gaveteiro divisória frontal em vidro e armário complemento', '<p>Plataforma com gaveteiro divis&oacute;ria frontal em vidro e arm&aacute;rio complemento</p>', 'plataforma-com-gaveteiro-divisoria-frontal-em-vidro-e-armario-complemento.jpg', 14, 121, 4),
(189, 'Plataforma com gaveteiro fixo', 'Plataforma com gaveteiro fixo', '<p>Plataforma com gaveteiro fixo</p>', 'plataforma-com-gaveteiro-fixo.jpg', 14, 121, 5),
(190, 'plataforma com gaveteiro pedestal divisória frontal em vidro e tampo Pomplementar', 'plataforma com gaveteiro pedestal divisória frontal em vidro e tampo complementar', '<p>plataforma com gaveteiro pedestal divis&oacute;ria frontal em vidro e tampo Pomplementar</p>', 'plataforma-com-gaveteiro-pedestal-divisoria-frontal-em-vidro-e-tampo-complementar.jpg', 14, 121, 6),
(191, 'Plataforma com gaveteiro suporte cpu divisoria lateral e nicho', 'Plataforma com gaveteiro suporte cpu divisoria lateral e nicho', '<p>Plataforma com gaveteiro suporte cpu divisoria lateral e nicho</p>', 'plataforma-com-gaveteiro-suporte-cpu-divisoria-lateral-e-nicho.jpg', 14, 121, 7),
(192, 'Plataforma com nichos e armários complementares', 'Plataforma com nichos e armários complementares', '<p>Plataforma com nichos e arm&aacute;rios complementares</p>', 'plataforma-com-nichos-e-armarios-complementares.jpg', 14, 121, 8),
(193, 'Plataforma com vidro frontal gaveteiro armário complementar e suporte cpu', 'Plataforma com vidro frontal gaveteiro armário complementar e suporte cpu', '<p>Plataforma com vidro frontal gaveteiro arm&aacute;rio complementar e suporte cpu</p>', 'plataforma-com-vidro-frontal-gaveteiro-armario-complementar-e-suporte-cpu.jpg', 14, 121, 9),
(194, 'Reunião semi oval', 'Reunião semi oval', '<p>Reuni&atilde;o semi oval</p>', 'reuniao-semi-oval.jpg', 14, 122, NULL),
(195, 'Reunião semi oval com cx de tomada', 'Reunião semi oval com cx de tomada', '<p>Reuni&atilde;o semi oval com cx de tomada</p>', 'reuniao-semi-oval-com-cx-de-tomada.jpg', 14, 122, NULL),
(196, 'Reunião tivoli redonda', 'Reunião tivoli redonda', '<p>Reuni&atilde;o tivoli redonda</p>', 'reuniao-tivoli-redonda.jpg', 14, 122, NULL),
(197, 'Estação com painéis metálicos', 'Estação com painéis metálicos', '<p>pain&eacute;is com placas em bp na parte inferior e vidro jateado listrado na parte superior</p>', '16.jpg', 14, 120, 1),
(198, 'Projeto personalizado', '', '<p>Projeto personalizado</p>', 'ambientestato01_FINAL.jpg', 17, 113, NULL),
(199, 'Estante e conjunto diretor integrados', 'Mobiliário em ártico', '<p>Estante e conjunto diretor integrados.</p>\r\n<p>Mobili&aacute;rio em &aacute;rtico.</p>', '17006-17025-17030_(jpg_baixa_-__300_dpis)[1].jpg.jpg', 15, 115, 4),
(200, 'Estação diretoria com bancadas', 'Estação em fresno. Armários em nogal.', '<p>Esta&ccedil;&atilde;o diretoria com tampos espessos em fresno.</p>\r\n<p>Arm&aacute;rios modul&aacute;veis em nogal.</p>', '17015-17062-17055-17036-17032-16006-16015(jpg_baixa)[1].jpg', 15, 115, 6),
(201, 'conjunto diretor', 'conjunto diretor em ártico. módulos de armários em fresno.', '<p>conjunto diretor com tampo de vidro preto e chapas em &aacute;rtico.</p>\r\n<p>m&oacute;dulos de arm&aacute;rios em fresno.</p>', '17016-17030-17020-17006-17042-17015_(jpg_baixa_-__300_dpis)[1].jpg.jpg', 15, 115, 8),
(202, 'conjunto diretor e armários', 'Conjunto diretor e armários em nogal.', '<p>Conjunto diretor e arm&aacute;rios modul&aacute;veis em nogal</p>', '17020-17030-17000-17016_(jpg_baixa_-__300_dpis)[1].jpg.jpg', 15, 115, 9),
(203, 'Estação diretoria com bancadas', 'Estação em vidro preto e nogal. Bancadas de armários em ártico.', '<p>Esta&ccedil;&atilde;o diretoria com mesas din&acirc;micas apoiadas sobre aparadores. Superf&iacute;cies em vidro preto, chapas nogal.</p>\r\n<p>Bancadas de m&oacute;dulos de arm&aacute;rios altos e baixos em &aacute;rtico.</p>', '17035-17036-17037-17006-17015-17026_(jpg_baixa_-__300_dpis)[1].jpg.jpg', 15, 115, 5),
(204, 'conjuntos diretoria', 'Conjuntos diretoria em ártico. Armários em ártico.', '<p>Conjuntos diretoria com tampos espessos em &aacute;rtico.</p>\r\n<p>Arm&aacute;rios modul&aacute;veis em &aacute;rtico.</p>', '17053-17054-17065-17040-18031_(jpg_baixa)[1].jpg.jpg', 15, 115, 7),
(205, 'Estação com painéis divisores suspensos', 'Estação com painéis divisores suspensos', '<p>Esta&ccedil;&atilde;o com pain&eacute;is divisores suspensos</p>', 'ESTACAO_COM_PAINEL_DIVISOR_copy.jpg', 14, 120, 6),
(206, 'Refeitório', 'Refeitório', '<p>Refeit&oacute;rio</p>', 'REFEITORIO_copy.jpg', 17, 123, NULL),
(207, 'Estação Modena com bancada', 'Mesas e bancada de armários em ártico. Painel em lino.', '<p>Esta&ccedil;&atilde;o modena.</p>\r\n<p>Mesas e bancada de arm&aacute;rios em &aacute;rtico. Painel em lino.</p>', 'estacao_modena_compactada.jpg', 15, 115, 10),
(210, 'Estações com painéis suspensos', 'Estações com painéis suspensos', '<p>Esta&ccedil;&otilde;es com pain&eacute;is suspensos</p>', 'sala_1_compactada.jpg', 14, 120, 3),
(211, 'Estações com painéis metálicos', 'Estações com painéis metálicos', '<p>Esta&ccedil;&otilde;es com pain&eacute;is met&aacute;licos</p>', 'sala_5_compactada.jpg', 14, 120, 2),
(212, 'Ancona', 'poltrona giratória estofada', '<p>poltrona girat&oacute;ria estofada</p>\r\n<p>op&ccedil;&otilde;es: espaldar m&eacute;dio (diretor) e espaldar alto (presidente). bra&ccedil;os opcionais</p>\r\n<p>regulagens: altura do assento, inclina&ccedil;&atilde;o do encosto com bloqueio multiponto atrav&eacute;s de alavanca, altura dos bra&ccedil;os</p>\r\n<p>materiais: assento e encosto blindados na parte traseira com capa de polipropileno, base em nylon injetado, rodizios com faixa de poliuretano&nbsp;</p>', 'Ancona.jpg', 1, 103, 4),
(213, 'Ventura', 'poltrona giratória telada', '<p>poltrona girat&oacute;ria telada</p>\r\n<p>regulagens: altura do assento, altura e rota&ccedil;&atilde;o do bra&ccedil;o, inclina&ccedil;&atilde;o do assento/encosto com bloqueio multiponto (mecanismo synchron), altura do apoio de cabe&ccedil;a, ajuste de tens&atilde;o lombar, ajuste de tens&atilde;o de inclina&ccedil;&atilde;o</p>\r\n<p>materiais: assento e encosto em tela de poliuretano, base em aluminio, chassis em polipropileno, rodizios com faixa de poliuretano&nbsp;</p>', 'Perspectiva.jpg', 1, 103, 0),
(214, 'Aprilia', 'empilhável, conectável', '<p>cadeira multiuso empilh&aacute;vel, conect&aacute;vel</p>\r\n<p>v&aacute;rias cores</p>', 'WEB_0061.jpg', 1, 107, 0),
(215, 'Loop', 'concha única com braços integrados', '<p>cadeira multiuso com concha &uacute;nica com bra&ccedil;os integrados</p>', 'Perspectiva_vermelha1.jpg', 1, 107, 1),
(216, 'Diva', 'fixa/giratória, telada', '<p>cadeira fixa/girat&oacute;ria, telada, multiuso</p>\r\n<p>op&ccedil;&otilde;es: base fixa continua cromada/preta ou girat&oacute;ria. bra&ccedil;os opcionais</p>\r\n<p>regulagens: altura do assento</p>\r\n<p>materiais: encosto em tela, assento estofado, base cromada/preta</p>', 'diva41.jpg', 1, 107, 3),
(217, 'Alumi', 'empilhável, pés em alumínio', '<p>cadeira multiuso empilh&aacute;vel</p>\r\n<p>p&eacute; em alum&iacute;nio, para uso externo</p>\r\n<p>v&aacute;rias cores</p>\r\n<p><object id="aa0a5908-39ca-b9a6-b8b9-0a94eab7aaaf" type="application/gas-events-abn" width="0" height="0"></object></p>', 'PLOP_0061.jpg', 1, 107, 4),
(219, 'Riesi', 'poltrona individual solta ou longarina com assento fixo ou retrátil', '<p>poltrona individual solta ou longarina com assento fixo ou retr&aacute;til</p>\r\n<p>op&ccedil;&otilde;es: prancheta escamote&aacute;vel, espaldar m&eacute;dio (diretor) e espaldar alto (presidente). bra&ccedil;os e/ou porta livros opcional(is)</p>\r\n<p>montagem: poltronas individuais soltas ou montagem em longarinas de multiplos lugares</p>', 'pm_01.jpg', 1, 105, 6),
(220, 'Diva', 'cadeira individual solta', '<p>cadeira individual solta, telada, multiuso</p>\r\n<p>op&ccedil;&otilde;es: base cromada/preta. bra&ccedil;os opcionais</p>\r\n<p>materiais: encosto em tela, assento estofado, base cromada/preta</p>', 'auditorio.jpg', 1, 105, 3),
(222, 'Sofia', 'modular, assento fixo', '<p>modular, assento fixo</p>\r\n<p>porta copos opcional</p>', 'isometrica.jpg', 1, 105, 7),
(223, 'Dueto', 'assento bicolor com tecnologia de injeção nas duas faces', '<p>assento bicolor com tecnologia de inje&ccedil;&atilde;o nas duas faces</p>\r\n<p>estrutura em polipropileno e fibra de vidro</p>\r\n<p><object id="29ebcbab-9999-92ae-29f9-f8a64b9d9789" type="application/gas-events-abn" width="0" height="0"></object></p>', 'Now_011.jpg', 1, 107, 2),
(224, 'Estação pé painel e divisórias suspensas', 'Estação pé painel e divisórias suspensas', '<p>Esta&ccedil;&atilde;o p&eacute; painel e divis&oacute;rias suspensas</p>', 'estacao_tivoli_pe_painel.jpg', 14, 120, 38),
(225, 'Reunião pé painel', 'Reunião pé painel', '<p>Reuni&atilde;o p&eacute; painel</p>\r\n<p><object id="79b90a59-1979-9db5-9929-7aba69a89eb5" type="application/gas-events-abn" width="0" height="0"></object></p>', 'reuniao_pe_painel_tivoli.jpg', 14, 122, 39),
(226, 'Nuova', 'fixa empilhável ou giratória', '<p>cadeira multiuso fixa empilh&aacute;vel ou girat&oacute;ria</p>\r\n<p>v&aacute;rias cores</p>\r\n<p>bra&ccedil;os opcionais</p>\r\n<p><object id="2abb4a58-d9f8-b981-bb59-9aa18a988996" type="application/gas-events-abn" width="0" height="0"></object></p>', 'nuova_fixa.jpg', 1, 107, 5),
(227, 'Convivência', 'Convivência', '<p>Conviv&ecirc;ncia</p>\r\n<p><object id="cb2afb5b-5afa-b9a0-6b78-8a9129a6b2b1" type="application/gas-events-abn" width="0" height="0"></object></p>', 'ambiente_dani.jpg', 17, 112, 40),
(228, 'Dani', 'fixa empilhável ou giratória', '<p>cadeira multiuso fixa empilh&aacute;vel ou girat&oacute;ria</p>\r\n<p>v&aacute;rias cores</p>\r\n<p>bra&ccedil;os opcionais</p>\r\n<p><object id="9979ea68-d8ea-9398-18fa-7aa68baebfbb" type="application/gas-events-abn" width="0" height="0"></object></p>', 'dani_sem_braco.jpg', 1, 107, 41),
(229, 'Trapani', 'poltrona giratória estofada', '<p>poltrona girat&oacute;ria estofada</p>\r\n<p>regulagens: altura do assento, ajuste de tens&atilde;o de inclina&ccedil;&atilde;o do conjunto assento/encosto (relax) com travamento na posi&ccedil;&atilde;o reta.</p>\r\n<p>materiais: assento e encosto em concha &uacute;nica estofada, base em a&ccedil;o cromado, rodizios com faixa de poliuretano</p>\r\n<p><object id="ab89db19-89b8-9f9d-6909-388d58b591a6" type="application/gas-events-abn" width="0" height="0"></object></p>', 'Turim_Perspectiva1.jpg', 1, 103, 14),
(230, 'Mesas pé painel', 'Mesas pé painel', '<p>Mesas p&eacute; painel</p>\r\n<p><object id="1b8b2a89-e9f9-b6ba-e968-a9b8b8bfaf89" type="application/gas-events-abn" width="0" height="0"></object></p>', 'mesa_pe_painel_tivoli.jpg', 14, 120, 43),
(231, 'Armários', 'Armários', '<p>Arm&aacute;rios</p>\r\n<p><object id="2abb0bd8-59b8-b3ab-ebd9-9bb93bb6bfa6" type="application/gas-events-abn" width="0" height="0"></object></p>', 'capa.jpg', 14, 119, 0),
(232, 'Vita cromo', '01, 02 ou 03 lugares', '<p>Sofanete Vita cromo</p>\r\n<p>01, 02 ou 03 lugares</p>\r\n<p><object id="2b88d86b-5a38-8693-aafb-8aa2a8ba9d9f" type="application/gas-events-abn" width="0" height="0"></object></p>', 'vitacromo.jpg', 1, 109, 44),
(233, 'Gaveteiros fixos', 'Gaveteiros fixos', '<p>Gaveteiros fixos</p>\r\n<p><object id="daab5b7b-bb29-98a9-3978-ca94bab786b4" type="application/gas-events-abn" width="0" height="0"></object></p>', '1_ec.jpg', 14, 119, 1),
(234, 'Gaveteiros volantes', 'Gaveteiros volantes', '<p>Gaveteiros volantes</p>\r\n<p><object id="38a9ea58-39eb-a097-aa2b-bb875880839c" type="application/gas-events-abn" width="0" height="0"></object></p>', 'gaveteiro0200001.jpg', 14, 119, 2),
(235, 'Gaveteiros junção', 'Gaveteiros junção', '<p>Gaveteiros jun&ccedil;&atilde;o</p>\r\n<p><object id="486b589b-0b1b-af97-0949-69996bb6a6a7" type="application/gas-events-abn" width="0" height="0"></object></p>', 'capa1.jpg', 14, 119, 3),
(236, 'Gaveteiros pedestal', 'Gaveteiros pedestal', '<p>Gaveteiros pedestal</p>\r\n<p><object id="1aea984b-1aca-ae90-7a78-ba9f28a6b0aa" type="application/gas-events-abn" width="0" height="0"></object></p>', '110.jpg', 14, 119, 4),
(237, 'Montagem', 'Montagem', '<p>Montagem</p>\r\n<p><object id="080a5a29-db7b-9fb1-192b-fb9ea982ac86" type="application/gas-events-abn" width="0" height="0"></object></p>', '116.jpg', 11, 126, 45),
(238, 'Treinamento', 'Treinamento', '<p>Treinamento</p>\r\n<p><object id="4939da89-8aaa-b080-5959-68a8faa083a6" type="application/gas-events-abn" width="0" height="0"></object></p>', 'wok2107_copy.jpg', 17, 127, 46),
(239, 'Mesa Delta atendimento', 'Mesa Delta atendimento', '<p>Mesa Delta atendimento</p>\r\n<p><object id="9a09fa88-e9d9-85b4-48ca-38b36ab7bfa5" type="application/gas-events-abn" width="0" height="0"></object></p>', '121.jpg', 14, 128, 1),
(240, 'Mesa Delta', 'Mesa Delta', '<p>Mesa Delta</p>\r\n<p><object id="b97a6838-7809-bba2-1b79-1bb84a8e9894" type="application/gas-events-abn" width="0" height="0"></object></p>', '414.jpg', 14, 128, 0),
(241, 'Mesa Reta', 'Mesa Reta', '<p>Mesa Reta</p>\r\n<p><object id="c8e9694b-5989-9f93-b999-1ab59889b2b2" type="application/gas-events-abn" width="0" height="0"></object></p>', '123.jpg', 14, 128, 47),
(242, 'Reunião', 'Reunião', '<p>Reuni&atilde;o</p>\r\n<p><object id="b8a8a96a-08f9-b6a8-19ca-68bc28babdad" type="application/gas-events-abn" width="0" height="0"></object></p>', '124.jpg', 14, 128, 48),
(243, 'Gioia', 'modular, assento fixo ou rebatível', '<p>modular, assento fixo ou rebat&iacute;vel</p>\r\n<p>op&ccedil;&otilde;es de acabamentos do contra assento/contra encosto: blindagem com capa de polipropileno, tecido, compensado laminado</p>\r\n<p>prancheta escamote&aacute;vel opcional</p>\r\n<p>&nbsp;</p>\r\n<p><object id="2afaf97a-9a79-80b5-7b88-7bb6a8ac9d9b" type="application/gas-events-abn" width="0" height="0"></object></p>', 'DSC05431.jpg', 1, 105, 2),
(244, 'Bari', 'poltrona giratória estofada', '<p>poltrona girat&oacute;ria estofada</p>\r\n<p>regulagens: altura do assento, ajuste de tens&atilde;o de inclina&ccedil;&atilde;o do conjunto assento/encosto (relax) com travamento na posi&ccedil;&atilde;o reta.</p>\r\n<p>materiais: assento e encosto em concha &uacute;nica estofada, base em alum&iacute;nio polido e estrutura do bra&ccedil;o em alum&iacute;nio polido, rodizios com faixa de poliuretano</p>\r\n<p>&nbsp;</p>', '_MG_8465.jpg', 1, 103, 6),
(245, 'Estação com mesa reta, armários e mesa delta', 'Estação com mesa reta, armários e mesa delta', '<p>Esta&ccedil;&atilde;o com mesa reta, arm&aacute;rios e mesa delta</p>', 'Linha_Alfa_copy_(1).jpg', 14, 120, 49),
(247, 'LC', 'Le Corbusier', '<p>Sof&aacute; LC</p>\r\n<p>Le Corbusier</p>', 'lc21.jpg', 1, 106, 50);

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos`
--

CREATE TABLE IF NOT EXISTS `projetos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `texto` text,
  `capa` varchar(255) DEFAULT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Extraindo dados da tabela `projetos`
--

INSERT INTO `projetos` (`id`, `titulo`, `texto`, `capa`, `created`, `updated`, `ordem`) VALUES
(7, 'Projeto 1', '<p>Projeto teste</p>', 'projeto_21.jpg', 1364471318, 0, 1),
(8, 'Projeto 2', '<p>Projeto teste</p>', 'projeto_21.jpg', 1365507454, 0, 2),
(9, 'Projeto 3', '<p>Projeto teste</p>', 'projeto_21.jpg', 1365507454, NULL, 3),
(10, 'Projeto 4', '<p>Projeto teste</p>', 'projeto_21.jpg', 1364471318, NULL, 4),
(11, 'Projeto 5', '<p>Projeto teste</p>', 'projeto_21.jpg', 1365507454, NULL, 5),
(12, 'Projeto 6', '<p>Projeto teste</p>', 'projeto_21.jpg', 1365507454, NULL, 6),
(13, 'Projeto 7', '<p>Projeto teste</p>', 'projeto_21.jpg', 1365507454, NULL, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) NOT NULL,
  `default` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `roles`
--

INSERT INTO `roles` (`id`, `role`, `default`) VALUES
(1, 'admin', 1),
(2, 'user', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE IF NOT EXISTS `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` text,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `titulo`, `descricao`, `created`, `updated`) VALUES
(2, 'Serviço teste 2', '<ul>\n<li>Conte&uacute;do 0</li>\n<li>Conte&uacute;do 1</li>\n<li>Conte&uacute;do 2</li>\n</ul>', 1362661426, 0),
(3, 'Serviço teste 1', '<ul>\n<li>Conte&uacute;do 0</li>\n<li>Conte&uacute;do 1</li>\n<li>Conte&uacute;do 2</li>\n</ul>', 1362661434, 0),
(4, 'Serviço Teste 3', '<ul>\n<li>Item 0</li>\n<li>Item 1</li>\n<li>Item 2</li>\n</ul>', 1363782196, 1363782196);

-- --------------------------------------------------------

--
-- Estrutura da tabela `showrooms`
--

CREATE TABLE IF NOT EXISTS `showrooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `endereco` varchar(255) DEFAULT NULL,
  `numero` varchar(255) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `uf` varchar(255) DEFAULT NULL,
  `cep` varchar(20) NOT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `inicio_seg_sex` varchar(255) DEFAULT NULL,
  `fim_seg_sex` varchar(255) DEFAULT NULL,
  `inicio_sab` varchar(255) DEFAULT NULL,
  `fim_sab` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `showrooms`
--

INSERT INTO `showrooms` (`id`, `endereco`, `numero`, `bairro`, `cidade`, `uf`, `cep`, `tel`, `email`, `twitter`, `facebook`, `inicio_seg_sex`, `fim_seg_sex`, `inicio_sab`, `fim_sab`) VALUES
(1, 'Avenida Pacaembu', '1.273', 'Pacaembu', 'São Paulo ', 'SP', '01234-001', '(11) 3801-3801', 'stato@stato.com.br', 'statomoveis', 'http://facebook.com/statomoveis', '9:00', '18:00', '9:00', '15:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `slides`
--

CREATE TABLE IF NOT EXISTS `slides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `padrao` tinyint(1) NOT NULL,
  `ordem` int(11) NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  `local` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Extraindo dados da tabela `slides`
--

INSERT INTO `slides` (`id`, `titulo`, `link`, `imagem`, `padrao`, `ordem`, `created`, `updated`, `local`) VALUES
(27, NULL, NULL, 'reuniao_02_FINAL1.jpg', 0, 1, 0, 0, 'home'),
(28, NULL, NULL, 'plataforma1jpeg1.jpg', 0, 2, 0, 0, 'home'),
(29, NULL, NULL, 'recepcao2jpeg1.jpg', 0, 3, 0, 0, 'home'),
(30, NULL, NULL, 'plataformacrownjpeg1.jpg', 0, 4, 0, 0, 'home'),
(31, NULL, NULL, 'reuniao_01_final1.jpg', 0, 5, 0, 0, 'home'),
(32, NULL, NULL, 'ravena11.jpg', 0, 6, 0, 0, 'home'),
(33, NULL, NULL, '45b3f7bbbee0c6cc56b5c58bbff524ac.jpg', 0, 1, 0, NULL, 'empresa'),
(35, NULL, NULL, 'bf8f5d79fa7a99b0c640f778387d2692.jpg', 0, 0, 0, NULL, 'home'),
(36, NULL, NULL, '2ccb8d53b5a7d62f07d1e26c8d9feddd.jpg', 0, 0, 0, NULL, 'empresa');

-- --------------------------------------------------------

--
-- Estrutura da tabela `subcategorias`
--

CREATE TABLE IF NOT EXISTS `subcategorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `categoria_id` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=130 ;

--
-- Extraindo dados da tabela `subcategorias`
--

INSERT INTO `subcategorias` (`id`, `titulo`, `slug`, `categoria_id`, `ordem`) VALUES
(59, 'Estações de Trabalho 2', NULL, NULL, NULL),
(60, 'Balcão', NULL, NULL, NULL),
(75, 'Mesa', NULL, NULL, NULL),
(76, 'Painéis de madeira', NULL, NULL, NULL),
(77, 'Painéis metálicos', NULL, NULL, NULL),
(78, 'Balcões', NULL, NULL, NULL),
(79, 'Composições', NULL, NULL, NULL),
(80, 'Mesas', NULL, NULL, NULL),
(90, 'Ambientes', NULL, '11', NULL),
(91, 'Divisórias Piso Teto', NULL, '12', NULL),
(92, 'Estações de Trabalho', NULL, NULL, NULL),
(97, 'Armários', NULL, NULL, NULL),
(98, 'Arquivos', NULL, NULL, NULL),
(99, 'Gaveteiros', NULL, NULL, NULL),
(103, 'Poltronas', NULL, '1', 0),
(104, 'Auditório', NULL, NULL, NULL),
(105, 'Auditório', NULL, '1', 1),
(106, 'Clássicos', NULL, '1', 3),
(107, 'Multiuso', NULL, '1', 2),
(109, 'Sofás', NULL, '1', 4),
(110, 'Opções', NULL, '11', NULL),
(111, 'Coffice', NULL, '17', 1),
(112, 'Convivência', NULL, '17', 2),
(113, 'Recepção', NULL, '17', 0),
(114, 'Toilet', NULL, '17', 3),
(115, 'Modena', NULL, '15', 0),
(116, 'Personalizados', NULL, '15', 2),
(117, 'Ravena', NULL, '15', 1),
(118, 'Tivoli', NULL, '15', 3),
(119, 'Arquivamento', NULL, '14', 3),
(120, 'Estações', NULL, '14', 1),
(121, 'Plataformas', NULL, '14', 0),
(122, 'Reunião', NULL, '14', 2),
(123, 'Refeitório', NULL, '17', 4),
(126, 'Montagem', NULL, '11', 1),
(127, 'Treinamento', NULL, '17', 5),
(128, 'Superfícies', NULL, '14', 4),
(129, 'Teste', NULL, '18', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `role_id` int(11) NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `role_id`, `activated`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`) VALUES
(1, 'trupe', '$P$Bm.afp74faAjfqPGOJPYzFREhWoN/S/', 'nilton@trupe.net', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '127.0.0.1', '2013-08-05 18:47:21', '2013-03-06 15:59:08', '2013-08-05 18:47:21'),
(5, 'stato', '$P$BbzS2MPHxFF7Ir/cz2RsNkVuFCQ0Zl0', 'fabiano@stato.com.br', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '177.139.165.177', '2013-07-06 09:51:40', '2013-05-22 15:05:25', '2013-07-06 12:51:40');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_autologin`
--

CREATE TABLE IF NOT EXISTS `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `country`, `website`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL),
(3, 0, NULL, NULL),
(4, 0, NULL, NULL),
(5, 5, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
        <title><?php echo $titulo; ?></title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="Trupe Agência Criativa">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet/less" href="<?php echo base_url(); ?>assets/css/main.less">
        <script src="<?php echo base_url(); ?>assets/js/less-1.3.0.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css">
        <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
   

    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <header>
              
        </header>

        <?php if(isset($conteudo_principal)){ $this->load->view($conteudo_principal); } ?>
  
        <footer>
        
	</footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.orbit-1.2.3.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/libs/bootstrap.min.js"></script>
        <script type="text/javascript">
            $(function() {
              $("#accordion").accordion({ autoHeight: false });
            });
        </script>
        </body>

        <script type="text/javascript">
             jQuery(function() {
                jQuery.support.placeholder = false;
                test = document.createElement('input');
                if('placeholder' in test) jQuery.support.placeholder = true;
            });
        </script>
       
        <script>
             $(function() {
                if(!$.support.placeholder) { 
                    var active = document.activeElement;
                    $(':text').focus(function () {
                        if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
                            $(this).val('').removeClass('hasPlaceholder');
                        }
                    }).blur(function () {
                        if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
                            $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
                        }
                    });
                    $(':text').blur();
                    $(active).focus();
                    $('form').submit(function () {
                        $(this).find('.hasPlaceholder').each(function() { $(this).val(''); });
                    });
                }
            });
        </script>
        <script type="text/javascript">
             jQuery(function() {
                $("#ajax-loader").ajaxComplete(function() {
                    $(this).hide();
                });
            });
        </script>
        <script type="text/javascript">
            $(window).load(function() {
                $('#featured').orbit({
                     animation: 'fade',                  // fade, horizontal-slide, vertical-slide, horizontal-push
                     animationSpeed: 800,                // how fast animtions are
                     timer: true,            // true or false to have the timer
                     advanceSpeed: 4000,         // if timer is enabled, time between transitions 
                     pauseOnHover: false,        // if you hover pauses the slider
                     startClockOnMouseOut: false,    // if clock should start on MouseOut
                     startClockOnMouseOutAfter: 1000,    // how long after MouseOut should the timer start again
                     directionalNav: true,       // manual advancing directional navs
                     captions: true,             // do you want captions?
                     captionAnimation: 'fade',       // fade, slideOpen, none
                     captionAnimationSpeed: 800,     // if so how quickly should they animate in
                     bullets: false,             // true or false to activate the bullet navigation
                 });
            });
        </script>
        <script>$(".clipping:nth-child(3)").css({'margin-right' : '0'});</script>
        <script>
            $(function() {
            $('#submit').click(function() {
                $('#ajax-loader').show(); 
            var form_data = {
            nome : $('.nome').val(),
            email : $('.email').val(),
            telefone : $('.telefone').val(),
            mensagem : $('.mensagem').val(),
            ajax : '1'
            };
            $.ajax({
            url: "<?php echo site_url('contato/ajax_check'); ?>",
            type: 'POST',
            async : false,
            data: form_data,
            success: function(msg) {
            $('#message').html(msg);
            }
            });
            return false;
            });
            });
        </script>

        <script>
            $(function() {
            $('#newslettersalva').click(function() {
            var form_data = {
            nome : $('.nome').val(),
            email : $('.email').val(),
            vagas : $('#vagas').val(),
            novidades : $('#novidades-news').val(),
            eventos : $('#eventos').val(),
            pesquisas : $('#resultados').val(),
            noticias: $('#noticias').val(),
            ajax : '1'
            };
            $.ajax({
            url: "<?php echo site_url('newsletters/ajax_check'); ?>",
            type: 'POST',
            async : false,
            data: form_data,
            success: function(msg) {
                alert(msg);
            }
            });
            return false;
            });
            });
        </script>
        <?php if(isset($vagaid)): ?>
            <script type="text/javascript">
                $(window).load(function(){
                    $('#myModal').modal({
                        show: true,
                        remote: "<?php echo base_url(); ?>/vagas/detalhe/<?php echo $tipo . '/interna/' . $vagaid; ?>"
                    });
                });
            </script>
        <?php endif; ?>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            var _gaq=[['_setAccount','UA-35751088-1'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
</html>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
        <?php $this->seo->build_meta(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css">
        <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
        <link rel="stylesheet" href="<?=base_url('assets/prettyphoto/css/prettyPhoto.css'); ?>">
        <link rel="stylesheet" href="<?=base_url('assets/css/jquery.maximage.css'); ?>">
        <title><?=$this->seo->get_title(); ?></title>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-68405409-1', 'auto');
            ga('send', 'pageview');
        </script>
    </head>
    <body>
        <div class="clearfix"></div>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <? if($pagina == 'home'): ?><div class="wrapper"><? endif; ?>
            <div class="clearfix"></div>
        <header class="<?=$pagina;?>">
            <nav id="mobile">
                <a <?=($pagina == 'home') ? 'class="active"' : ''; ?>
                    href="<?=site_url(); ?>">Home
                </a>
                <a <?=($pagina == 'empresa') ? 'class="active"' : ''; ?>
                    href="<?=site_url('empresa'); ?>">Empresa
                </a>
                <a <?=($pagina == 'produtos') ? 'class="active"' : ''; ?>
                    href="<?=site_url('produtos'); ?>">Produtos
                </a>
                <a <?=($pagina == 'cases') ? 'class="active"' : ''; ?>
                    href="<?=site_url('cases'); ?>">Cases
                </a>
                <a <?=($pagina == 'artigos') ? 'class="active"' : ''; ?>
                    href="<?=site_url('artigos'); ?>">Artigos
                </a>
                <a <?=($pagina == 'showroom') ? 'class="active"' : ''; ?>
                    href="<?=site_url('localizacao'); ?>">Localização
                </a>
                <a <?=($pagina == 'contato') ? 'class="active"' : ''; ?>
                    href="<?=site_url('contato'); ?>">Contato
                </a>
                <a href="<?=site_url('contato'); ?>" class="call-to-action">
                    <p class="telefone"><?php echo Modules::run('showrooms/telefone') ?></p>
                    <p class="chamada">solicite uma visita »</p>
                </a>
            </nav>
            <div class="interna">
                <a href="<?=site_url(); ?>" class="marca left"></a>
                <nav id="desktop">
                    <ul>
                        <li>
                            <a <?=($pagina == 'home') ? 'class="active"' : ''; ?>
                                href="<?=site_url(); ?>">Home
                            </a>
                        </li>
                        <li>
                            <a <?=($pagina == 'empresa') ? 'class="active"' : ''; ?>
                                href="<?=site_url('empresa'); ?>">Empresa
                            </a>
                        </li>
                        <li>
                            <a <?=($pagina == 'produtos') ? 'class="active"' : ''; ?>
                                href="<?=site_url('produtos'); ?>">Produtos
                            </a>
                        </li>
                        <li>
                            <a <?=($pagina == 'cases') ? 'class="active"' : ''; ?>
                                href="<?=site_url('cases'); ?>">Cases
                            </a>
                        </li>
                        <li>
                            <a <?=($pagina == 'artigos') ? 'class="active"' : ''; ?>
                                href="<?=site_url('artigos'); ?>">Artigos
                            </a>
                        </li>
                        <li>
                            <a <?=($pagina == 'showroom') ? 'class="active"' : ''; ?>
                                href="<?=site_url('localizacao'); ?>">Localização
                            </a>
                        </li>
                        <li>
                            <a <?=($pagina == 'contato') ? 'class="active"' : ''; ?>
                                href="<?=site_url('contato'); ?>">Contato
                            </a>
                        </li>
                    </ul>
                </nav>
                <a href="<?=site_url('contato'); ?>" class="call-to-action">
                    <p class="telefone"><?php echo Modules::run('showrooms/telefone') ?></p>
                    <p class="chamada">solicite uma visita »</p>
                </a>
                <button id="mobile-toggle" type="button" role="button">
                    <span>MENU</span>
                </button>
                <div class="clearfix"></div>
                <div class="sociais-busca">
                    <ul>
                        <?php echo Modules::run('showrooms/header') ?>
                        <li>
                            <div class="busca-form">
                                <?php echo form_open('busca/lista', 'id="busca-form"') ?>
                                <?php echo form_input(array('name'=>'query', 'class'=>'query')) ?>
                                <?php echo form_submit('', '') ?>
                                <?php echo form_close() ?>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <div class="header-shadow"></div>
        <div id="main" class="<?php echo $pagina; ?>">
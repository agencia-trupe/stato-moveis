            </div>
            <div class="clearfix"></div>
        <? if($pagina == 'home'): ?></div><? endif; ?>
        <footer>
            <div class="clearfix"></div>
            <div class="interna">
                <div class="rodape-wrapper">
                    <?php echo Modules::run('showrooms/parcial') ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
         <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
        <script src="<?=base_url('assets/prettyphoto/js/jquery.prettyPhoto.js'); ?>"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <?php if ($pagina == 'home'): ?>
        <script>
            runSlider();
        </script>
        <?php endif; ?>
        <script id="produto_template" type="text/html">
            <a class="produto-lista-wrapper {{categoria}}" href="<?php echo site_url('produtos/detalhe/{{produto_id}}/{{offset}}') ?>">
                <div class="produto-thumb-wrapper">
                    <img src="<?php echo base_url('assets/img/produtos/lista/{{imagem}}') ?>" alt="{{titulo}}">
                </div>
                <div class="produto-texto-wrapper">
                    <h2 class="produto-titulo">{{titulo}}</h2>
                    <p class="produto-resumo">{{resumo}}</p>
                </div>
            </a>
        </script>
        <script>
            var _gaq=[['_setAccount','UA-35751088-1'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
</html>

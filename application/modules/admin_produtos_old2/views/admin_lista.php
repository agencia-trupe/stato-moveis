<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="span6">
            <legend>Produtos <?php echo anchor('painel/produtos/cadastra', 'Novo', 'class="btn btn-info btn-mini"'); ?></legend>
        </div>
    </div>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Categoria</th><th>Título</th><th>Ações</th>
            </tr>
        </thead>
        <?php if ($produtos): ?>
            <?php foreach ($produtos as $produto): ?>
                <tr>
                    <td><?php echo $produto->categoria->titulo ?></td>
                    <td><?php echo $produto->titulo ?></td>
                    <td>
                        <a class="btn btn-info btn-mini" href="<?php echo site_url('painel/produtos/editar/' . $produto->id) ?>">Editar</a>
                        <a class="btn btn-danger btn-mini" href="<?php echo site_url('painel/produtos/apagar/' .$produto->id) ?>">Apagar</a>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php endif ?>
    </table>
</div><!--/span-->
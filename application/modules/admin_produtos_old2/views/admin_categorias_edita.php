<div class="row-fluid">
    <div class="span9">
        <?php if($this->session->flashdata('error') != NULL): ?>
        <div class="alert alert-error">
            <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php endif; ?>
    <?php if(isset($error)): ?>
        <div class="alert alert-error">
            <?php echo $error['error']; ?>
        </div>
    <?php endif; ?>
    <legend><?php echo ( $acao == 'editar' ) ? 'Editar' : 'Cadastrar'; ?> Categoria</legend>
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'painel/produtos/categorias/processa';
                    break;
                
                default:
                    $action = 'painel/produtos/categorias/processa_cadastro';
                    break;
            }
    ?>
    <?php echo form_open($action); ?>
    <?php if($acao == 'editar'): ?>
    <input type="hidden" name="id" value="<?php echo $categoria->id; ?>" class="id" />
    <?php endif; ?>
    <!--Título-->
    <?php echo form_label('Título'); ?>
    <?php echo form_input(array(
        'name' => 'titulo',
        'value' => set_value('titulo', ( $acao == 'editar' ) ? $categoria->titulo : '')
    )); ?>
    <?php echo form_error('titulo'); ?>
    <!--fim de Título-->
    <div class="clearfix"></div>
    <?php echo form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?php echo anchor('painel/produtos/categorias', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?php echo form_close(); ?>
    <div class="clearfix"></div>


    <?php if ($acao == 'editar'): ?>

    <!--Subcategorias-->
    <legend>Subcategorias</legend>
    <!--Adicionar Subcategoria-->
    <?php echo form_input(array('name'=>'subcategoria', 'class'=>'subcategoria', 'placeholder'=>'Subcategoria')) ?>
    <a href="#" class="add-subcategoria btn btn-info btn-small">Adicionar</a>
    <!--fim de adicionar Subcategoria-->
    <!--Listar subcategorias-->
    <div class="subcategorias-wrapper">
        <!--Alerta de Subcategoria-->
        <div class="row-fluid">
            <div class="alert alert-info span6">
                Clique na subcategoria para editar.
            </div>
        </div>
        <!--fim de Alerta de Subcategoria-->
        <ul class="subcategorias-list">
            <?php foreach ($categoria->subcategoria as $subcategoria): ?>
                <li class="subcategoria-item">
                    <a href="#" class="edit-subcategoria"><?php echo $subcategoria->titulo ?></a>
                    <!--Editar Subcategoria-->
                    <div class="editar-subcategoria-wrapper">
                        <?php echo form_input(array('name'=>'subcategoria-edit', 'class'=>'input-subcategoria', 'value'=>$subcategoria->titulo)) ?>
                        <input type="hidden" name="subcategoria_id" value="<?php echo $subcategoria->id ?>" class="subcategoria_id">
                        <a href="#" class="update-subcategoria btn btn-info btn-small">Salvar</a>
                        <a href="#" class="delete-subcategoria btn btn-danger btn-small">Apagar</a>  
                        <a href="#" class="cancel-edit-subcategoria btn btn-warning btn-small">Cancelar</a>
                        <div class="clearfix"></div>
                        <p class="subcategoria-message"></p>
                    </div>
                    <!--fim de Editar Subcategoria-->
                </li>
            <?php endforeach ?>
        </ul>
    </div>
    <!--fim de Listar Subcategorias-->
    <!--fim de Subcategorias-->
    <?php endif; ?>
    
    </div>
</div>
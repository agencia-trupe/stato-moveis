<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?>
    <div class="span9">
    <legend>Selecione um período para gerar a lista de emails</legend>
    <?php echo form_open('painel/cadastros/seleciona'); ?>
    <div class="row-fluid">
        <div class="control-group">
            <label class="control-label" for="data-de">De</label>
            <div class="controls">
              <?php echo form_input('data-de', set_value('data-de'), 'id="from"'); ?>
              <span class="help-inline"><?php echo form_error('data-de'); ?></span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="data-para">Até</label>
            <div class="controls">
              <?php echo form_input('data-para', set_value('data-para'), 'id="to"'); ?>
              <span class="help-inline"><?php echo form_error('data-para'); ?></span>
            </div>
        </div>
        <?php echo form_submit('submit', 'Gerar arquivo .csv', 'class="btn btn-primary"'); ?>
    </div>
</div><!--/span-->
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_cadastros extends MX_Controller
{
    function index()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
            $this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Meta Talentos - Painel de Controle';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            $data['title'] = 'Meta Talentos - Newsletter';
            $data['module'] = 'cadastros';
            $data['main_content'] = 'admin_cadastros/newsletters_view';
            $this->load->view('includes/template', $data);
        }
    }

    function seleciona()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
            $this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Meta Talentos - Painel de Controle';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            $config = array(
                array(
                    'field' => 'data-de',
                    'label' => 'de',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'data-para',
                    'label' => 'até',
                    'rules' => 'required',
                ),
            );
            $this->load->library('form_validation');
            $this->form_validation->set_rules($config);
            $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');

            if($this->form_validation->run() == FALSE ){
                 $data['title'] = 'Meta Talentos - Newsletter';
                $data['module'] = 'cadastros';
                $data['main_content'] = 'newsletters_view';
                $this->load->view('includes/template', $data);
            }
            else
            {
                $from = strtotime(str_replace('/', '-', $this->input->post('data-de')));
                $to = strtotime(str_replace('/', '-', $this->input->post('data-para')));

                $query = $this->db->query("SELECT `newsletters_nome` 
                    AS `nome`,  `newsletters_email` AS `email`
                    FROM `cadastros` WHERE `newsletters_data_cadastro` > $from AND
                     `newsletters_data_cadastro` < $to");
                if($query->num_rows() > 0)
                {
                    $this->load->helper('csv');
                    echo query_to_csv($query, TRUE, 'newsletter.csv');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Nenhum registro foi
                        encontrado para a data solicitada');
                                redirect('painel/newsletter');
                }
            }

        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
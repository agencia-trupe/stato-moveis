<?php

class Item extends DataMapper {
    
    var $table = 'itens';
    var $has_many = array("turma");
    
     public function __construct()
    {
        // model constructor
        parent::__construct();
    }
    
    function get_item($slug)
    {
        $item = new Item();
        $item->where('slug', $slug);
        $item->limit(1);
        $item->get();
        
        if($item->exists())
        {
            return $item;
        }
        else
        {
            return FALSE;
        }
    }
    
    function get_item_by_id($id)
    {
        $item = new Item();
        $item->where('id', $id);
        $item->limit(1);
        $item->get();
        
        if($item->exists())
        {
            return $item;
        }
        else
        {
            return FALSE;
        }
    }
    
    
    
    function get_id($slug)
    {
        $i = new Item();
        $i->where('slug', $slug);
        $i->limit(1);
        $i->get();
        
        return $i->id;
    }
    
    /*function get_turmas($id)
    {
        $i = new Item();
        $i->where('id', $id);
        $i->get();
        
        $i->turma->get_iterated();
        
        $arr = array();
        foreach($i->turma->all as $turmas)
        {
            $arr[] = $turmas;
        }
        
        return $arr;
        
    } */
    
    function get_turmas($id)
    {
        $item = new Turma();
        $item->where('item_id', $id);
        $item->get();
        
        $arr = array();
        foreach($item->all as $turma)
        {
            $arr[] = $turma;
        }
        
        return $arr;
    }
    
    
}

/* End of file employee.php */
/* Location: ./application/moules/atleta/models/atleta_model.php */
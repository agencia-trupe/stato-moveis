<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_empresa extends MX_Controller
{
    function index()
    {
        $this->lista();
    }
    /**
     *Lista os campos editáveis da página Empresa
     *
     * @return [type] [description]
     */
    function lista()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                $this->load->library('table');
                $this->load->model('empresas/empresa');
                $data['result'] = $this->empresa->
                get_empresa();

                $data['title'] = 'Stato Móveis - A empresa';
                $data['module'] = 'empresa';
                $data['main_content'] = 'e_lista_view';
                $this->load->view('includes/template', $data);
            }
            else
            {
                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                Você precisa ser 
                                                administrador para realizar essa ação');
                redirect();

            } 
        }
    }
        /**
         * Mostra a página de edição de um box cujo id foi passado como 
         * parâmetro.
         *
         * @param  [int] $id [description]
         * @return [mixed]     [description]
         */
        function editar($campo)
        {
            /**
             * Verifica se o usuário está logado para então prosseguir ou não.
             */
            if (!$this->tank_auth->is_logged_in())
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Stato Móveis - Painel de Controle';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                //Verifica se o usuário tem nível de acesso permitido
                if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
                {
                    $campo = $this->uri->segment(4);
                    if(!$campo)
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                        redirect('painel/empresas');
                    }
                    else
                    {
                        $this->load->model('empresas/empresa');
                        $data['module'] = 'empresa';
                        $data['title'] = 'Stato Móveis &middot; Empresa &middot; Editar';
                        $data['empresa'] = $this->empresa->get_empresa();
                        $data['acao'] = 'editar';
                        $data['campo'] = $campo;
                        $data['main_content'] = 'e_cadastra_view';
                        $this->load->view('includes/template', $data);
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'Erro de permissão. 
                                                        Você precisa ser administrador para realizar essa ação');
                    redirect();
                }
            }
        }

        function atualiza()
        {
            if (!$this->tank_auth->is_logged_in()) 
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Stato Móveis - Painel de Controle';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
                {
                    $config = array(
                        array(
                            'field' => 'texto',
                            'label' => 'texto',
                            'rules' => 'required',
                        ),
                    );
                    $this->load->library('form_validation');
                    $this->form_validation->set_rules($config);
                    $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');
                    $this->load->model('empresas/empresa');

                    if($this->form_validation->run() == FALSE )
                    {
                        $data['module'] = 'empresa';
                        $data['title'] = 'Stato Móveis &middot; Empresa &middot; Editar';
                        $data['empresa'] = $this->empresa->get_empresa();
                        $data['acao'] = 'editar';
                        $data['campo'] = $this->input->post('campo');
                        $data['main_content'] = 'e_cadastra_view';
                        $this->load->view('includes/template', $data);
                    }
                    else
                    {       //prepara o array com os dados para enviar ao model
                            $dados = array(
                                    'campo' => $this->input->post('campo'),
                                    'texto' => $this->input->post('texto'),
                                );

                                if( ! $this->empresa->atualiza($dados))
                                {
                                    $this->session->set_flashdata('error', 'A ação não pode ser
                                    realizada, tente novamente ou entre em contato com o suporte');
                                    redirect('painel/empresa/atualiza/' . $this->input->post('campo'));
                                }
                                else
                                {
                                    $this->session->set_flashdata('success', 'Registro alterado
                                    com sucesso!');
                                    redirect('painel/empresa/');
                                }
                    }

                  }
                  else
                  {
                  Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                  $this->session->set_flashdata('error', 'Erro de permissão. 
                                                        Você precisa ser administrador para realizar essa ação');
                  redirect();

                  }
            }
        }

        function apaga($id)
        {
            if (!$this->tank_auth->is_logged_in())
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Stato Móveis - Painel de Controle';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
                {
                    $id = $this->uri->segment(4);
                    if(!$id)
                    {
                    $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                    redirect('painel/box/lista');
                    }
                    else
                    {
                        $this->load->model('boxs/box');
                        if($this->box->delete_box($id))
                        {
                            $this->session->set_flashdata('success', 'Registro apagado
                            com sucesso');
                             redirect('painel/box/lista');
                        }
                         else
                        {
                            $this->session->set_flashdata('error', 'A ação não pode ser
                             realizada, tente novamente ou entre em contado com o suporte');
                            redirect('painel/box/lista');
                        }
                    }
                }
                else
                {
                    Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                    $this->session->set_flashdata('error', 'Erro de permissão. 
                                                        Você precisa ser administrador para realizar essa ação');
                    redirect();

                }
            }

        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
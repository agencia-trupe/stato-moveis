<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="span6">
            <legend>Empresa</legend>
        </div>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="span2">Campo</th><th>Detalhes</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>A Empresa</td>
                <td><?=$result->descricao; ?></td>
                <td><a  href="<?=site_url( 'painel/empresa/editar/descricao' ); ?>"
                        class="btn btn-warning btn-small">editar</a></td>
            </tr>
            <tr>
                <td>Missão</td>
                <td><?=$result->missao; ?></td>
                <td><a  href="<?=site_url( 'painel/empresa/editar/missao' ); ?>"
                        class="btn btn-warning btn-small">editar</a></td>
            </tr>
            <tr>
                <td>Visão</td>
                <td><?=$result->visao; ?></td>
                <td><a  href="<?=site_url( 'painel/empresa/editar/visao' ); ?>"
                        class="btn btn-warning btn-small">editar</a></td>
            </tr>
            <tr>
                <td>Valores</td>
                <td><?=$result->valores; ?></td>
                <td><a  href="<?=site_url( 'painel/empresa/editar/valores' ); ?>"
                        class="btn btn-warning btn-small">editar</a></td>
            </tr>
        </tbody>
    </table>
        

</div><!--/span-->
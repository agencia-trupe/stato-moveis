<?php
class Home extends MX_Controller
{
    var     $data;

    public function __construct()
    {
        parent::__construct();
        $seo['title'] = 'Stato Móveis &middot; Home';
        $seo['description'] = 'Stato Móveis, especializada em arquitetura de 
                                  interiores';
        $this->load->library('seo', $seo);
        $this->data['pagina'] = 'home';
        $this->load->model('home/slide');
    }

    public function index()
    {
        $this->data['slides'] = $this->slide->get_all('home');
        $this->data['conteudo'] = 'home/index';
        $this->load->view('layout/template', $this->data);
    }

    public function insere()
    {
        $dados = array();
        $dados['titulo'] = 'Projeto 01';
        $dados['link']  = 'http://google.com';
        $dados['imagem'] = 'slide_home.jpg';
        $insere = $this->slide->insert($dados);
        if($insere)
        {
            echo 'Ok!';
        }
        else
        {
            echo 'Nooooooooo!';
        }
    }

    public function serve_slides()
    {
        $slides = $this->slide->get_all('home');
        foreach($slides as $slide)
        {
            $result[] = $slide->imagem;
        }
        $result = json_encode($result);
        echo $result;
    }

}
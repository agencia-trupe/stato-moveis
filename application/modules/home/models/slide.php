<?php
class Slide extends Datamapper
{
    var $table = 'slides';

    function get_all($local)
    {
        $s = new Slide();
        $s->where('local', $local);
        $s->order_by('ordem','ASC');
        $s->get();
        $arr = array();
        foreach ($s->all as $slide)
        {
            $arr[] = $slide;
        }
        if(sizeof($arr))
        {
            return $arr;
        }
        return NULL;
    }

    function insert($dados)
    {
        $slide = new Slide();
        foreach ($dados as $key => $value)
        {
            $slide->$key = $value;
        }
        $slide->created = time();
        $insert = $slide->save();
        if($insert)
        {
            return TRUE;
        }
        return FALSE;
    }
}
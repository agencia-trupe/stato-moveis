<div class="span9">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <legend>Slides <a href="<?=site_url('painel/slideshow/cadastra'); ?>" class="btn btn-action">Novo Slide</a></legend>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Ordem</th><th class="span3">Imagem</th><th>Ações</th>
            </tr>
            <tbody>
                <?php foreach ($slides as $slide): ?>
                <tr>
                    <td><?=$slide->ordem; ?></td>
                    <td><img width="100%" height="100%" src="<?=base_url(); ?>assets/img/slides/<?=$slide->imagem; ?>" alt=""></td>
                    <td><a href="<?=site_url('painel/slideshow/editar/' . $slide->id); ?>" class="btn btn-info btn-mini">Editar</a>
                        <a href="<?=site_url('painel/slideshow/apaga/' . $slide->id); ?>" class="btn btn-danger btn-mini">Apagar</a>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </thead>
    </table>
</div>
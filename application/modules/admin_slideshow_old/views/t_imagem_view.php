<div class="span9">
    <?php if(isset($item)): ?>
    <div id="curso">
        <header>
            <h3>Curso</h3>
            <h1><?php echo $item->titulo; ?></h1>
                
        </header>
        <div class="clearfix"></div>
        <div class="row-fluid">
            <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success">
                <p>
                    <?php echo $this->session->flashdata('success'); ?>
                </p>
            </div>
            <?php endif; ?>
            <?php if($this->session->flashdata('error')): ?>
            <div class="alert alert-error">
                <p>
                    <?php echo $this->session->flashdata('error'); ?>
                </p>
            </div>
            <?php endif; ?>
        <div class="alert alert-info">
            <p>
                Para adicionar uma imagem relacionada ao curso, utilize o formulário abaixo.
            </p>
        </div>
        </div>
        
        <div id="curso-content">
            <?php echo form_open_multipart('treinamentos/salva_imagem');?>

            <?php echo form_upload('itemdata'); ?>
            <?php echo form_hidden('id', $item->id); ?>
            <?php echo form_hidden('slug', $item->slug); ?>

            <br /><br />

            <input type="submit" value="Enviar foto" class="btn btn-info" />

            </form>
        </div>
         
    </div>
    <?php else: ?>
    <p>Treinamento não encontrado</p>
    <?php endif; ?>
    
</div>						
                <div class="clearfix"></div>

		</div>
		

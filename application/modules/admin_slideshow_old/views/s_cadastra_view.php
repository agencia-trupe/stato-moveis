<div class="span9">
<?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
<legend><?php echo $acao == 'editar' ? 'Editar slideshow' : 'Novo slideshow' ;?></legend>
    <?php echo isset($error) ? $error['error'] : ''; ?>
    <?php
    echo form_open_multipart(($acao == 'editar') ? 'painel/slideshow/atualiza' : 'painel/slideshow/salva', 'class="well"'); 
    ?>

    <div class="row-fluid">
      <?php if($acao == 'edita')
      {
        echo form_hidden('acao', 'edita');
      }
      else{
        echo form_hidden('acao', 'cadastra');
      }
      ?>  
      </div>
    <?php if($acao == 'editar') : ?>
    <?php echo
    form_hidden('id', set_value('id', $slide->id));
    ?>
  <?php endif; ?>
  <div class="row-fluid">
      <div class="clearfix"></div>
      <div class="control-group">
            <label class="control-label" for="ordem">Ordem</label>
            <div class="controls">
              <?php echo form_input('ordem', set_value('ordem', $acao == 'editar' ? $slide->ordem : ''), 'class="span1"'); ?>
              <span class="help-inline"><?php echo form_error('ordem'); ?></span>
            </div>
      </div>
     <?php if($acao == 'editar'): ?>
     <?php if($slide->imagem):?>
     <img src="<?php echo base_url(); ?>assets/img/slides/<?php echo $slide->imagem; ?>" alt="<?php echo $slide->titulo; ?>" >
     <?php endif; ?>
     <?php endif; ?>
     <div class="control-group">
            <label class="control-label" for="imagem"><?php echo ($acao == 'editar' ? 'Adicionar / Alterar Imagem' : 'Imagem'); ?></label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem'), 'id="datepicker"'); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
  </div>
  <?php echo form_submit('submit', ($acao == 'editar') ? 'Salvar' : 'Cadastrar', 'class="btn btn-primary"'); ?>
  <?php echo form_close(); ?> 
</div>
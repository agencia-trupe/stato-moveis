<?php
class Produtos extends MX_Controller
{
    var   $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('produtos/produto');
        $this->load->model('produtos/categoria', 'categoria_model');
        $this->load->model('produtos/subcategoria');
        $this->load->model('produtos/foto');
    }

    public function index()
    {
        $this->categoria('');
    }

    public function categoria($slug)
    {
        if($slug == NULL)
        {
            $categorias = $this->categoria_model->fetch_all();
            $slug = $categorias[0]->slug;
        }
        //Retorno de dados
        $categoria = $this->categoria_model->get_by_slug($slug);
        //Seo
        $seo['title'] = 'Stato &middot; Produtos &middot; ' . $categoria->titulo;
        $seo['description'] = 'Stato Móveis ' . $categoria->titulo;
        $this->load->library('seo', $seo);
        //View
        
        $this->data['pagina'] = 'produtos';
        $this->data['categoria'] = $categoria;
        $this->data['categorias'] = $this->categoria_model->fetch_all();
        $this->data['conteudo'] = 'produtos/categoria';
        $this->load->view('layout/template', $this->data);
    }

    public function parcial($subcategoria_id)
    {
    
        $subcategoria = $this->subcategoria->get_conteudo($subcategoria_id);

        $categoria = $subcategoria->categoria->slug;

        $pagination = $this->produto->get_pagination($per_page = 6, $subcategoria_id, $categoria);

        $produtos = $this->produto->fetch_limit($subcategoria_id, $per_page, $this->uri->segment(4));

        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
        {
            foreach($produtos as $produto)
            {
                $produto_result[$produto->id]['imagem'] = $produto->imagem;
                $produto_result[$produto->id]['titulo'] = $produto->titulo;
                $produto_result[$produto->id]['resumo'] = $produto->resumo;
            }
            $result['produtos'] = $produto_result;
            $result['pagination'] = $pagination;
            echo json_encode($result);
        }
        else
        {
            $this->data['pagination'] = $pagination;
            $this->data['produtos'] = $produtos;
            $this->load->view('produtos/parcial', $this->data); 
        }
               
    }

    public function detalhe($produto_id)
    {
        //Retorno de dados
        $fotos = $this->foto->get_by_produto($produto_id);
        $produto = $this->produto->get_conteudo($produto_id);
        $this->data['image_count'] = $this->db->where('produto_id', $produto_id)->get('fotos')->num_rows();
        //Seo
        $seo['title'] = 'Stato &middot; Produtos';
        $seo['description'] = 'Stato Móveis';
        $this->load->library('seo', $seo);
        //View
        $this->data['pagina'] = 'produtos';
        $this->data['produto'] = $produto;
        $this->data['fotos'] = $fotos;
        $this->data['categorias'] = $this->categoria_model->fetch_all();
        $this->data['conteudo'] = 'produtos/detalhe';
        $this->load->view('layout/template', $this->data);
        
    }
}
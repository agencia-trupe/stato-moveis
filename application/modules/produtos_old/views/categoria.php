<div class="conteudo produtos">
	<div class="clearfix"></div>
	<div class="inner">
		<div class="sidebar">
			<h1>Produtos</h1>
			<nav>
				<ul>
					<?php foreach ($categorias as $cat): ?>
						<li>
							<a <?php if ($cat->id == $categoria->id) echo 'class="active"' ?>href="<?php echo site_url('produtos/categoria/' . $cat->slug) ?>">
								<span><?php echo $cat->titulo ?></span>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
			</nav>
		</div>
		<div id="tabs">
			<ul>
				<?php foreach ($categoria->subcategoria as $subcategoria): ?>
					<li>
						<a href="#tabs-<?php echo $subcategoria->id ?>"><?php echo $subcategoria->titulo ?></a>
					</li>
				<?php endforeach ?>
			</ul>
			<div class="clearfix"></div>
			<div class="clearfix">
				<?php foreach ($categoria->subcategoria as $subcategoria): ?>
					<div id="tabs-<?php echo $subcategoria->id ?>">
						<?php echo Modules::run('produtos/parcial', $subcategoria->id, $categoria->id) ?>
					</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
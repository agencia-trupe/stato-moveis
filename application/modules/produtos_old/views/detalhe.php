<div class="conteudo produtos">
	<div class="clearfix"></div>
	<div class="inner">
		<div class="sidebar">
			<h1>Produtos</h1>
			<nav>
				<ul>
					<?php foreach ($categorias as $cat): ?>
						<li>
							<a <?php if ($cat->id == $produto->categoria->id) echo 'class="active"' ?> href="<?php echo site_url('produtos/categoria/' . $cat->slug) ?>">
								<span><?php echo $cat->titulo ?></span>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
			</nav>
		</div>
		<div class="produto-wrapper">
			<h2><?php echo $produto->titulo ?></h2>
			<div class="produto-image-wrapper">
				<div class="produto-slider">
					<div class="produto-slide">
						<img src="<?php echo base_url('assets/img/produtos/ampliadas/' . $produto->imagem) ?>" alt="$produto->titulo">
					</div>
					<?php if(isset($fotos) && $fotos != NULL): ?>
					<?php foreach ($fotos as $foto): ?>
						<div class="produto-slide">
							<img src="<?php echo base_url('assets/img/produtos/ampliadas/' . $foto->imagem) ?>" alt="$produto->titulo">
						</div>
					<?php endforeach ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="produto-thumbs">
					<a data-slide-index="0" href="#"><img src="<?php echo base_url('assets/img/produtos/thumbs/' . $produto->imagem) ?>" alt="<?php echo $produto->titulo ?>"></a></li>
					<?php if(isset($fotos) && $fotos != NULL): ?>
					<?php for ($i=1; $i < $image_count + 1; $i++): ?>
						<a  data-slide-index="<?php echo $i ?>" href="#"><img src="<?php echo base_url('assets/img/produtos/thumbs/' . $fotos[$i - 1]->imagem) ?>" alt="<?php echo $produto->titulo ?>"></a></li>
					<?php endfor ?>
					<?php endif; ?>
				<div class="clearfix"></div>
			</div>
			<div class="case-description">
				<?php echo $produto->descricao ?>
			</div>
			<div class="voltar-wrapper">
				<a href="<?php echo site_url('produtos') ?>" class="produto-voltar">voltar</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
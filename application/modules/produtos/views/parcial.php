<?php if($produtos): ?>
	<div id="sub-<?php echo $produtos[0]->subcategoria_id ?>" class="produtos-wrapper">
		<?php foreach ($produtos as $produto): ?>
			<a class="produto-lista-wrapper <?php echo $produto->categoria->slug ?>" href="<?php echo site_url('produtos/detalhe/' . $produto->id . '/' . $offset ) ?>">
				<div class="produto-thumb-wrapper">
					<img src="<?php echo base_url('assets/img/produtos/lista/' . $produto->imagem) ?>" alt="">
				</div>
				<div class="produto-texto-wrapper">
					<h2 class="produto-titulo"><?php echo $produto->titulo ?></h2>
					<p class="produto-resumo"><?php echo $produto->resumo ?></p>
				</div>
			</a>
		<?php endforeach ?>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
	<div class="pagination-wrapper pagination-<?php echo $produtos[0]->subcategoria_id ?>">
		<?php print_r($pagination) ?>
	</div>
<?php endif; ?>
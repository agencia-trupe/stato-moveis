<div class="conteudo produtos">
	<div class="clearfix"></div>
	<div class="inner">
		<div class="sidebar">
			<h1>Produtos</h1>
			<nav>
				<ul>
					<?php foreach ($categorias as $cat): ?>
						<li>
							<a <?php if ($cat->id == $categoria->id) echo 'class="active"' ?>href="<?php echo site_url('produtos/categoria/' . $cat->slug) ?>">
								<span><?php echo $cat->titulo ?></span>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
			</nav>
		</div>
		<div id="tabs" class="bbq">
			<ul>
				<?php foreach ($subcategorias as $subcategoria): ?>
					<li>
						<a href="#subcategoria-<?php echo $subcategoria->id ?>"><?php echo $subcategoria->titulo ?></a>
					</li>
				<?php endforeach ?>
			</ul>
			<div class="clearfix"></div>
			<div class="clearfix">
				<?php foreach ($categoria->subcategoria as $subcategoria): ?>
					<div id="subcategoria-<?php echo $subcategoria->id ?>">
						<?php echo Modules::run('produtos/parcial', $subcategoria->id, ($active == $subcategoria->id) ? $offset : '') ?>
					</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
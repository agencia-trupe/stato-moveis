<?php 

Class Produto extends Datamapper
{
	var $table = 'produtos';
    var $has_one = array('categoria', 'subcategoria');
    var $has_many = array('foto');
    var $auto_populate_has_one = TRUE;
    var $auto_populate_has_many = TRUE;

	public function fetch_limit($subcategoria_id, $limit, $offset)
	{
		$p = new Produto();
		$p->where('subcategoria_id', $subcategoria_id)
            ->order_by('ordem','ASC')
            ->get($limit, $offset);

		$result_set = array();
		foreach($p->all as $produto)
		{
			$result_set[] = $produto;
		}

		if( ! count($result_set) ) return FALSE;

		return $result_set;
	}

    public function fetch_all($categoria_id, $order = NULL)
    {
        $p = new Produto();
        if($order)
        {
            $p->order_by('ordem','ASC');
        }
        $p->where('categoria_id', $categoria_id)->get();

        $result_set = array();
        foreach($p->all as $produto)
        {
            $result_set[] = $produto;
        }

        if( ! count($result_set) ) return FALSE;

        return $result_set;
    }

    public function get_conteudo($id)
    {
        $p = new Produto();
        $p->where('id', $id)->get(1);

        if( ! $p->exists() ) return FALSE;
        return $p;
    }

    public function get_like($name)
    {
        $p = new Produto();
        $p->like('titulo', $name)
          ->or_like('descricao', $name)
          ->or_like('resumo', $name)
          ->get();

        $result_set = array();
        foreach($p->all as $produto)
        {
            $result_set[] = $produto;
        }
        if( ! count($result_set) ) return FALSE;
        return $result_set;
    }

	public function get_pagination($per_page, $subcategoria_id, $categoria_id)
	{
		$ci =& get_instance();

		$num_rows = $ci->db->where('subcategoria_id', $subcategoria_id)
                             ->get('produtos')->num_rows();

        $num_links = $num_rows / $per_page;

        //configura o offset para o link anterior
        $anterior = $ci->uri->segment(4) - $per_page;
        $anterior = ($anterior < 0) ? NULL : $anterior;

        //link anterior
        $pagination = '<ul><li class="prev">' . anchor('produtos/parcial/' . $subcategoria_id . '/' . $anterior, 'Anterior',array('class'=>'navigation-link', 'data-subcategoria' => $subcategoria_id, 'data-categoria' => $categoria_id)) . '</li>';

        for ($i=0; $i < $num_links; $i++) 
        { 
            $offset = $per_page * $i;

            $class = ($offset == $ci->uri->segment(4)) ? 'navigation-link active' 
            											: 'navigation-link';
            if($offset == null)
            {
                $pagination .= '<li>' . anchor('produtos/parcial/'. $subcategoria_id .'/', '1', array('data-subcategoria' => $subcategoria_id, 'data-categoria' => $categoria_id, 'data-offset' => '', 'class' => $class)) . '</li>';
            }
            else
            {
                $pagination .= '<li>' . anchor('produtos/parcial/'. $subcategoria_id .'/' . $offset, $i + 1, array('data-subcategoria' => $subcategoria_id, 'data-categoria' => $categoria_id, 'data-offset' => $offset, 'class' => $class)) . '</li>';
            }

        }

        //configura o offset do próximo link
        $proximo = $ci->uri->segment(4) + $per_page;
        $proximo = ($proximo > $num_rows) ? $ci->uri->segment(4) : $proximo;

        $pagination .= '<li class="next">' . anchor('produtos/parcial/' . $subcategoria_id . '/' . $proximo, 'Próxima', array('class'=>'navigation-link', 'data-subcategoria' => $subcategoria_id, 'data-categoria' => $categoria_id)) . '</li></ul>';

        return $pagination;
	}

    function change($dados)
    {
        $produto = new Produto();
        $produto->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update = $produto->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    function insert($dados)
    {
        $produto = new Produto();
        foreach ($dados as $key => $value)
        {
            $produto->$key = $value;
        }
        $insert = $produto->save();
        if($insert)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function apaga($id)
    {
        $produto = new Produto();
        $produto->where('id', $id)->get(1);

        if( ! $produto->delete() ) return FALSE;

        return TRUE;
    }

    function ordena($dados)
    {
        $result = array();
        foreach($dados as $key => $value)
        {
            $produto = new Produto();
            $produto->where('id', $value);
            $update_data = array(
                'ordem' => $key
                );
            if($produto->update($update_data))
            {
                $result[] = $value;
            }
        }
        if(sizeof($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}
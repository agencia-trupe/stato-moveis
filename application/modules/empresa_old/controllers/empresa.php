<?php
class Empresa extends MX_Controller
{
    var   $data;

    public function index()
    {
        $seo['title'] = 'Stato &middot; Empresa';
        $seo['description'] = 'Stato Móveis';
        $this->load->library('seo', $seo);
        $this->data['pagina'] = 'empresa';
        $this->data['conteudo'] = 'empresa/index';
        $this->load->view('layout/template', $this->data);
    }
}
<div class="conteudo empresa">
	<div class="inner">

		<div class="texto">
		<div class="empresa">
			<h1>Empresa</h1>
			<p>
				A Stato está no mercado há 10 anos, oferecendo móveis corporativos
				e toda acessoria nos projetos de ambientação de seus clientes.
			</p>
			<p>
				Dentro de seu escopo de atuação, Stato prioriza identificar as 
				reais necessidades dos clientes, para oferecer soluções que agreguem o 
				benefício de tempo, investimento e qualificação do ambiente projetado.
			</p>
			<p>A Stato agradece sua visita e coloca-se a disposição para atende-lo.</p>
		</div>
		<div class="missao-visao">
			<div class="missao">
				<h2>Missão</h2>
				<p>
					Viabilizar parcerias oferecendo soluções que qualifiquem espaços para o bem estar das pessoas.
				</p>
			</div>
			<div class="visao">
				<h2>Visao</h2>
				<p>
					Fortalecer a marca como referência no segmento corporativo 
					por meio de parcerias na qualificação de espaços.
				</p>
			</div>
		</div>
	</div>
	<div class="img-wrapper">
		<img src="<?php echo base_url('assets/img/foto-empresa.jpg') ?>" alt="">
	</div>
	</div>
</div>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?php echo base_url(); ?>">Stato Móveis</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="dropdown">
                <a href="#" class="<?php echo ($module == 'slideshow') ? 'active' : ''; ?> dropdown-toggle" data-toggle="dropdown">
                  Slideshow
                  <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url('painel/slideshow/lista/home'); ?>">Home</a></li>
                  <li><a href="<?php echo site_url('painel/slideshow/lista/empresa'); ?>">Empresa</a></li>
                </ul>
              </li>
              <li class="<?php echo ($module == 'produtos') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('painel/produtos/categorias'); ?>">Produtos</a>
              </li>
              <li class="<?php echo ($module == 'cases') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('painel/cases'); ?>">Cases</a>
              </li>
              <li class="<?php echo ($module == 'noticias') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('painel/noticias'); ?>">Notícias</a>
              </li>
              <li class="<?php echo ($module == 'newsletters') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('painel/newsletters'); ?>">Newsletters (HTML)</a>
              </li>
              <li class="<?php echo ($module == 'cadastros') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('painel/cadastros'); ?>">Newsletters (Cadastros)</a>
              </li>
              <li class="<?php echo ($module == 'showroom') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('painel/showroom'); ?>">Showroom / Contatos</a>
              </li>
               <li class="<?php echo ($module == 'usuarios') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('auth'); ?>">Usuários</a>
              </li>
              <li>
                <div class="alert span6 top-alert">
                  <span class="alert-content"></span>
                  <a class="close" href="#">&times;</a>
                </div>
              </li>
              
            </ul>
            <ul class="nav pull-right">
                <li><?php echo anchor('logout', 'Sair'); ?></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row-fluid">
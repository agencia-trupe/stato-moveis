<?php
class Pagina extends Datamapper
{
    var $table = 'paginas';

    function get_all()
    {
        $pagina = new Pagina();
        $pagina->order_by('titulo', 'ASC')->get();
        $arr = array();
        foreach($pagina->all as $pagina)
        {
            $arr[] = $pagina;
        }
        if(sizeof($arr))
        {
            return $arr;
        }
        return FALSE;
    }
    function get_conteudo($identifier, $type)
    {
        $pagina = new Pagina();
        switch ($type) 
        {
            case 'id':
                $pagina->where('id', $identifier)->get();
                break;
            case 'slug':
                $pagina->where('slug', $identifier)->get();
                break;
        }
        if($pagina->exists()){
            return $pagina;
        }
        return NULL;
    }

    function insert($dados)
    {
        $pagina = new Pagina();
        foreach ($dados as $key => $value)
        {
            $pagina->$key = $value;
        }
        $pagina->updated = time();
        $insert = $pagina->save();
        if($insert)
        {
            return TRUE;
        }
        return FALSE;
    }
    function change($dados)
    {
        $pagina = new Pagina();
        $pagina->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update_data['updated'] = time();
        $update = $pagina->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    function change_by_slug($dados)
    {
        $pagina = new Pagina();
        $pagina->where('slug', $dados['slug']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update_data['updated'] = time();
        $update = $pagina->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }
}
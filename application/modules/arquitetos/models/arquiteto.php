<?php

class Arquiteto extends Datamapper
{
	var $table = 'arquitetos';

	public function fetch_all()
	{
		$a = new Arquiteto();
		$a->order_by('created', 'DESC')->get();

		$result = array();
		foreach($a->all as $arquiteto)
		{
			$result[] = $arquiteto;
		}
		if(count($result))
			return $result;
		return false;
	}

	public function insert($dados)
	{
		$arquiteto = new Arquiteto();

		foreach($dados as $key => $value)
		{
			$arquiteto->$key = $value;
		}

		if($arquiteto->save()) 
			return TRUE;
		return false;
	}

	public function get_conteudo($value, $key)
	{
		$arquiteto = new Arquiteto();
		$arquiteto->where($key, $value)->get(1);
		if( !$arquiteto->exists() )
			return false;
		return $arquiteto;
	}

	public function marcar($acao, $id)
	{
		$arquiteto = new Arquiteto();
		$arquiteto->where('id', $id);
		if($acao == 'lida')
		{
			if( !$arquiteto->update(array('lida'=>TRUE)) )
				throw new Exception("Erro ao marcar como lida", 1);
			return TRUE;
		}
		else
		{
			if( !$arquiteto->update(array('lida'=>FALSE)) )
				throw new Exception("Erro ao marcar como não lida", 1);
			return TRUE;
		}
	}

	public function apaga($id)
	{
		$arquiteto = new Arquiteto();
		$arquiteto->where('id', $id)->get(1);
		if( !$arquiteto->delete() )
			return FALSE;
		return TRUE;
	}
}
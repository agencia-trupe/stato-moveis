<div class="conteudo arquitetos">
	<div class="inner">
		<div class="clearfix"></div>
		<h1>Espaço do arquiteto</h1>
		<p>
			Se você está procurando um arquiteto, entre em contato com a Stato para 
			que possamos recomendar um profissional que melhor atenda ao seu projeto.
			 Se você for um arquiteto, preencha o cadastro abaixo para participar dos nossos
			 canais de divulgação, receber materiais atualizados e indicações para novos projetos.
		</p>
		<div class="clearfix"></div>
		<div class="statocad-form">
			<div class="statocad">
				<div class="statocad-conheca">
					<h2>Conheça o statocad</h2>
					<p>
						O statocad é uma ferramenta desenvolvida pela Stato para auxiliar arquitetos e profissionais de interiores no desenvolvimento de seus projetos.
						<br>
						O sistema é executável na plataforma Autocad, disponibilizando blocos dos mobiliários Stato em 2D e 3D, além de possuir uma série de atalhos e ferramentas que tornam a elaboração do projeto muito mais dinâmica.
					</p>
				</div>
				<div class="statocad-image-wrapper">
					<img src="<?php echo base_url('assets/img/computador.png') ?>" alt="Stato Móveis - Statocad">
				</div>
				<div class="statocad-suporte">
					<h3>suporte técnico</h3>
					<p>
						Caso você já tenha recebido seu CD do Statocad, mas está com dificuldades para utilizá-lo, entre em contato conosco através do telefone: (11) 3801-3801
					</p>
				</div>
			</div>
			<div class="form">
				<h2>Cadastre-se e solicite o Statocad</h2>
				<?php echo form_open('', 'class="statocad-form"') ?>
				<?php echo form_input( array('name'=>'nome', 'placeholder'=>'nome', 'class'=>'statocad_nome input4') ) ?>
				<?php echo form_input( array('name'=>'email', 'placeholder'=>'email', 'class'=>'statocad_email input4') ) ?>
				<?php echo form_input( array('name'=>'emrpesa', 'placeholder'=>'site da empresa/profissional', 'class'=>'statocad_empresa input4') ) ?>
				<?php echo form_input( array('name'=>'endereco', 'placeholder'=>'endereço', 'class'=>'statocad_endereco input3') ) ?>
				<?php echo form_input( array('name'=>'numero', 'placeholder'=>'número', 'class'=>'statocad_numero input1') ) ?>
				<div class="clearfix"></div>
				<?php echo form_input( array('name'=>'complemento', 'placeholder'=>'complemento', 'class'=>'statocad_complemento input3') ) ?>
				<?php echo form_input( array('name'=>'cep', 'placeholder'=>'cep', 'class'=>'statocad_cep terco input1') ) ?>
				<div class="clearfix"></div>
				<?php echo form_input( array('name'=>'cidade', 'placeholder'=>'cidade', 'class'=>'statocad_cidade input3') ) ?>
				<?php echo form_input( array('name'=>'estado', 'placeholder'=>'estado', 'class'=>'statocad_estado input1') ) ?>
				<div class="clearfix"></div>
				<?php echo form_input( array('name'=>'telefone', 'placeholder'=>'telefone', 'class'=>'statocad_telefone input4') ) ?>
				<?php echo form_input( array('name'=>'celular', 'placeholder'=>'celular', 'class'=>'statocad_celular input4') ) ?>
				<span class="legend input4">Solicitações:</span>
				<label><?php echo form_checkbox('statocad', '1', FALSE, 'class="ferramenta_statocad"')?>  <span>Solicitação da Ferramenta Statocad</span></label>
				<div class="clearfix"></div>
				<label><?php echo form_checkbox('catalogo_eletronico', '1', FALSE, 'class="catalogo_eletronico"')?>  <span>Catálogo Eletrônico</span></label>
				<div class="clearfix"></div>
				<label><?php echo form_checkbox('catalogo_impresso', '1', FALSE, 'class="catalogo_impresso"')?>  <span>Catálogo Impresso</span></label>
				<div class="clearfix"></div>
				<label><?php echo form_checkbox('agendar_visita', '1', FALSE, 'class="agendar_visita"')?>  <span>Agendar Visita na Loja</span></label>
				<br>
				<span class="legend input4">mensagem</span>
				<?php echo form_textarea( array('name'=>'mensagem', 'class'=>'statocad_mensagem') ) ?>
				<div class="clearfix"></div>
				<div class="clearfix"></div>
				<input type="submit" class="statocad_submit" value="enviar">
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
</div>
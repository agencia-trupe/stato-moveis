<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
       <div class="span11">
            <legend>
                Envio - Espaço do Arquiteto
                <a href="<?php echo site_url('painel/arquitetos/apagar/' . $arquiteto->id) ?>" class="btn btn-danger btn-mini msg-unread">apagar</a>
            </legend>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span11">
            <table class="table table-hover">
            <tr>
                <td class="span3"><b>Data</b></td>
                <td><?php echo date('d/m/y', $arquiteto->created) . ' às ' . date('H:i', $arquiteto->created) . 'h'?></td>
            </tr>
            <tr>
                <td><b>IP</b></td>
                <td><?php echo long2ip($arquiteto->ip) ?></td>
            </tr>
            <tr>
                <td><b>Nome</b></td>
                <td><?php echo $arquiteto->nome ?></td>
            </tr>
            <tr>
                <td><b>Email</b></td>
                <td><?php echo $arquiteto->email ?></td>
            </tr>
            <tr>
                <td><b>Site</b></td>
                <td><?php echo $arquiteto->url ?></td>
            </tr>
            <tr>
                <td><b>Endereço</b></td>
                <td><?php echo $arquiteto->endereco ?></td>
            </tr>
            <tr>
                <td><b>Número</b></td>
                <td><?php echo $arquiteto->numero ?></td>
            </tr>
            <tr>
                <td><b>Complemento</b></td>
                <td><?php echo $arquiteto->complemento ?></td>
            </tr>
            <tr>
                <td><b>CEP</b></td>
                <td><?php echo $arquiteto->cep ?></td>
            </tr>
            <tr>
                <td><b>Cidade</b></td>
                <td><?php echo $arquiteto->cidade ?></td>
            </tr>
            <tr>
                <td><b>Estado</b></td>
                <td><?php echo $arquiteto->estado ?></td>
            </tr>
            <tr>
                <td><b>Telefone</b></td>
                <td><?php echo $arquiteto->telefone ?></td>
            </tr>
            <tr>
                <td><b>Celular</b></td>
                <td><?php echo $arquiteto->celular ?></td>
            </tr>
            <tr>
                <td><b>Ferramenta Statocad</b></td>
               <td><?php echo ($arquiteto->statocad == 1) ? 'sim' : 'não' ?></td>
            </tr>
            <tr>
                <td><b>Catálogo eletrônico</b></td>
                <td><?php echo ($arquiteto->catalogo_eletronico == 1) ? 'sim' : 'não' ?></td>
            </tr>
            <tr>
                <td><b>Catálogo impresso</b></td>
                <td><?php echo ($arquiteto->catalogo_impresso == 1) ? 'sim' : 'não' ?></td>
            </tr>
            <tr>
                <td><b>Agendar visita</b></td>
                <td><?php echo ($arquiteto->agendar_visita == 1) ? 'sim' : 'não' ?></td>
            </tr>
            <tr>
                <td><b>Mensagem</b></td>
                <td><?php echo $arquiteto->mensagem ?></td>
            </tr>
    </table>
        </div>
    </div>
    
</div><!--/span-->
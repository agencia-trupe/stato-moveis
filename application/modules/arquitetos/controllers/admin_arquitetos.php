<?php
class Admin_arquitetos extends MX_Controller
{
    var $data;
    public function __construct()
    {
        parent::__construct();
        $this->data['module'] = 'arquitetos';
        $this->load->model('arquitetos/arquiteto');
    }
    public function index()
    {
        $this->lista();
    }

    public function lista()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['module'] = 'arquitetos';   
            $this->data['arquitetos'] = $this->arquiteto->fetch_all();
            $this->data['conteudo'] = 'arquitetos/admin_lista_arquitetos';
            $this->load->view('start/template', $this->data);
        }
    }

    public function detalhe($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if($this->input->post('unread'))
            {
                $this->data['arquiteto'] = $this->arquiteto->get_conteudo($this->input->post('id'), 'id');
                try 
                {
                    $this->arquiteto->marcar('nao_lida', $id);
                } 
                catch (Exception $e) 
                {
                    echo $e->getMessage() . "\n";
                }
            }
            else
            {
                $this->data['arquiteto'] = $this->arquiteto->get_conteudo($id, 'id');
                if($this->data['arquiteto']->lida == FALSE)
                {
                    try 
                    {
                        $this->arquiteto->marcar('lida', $id);
                    } 
                    catch (Exception $e) 
                    {
                        echo $e->getMessage() . "\n";
                    }
                }
            }
            $this->data['conteudo'] = 'arquitetos/admin_detalhe_arquitetos';
            $this->load->view('start/template', $this->data);
        }
    }


    public function apagar($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $apaga = $this->arquiteto->apaga($id);
            if($apaga)
            {
                $this->session->set_flashdata('success', 'Registro removido com sucesso');
                redirect('painel/arquitetos');
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível remover o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/arquitetos/');
            }
        }
    }
}
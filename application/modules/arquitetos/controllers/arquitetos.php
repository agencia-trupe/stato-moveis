<?php
class Arquitetos extends MX_Controller
{
    var   $data;

    public function index()
    {
        $seo['title'] = 'Stato &middot; Espaço do Arquiteto';
        $seo['description'] = 'Stato Móveis - Espaço do Arquiteto';
        $this->load->library('seo', $seo);
        $this->data['pagina'] = 'arquitetos';
        $this->data['conteudo'] = 'arquitetos/index';
        $this->load->view('layout/template', $this->data);
    }

    public function form_check()
    {
        $this->form_validation->set_error_delimiters('', '');
        if($this->form_validation->run('arquitetos') == FALSE) 
        {
                echo validation_errors();
        } 
        else 
        {
            try 
            {
                $this->_salva_envio($this->input->post());
            } 
            catch (Exception $e) 
            {
                echo $e->getMessage() . "\n";
            }
            $this->load->library('typography');
            $this->load->library('MY_PHPMailer');
            $mail = new PHPMailer();
            // Define os dados do servidor e tipo de conexão
            // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
            $mail->IsSMTP(); // Define que a mensagem será SMTP
            //$mail->Host = "localhost"; // Endereço do servidor SMTP (caso queira utilizar a autenticação, utilize o host smtp.seudomínio.com.br)
            $mail->SMTPAuth = true; // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
            $mail->Username = 'noreply@stato.com.br'; // Usuário do servidor SMTP (endereço de email)
            $mail->Password = 'n0replyst'; // Senha do servidor SMTP (senha do email usado)
            
            $mail->Host = 'smtp.stato.com.br';
            $mail->Port = '587'; 
            // Define o remetente
            // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
            $mail->From = "noreply@stato.com.br"; // Seu e-mail
            $mail->Sender = "noreply@stato.com.br"; // Seu e-mail
            $mail->FromName = $this->input->post('nome'); // Seu nome
             
            // Define os destinatário(s)
            // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
            $mail->AddAddress('contato@stato.com.br', 'Contato Stato');
            //$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
            $mail->AddBCC('nilton@trupe.net', 'Nilton - Trupe'); // Cópia Oculta
             
            // Define os dados técnicos da Mensagem
            // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
            $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
            $mail->CharSet = 'UTF-8'; // Charset da mensagem (opcional)
             
            // Define a mensagem (Texto e Assunto)
            // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
            $mail->Subject  = '=?UTF-8?B?'.base64_encode("Envio do Espaço do Arquiteto Stato Móveis").'?='; // Assunto da mensagem
            
            $data['dados'] = array(
                                'nome' => $this->input->post('nome'),
                                'email' => $this->input->post('email'),
                                'endereco' => $this->input->post('endereco'),
                                'numero' => $this->input->post('numero'),
                                'complemento' => $this->input->post('complemento'),
                                'cep' => $this->input->post('cep'),
                                'cidade' => $this->input->post('cidade'),
                                'estado' => $this->input->post('estado'),
                                'telefone' => $this->input->post('telefone'),
                                'celular' => $this->input->post('celular'),
                                'ferramenta_statocad' => $this->input->post('ferramenta_statocad'),
                                'catalogo_eletronico' => $this->input->post('catalogo_eletronico'),
                                'catalogo_impresso' => $this->input->post('catalogo_impresso'),
                                'agendar_visita' => $this->input->post('agendar_visita'),
                                'statocad_mensagem' => $this->typography->auto_typography($this->input->post('statocad_mensagem')),
                        );
            $email_view = $this->load->view('arquitetos/email', $data, TRUE);

            $mail->Body = $email_view;

           //print_r($this->input->post()); exit; 

            if(!$mail->Send()) {
              echo "Erro: " . $mail->ErrorInfo;
            } else {
              echo "Mensagem enviada com sucesso!";
            }
        }
    }

    private function _salva_envio($dados)
    {
        //adiciona a data/hora de processamento e o ip
        $dados['created'] = $_SERVER['REQUEST_TIME'];
        $dados['ip'] = ip2long($_SERVER['REMOTE_ADDR']);
        $dados['url'] = $dados['site'];
        $dados['mensagem'] = $dados['statocad_mensagem'];
        $dados['statocad'] = $dados['ferramenta_statocad'];
        $this->load->model('arquitetos/arquiteto');

        if( !$this->arquiteto->insert($dados) )
        {
            throw new Exception("Erro ao salvar de dados", 1);
        }
        else{
            echo 'salvo!'; exit;
        }
    }
}
<?php
class Showroom extends Datamapper
{
	var $table = 'showrooms';

	public function get_conteudo($id)
	{
		$show = new Showroom();
		$show->where('id', $id)->get(1);

		if( ! $show->exists() ) return FALSE;
		return $show;
	}

	function change($dados)
    {
        $showroom = new Showroom();
        $showroom->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update = $showroom->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    function insert($dados)
    {
        $showroom = new Showroom();
        foreach ($dados as $key => $value)
        {
            $showroom->$key = $value;
        }
        $insert = $showroom->save();
        if($insert)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function apaga($id)
    {
        $showroom = new Showroom();
        $showroom->where('id', $id)->get(1);

        if( ! $showroom->delete() ) return FALSE;

        return TRUE;
    }
}
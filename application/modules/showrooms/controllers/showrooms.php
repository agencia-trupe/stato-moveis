<?php
class Showrooms extends MX_Controller
{
    var   $data;

    public function index()
    {
        $this->load->model('showrooms/showroom');
        $seo['title'] = 'Stato &middot; Showroom';
        $seo['description'] = 'Stato Móveis - Showroom';
        $this->data['showroom'] = $this->showroom->get_conteudo(1);
        $this->load->library('seo', $seo);
        $this->data['pagina'] = 'showroom';
        $this->data['conteudo'] = 'showrooms/index';
        $this->load->view('layout/template', $this->data);
    }

    public function parcial()
    {
        $this->load->model('showrooms/showroom');
        $this->data['showroom']  = $this->showroom->get_conteudo(1);
        $this->load->view('showrooms/parcial', $this->data);
    }

    public function header()
    {
        $this->load->model('showrooms/showroom');
        $this->data['showroom']  = $this->showroom->get_conteudo(1);
        $this->load->view('showrooms/header', $this->data);   
    }
}
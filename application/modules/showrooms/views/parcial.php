<div class="contato-rodape">
	<div class="telefone-email">
		<div class="telefone-rodape">
			<div class="icone-telefone-rodape"></div>
			<?php echo str_replace('-', ' ', str_replace(')', '</span>', 
						str_replace('(', '<span class="ddd">', $showroom->tel))) ?>
		</div>
		<div class="email-rodape">
			<div class="icone-email-rodape"></div>
			<span class="email"><a href="mailto://<?php echo $showroom->email ?>"><?php echo $showroom->email ?></a></span>
		</div>
	</div>
	<div class="endereco">
		<?php echo $showroom->endereco ?>, <?php echo $showroom->numero ?> - 
		<?php echo $showroom->bairro ?> - <?php echo $showroom->cidade ?> - 
		<?php echo $showroom->uf ?> CEP: <?php echo $showroom->cep ?>
	</div>
</div>
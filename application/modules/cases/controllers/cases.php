<?php
class Cases extends MX_Controller
{
    var   $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('cases/caso');
        $this->load->model('cases/cfoto');
    }

    public function index()
    {
        $seo['title'] = 'Stato &middot; Cases';
        $seo['description'] = 'Cases da Stato Móveis.';
        $this->load->library('seo', $seo);
        $this->data['pagina'] = 'cases';
        $this->data['cases'] = $this->caso->fetch_all();
        $this->load->model('paginas/pagina');
        $texto = $this->pagina->get_conteudo('cases', 'slug');
        $this->data['texto_superior'] = $texto->texto;
        $this->data['conteudo'] = 'cases/index';
        $this->load->view('layout/template', $this->data);
    }

    public function detalhe($id)
    {
        $case = $this->caso->get_by_id($id);
        $cases = $this->caso->fetch_all();
        $fotos = $this->cfoto->get_by_case($id);
     
        $this->data['image_count'] = $this->db->where('caso_id', $id)->get('cfotos')->num_rows();

        $seo['title'] = 'Stato &middot; Cases &middot; ' . $case->titulo;
        $seo['description'] = 'Cases da Stato Móveis.';
        $this->load->library('seo', $seo);

        $this->data['case'] = $case;
        $this->data['cases'] = $cases;
        $this->data['fotos'] = $fotos;
        
        $this->data['pagina'] = 'cases';
        $this->data['conteudo'] = 'cases/detalhe';
        $this->load->view('layout/template', $this->data);

    }
}
<?php
class Cfoto extends Datamapper
{
    var $table = 'cfotos';
    var $has_one = array('caso');

    function get_by_case($id)
    {
        $foto = new Cfoto();
        $foto->where('caso_id', $id);
        $foto->order_by('ordem', 'asc');
        $foto->get();
        $arr = array();
        foreach( $foto->all as $f )
        {
            $arr[] = $f;
        }
        if( sizeof( $arr ) )
        {
            return $arr;
        }
        return FALSE;
    }
    function get_conteudo( $id )
    {
        $foto = new Cfoto();
        $foto->where( 'id', $id )->get();
        if( $foto->exists() ){
            return $foto;
        }
        return NULL;
    }

    function get_related( $id, $position )
    {
        $foto = new Cfoto();
        switch ( $position ) {
            case 'prev':
                $foto->where( 'id <', $id);
                break;
            case 'next':
                $foto->where( 'id >', $id);
                break;
        }
        $foto->get(1);
        if($foto->exists())
        {
            return $foto->id;
        }
        return FALSE;
    }
    /**
     * Temporario para desenvolvimento
     * @param  [type] $dados [description]
     * @return [type]        [description]
     */
    function insert($dados)
    {
        $foto = new Cfoto();
        foreach ($dados as $key => $value)
        {
            $foto->$key = $value;
        }
        $foto->created = time();
        $insert = $foto->save();
        if($insert)
        {
            return $foto->id;
        }
        return FALSE;
    }

    function ordena($dados)
    {
        $result = array();
        foreach($dados as $key => $value)
        {
            $foto = new Cfoto();
            $foto->where('id', $value);
            $update_data = array(
                'ordem' => $key
                );
            if($foto->update($update_data))
            {
                $result[] = $value;
            }
        }
        if(sizeof($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function deleta_foto($foto_id)
    {
        $foto = new Cfoto();
        $foto->where('id', $foto_id)->get();
        if($foto->delete())
        {
            return TRUE;
        }
        return FALSE;
    }
}
<?php

class Caso extends Datamapper
{
	var $table = 'casos';
    var $has_many = array('cfoto');
    var $auto_populate_has_many = TRUE;

	public function fetch_all()
	{
		$c = new Caso();
		$c->order_by('ordem', 'ASC')->get();

		$result_set = array();

		foreach($c->all as $Caso)
		{
			$result_set[] = $Caso;
		}

		if( ! count( $result_set) ) return FALSE;

		return $result_set;
	}

	public function get_by_slug($slug)
	{
		$Caso = new Caso();
		$Caso->where('slug', $slug)->get(1);
		
		if( ! $Caso->exists() ) return FALSE;
		
		return $Caso;
	}

	public function get_by_id($id)
	{
		$Caso = new Caso();
		$Caso->where('id', $id)->get(1);
		
		if( ! $Caso->exists() ) return FALSE;

		return $Caso;
	}


	function change($dados)
    {
        $Caso = new Caso();
        $Caso->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update = $Caso->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    function insert($dados)
    {
        $casos = new Caso();
        $casos->select_max('ordem')->get();

    	$Caso = new Caso();
    	foreach($dados as $key => $value)
    	{
    		$Caso->$key = $value;
    	}
        $Caso->ordem = $casos->ordem + 1;
    	
    	if( ! $Caso->save() ) return FALSE;

    	return TRUE;
    }

    function apaga($id)
    {
        $Caso = new Caso();
        $Caso->where('id', $id)->get(1);

        if( ! $Caso->delete() ) return FALSE;
        
        return TRUE;

    }

    function ordena($dados)
    {
        $result = array();
        foreach($dados as $key => $value)
        {
            $produto = new Caso();
            $produto->where('id', $value);
            $update_data = array(
                'ordem' => $key
                );
            if($produto->update($update_data))
            {
                $result[] = $value;
            }
        }
        if(sizeof($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}
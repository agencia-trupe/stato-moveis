<div class="conteudo cases">
	<div class="clearfix"></div>
	<div class="cases-description">
		<h1>Cases</h1>
		<?php echo $texto_superior ?>
	</div>
	<div class="cases-content">
		<?php foreach ($cases as $case): ?>
			<a href="<?php echo site_url('cases/detalhe/' . $case->id) ?>" class="case-wrapper">
				<div class="case-image">
					<img src="<?php echo base_url('assets/img/cases/lista/' . $case->imagem) ?>" alt="<?php echo $case->titulo ?>">
				</div>
				<div class="case-titulo">
					<span><?php echo $case->titulo ?></span>
				</div>
			</a>
		<?php endforeach ?>
	</div>
</div>
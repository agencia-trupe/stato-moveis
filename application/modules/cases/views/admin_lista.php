<div class="span9">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <legend>Cases <a class="btn btn-mini btn-info" href="<?php echo site_url('painel/cases/cadastrar') ?>">Novo</a></legend>
    <table class="table table-striped">
        <thead>
            <th>Título</th><th>Ações</th>
        </thead>
        <tbody>
            <?php foreach ($result as $case): ?>
                <tr>
                    <td><?php echo $case->titulo ?></td>
                    <td><a class="btn btn-info btn-mini" href="<?php echo site_url('painel/cases/editar/' . $case->id) ?>">Editar</a>
                        <a class="btn btn-danger btn-mini" href="<?php echo site_url('painel/cases/apagar/' . $case->id) ?>">Apagar</a>
                    </td>
                </tr>    
            <?php endforeach ?>
        </tbody>
    </table>
</div><!--/span-->
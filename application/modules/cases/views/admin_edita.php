<div class="span9">
<?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
<legend><?php echo $acao == 'editar' ? 'Editar case' : 'Novo case' ;?></legend>
    <?php echo isset($error) ? $error['error'] : ''; ?>
    <?php
    echo form_open_multipart(($acao == 'editar') ? 'painel/cases/atualiza' : 'painel/cases/salva', 'class="well"'); 
    ?>

    <div class="row-fluid">
      <?php if($acao == 'edita')
      {
        echo form_hidden('acao', 'edita');
      }
      else{
        echo form_hidden('acao', 'cadastra');
      }
      ?>  
      </div>
    <?php if($acao == 'editar') : ?>
    <?php echo
    form_hidden('id', set_value('id', $case->id));
    ?>
  <?php endif; ?>
    <div class="row-fluid">
        <div class="span9">
            <label for="">Título</label>
            <?php echo form_input(array('name'=>'titulo', 'id' => 'titulo', 'value'=>set_value('titulo', $acao == 'editar' ? $case->titulo : ''), 'class' => 'span12',)); ?>
            <?php echo form_error('titulo'); ?>
        </div>
    </div>
  <div class="row-fluid">
      <div class="clearfix"></div>
      <div class="control-group">
            <label class="control-label" for="descricao">Descricao</label>
            <div class="controls">
              <?php echo form_textarea('descricao', set_value('descricao', $acao == 'editar' ? $case->descricao : ''), 'class="span11 tinymce"'); ?>
              <span class="help-inline"><?php echo form_error('descricao'); ?></span>
            </div>
      </div>
     <?php if($acao == 'editar'): ?>
     <?php if($case->imagem):?>
     <img src="<?php echo base_url(); ?>assets/img/cases/<?php echo $case->imagem; ?>" alt="<?php echo $case->titulo; ?>" >
     <?php endif; ?>
     <?php endif; ?>
     <div class="control-group">
            <label class="control-label" for="imagem"><?php echo ($acao == 'editar' ? 'Adicionar / Alterar Imagem' : 'Imagem'); ?></label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem'), 'id="datepicker"'); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
  </div>
  <?php echo form_submit('submit', ($acao == 'editar') ? 'Salvar' : 'Cadastrar', 'class="btn btn-primary"'); ?>
  <?php echo form_close(); ?> 
</div>
<?php
class Newsletters extends MX_Controller
{
    var   $data;

    public function index()
    {
        $this->lista();
    }

    public function lista()
    {      
        $this->load->helper('date');
        $this->load->library('pagination');
        $pagination_config = array(
                    'base_url'       => base_url() . '/newsletter/lista/',
                    'total_rows'     => $this->db->get('newsletters')->num_rows(),
                    'per_page'       => 3,
                    'num_links'      => 5,
                    'next_link'      => 'próximo',
                    'prev_link'      => 'anterior',
                    'first_link'     => FALSE,
                    'last_link'      => FALSE, 
                    'full_tag_open'  => '<div class="pagination-wrapper"><ul>',
                    'full_tag_close' => '</ul></div>',
                    'cur_tag_open'   => '<li class="active"><a href="#">',
                    'cur_tag_close'  => '</a></li>',
                    'num_tag_open'   => '<li>',
                    'num_tag_close'  => '</li>',
                    'next_tag_open'   => '<li class="next">',
                    'next_tag_close'  => '</li>',
                    'prev_tag_open'   => '<li class="prev">',
                    'prev_tag_close'  => '</li>',
            );
        $this->pagination->initialize($pagination_config);
        //Obtendo resultados no banco
        $this->load->model('newsletters/newsletter');
        $data['results'] = $this->newsletter->get_all($pagination_config['per_page'], $this->uri->segment(3));

        $seo['title'] = 'Stato &middot; Newsletter';
        $seo['description'] = 'Stato Móveis - Newsletter';
        $this->load->library('seo', $seo);
        $data['pagina'] = 'newsletter';
        $data['conteudo'] = 'newsletters/index';
        $this->load->view('layout/template', $data);
    }

    public function detalhe($id)
    {
        $this->load->model('newsletters/newsletter');
        $data['html'] = $this->newsletter->get_html($id);
        $this->load->view('newsletters/detalhe', $data);
    }
}
<?php

class Newsletter extends datamapper
{
	var $table = 'newsletters';

	public function get_all($limit, $offset)
	{
		$n = new Newsletter();
		$n->order_by('data', 'DESC')
		  ->get($limit, $offset);

		$result_set = array();

		foreach ($n->all as $newsletter)
		{
			$result_set[] = $newsletter;
		}

		if( ! count($result_set) ) return FALSE;
		return $result_set;
	}

	public function fetch_all()
    {
        $p = new Newsletter();
        $p->get();

        $result_set = array();
        foreach($p->all as $newsletter)
        {
            $result_set[] = $newsletter;
        }

        if( ! count($result_set) ) return FALSE;

        return $result_set;
    }

    function change($dados)
    {
        $newsletter = new Newsletter();
        $newsletter->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update = $newsletter->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    function get_html($id)
    {
    	$n = new Newsletter();
    	$n->where('id', $id)->select('html')->get(1);

    	if( ! $n->exists() ) return FALSE;
    	return $n->html;
    }

    function insert($dados)
    {
        $newsletter = new Newsletter();
        foreach ($dados as $key => $value)
        {
            $newsletter->$key = $value;
        }
        $insert = $newsletter->save();
        if($insert)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function apaga($id)
    {
        $newsletter = new Newsletter();
        $newsletter->where('id', $id)->get(1);

        if( ! $newsletter->delete() ) return FALSE;

        return TRUE;
    }

}
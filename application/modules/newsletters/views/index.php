<div class="conteudo newsletters">
	<div class="inner">
		<div class="clearfix"></div>
		<h1>Newsletter</h1>
		<section class="icones">
			<?php foreach ($results as $newsletter): ?>
				<a href="<?php echo base_url('assets/img/newsletters/' . $newsletter->imagem) ?>" rel="prettyPhoto" class="newsletter-wrapper">
					<img src="<?php echo base_url('assets/img/newsletters/thumbs/' . $newsletter->imagem) ?>" alt="<?php echo $newsletter->titulo ?>">
					<span class="newsletter-data-wrapper">
						<?php echo month_name($newsletter->data, TRUE, TRUE) ?>
						<div class="clearfix"></div>
						<span class="newsletter-ano">
							<?php echo date('Y', $newsletter->data) ?>
						</span>
					</span>
				</a>
			<?php endforeach ?>
			<div class="clearfix"></div>
		</section>
		<?php echo $this->pagination->create_links() ?>
	</div>
	<div class="clearfix"></div>
</div>
<?php 

class Uploads extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('uploads/files_model');
    }
    public function index()
    {
        $data = array(
                'titulo' => 'Trabalhe conosco',
                'pagina' => 'trabalhe'
            );
        $data['conteudo_principal'] = 'static_pages/trabalhe';
        $this->load->view('layout/template', $data);
    }

    public function upload_file()
    { 
        $this->load->library('form_validation');

        $status = "";
        $msg = "";
        $file_element_name = 'realupload';
   
        $this->form_validation->set_rules('nome', 'nome', 'required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('email', 'email', 'trim|valid_email|required|xss_clean');
        $this->form_validation->set_rules('telefone', 'telefone', 'required|xss_clean|max_length[18]');

        if($this->form_validation->run() == FALSE) 
        {
            $status = "error";
            $msg =  strip_tags(validation_errors());
        } 
        else
        {
            $config['upload_path'] = './assets/files';
            $config['allowed_types'] = 'pdf|doc|docx|rtf|txt';
            $config['max_size']  = 1024 * 8;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($file_element_name))
            {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            }
            else
            {
                $dados_arquivo = $this->upload->data();
                try 
                {
                    $this->_salva_curriculo($this->input->post(), $dados_arquivo['file_name']);
                } 
                catch (Exception $e) 
                {
                    echo $e->getMessage() . "\n";
                }

                $dados = array(
                    'file_data' => $this->upload->data(),
                    'nome' => $this->input->post('nome'),
                    'email' => $this->input->post('email'),
                    'telefone' => $this->input->post('telefone'),
                    );
                if($this->files_model->send_email($dados))
                {
                    $status = "success";
                    $msg = "Currículo enviado com sucesso";
                }
                else
                {
                    unlink($data['full_path']);
                    $status = "error";
                    $msg = "Houve um erro ao enviar seu curríclo, por favor, tente novamente.";
                }
                 $status = "success";
                $msg = "Currículo enviado com sucesso";
            }   
            @unlink($_FILES[$file_element_name]);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    private function _salva_curriculo($dados, $filename)
    {
        //adiciona a data/hora de processamento e o ip
        $dados['created'] = $_SERVER['REQUEST_TIME'];
        $dados['ip'] = ip2long($_SERVER['REMOTE_ADDR']);
        $dados['filename'] = $filename;
        $this->load->model('uploads/curriculo');

        if( !$this->curriculo->insert($dados) )
        {
            throw new Exception("Erro ao salvar de dados", 1);
        }
    }
}
<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
       <div class="span11">
            <legend>
                Currículos - Trabalhe Conosco
            </legend>
        </div>
    </div>
    <div class="row-fluid">
    <div class="span11">
    <table class="table">
        <thead>
            <tr>
                <th>Data</th><th>IP</th><th>Nome</th><th>Email</th><th><i class="icon-cog"></i></th>
            </tr>
        </thead>
        <?php if ($curriculos): ?>
            <?php foreach ($curriculos as $curriculo): ?>
                <tr>
                    <td><?php echo date('d/m/y', $curriculo->created) . ' às ' . date('H:i', $curriculo->created) . 'h'?></td>
                    <td><?php echo long2ip($curriculo->ip) ?></td>
                    <td><?php echo $curriculo->nome ?></td>
                    <td><?php echo $curriculo->email ?></td>
                    <td>
                        <a class="btn btn-info btn-mini msg-show" href="<?php echo site_url('assets/files/' . $curriculo->filename) ?>">Download do currículo</a>
                        <a class="btn btn-danger btn-mini" href="<?php echo site_url('painel/curriculos/apagar/' . $curriculo->id) ?>">Apagar</a>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php endif ?>
    </table>
    </div>
    </div>
</div><!--/span-->
<?php

class Files_model extends CI_Model
{

   public function send_email($dados)
   {
        // send an email  
                    $this->load->library('My_PHPMailer');

                    // Inicia a classe PHPMailer
                $mail = new PHPMailer();
                 
                // Define os dados do servidor e tipo de conexão
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->IsSMTP(); // Define que a mensagem será SMTP
                //$mail->Host = "localhost"; // Endereço do servidor SMTP (caso queira utilizar a autenticação, utilize o host smtp.seudomínio.com.br)
                $mail->SMTPAuth = true; // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
                $mail->Username = 'noreply@stato.com.br'; // Usuário do servidor SMTP (endereço de email)
                $mail->Password = 'n0replyst'; // Senha do servidor SMTP (senha do email usado)
                
                $mail->Host = 'smtp.stato.com.br';
                $mail->Port = '587'; 
                // Define o remetente
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->From = "noreply@stato.com.br"; // Seu e-mail
                $mail->Sender = "noreply@stato.com.br"; // Seu e-mail
                $mail->FromName = "No Reply - Stato Móveis"; // Seu nome
                 
                // Define os destinatário(s)
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->AddAddress('contato@stato.com.br', 'Contato Stato');
                //$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
                $mail->AddBCC('nilton@trupe.net', 'Nilton - Trupe'); // Cópia Oculta
                 
                // Define os dados técnicos da Mensagem
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
                //$mail->CharSet = 'iso-8859-1'; // Charset da mensagem (opcional)
                 
                // Define a mensagem (Texto e Assunto)
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->Subject  = "Currículo enviado pelo site Stato"; // Assunto da mensagem
                
                $data['dados'] = array(
                        'nome' => $dados['nome'],
                        'email' => $dados['email'],
                        'telefone' => $dados['telefone'],
                );
                $email_view = $this->load->view('uploads/email', $data, TRUE);

                $mail->Body = $email_view;

                $pathToUploadedFile = $dados['file_data']['full_path'];
                $mail->AddAttachment($pathToUploadedFile);
                                 
                // Define os anexos (opcional)
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                //$mail->AddAttachment("/home/login/documento.pdf", "novo_nome.pdf");  // Insere um anexo
                 
                // Envia o e-mail
                $enviado = $mail->Send();
                 
                // Limpa os destinatários e os anexos
                $mail->ClearAllRecipients();
                $mail->ClearAttachments();
                 
                // Exibe uma mensagem de resultado
                if ($enviado) {
                return TRUE;
                } else {
                return FALSE;
                }
        }

}
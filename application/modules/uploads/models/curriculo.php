<?php

class Curriculo extends Datamapper
{
	var $table = 'curriculos';
	
	public function fetch_all()
	{
		$c = new Curriculo();
		$c->order_by('created', 'DESC')->get();

		$result = array();
		foreach($c->all as $curriculo)
		{
			$result[] = $curriculo;
		}
		if(count($result))
			return $result;
		return false;
	}

	public function insert($dados)
	{
		$curriculo = new Curriculo();

		foreach($dados as $key => $value)
		{
			$curriculo->$key = $value;
		}

		if($curriculo->save()) 
			return TRUE;
		return false;
	}

	public function get_conteudo($value, $key)
	{
		$curriculo = new Curriculo();
		$curriculo->where($key, $value)->get(1);
		if( !$curriculo->exists() )
			return false;
		return $curriculo;
	}

	public function apaga($id)
	{
		$curriculo = new Curriculo();
		$curriculo->where('id', $id)->get(1);
		if( !$curriculo->delete() )
			return FALSE;
		return TRUE;
	}
}
<?php 

Class Subcategoria extends Datamapper
{
	var $table = 'subcategorias';
	var $has_one = array('categoria', 'produto');
	var $auto_populate_has_one = TRUE;

	function change($dados)
    {
        $subcategoria = new Subcategoria();
        $subcategoria->where('id', $dados['id']);
        $update_data = array();
        
        $update = $subcategoria->update(array('titulo' => $dados['titulo']));
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function insert($titulo, $categoria_id)
    {
    	$subcategoria = new Subcategoria();
    	$subcategoria->titulo = $titulo;
    	$subcategoria->categoria_id = $categoria_id;
    	if( ! $subcategoria->save() ) return FALSE;
    	return $subcategoria;
    }

    public function remove($id)
    {
        $subcategoria = new Subcategoria();
        $subcategoria->where('id', $id)->get(1);

        if( ! $subcategoria->delete() ) return FALSE;

        return TRUE;
    }

    public function get_conteudo($id)
    {
        $sub = new Subcategoria();
        $sub->where('id', $id)->get(1);
        return $sub;
    }

    public function get_by_categoria($categoria_id)
    {
        $subcategoria = new Subcategoria();
        $subcategoria->where('categoria_id', $categoria_id)->get();

        $result_set = array();

        foreach ($subcategoria->all as $sub) {
            $result_set[$sub->id]['titulo'] = $sub->titulo;
        }

        if( ! count($result_set) ) return FALSE;

        return $result_set;
    }

    public function get_all_by_categoria($categoria_id)
    {
        $subcategoria = new Subcategoria();
        $subcategoria->order_by('ordem','ASC');
        $subcategoria->where('categoria_id', $categoria_id)->get();

        $result_set = array();

        foreach ($subcategoria->all as $sub) {
            $result_set[] = $sub;
        }

        if( ! count($result_set) ) return FALSE;

        return $result_set;
    }

    function ordena($dados)
    {
        $result = array();
        foreach($dados as $key => $value)
        {
            $subcategoria = new Subcategoria();
            $subcategoria->where('id', $value);
            $update_data = array(
                'ordem' => $key
                );
            if($subcategoria->update($update_data))
            {
                $result[] = $value;
            }
        }
        if(sizeof($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}
<?php 

Class Categoria extends Datamapper
{
	var $table = 'categorias';
	var $has_many = array('subcategoria', 'produto');
	var $auto_populate_has_many = TRUE;


	public function get_by_slug($slug)
	{
		$c = new Categoria();
		$c->where('slug', $slug)->get(1);
		

		if( ! $c->exists() ) return FALSE;
		return $c;
	}

	public function fetch_all($order = null)
	{
		$c = new Categoria();
        if($order)
            $c->order_by('ordem','ASC');
		$c->get();

		$result_set = array();
		foreach($c->all as $categoria)
		{
			$result_set[] = $categoria;
		}

		if( ! count($categoria) ) return FALSE;

		return $result_set;
	}

	public function get_conteudo($id)
	{
		$categoria = new Categoria();
		$categoria->where('id', $id)->get(1);

		if( ! $categoria->exists() ) return FALSE;

		return $categoria;
	}

	function change($dados)
    {
        $categoria = new Categoria();
        $categoria->where('id', $dados['id']);
        $update_data = array();
        
        $update = $categoria->update(array('titulo' => $dados['titulo'], 'slug'=>$dados['slug']));
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    function insert($dados)
    {
    	$categoria = new Categoria();
    	$categoria->titulo = $dados['titulo'];
    	$categoria->slug = $dados['slug'];

    	if( ! $categoria->save() ) return FALSE;

    	return TRUE;
    }

    public function apaga($id)
    {
        $categoria = new Categoria();
        $categoria->where('id', $id)->get(1);

        if( ! $categoria->delete() ) return FALSE;

        return TRUE;
    }

    public function get_slug($id)
    {
    	$cat = new Categoria();
    	$cat->where('id', $id)->get(1);
    	return $cat->slug;
    }

    function ordena($dados)
    {
        $result = array();
        foreach($dados as $key => $value)
        {
            $categoria = new Categoria();
            $categoria->where('id', $value);
            $update_data = array(
                'ordem' => $key
                );
            if($categoria->update($update_data))
            {
                $result[] = $value;
            }
        }
        if(sizeof($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}
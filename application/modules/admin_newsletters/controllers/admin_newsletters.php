<?php
class Admin_newsletters extends MX_Controller
{
    var $data;
    public function __construct()
    {
        parent::__construct();
        $this->data['module'] = 'newsletters';
        $this->load->model('newsletters/newsletter');
        $this->load->helper('date');
    }
    public function index()
    {
        $this->lista();
    }

    public function lista()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['newsletters'] = $this->newsletter->fetch_all();
            $this->data['conteudo'] = 'admin_newsletters/admin_lista';
            $this->load->view('start/template', $this->data);
        }
    }

    public function editar($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'editar';
            $this->data['newsletter'] = $this->newsletter->get_by_id($id);
            $this->data['conteudo'] = 'admin_newsletters/admin_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function cadastrar()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'cadastrar';
            $this->data['conteudo'] = 'admin_newsletters/admin_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function processa_cadastro()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('newsletters'))
            {
                $this->data['acao'] = 'cadastrar';
                $this->data['conteudo'] = 'admin_newsletters/admin_edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }

                $post['data'] = strtotime($post['ano'] . '-' . $post['mes']);

                unset($post['ano']);
                unset($post['mes']);

                if(strlen($_FILES["imagem"]["name"])>0)
                {
                    
                    $config['upload_path'] = './assets/img/newsletters';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '8000';
                    $config['max_width']  = '6000';
                    $config['max_height']  = '4000';

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('imagem'))
                    {
                        $this->data['error'] = array(
                                'error' => $this->upload->display_errors());
                        $this->data['newsletter'] = $this->newsletter
                            ->get_by_id($id, $this->input->post('id'));
                        $this->data['categorias'] = $this->categoria
                            ->fetch_all();
                        $this->data['conteudo'] = 'admin_newsletters/admin_edita';
                        $this->load->view('start/template', $this->data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
                        $thumb = $upload_data['file_path'] . './thumbs/' . $upload_data['file_name'];

                        if(
                            $this->image_moo->load($file_uploaded)
                                            ->resize(900,900,true)
                                            ->save($thumb,true)
                            &&
                            $this->image_moo->load($file_uploaded)
                                            ->resize_crop(290,220)
                                            ->save($thumb,true)
                        )
                        {
                            $post['imagem'] = $upload_data['file_name'];
                        }
                    }
                }
                if($this->newsletter->insert($post))
                {
                    $this->session->set_flashdata('success', 'Registro adicionado com sucesso');
                    redirect('painel/newsletters');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível adicionar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/newsletters/');
                }
            }
        }
    }

    public function apagar($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $apaga = $this->newsletter->apaga($id);
            if($apaga)
            {
                $this->session->set_flashdata('success', 'Registro removido com sucesso');
                redirect('painel/newsletters');
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível remover o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/newsletters/');
            }
        }
    }

    public function processa()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('newsletters'))
            {
                $this->data['acao'] = 'editar';
                $this->data['newsletter'] = $this->newsletter
                                    ->get_by_id($this->input->post('id'));
                $this->data['fotos'] = NULL;
                $this->data['conteudo'] = 'admin_newsletters/admin_edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }

                $post['data'] = strtotime($post['ano'] . '-' . $post['mes']);

                unset($post['ano']);
                unset($post['mes']);

                if(strlen($_FILES["imagem"]["name"])>0)
                {
                    
                    $config['upload_path'] = './assets/img/newsletters';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '8000';
                    $config['max_width']  = '6000';
                    $config['max_height']  = '4000';

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('imagem'))
                    {
                        $this->data['error'] = array(
                                'error' => $this->upload->display_errors());
                        $this->data['newsletter'] = $this->newsletter
                            ->get_by_id($id, $this->input->post('id'));
                        $this->data['categorias'] = $this->categoria
                            ->fetch_all();
                        $this->data['conteudo'] = 'admin_newsletters/admin_edita';
                        $this->load->view('start/template', $this->data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
                        $thumb = $upload_data['file_path'] . './thumbs/' . $upload_data['file_name'];

                        if(
                            $this->image_moo->load($file_uploaded)
                                            ->resize(900,900,true)
                                            ->save($thumb,true)
                            &&
                            $this->image_moo->load($file_uploaded)
                                            ->resize_crop(290,220)
                                            ->save($thumb,true)
                        )
                        {
                            $post['imagem'] = $upload_data['file_name'];
                        }
                    }
                }
                if($this->newsletter->change($post))
                {
                    $this->session->set_flashdata('success', 'Registro alterado com sucesso');
                    redirect('painel/newsletters');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível alterar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/newsletters/edita/' . $post['id']);
                }
            }
        }
    }

}
<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="span6">
            <legend>Categorias <?php echo anchor('painel/produtos/categorias/cadastrar', 'Novo', 'class="btn btn-info btn-mini"'); ?></legend>
        </div>
    </div>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Título</th><th>Ações</th>
            </tr>
        </thead>
        <?php if ($categorias): ?>
            <?php foreach ($categorias as $categoria): ?>
                <tr>
                    <td><?php echo $categoria->titulo ?></td>
                    <td>
                        <a class="btn btn-info btn-mini" href="<?php echo site_url('painel/produtos/categorias/editar/' . $categoria->id) ?>">Editar</a>
                        <a class="btn btn-danger btn-mini" href="<?php echo site_url('painel/produtos/categorias/apagar/' .$categoria->id) ?>">Apagar</a>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php endif ?>
    </table>
</div><!--/span-->
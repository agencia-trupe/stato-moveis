<div class="row-fluid">
    <div class="span9">
        <?php if($this->session->flashdata('error') != NULL): ?>
        <div class="alert alert-error">
            <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php endif; ?>
    <?php if(isset($error)): ?>
        <div class="alert alert-error">
            <?php echo $error['error']; ?>
        </div>
    <?php endif; ?>
    <legend><?=( $acao == 'editar' ) ? 'Editar' : 'Cadastrar'; ?> Newsletter</legend>
    <!--Tipo de Form-->
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'painel/newsletters/processa';
                    break;
                
                default:
                    $action = 'painel/newsletters/processa_cadastro';
                    break;
            }
    ?>
    <!--fim de Tipo de Form-->
    <!--Form open-->
    <?=form_open_multipart($action); ?>
    <!--Id do objeto-->
    <?php if($acao == 'editar'): ?>
    <input type="hidden" name="id" value="<?=$newsletter->id; ?>" class="id" />
    <?php endif; ?>
    <!--fim de Id do objeto-->
    <!--Mês-->
    <?=form_label('Mês (apenas números)'); ?>
    <?=form_input(array(
        'name' => 'mes',
        'value' => set_value('mes', ( $acao == 'editar' ) ? date('m', $newsletter->data) : ''),
    )); ?>
    <?=form_error('mes'); ?>
    <!--fim de Mês-->
    <!--Ano-->
    <?=form_label('Ano'); ?>
    <?=form_input(array(
        'name' => 'ano',
        'value' => set_value('ano', ( $acao == 'editar' ) ? date('Y', $newsletter->data) : ''),
    )); ?>
    <?=form_error('ano'); ?>
    <!--fim de Ano-->
    <div class="clearfix"></div>    
    <br>
    <?php if ($acao == 'editar') : ?>
    <img src="<?php echo base_url(); ?>assets/img/newsletters/thumbs/<?php echo $newsletter->imagem; ?>" alt="" >
    <?php endif; ?>
    <div class="control-group">
            <label class="control-label" for="imagem">Foto de capa</label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem')); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
    <div class="clearfix"></div>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('newsletters/admin_newsletters/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
    <div class="clearfix"></div>
    </div>
</div>
<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="span6">
            <legend>newsletters <?php echo anchor('painel/newsletters/cadastra', 'Novo', 'class="btn btn-info btn-mini"'); ?></legend>
        </div>
    </div>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Data</th><th>Ações</th>
            </tr>
        </thead>
        <?php if ($newsletters): ?>
            <?php foreach ($newsletters as $newsletter): ?>
                <tr>
                    <td><?php echo month_name($newsletter->data, FALSE, FALSE) . ' ' . date('Y', $newsletter->data)?></td>
                    <td>
                        <a class="btn btn-info btn-mini" href="<?php echo site_url('painel/newsletters/editar/' . $newsletter->id) ?>">Editar</a>
                        <a class="btn btn-danger btn-mini" href="<?php echo site_url('painel/newsletters/apagar/' .$newsletter->id) ?>">Apagar</a>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php endif ?>
    </table>
</div><!--/span-->
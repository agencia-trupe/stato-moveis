<div class="row-fluid">
    <div class="span9">
        <?php if($this->session->flashdata('error') != NULL): ?>
        <div class="alert alert-error">
            <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php endif; ?>
    <?php if(isset($error)): ?>
        <div class="alert alert-error">
            <?php echo $error['error']; ?>
        </div>
    <?php endif; ?>
    <legend><?=( $acao == 'editar' ) ? 'Editar' : 'Cadastrar'; ?> Showroom</legend>
    <!--Tipo de Form-->
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'painel/showroom/processa';
                    break;
                
                default:
                    $action = 'painel/showroom/processa_cadastro';
                    break;
            }
    ?>
    <!--fim de Tipo de Form-->
    <!--Form open-->
    <?=form_open_multipart($action); ?>
    <!--Id do objeto-->
    <?php if($acao == 'editar'): ?>
    <input type="hidden" name="id" value="<?=$showroom->id; ?>" class="id" />
    <?php endif; ?>
    <!--fim de Id do objeto-->
    <!--Endereço-->
    <?=form_label('Endereço'); ?>
    <?=form_input(array(
        'name' => 'endereco',
        'value' => set_value('endereco', ( $acao == 'editar' ) ? $showroom->endereco : ''),
    )); ?>
    <?=form_error('endereco'); ?>
    <!--fim de Endereço-->
    <!--Número-->
    <?=form_label('Número'); ?>
    <?=form_input(array(
        'name' => 'numero',
        'value' => set_value('numero', ( $acao == 'editar' ) ? $showroom->numero : ''),
        'class' => 'span2'
    )); ?>
    <?=form_error('numero'); ?>
    <!--fim de Número-->
    <div class="clearfix"></div>
    <!--Bairro-->
    <?=form_label('Bairro'); ?>
    <?=form_input(array(
        'name' => 'bairro',
        'value' => set_value('bairro', ( $acao == 'editar' ) ? $showroom->bairro : ''),
    )); ?>
    <?=form_error('bairro'); ?>
    <!--fim de Bairro-->
     <div class="clearfix"></div>
     <!--Cidade-->
    <?=form_label('Cidade'); ?>
    <?=form_input(array(
        'name' => 'cidade',
        'value' => set_value('cidade', ( $acao == 'editar' ) ? $showroom->cidade : ''),
    )); ?>
    <?=form_error('cidade'); ?>
    <!--fim de Cidade-->
     <div class="clearfix"></div>
     <!--UF-->
    <?=form_label('UF'); ?>
    <?=form_input(array(
        'name' => 'uf',
        'value' => set_value('uf', ( $acao == 'editar' ) ? $showroom->uf : ''),
    )); ?>
    <?=form_error('uf'); ?>
    <!--fim de UF-->
     <!--CEP-->
    <?=form_label('CEP'); ?>
    <?=form_input(array(
        'name' => 'cep',
        'value' => set_value('cep', ( $acao == 'editar' ) ? $showroom->cep : ''),
    )); ?>
    <?=form_error('cep'); ?>
    <!--fim de CEP-->
     <div class="clearfix"></div>
     <!--Telefone-->
    <?=form_label('Telefone'); ?>
    <?=form_input(array(
        'name' => 'tel',
        'value' => set_value('tel', ( $acao == 'editar' ) ? $showroom->tel : ''),
    )); ?>
    <?=form_error('tel'); ?>
    <!--fim de Telefone-->
     <div class="clearfix"></div>
     <!--Email-->
    <?=form_label('Email'); ?>
    <?=form_input(array(
        'name' => 'email',
        'value' => set_value('email', ( $acao == 'editar' ) ? $showroom->email : ''),
    )); ?>
    <?=form_error('email'); ?>
    <!--fim de Email-->
     <div class="clearfix"></div>
       <!--Facebook-->
    <?=form_label('Facebook'); ?>
    <?=form_input(array(
        'name' => 'facebook',
        'value' => set_value('facebook', ( $acao == 'editar' ) ? $showroom->facebook : ''),
    )); ?>
    <?=form_error('facebook'); ?>
    <!--fim de Facebook-->
     <div class="clearfix"></div>
       <!--Twitter-->
    <?=form_label('Twitter'); ?>
    <?=form_input(array(
        'name' => 'twitter',
        'value' => set_value('twitter', ( $acao == 'editar' ) ? $showroom->twitter : ''),
    )); ?>
    <?=form_error('twitter'); ?>
    <!--fim de Twitter-->
     <div class="clearfix"></div>
     <!--Início de atentimento segunda à sexta-->
    <?=form_label('Início de atentimento segunda à sexta'); ?>
    <?=form_input(array(
        'name' => 'inicio_seg_sex',
        'value' => set_value('inicio_seg_sex', ( $acao == 'editar' ) ? $showroom->inicio_seg_sex : ''),
    )); ?>
    <?=form_error('inicio_seg_sex'); ?>
    <!--fim de Início de atentimento segunda à sexta-->
     <div class="clearfix"></div>
     <!--Fim de atendimento segunda à sexta-->
    <?=form_label('Fim de atendimento segunda à sexta'); ?>
    <?=form_input(array(
        'name' => 'fim_seg_sex',
        'value' => set_value('fim_seg_sex', ( $acao == 'editar' ) ? $showroom->fim_seg_sex : ''),
    )); ?>
    <?=form_error('fim_seg_sex'); ?>
    <!--fim de Fim de atendimento segunda à sexta-->
     <div class="clearfix"></div>
     <!--Início de atendimento sábado-->
    <?=form_label('Início de atendimento sábado'); ?>
    <?=form_input(array(
        'name' => 'inicio_sab',
        'value' => set_value('inicio_sab', ( $acao == 'editar' ) ? $showroom->inicio_sab : ''),
    )); ?>
    <?=form_error('inicio_sab'); ?>
    <!--fim de Início de atendimento sábado-->
     <div class="clearfix"></div>
     <!--Fim de atendimento sábado-->
    <?=form_label('Fim de atendimento sábado'); ?>
    <?=form_input(array(
        'name' => 'fim_sab',
        'value' => set_value('fim_sab', ( $acao == 'editar' ) ? $showroom->fim_sab : ''),
    )); ?>
    <?=form_error('fim_sab'); ?>
    <!--fim de Fim de atendimento sábado-->
    <div class="clearfix"></div>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('showrooms/admin_showrooms/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
    <div class="clearfix"></div>
    </div>
</div>
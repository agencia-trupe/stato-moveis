<?php
class Admin_showrooms extends MX_Controller
{
    var $data;
    public function __construct()
    {
        parent::__construct();
        $this->data['module'] = 'showroom';
        $this->load->model('showrooms/showroom');
    }
    public function index()
    {
        $this->editar(1);
    }

    public function editar($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'editar';
            $this->data['showroom'] = $this->showroom->get_by_id($id);
            $this->data['conteudo'] = 'admin_showrooms/admin_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function cadastrar()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'cadastrar';
            $this->data['conteudo'] = 'admin_showrooms/admin_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function processa_cadastro()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('showrooms'))
            {
                $this->data['acao'] = 'cadastrar';
                $this->data['conteudo'] = 'admin_showrooms/admin_edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }

            
                if($this->showroom->insert($post))
                {
                    $this->session->set_flashdata('success', 'Registro adicionado com sucesso');
                    redirect('painel/showroom');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível adicionar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/showroom');
                }
            }
        }
    }

    public function processa()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('showrooms'))
            {
                $this->data['acao'] = 'editar';
                $this->data['showroom'] = $this->showroom
                                    ->get_by_id($this->input->post('id'));
                $this->data['fotos'] = NULL;
                $this->data['conteudo'] = 'admin_showrooms/admin_edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }

                if($this->showroom->change($post))
                {
                    $this->session->set_flashdata('success', 'Registro alterado com sucesso');
                    redirect('painel/showroom');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível alterar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/showroom');
                }
            }
        }
    }

}
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* news
*
* Model responsável pelas regras de negócio dos newss de estágio, trainee e aprendiz
*
* @author Nilton de Freitas - Trupe
* @link http://trupe.net
*
*/
Class Cadastro extends Datamapper{
    var $table = 'cadastros';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }
    /**
     * Retorna os newss ativos em ordem crescente de antiguidade
     * @param    int    $limit   quantidade limite por retorno
     * @param    int    $offset  offset para a paginação
     * @return   array           array com a lista de newss ativos
     */
    function get_all()
    {
        $news = new Cadastro();
        $news->get();

        $arr = array();
        foreach($news->all as $newss)
        {
            $arr[] = $newss;
        }

        return $arr;

    }


    function cadastra($dados){

        $news = new Cadastro();

        $news->newsletters_nome = $dados['nome'];
        $news->newsletters_email = $dados['email'];
        $news->newsletters_data_cadastro = time();

        if($news->save()){
            return TRUE;
        } else
        {
            return FALSE;
        }

    }
}
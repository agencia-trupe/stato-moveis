<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Contato
*
* @author Nilton de Freitas - Trupe
* @link http://trupe.net
*
*/
Class Cadastros extends MX_Controller
{

        function ajax_check()
        {
                $this->load->library('form_validation');
                $this->load->library('typography');

                        $this->form_validation->set_rules('nome', 'nome',
                            'required|xss_clean|max_length[255]');
                        $this->form_validation->set_rules('email', 'email',
                            'trim|valid_email|required|xss_clean');
                        $this->form_validation->set_error_delimiters('', '');

                        if($this->form_validation->run() == FALSE)
                        {
                                echo validation_errors();
                        }
                        else
                        {
                            $this->load->model('cadastro');

                            $dados = array(
                                'nome' => $this->input->post('nome'),
                                'email' => $this->input->post('email'),
                            );

                           if($this->cadastro->cadastra($dados))
                           {
                                echo 'Cadastro efetuado com sucesso!';
                           }
                           else
                           {
                                echo 'Houve um erro ao efetuar seu cadastro,
                                por favor, tente novamente.';
                           }
                        }
        }
}


<div class="span12">
    <?php if(isset($error)){var_dump($error);} ?>
    <legend>Alterar foto secundaria - Unidade <?=$foto->unidade->nome; ?></legend>
    <h5>Foto Atual</h5>
    <img src="<?=base_url(); ?>assets/img/unidades/<?=$foto->imagem; ?>" alt="">
    <br>
    <h5>Nova Foto</h5>
    <?=form_open_multipart('painel/fotos/upload'); ?>
    <?=form_hidden('foto_id', $foto->id); ?>
    <?=form_upload('foto_secundaria'); ?>
    <div class="clearfix"></div>
    <br>
    <?=form_submit('salvar', 'Salvar', 'class="btn btn-success"'); ?>
</div>
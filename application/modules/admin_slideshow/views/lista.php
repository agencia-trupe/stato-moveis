<div class="span9">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="span11">
        <legend>
            Slides - <?php echo ucfirst($local) ?>
            <a href="<?=site_url('painel/slideshow/cadastra/' . $local); ?>" class="btn btn-action btn-mini">Novo Slide</a>
            <a href="#" class="ordenar-slides btn btn-mini btn-info">ordenar slides</a>
            <a href="#" class="salvar-ordem-slides hide btn btn-mini btn-warning">salvar ordem</a>
        </legend>
        <div class="alert alert-info hide slides-mensagem">
            <span>Para ordenar, clique na miniatura do slide e arraste até a posição desejada</span>
            <a class="close" data-dismiss="alert" href="#">&times;</a>
        </div>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Ordem</th><th class="span3">Imagem</th><th>Ações</th>
            </tr>
            <tbody>
                <?php foreach ($slides as $slide): ?>
                <tr id="slide_<?php echo $slide->id ?>">
                    <td><?=$slide->ordem; ?></td>
                    <td><img width="100%" height="100%" src="<?=base_url(); ?>assets/img/slides/<?=$slide->imagem; ?>" alt=""></td>
                    <td><a href="<?=site_url('painel/slideshow/editar/' . $slide->local . '/' . $slide->id); ?>" class="btn btn-info btn-mini">Editar</a>
                        <a href="<?=site_url('painel/slideshow/apaga/' . $slide->id); ?>" class="btn btn-danger btn-mini">Apagar</a>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </thead>
    </table>
</div>
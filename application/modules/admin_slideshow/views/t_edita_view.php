<div class="span9">
 
<ul class="nav nav-pills">
  <li><a href="<?php echo base_url(); ?>treinamentos/lista">Listar Cursos</a></li>
  <li class="active">
    <a href="#">Alterar Curso</a>
  </li>
</ul>

    <?php
   
    $titulo = array(
              'name'        => 'titulo',
              'id'          => 'titulo',
              'value'       =>  set_value('titulo', $item->titulo),
              'class'   =>  'span11',
              
            );
    $slug = array(
              'name'        => 'slug',
              'id'          => 'slug',
              'value'       =>  set_value('slug', $item->slug),
              'class'   =>  'span11',
              
            );
    
    $resumo = array(
              'name'        => 'resumo',
              'id'          => 'resumo',
              'value'       =>  set_value('resumo', $item->resumo),
              'cols'   => '10',
              'rows'        => '3',
              'class'   =>  'span6 tinymce',
              
            );
    
    $tipo = array(
        '' => '',
        'coaching' => 'Coaching',
        'curso' => 'Curso',
        'oficina' => 'Oficina',
        'palestra' => 'Palestra',
        'workshop' => 'Workshop'
    );
    
    $objetivos = array(
              'name'        => 'objetivos',
              'id'          => 'objetivos',
              'value'       =>  set_value('objetivos', $item->objetivos),
              'cols'   => '10',
              'rows'        => '5',
              'class'   =>  'span11 tinymce',
              
            );
    $requisitos = array(
              'name'        => 'requisitos',
              'id'          => 'requisitos',
              'value'       =>  set_value('requisitos', $item->requisitos),
              'cols'   => '10',
              'rows'        => '5',
              'class'   =>  'span11 tinymce',
              
            );
    $programa = array(
              'name'        => 'programa',
              'id'          => 'programa',
              'value'       =>  set_value('programa', $item->programa),
              'cols'   => '10',
              'rows'        => '5',
              'class'   =>  'span11 tinymce',
              
            );
    $metodologia = array(
              'name'        => 'metodologia',
              'id'          => 'metodologia',
              'value'       =>  set_value('metodologia', $item->metodologia),
              'cols'   => '10',
              'rows'        => '5',
              'class'   =>  'span11 tinymce',
              
            );
    $publico = array(
              'name'        => 'publico',
              'id'          => 'publico',
              'value'       =>  set_value('publico', $item->publico),
              'cols'   => '10',
              'rows'        => '5',
              'class' =>  'tinymce',
              
              
            );
    echo form_open('treinamentos/atualiza', 'class="well"'); ?>

    <div class="row-fluid">
        <?php echo form_hidden('id', $item->id); ?>
        </div>
    
    <div class="row-fluid">
        
        <div class="span4">
            
        <label for="titulo">Tipo</label>
        
                <?php 
                echo form_dropdown('tipo', $tipo, set_value('tipo', $item->tipo), 'class="span11"'); 
                ?>
                <?php echo form_error('tipo'); ?>
        </div>
        <div class="span8">
            
        <label for="titulo">Título do treinamento</label>
                
                <?php 
                echo form_input($titulo);
                ?>
                <?php echo form_error('titulo'); ?>
        </div>
    
    </div>
    <div class="row-fluid">
        
        <div class="span6">
            
        <label for="titulo">Slug</label>
                
                <?php 
                echo form_input($slug);
                ?>
                 <?php echo form_error('slug'); ?>
        </div>
    
    </div>
  <br>
  <br>
  
  <div class="row-fluid">
        
        <div class="span6">
            
        <label for="resumo">Resumo</label>
                
                <?php 
                echo form_textarea($resumo);
                ?>
                 <?php echo form_error('resumo'); ?>
        </div>
    
    </div>
  <br>
  <br>
     <div class="row-fluid">
        
        <div class="span3">
            
        <label for="titulo">Carga Horária (em horas)</label>
                
                <?php echo form_input('horas', set_value('horas', $item->horas), 'class="span4"'); ?>
                 <?php echo form_error('horas'); ?>
        </div>
    
    </div>
  <br>
  <br>
    
    <div class="row-fluid">
        
        <div class="span12">
            
            <label for="objetivos">Objetivos</label>
            
                <?php echo form_textarea($objetivos); ?>
                 <?php echo form_error('objetivos'); ?>
        </div>
        
    </div>
  <br>
  <br>
    <div class="row-fluid">
        <div class="span12">
            
             <label for="publico">Público Alvo</label>
            
                <?php echo form_textarea($publico); ?>
                 <?php echo form_error('publico'); ?>
        </div>
        
    </div>
  <br>
  <br>
          <div class="row-fluid">
        
        <div class="span12">
            
            <label for="programa">Programa</label>
            
                <?php echo form_textarea($programa); ?>
                 <?php echo form_error('programa'); ?>
        </div>
        
    </div>
  <br>
  <br>
     <div class="row-fluid">
        
        <div class="span12">
            
            <label for="metodologia">Metodologia</label>
            
                <?php echo form_textarea($metodologia); ?>
                 <?php echo form_error('metodologia'); ?>
        </div>
        
    </div>
  <br>
  <br>
         <div class="row-fluid">
            <div class="span6">
                <label for="tags">Tags</label>
                <?php echo form_input('tags', set_value('tags', $item->tags), 'class="span11"'); ?>
                 <?php echo form_error('tags'); ?>
            </div>
        </div>
        
        <?php echo form_submit('submit', 'Alterar', 'class="btn btn-danger"'); ?>

    <?php echo form_close(); ?>
    
    
</fieldset>
</div>
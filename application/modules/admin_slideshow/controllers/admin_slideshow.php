<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_slideshow extends MX_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->model('admin_slideshow/slide');
    }
    function index(){

            $this->lista();
        }
    /**
     *Lista todos os fotos cadastrados atualmente
     *
     * @return [type] [description]
     */
    function lista($local)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Stato Móveis ';
            $this->load->view('start/templatenonav', $data);
        } else {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager')  )
            {
                $this->load->model('admin_slideshow/slide');
                $data['slides'] = $this->slide->get_all($local);
                $data['module'] = 'slideshow';
                $data['local'] = $local;
                $data['main_content'] = 'admin_slideshow/lista';
                $this->load->view('includes/template', $data);
            } else {

                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                Você precisa ser 
                                                administrador para realizar essa ação');
            redirect();

          }
        }
    }
    /**
     * Mostra a página de edição de um foto cujo id foi passado como 
     * parâmetro.
     *
     * @param  [int] $id [description]
     * @return void
     */
    

    function editar($local, $id)
    {
        /**
         * Verifica se o usuário está logado para então prosseguir ou não.
         */
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Stato Móveis ';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            //Verifica se o usuário tem nível de acesso permitido
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager')  )
            {
                $id = $this->uri->segment(5);
                if(!$id)
                {
                    $this->session->set_flashdata('error', 'A ação não pode ser
                    realizada, tente novamente ou entre em contato com o suporte');
                    redirect('painel/slideshow/lista');
                }
                else
                {
                    $this->load->model('admin_slideshow/slide');
                    $data['module'] = 'slideshow';
                    $data['title'] = 'Stato Móveis - slideshow - Editar';

                    if($this->slide->get_slide($id))
                    {
                        $data['slide'] = $this->slide->get_slide($id);
                        $data['acao'] = 'editar';
                        $data['local'] = $local;
                        $data['main_content'] = 's_cadastra_view';
                        $this->load->view('includes/template', $data);
                    }
                    else
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                        redirect('painel/slideshow/lista');
                    }
                }
            }
            else
            {
                Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
                redirect();
            }
        }
    }

    function cadastra($local)
    {

        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Stato Móveis ';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
              if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager')  )
              {
                  $data['title'] = 'Stato Móveis - slideshow - Novo noticia';
                  $data['module'] = 'slideshow';
                  $data['acao'] = 'cadastra';
                  $data['local'] = $local;
                  $data['main_content'] = 's_cadastra_view';
                  $this->load->view('includes/template', $data);
              }
              else
              {
              Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
              $this->session->set_flashdata('error', 'Erro de permissão.
                                                    Você precisa ser administrador para realizar essa ação');
              redirect();

              }
        }
    }

    function salva(){

        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Stato Móveis ';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            $config = array(
                array(
                    'field' => 'ordem',
                    'label' => 'ordem',
                    'rules' => 'required',
                ),
            );
            $this->load->library('form_validation');
            $this->form_validation->set_rules($config);
            $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');
            $data['acao'] = $this->input->post('acao');

            if($this->form_validation->run() == FALSE ){
                  $data['title'] = 'Stato Móveis - Novo Slide';
                  $data['module'] = 'slideshow';
                  $data['main_content'] = 's_cadastra_view';
                  $this->load->view('includes/template', $data);
            }
            else
            {   //Verifica se foi feito o upload de uma imagem
                if(strlen($_FILES["imagem"]["name"])>0)
                {
                    $config['upload_path'] = './assets/img/slides/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '7000';
                    $config['max_width']  = '4000';
                    $config['max_height']  = '3000';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('imagem'))
                    {
                            $data['error'] = array('error' => $this->upload->display_errors());
                            $data['title'] = 'Stato Móveis - Novo Slide';
                            $data['module'] = 'slideshow';
                            $data['main_content'] = 's_cadastra_view';
                            $this->load->view('includes/template', $data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

                        
                            $this->load->model('admin_slideshow/slide');

                            if($this->input->post('local') == 'empresa')
                            {
                                $resize = $this->image_moo->load($file_uploaded)->resize(359,400)->save($new_file,true);
                            }
                            else{
                                $resize = TRUE;
                            }
                            //prepara o array com os dados para enviar ao model
                            $dados = array(
                                    'imagem' => $upload_data['file_name'],
                                    'ordem' => $this->input->post('ordem'),
                                    'local' => $this->input->post('local'),
                                );


                            if( ! $this->slide->cadastra_imagem($dados) || ! $resize)
                            {
                                $this->session->set_flashdata('error', 'A ação não pode ser
                                realizada, tente novamente ou entre em contato com o suporte');
                                redirect('painel/slideshow/cadastra/' . $slide->local);
                            }
                            else
                            {
                                $this->session->set_flashdata('success', 'Slide cadastrado com 
                                    sucesso!');
                                redirect('painel/slideshow/lista/' . $this->input->post('local'));
                            }
                        
                    }
                }
                else
                {
                    $data['title'] = 'Stato Móveis - Novo Slide';
                    $data['module'] = 'slideshow';
                    $data['main_content'] = 's_cadastra_view';
                    $this->load->view('includes/template', $data);
                }
            }
        }
    }

    function atualiza()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Stato Móveis ';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager')  )
            {
                $data['slide'] = $this->slide->get_slide($this->input->post('id'));
                $config = array(
                    array(
                        'field' => 'ordem',
                        'label' => 'ordem',
                        'rules' => 'required',
                    ),
                );
                $this->load->library('form_validation');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');

                if($this->form_validation->run() == FALSE )
                {
                    $id  = $this->input->post('id');
                    $this->load->model('admin_slideshow/slide');
                    $data['module'] = 'slideshow';
                    $data['title'] = 'Stato Móveis - Editar Slide';
                    $data['slide'] = $this->slide->get_slide($id);
                    $data['acao'] = 'editar';
                    $data['main_content'] = 's_cadastra_view';
                    $this->load->view('includes/template', $data);
                }
                else
                {
                    //verifica se foi postada uma imagem
                    if(strlen($_FILES["imagem"]["name"])>0)
                    {
                        $config['upload_path'] = './assets/img/slides/';
                        $config['allowed_types'] = 'gif|jpg|png';
                        $config['max_size'] = '7000';
                        $config['max_width']  = '4000';
                        $config['max_height']  = '3000';

                        $this->load->library('upload', $config);

                        if ( ! $this->upload->do_upload('imagem'))
                        {
                                $data['error'] = array('error' => $this->upload->display_errors());
                                $id  = $this->input->post('id');
                                $this->load->model('admin_slideshow/slide');
                                $data['module'] = 'slideshow';
                                $data['title'] = 'Stato Móveis - Editar Slide';
                                $data['slide'] = $this->slide->get_slide($id);
                                $data['acao'] = 'editar';
                                $data['main_content'] = 's_cadastra_view';
                                $this->load->view('includes/template', $data);
                        }
                        else
                        {
                            $this->load->library('image_moo');
                            //Is only one file uploaded so it ok to use it with $uploader_response[0].
                            $upload_data = $this->upload->data();
                            $file_uploaded = $upload_data['full_path'];
                            $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
        
                            $this->load->model('admin_slideshow/slide');

                                if($this->input->post('local') == 'empresa')
                                {
                                    $resize = $this->image_moo->load($file_uploaded)->resize(359,400)->save($new_file,true);
                                }
                                else{
                                    $resize = TRUE;
                                }

                                //prepara o array com os dados para enviar ao model
                                $dados = array(
                                        'id'     => $this->input->post('id'),
                                        'imagem' => $upload_data['file_name'],
                                        'ordem' => $this->input->post('ordem'),
                                    );

                                if( ! $this->slide->atualiza_imagem($dados) || ! $resize )
                                {
                                    $this->session->set_flashdata('error', 'A ação não pode ser
                                    realizada, tente novamente ou entre em contato com o suporte');
                                    redirect('painel/slideshow/cadastra/' . $slide->local);
                                }
                                else
                                {
                                    $this->session->set_flashdata('success', 'Slide cadastrado
                                    com sucesso!');
                                    redirect('painel/slideshow/lista/' . $data['slide']->local);
                                }
                        }
                    }
                    else
                    {
                         $this->load->model('admin_slideshow/slide');

                                //prepara o array com os dados para enviar ao model
                                $dados = array(
                                        'id'     => $this->input->post('id'),
                                        'ordem' => $this->input->post('ordem'),
                                    );

                                if( ! $this->slide->atualiza($dados))
                                {
                                    $this->session->set_flashdata('error', 'A ação não pode ser
                                    realizada, tente novamente ou entre em contato com o suporte');
                                    redirect('painel/slideshow/cadastra/' . $slide->local);
                                }
                                else
                                {
                                    $this->session->set_flashdata('success', 'Slide atualizado
                                    com sucesso!');
                                    redirect('painel/slideshow/lista/' . $data['slide']->local);
                                }
                    }
                }

              }
              else
              {
              Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
              $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
              redirect();

              }
        }
    }

    function apaga($id)
    {
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Stato Móveis ';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager')  )
            {
                $id = $this->uri->segment(4);
                if(!$id)
                {
                $this->session->set_flashdata('error', 'A ação não pode ser
                    realizada, tente novamente ou entre em contato com o suporte');
                redirect('painel/slideshow/lista');
                }
                else
                {
                    $slide = $this->slide->get_slide($id);
                    $this->load->model('admin_slideshow/slide');
                    if($this->slide->delete_slide($id))
                    {
                        $this->session->set_flashdata('success', 'Registro apagado
                        com sucesso');
                         redirect('painel/slideshow/lista/' . $slide->local);
                    }
                     else
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                         realizada, tente novamente ou entre em contado com o suporte');
                        redirect('painel/slideshow/lista/' . $slide->local);
                    }
                }
            }
            else
            {
                Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
                redirect();

            }
        }

    }

    public function sort_slides()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Concept - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $itens = $this->input->post('slide');
            //print_r($itens); exit;
            if ($itens)
            {
                $ordenar = $this->slide->ordena($itens);
                if($ordenar)
                {
                    echo 'Ordenado';
                }
                else
                {
                    echo 'Erro!';
                }
                /*foreach($items as $key => $value) 
                {           
                    // Use whatever SQL interface you're using to do
                    // something like this:
                    // UPDATE image_table SET sort_order = $key WHERE image_id = $value
                }*/
            } 
            else 
            {
              echo 'Erro!';
            }
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
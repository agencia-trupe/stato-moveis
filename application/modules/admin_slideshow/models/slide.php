<?php
class Slide extends Datamapper
{
    var $table = 'slides';
    
    function get_all($local)
    {
        $s = new Slide();
        $s->where('local',$local);
        $s->order_by('ordem', 'asc');
        $s->get();

        $arr = array();
        foreach ($s->all as $slide)
        {
            $arr[] = $slide;
        }

        return $arr;
    }

    function cadastra_imagem($dados){

        $slide = new Slide();

        $slide->ordem = $dados['ordem'];
        $slide->imagem = $dados['imagem'];
        $slide->local = $dados['local'];

        if($slide->save()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }

    function atualiza_imagem($dados){

        $slide = new Slide();
        $slide->where('id', $dados['id']);
        $update = $slide->update(array(
            'ordem' => $dados['ordem'],
            'imagem' => $dados['imagem'],
        ));
        return $update;
    }

    function atualiza($dados){

        $slide = new Slide();
        $slide->where('id', $dados['id']);
        $update = $slide->update(array(
            'ordem' => $dados['ordem'],
        ));
        return $update;
    }

    function get_slide($id)
    {
        $slide = new Slide();
        $slide->where('id', $id);
        $slide->limit(1);
        $slide->get();

        return $slide;
    }

    function delete_slide($id){
        $slide = new slide();
        $slide->where('id', $id)->get();


        if($slide->delete()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }

    function ordena($dados)
    {
        $result = array();
        foreach($dados as $key => $value)
        {
            $slide = new Slide();
            $slide->where('id', $value);
            $update_data = array(
                'ordem' => $key
                );
            if($slide->update($update_data))
            {
                $result[] = $value;
            }
        }
        if(sizeof($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}
<div id="direita">
<div class="row-fluid inscreve">
<?php
if ($use_username) {
	$username = array(
		'name'	=> 'username',
		'id'	=> 'username',
		'value' => set_value('username'),
		'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
		'size'	=> 30,
	);
}
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'value' => set_value('password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
	'value' => set_value('confirm_password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);
?>

                  <div class="clear fix"></div>
<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
	<fieldset>
            <legend>Novo usuário <a href="<?=site_url('auth/lista'); ?>" class="btn btn-action btn-mini">Voltar</a></legend>
        <?php if ($use_username) { ?>
            <div class="control-group">
		 <div class="control-label"><?php echo form_label('Nome de Usuário', $username['id']); ?></div>
                 <div class="controls">
		 <?php echo form_input($username); ?> 
                 <?php echo form_error($username['name']); ?><?php echo isset($errors[$username['name']])?$errors[$username['name']]:''; ?> 
                 </div>
            </div>
	<?php } ?>
            
            <div class="control-group">
                <div class="control-label"><?php echo form_label('Email', $email['id']); ?></div>
                <div class="controls">
                    <?php echo form_input($email); ?> 
                    <?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?> 
                </div>
            </div>
            <div class="control-group">
                <div class="control-label"><?php echo form_label('Senha', $password['id']); ?></div>
                <div class="controls">
		 <?php echo form_password($password); ?> 
		<?php echo form_error($password['name']); ?> 
                </div>
            </div>
	  
            <div class="control-group">
                <div class="control-label"><?php echo form_label('Confirmar senha', $confirm_password['id']); ?></div>
                <div class="controls">
		 <?php echo form_password($confirm_password); ?> 
		 <?php echo form_error($confirm_password['name']); ?> 
                </div>
            </div>
	  
        </fieldset>
	<?php if ($captcha_registration) {
		if ($use_recaptcha) { ?>
	 
		 
			<div id="recaptcha_image"></div>
		 
		 
			<a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a>
			<div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
			<div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>
		 
	  
	 
		 
			<div class="recaptcha_only_if_image">Enter the words above</div>
			<div class="recaptcha_only_if_audio">Enter the numbers you hear</div>
		 
		 <input type="text" id="recaptcha_response_field" name="recaptcha_response_field" /> 
		<td style="color: red;"><?php echo form_error('recaptcha_response_field'); ?> 
		<?php echo $recaptcha_html; ?>
	  
	<?php } else { ?>
	 
		<td colspan="3">
			<p>Enter the code exactly as it appears:</p>
			<?php echo $captcha_html; ?>
		 
	  
	 
		 <?php echo form_label('Confirmation Code', $captcha['id']); ?> 
		 <?php echo form_input($captcha); ?> 
		<td style="color: red;"><?php echo form_error($captcha['name']); ?> 
	  
	<?php }
	} ?>
                  <div class="clearfix"></div>
<?php echo form_submit('register', 'Registrar', 'class="btn btn-success"'); ?>
<?php echo form_close(); ?>
                  
</div>
</div>

<div class="clearfix"></div>

		</div>
                <div id="container-base"></div>
		
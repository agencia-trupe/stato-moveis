<div class="row-fluid">
    <div class="span9">
        <legend><?=( $acao == 'editar' ) ? 'Editar' : 'Cadastrar'; ?> Dica</legend>
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'dicas/admin_dicas/processa';
                    break;
                
                default:
                    $action = 'dicas/admin_dicas/processa_cadastro';
                    break;
            }
    ?>
    <?=form_open($action); ?>
    <?php if($acao == 'editar'): ?>
        <?=form_hidden('id', $dica->id); ?>
    <?php endif; ?>
    <?=form_label('Título (navegação)'); ?>
    <?=form_input(array(
        'name' => 'titulo_nav',
        'value' => set_value('titulo_nav', ($acao == 'editar') ? $dica->titulo_nav : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('titulo_nav'); ?>

    <?=form_label('Título (conteúdo)'); ?>
    <?=form_input(array(
        'name' => 'titulo_conteudo',
        'value' => set_value('titulo_conteudo', ($acao == 'editar') ? $dica->titulo_conteudo : ''),
        'class' => 'span8'
    )); ?>
    <?=form_error('titulo_conteudo'); ?>

    <?=form_label('Texto'); ?>
    <?=form_textarea(array(
        'name' => 'texto',
        'value' => set_value('texto', ($acao == 'editar') ? $dica->texto : ''),
        'class' => 'tinymce span9'
    )); ?>
    <?=form_error('texto'); ?>
    <div class="clearfix"></div>
    <br>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('dicas/admin_dicas/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
    </div>
</div>
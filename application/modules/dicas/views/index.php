<div class="conteudo dicas">
    <div id="tabs">
        <div class="sidebar">
            <ul>
                <? foreach ($dicas as $dica): ?>
                <li><a href="#tabs-<?=$dica->id; ?>"><?=$dica->titulo_nav; ?></a></li>
                <? endforeach; ?>
            </ul>
        <div class="clearfix"></div>
        </div>
        <div class="tabs-content">
            <? foreach ($dicas as $dica): ?>
            <div id="tabs-<?=$dica->id; ?>">
                <h1><?=$dica->titulo_conteudo; ?></h1>
                <?=$dica->texto; ?>
            </div>
            <? endforeach; ?>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
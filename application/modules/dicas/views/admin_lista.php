<div class="row-fluid">
    <div class="span9">
        <legend>Dicas <a class="btn btn-info btn-mini" href="<?=site_url('painel/dicas/cadastrar'); ?>">Novo</a></legend>
     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="span7">Título</th><th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dicas as $dica): ?>
                <tr>
                    <td><?=$dica->titulo_nav; ?></td>
                    <td><?=anchor('dicas/admin_dicas/editar/' . $dica->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?></a>
                        <?=anchor('painel/dicas/deleta_dica/' . $dica->id, 'Remover', 'class="btn btn-mini btn-danger"'); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
</div>
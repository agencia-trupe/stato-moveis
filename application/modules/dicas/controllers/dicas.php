<?php
class Dicas extends MX_Controller
{
    var $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('dicas/dica');
        $this->data['pagina'] = 'dicas';
    }
    public function index()
    {
        $this->lista();
    }
    public function lista()
    {
        $this->data['dicas'] = $this->dica->get_all();
        $this->data['conteudo'] = 'dicas/index';
        $seo = array(
            'titulo' => 'Dicas',
            'descricao' =>  'Dicas da Stato Móveis' 
            );
        $this->load->library( 'seo', $seo );
        $this->load->view( 'layout/template', $this->data );
    }

    public function insert()
    {
        for ($i=0; $i < 5; $i++) { 
            $dados = array();
            $dados['titulo_nav'] = 'Dica ' . $i;
            $dados['titulo_conteudo'] = 'Dica ' . $i;
            $dados['texto'] = '<p>Dica Stato Móveis';
            $insert = $this->dica->insert($dados);
            if($insert)
            {
                echo 'Ok!';
            }
            else
            {
                echo 'Nooooo!';
            }
        }
    }
}
<?php
class Dica extends Datamapper
{
    var $table = 'dicas';

    function get_all()
    {
        $dica = new Dica();
        $dica->get();
        $arr = array();
        foreach( $dica->all as $dica )
        {
            $arr[] = $dica;
        }
        if( sizeof( $arr ) )
        {
            return $arr;
        }
        return NULL;
    }
    function get_conteudo( $id )
    {
        $dica = new Dica();
        $dica->where( 'id', $id )->get();
        if( $dica->exists() ){
            return $dica;
        }
        return NULL;
    }

    function get_related( $id, $position )
    {
        $dica = new Dica();
        switch ( $position ) {
            case 'prev':
                $dica->where( 'id <', $id);
                break;
            case 'next':
                $dica->where( 'id >', $id);
                break;
        }
        $dica->get(1);
        if($dica->exists())
        {
            return $dica->id;
        }
        return FALSE;
    }

    function insert($dados)
    {
        $dica = new Dica();
        foreach ($dados as $key => $value)
        {
            $dica->$key = $value;
        }
        $dica->created = time();
        $insert = $dica->save();
        if($insert)
        {
            return TRUE;
        }
        return FALSE;
    }

    function change($dados)
    {
        $dica = new Dica();
        $dica->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update_data['updated'] = time();
        $update = $dica->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    function apaga($id)
    {
        $servico = new Dica();
        $servico->where('id', $id)->get();
        if($servico->delete())
        {
            return TRUE;
        }
        return FALSE;
    }
}
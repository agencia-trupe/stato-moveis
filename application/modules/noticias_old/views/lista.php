<div class="conteudo artigos">
  	<div class="inner">
		<div class="clearfix"></div>
		<h1>Artigos</h1>
		<section class="novidades">
		  	<? foreach($results as $result): ?>
		  		<article class="novidade">
		  			<? if($result->noticia_imagem != NULL): ?>
				    	<img class="noticia-img" src="<?=base_url(); ?>assets/img/noticias/thumbs/<?=$result->noticia_imagem; ?>" alt="<?=$result->noticia_titulo; ?>" class="novidade-imagem">
				    <? endif; ?>
		    		<div class="novidade-conteudo">
		      			<div class="novidade-texto <?=($result->noticia_imagem != NULL) ? 'imagem' : ''; ?>">
		        			<h1><a class="novidade-titulo" href="<?=site_url('artigos/post/' . $result->id); ?>"><?=$result->noticia_titulo; ?></a></h1>
		        			<p>
			        			<a href="<?=site_url('artigos/post/' . $result->id); ?>">
						        <?php
						        if($result->noticia_imagem != NULL){
						              $body = strip_tags($result->noticia_conteudo);
						              echo substr($body, 0, strpos($body, ' ', 300));
						        } else {
						              $body = strip_tags($result->noticia_conteudo);
						              echo substr($body, 0, strpos($body, ' ', 500));
						        }
						                   ?>
						        </a>
		      				</p>
		      			</div>
		    			<div class="clearfix"></div>
		    		</div>
		    		<div class="clearfix"></div>
		    		<a href="<?=site_url( 'artigos/post/' . $result->id); ?>" class="noticias-mais">continue lendo &raquo;</a>
		    		<div class="clearfix"></div>
		  		</article>
		  		<div class="separador-noticias"></div>
		  	<? endforeach; ?>
		</section>
  		<div class="clearfix"></div>
  	</div>
</div>
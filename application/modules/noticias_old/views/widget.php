<?php if(isset($parceiros)): ?>
    <?php foreach($parceiros as $parceiro): ?>
        <div class="parceiro-bloco">
            <header>
                <h1><a href="<?php echo site_url('parceiros/post/' . $parceiro->id); ?>"><?php echo $parceiro->parceiro_titulo; ?></a></h1>
            </header>
            <div class="conteudo">
                <p><?php echo substr(strip_tags($parceiro->parceiro_resumo), 0, 50); ?>  
                <a class="more" href="<?php echo base_url() . 'parceiros/detalhe/' . $parceiro->id; ?>">&raquo;</a> 
                </p>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
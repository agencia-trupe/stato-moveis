<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* clientes 
* 
* @author Nilton de Freitas - Trupe 
* @link http://trupe.net
*
*/
Class noticias extends MX_Controller
{
    public function index()
    {
        $this->lista();
    }
    public function home()
    {
        $this->load->model('noticias/noticia');
        $data['noticias'] = $this->noticia->get_home();
        $this->load->view('noticias/widget', $data);
    }
    public function lista()
    {      
        $this->load->library('calendar');
        $this->load->library('pagination');
            $pagination_config = array(
                        'base_url'       => base_url() . '/artigos/',
                        'total_rows'     => $this->db->where(array(
                                                'noticia_data_publicacao <' => time())
                                            )->get('noticias')->num_rows(),
                        'per_page'       => 8,
                        'num_links'      => 5,
                        'next_link'      => 'próximo',
                        'prev_link'      => 'anterior',
                        'first_link'     => FALSE,
                        'last_link'      => FALSE, 
                        'full_tag_open'  => '<div class="pagination-wrapper"><ul>',
                        'full_tag_close' => '</ul></div>',
                        'cur_tag_open'   => '<li class="active"><a href="#">',
                        'cur_tag_close'  => '</a></li>',
                        'num_tag_open'   => '<li>',
                        'num_tag_close'  => '</li>',
                        'next_tag_open'   => '<li>',
                        'next_tag_close'  => '</li>',
                        'prev_tag_open'   => '<li>',
                        'prev_tag_close'  => '</li>',
                );
            $this->pagination->initialize($pagination_config);
            //Obtendo resultados no banco
            $this->load->model('noticias/noticia');
            $data['results'] = $this->noticia->get_all($pagination_config['per_page'], $this->uri->segment(3));

        $data['pagina'] = 'artigos';
        $seo['title'] = 'Stato &middot; Artigos';
        $seo['description'] = 'Stato & middot; Artigos';
        $this->load->library('seo', $seo);
        $data['conteudo'] = "noticias/lista";
        $this->load->view('layout/template', $data);
    }

    public function post($id)
    {
        $this->load->library('calendar');
        $this->load->model('noticias/noticia');
        $data['post'] = $this->noticia->get_noticia($id);
        $data['pagina'] = 'artigos';
        $seo['title'] = 'Stato &middot; Artigos';
        $seo['description'] = 'Stato & middot; Artigos';
        $this->load->library('seo', $seo);
        $data['conteudo'] = "noticias/post";
        $this->load->view('layout/template', $data);
    }

    /**
     * Verifica se o tipo de cliente passado como parâmetro está no array
     * dos clientes e retorna a lista de clientes do tipo caso verdadeiro.
     * @return mixed Lista de clientes ativos
     */


    /**
     * Exibe a página de detalhes de um case
     * @param  int $id 
     */
    public function detalhe($id = NULL)
    {
        if($id == NULL)
        {
            redirect('clientes');
        }
        else
        {
            $this->load->model('clientes/cliente');
            //Verifica a existencia do cliente/case e retorna ao index caso falso.
            if(!$this->cliente->get_cliente($id))
            {
                redirect('clientes');
            }
            else{
                $data['cliente'] = $this->cliente->get_cliente($id);
                //Verifica a existência de registros anteriores e próximos e os retorna
                //caso seja verdadeiro.
                $anterior = $this->cliente->get_anterior($id);
                $data['anterior'] = $anterior ? $anterior->id : NULL;
                $proximo = $this->cliente->get_proximo($id);
                $data['proximo'] = $proximo ? $proximo->id : NULL;
                //Layout
                $data['titulo'] = 'Transpety &middot; Clientes | Cases - ' . $data['cliente']->nome;
                $data['pagina'] = 'clientes';
                $data['conteudo_principal'] = "clientes/detalhe";
                $this->load->view('layout/main', $data);
            }
        }
    }
}
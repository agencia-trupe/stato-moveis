<?php
class Empresa extends MX_Controller
{
    var   $data;

    public function index()
    {
        $this->load->model('admin_slideshow/slide');
        $seo['title'] = 'Stato &middot; Empresa';
        $seo['description'] = 'Stato Móveis';
        $this->data['fotos'] = $this->slide->get_all('empresa');
        $this->load->library('seo', $seo);
        $this->data['pagina'] = 'empresa';
        $this->data['conteudo'] = 'empresa/index';
        $this->load->view('layout/template', $this->data);
    }
}
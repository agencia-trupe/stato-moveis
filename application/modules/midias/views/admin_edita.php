<div class="row-fluid">
    <div class="span9">
        <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <legend><?=($acao == 'editar') ? 'Editar' : 'Cadastrar'; ?> Mídia</legend>
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'midias/admin_midias/processa';
                    break;
                
                default:
                    $action = 'midias/admin_midias/processa_cadastro';
                    break;
            }
    ?>
    <?=form_open_multipart($action); ?>
    <?php if($acao == 'editar'): ?>
        <?=form_hidden( 'id', $midia->id ); ?>
    <?php endif; ?>
    <?=form_label('Título'); ?>
    <?=form_input(array(
        'name' => 'titulo',
        'value' => set_value('titulo', ($acao == 'editar') ? $midia->titulo : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('titulo'); ?>
    <!--<select>
        <?php //foreach( $templates as $template ): ?>
        <option value="<?//=str_replace('.php', '', $template); ?>">
            <?//=ucfirst(str_replace('.php', '', $template)); ?>
        </option>
        <?php //endforeach; ?>
    </select>-->
    <br>
    <div class="clearfix"></div>
    <br>

     <?php if($acao == 'editar' && $midia->imagem):?>
     <img src="<?php echo base_url(); ?>assets/img/midia/thumbs/<?php echo $midia->imagem; ?>" alt="" >
     <?php endif; ?>
     <br>
     <div class="control-group">
            <label class="control-label" for="imagem"><?php echo ( $acao == 'editar' ) ? 'Alterar Imagem' : 'Adicionar Imagem'; ?></label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem')); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
    <!--<div class="row-fluid">
        <div class="alert alert-info span4">Criada em <?//=date( 'd/m/Y', $midia->created ); ?></div>
        <div class="alert alert-success span4 ">Última atualização: 
            <?//=(isset($midia->updated)) ? date( 'd/m/Y', $midia->updated ) : 'Nunca'; ?></div>
    </div>-->
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('midias/admin_midias/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
    </div>
</div>
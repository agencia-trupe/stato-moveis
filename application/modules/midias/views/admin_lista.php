<div class="row-fluid">
    <div class="span9">
        <legend>Mídia <a class="btn btn-info btn-mini" href="<?=site_url('painel/midia/cadastrar'); ?>">Novo</a></legend>
     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="span7">Título</th><th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php if($midias): ?>
            <?php foreach ($midias as $midia): ?>
                <tr>
                    <td><?=$midia->titulo; ?></td>
                    <td><?=anchor('midias/admin_midias/editar/' . $midia->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?>
                    <?=anchor('painel/midia/deleta_midia/' . $midia->id, 'Remover', 'class="btn btn-mini btn-danger"'); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
    </div>
</div>
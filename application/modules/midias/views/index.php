<div class="conteudo midias">
    <?php if ( isset($midias) ):
          foreach ( $midias as $midia ): ?>
        <a rel="prettyPhoto['midia']" href="<?=base_url( 'assets/img/midia/' . $midia->imagem ); ?>" class="midia-box">
            <img src="<?=base_url( 'assets/img/midia/thumbs/' . $midia->imagem ); ?>" 
             alt="<?=$midia->titulo; ?>">
            <div class="titulo"><span><?=$midia->titulo; ?></span></div>
            <span class="lupa"></span>
        </a>
    <?php endforeach; 
        endif; ?>
    <div class="clearfix"></div>
</div>
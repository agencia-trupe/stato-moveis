<div class="conteudo projetos">
    <div class="projeto-sidebar">
        <div class="projeto-meta">
           <h1><?=$projeto->titulo; ?></h1>
           <span class="data"><?=$projeto->data; ?></span>
           <div class="separador"></div>
           <div class="projeto-descricao"><?=$projeto->texto; ?></div>
        </div>
        <div class="projeto-navegacao">
            <div class="prev-container">
                <?=anchor( 'projetos/detalhe/' . $prev, 'anterior', 'class="projeto-anterior"'); ?>
            </div>
            <div class="grid-container">
                <?=anchor( 'projetos/detalhe', '', 'class="projeto-grid"'); ?>
            </div>
            <div class="next-containter">
                <?=anchor( 'projetos/detalhe/' . $next, 'próximo', 'class="projeto-proximo"'); ?>
            </div>
        </div>
    </div>
    <div class="projeto-galeria">
        <?php foreach ( $projeto->foto as $foto ): ?>
            <a href="#" class="projeto-foto">
                <img src="<?=base_url('assets/img/projetos/fotos/' . $foto->imagem ); ?>" alt="<?=$foto->titulo; ?>">
            </a>
        <?php endforeach; ?>
    </div>
</div>
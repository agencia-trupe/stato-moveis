<?php
class Midias extends MX_Controller
{
    var $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('midias/midia');
        $this->data['pagina'] = 'midia';
    }
    public function index()
    {
        $this->lista();
    }
    public function lista()
    {
        $this->data['midias'] = $this->midia->get_all();
        $this->data['conteudo'] = 'midias/index';
        $seo = array(
            'titulo' => 'Mídia',
            'descricao' =>  'Stato Móveis na mídia' 
            );
        $this->load->library( 'seo', $seo );
        $this->load->view( 'layout/template', $this->data );
    }

    public function insert()
    {
        for ($i=0; $i < 9; $i++) { 
            $dados = array();
            $dados['titulo'] = 'Midia ' . $i;
            $dados['imagem'] = 'midia_' . $i . '.jpg';
            $insert = $this->midia->insert($dados);
            if($insert)
            {
                echo 'Ok!';
            }
            else
            {
                echo 'Nooooo!';
            }
        }
    }
}
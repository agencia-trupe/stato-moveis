<?php
class Midia extends Datamapper
{
    var $table = 'midia';

    function get_all()
    {
        $midia = new Midia();
        $midia->get();
        $arr = array();
        foreach( $midia->all as $midia )
        {
            $arr[] = $midia;
        }
        if( sizeof( $arr ) )
        {
            return $arr;
        }
        return NULL;
    }
    function get_conteudo( $id )
    {
        $midia = new Midia();
        $midia->where( 'id', $id )->get();
        if( $midia->exists() ){
            return $midia;
        }
        return NULL;
    }

    function get_related( $id, $position )
    {
        $midia = new Midia();
        switch ( $position ) {
            case 'prev':
                $midia->where( 'id <', $id);
                break;
            case 'next':
                $midia->where( 'id >', $id);
                break;
        }
        $midia->get(1);
        if($midia->exists())
        {
            return $midia->id;
        }
        return FALSE;
    }

    function insert($dados)
    {
        $midia = new Midia();
        foreach ($dados as $key => $value)
        {
            $midia->$key = $value;
        }
        $midia->created = time();
        $insert = $midia->save();
        if($insert)
        {
            return TRUE;
        }
        return FALSE;
    }

    function change($dados)
    {
        $midia = new Midia();
        $midia->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update_data['updated'] = time();
        $update = $midia->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    function apaga($id)
    {
        $servico = new Midia();
        $servico->where('id', $id)->get();
        if($servico->delete())
        {
            return TRUE;
        }
        return FALSE;
    }
}
<div class="conteudo artigos post">
	<div class="inner">
		<div class="clearfix"></div>
		<h1>Artigos</h1>
  		<section class="novidades">
			<article class="novidade">
	  			<div class="novidade-conteudo">
					<h1><?=$post->noticia_titulo; ?></h1>
					<? if($post->noticia_imagem != NULL): ?>
						<div class="novidade-post-imagem">
							<img src="<?=base_url(); ?>assets/img/noticias/<?=$post->noticia_imagem; ?>" alt="<?=$post->noticia_titulo; ?>" class="novidade-detalhe-imagem">
						</div>
					<? endif; ?>
					<div class="novidade-texto">
					  	<?=$post->noticia_conteudo; ?>
					</div>
	  			</div>
			</article>
			<div class="clearfix"></div>
  		</section>
  		<div class="voltar-wrapper">
  			<a href="<?php echo site_url('artigos') ?>" class="noticias-voltar">voltar</a>
  		</div>
	</div>
  	<div class="clearfix"></div>
</div>
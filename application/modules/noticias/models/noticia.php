<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* noticia
*
* Model responsável pelas regras de negócio dos noticias de estágio, 
* trainee e aprendiz
*
* @author Nilton de Freitas - Trupe
* @link http://trupe.net
*
*/
Class Noticia extends Datamapper{
    var $table = 'noticias';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }
    /**
     * Retorna os noticias ativos em ordem crescente de antiguidade
     * @param    int    $limit   quantidade limite por retorno
     * @param    int    $offset  offset para a paginação
     * @return   array           array com a lista de noticias ativos
     */
    function get_all($limit, $offset)
    {
        $noticia = new noticia();
        $noticia->where('noticia_data_publicacao <', time());
        $noticia->order_by('noticia_data_publicacao', 'desc');
        $noticia->get($limit, $offset);

        $arr = array();
        foreach($noticia->all as $noticias)
        {
            $arr[] = $noticias;
        }

        return $arr;

    }

    function get_home()
    {
        $noticia = new noticia();
        $noticia->where('noticia_data_publicacao <', time());
        $noticia->order_by('noticia_data_publicacao', 'desc');
        $noticia->get(3);

        $arr = array();
        foreach($noticia->all as $noticias)
        {
            $arr[] = $noticias;
        }

        return $arr;

    }

    function get_all_all($limit, $offset)
    {
        $noticia = new noticia();
        $noticia->order_by('noticia_data_publicacao', 'desc');
        $noticia->get($limit, $offset);

        $arr = array();
        foreach($noticia->all as $noticias)
        {
            $arr[] = $noticias;
        }

        return $arr;

    }



    function cadastra($dados){

        $noticia = new noticia();

        $noticia->noticia_titulo = $dados['titulo'];
        $noticia->noticia_resumo = $dados['resumo'];
        $noticia->noticia_conteudo = $dados['conteudo'];
        $noticia->noticia_data_publicacao = strtotime(str_replace('/', '-', $dados['data_publicacao']));;

        if($noticia->save()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }

    function cadastra_imagem($dados){

        $noticia = new noticia();

        $noticia->noticia_titulo = $dados['titulo'];
        $noticia->noticia_resumo = $dados['resumo'];
        $noticia->noticia_conteudo = $dados['conteudo'];
        $noticia->noticia_imagem = $dados['imagem'];
        $noticia->noticia_data_publicacao = strtotime(str_replace('/', '-', $dados['data_publicacao']));;

        if($noticia->save()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }

    function atualiza_imagem($dados){

        $noticia = new noticia();
        $noticia->where('id', $dados['id']);
        $update = $noticia->update(array(
            'noticia_titulo' => $dados['titulo'],
            'noticia_resumo' => $dados['resumo'],
            'noticia_conteudo' => $dados['conteudo'],
            'noticia_imagem' => $dados['imagem'],
            'noticia_data_publicacao' => strtotime(str_replace('/', '-',
                $dados['data_publicacao'])),
        ));
        return $update;
    }

    function atualiza($dados){

        $noticia = new noticia();
        $noticia->where('id', $dados['id']);
        $update = $noticia->update(array(
            'noticia_titulo' => $dados['titulo'],
            'noticia_resumo' => $dados['resumo'],
            'noticia_conteudo' => $dados['conteudo'],
            'noticia_data_publicacao' => strtotime(str_replace('/', '-',
                $dados['data_publicacao'])),
        ));
        return $update;
    }
    function get_noticia($id)
    {
        $noticia = new noticia();
        $noticia->where('id', $id);
        $noticia->limit(1);
        $noticia->get();

        return $noticia;
    }

    function get_relate($id)
    {
        $given = new noticia();
        $atual = $given->where('id', $id)->get();

        $noticia = new noticia();
        $noticia->order_by('noticia_data_publicacao', 'desc');
        $noticia->where('noticia_data_publicacao <', $atual->noticia_data_publicacao);
        $noticia->limit(5);
        $noticia->get();

        $arr = array();
        foreach($noticia->all as $noticias)
        {
            $arr[] = $noticias;
        }

        return $arr;
    }

function get_anterior($id)
    {
        $given = new Noticia();
        $atual = $given->where('id', $id)->get();

        $anterior = new Noticia();
        $anterior->order_by('noticia_data_publicacao', 'desc');
        $anterior->where('noticia_data_publicacao <', $atual->noticia_data_publicacao);
        $anterior->limit(1);
        $anterior->get();
        if(!$anterior->exists())
        {
            return FALSE; 
        }
        else
        {
            return $anterior;
        }
    }

    /**
     * Retorna o registro anterior ao registro cujo id foi passado como parâmetro
     * @param  int $id 
     * @return mixed
     */
    function get_proximo($id)
    {
        $given = new Noticia();
        $atual = $given->where('id', $id)->get();

        $proximo = new Noticia();
        $proximo->order_by('noticia_data_publicacao', 'asc');
        $proximo->where('noticia_data_publicacao >', $atual->noticia_data_publicacao);
        $proximo->limit(1);
        $proximo->get();
        if(!$proximo->exists())
        {
            return FALSE; 
        }
        else
        {
            return $proximo;
        }
    }


     function delete_noticia($id){
        $noticia = new noticia();
        $noticia->where('id', $id)->get();


        if($noticia->delete()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }

}
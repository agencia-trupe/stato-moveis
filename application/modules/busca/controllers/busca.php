<?php
class Busca extends MX_Controller
{
    var   $data;

    public function lista()
    {
        $seo['title'] = 'Stato &middot; Busca';
        $seo['description'] = 'Stato Móveis';
        $this->load->library('seo', $seo);
        $this->data['pagina'] = 'produtos';
        $this->load->model('produtos/produto');
        $this->load->model('produtos/categoria');

        $this->data['query'] = $this->input->post('query');

        if( $this->data['query'] != NULL)
        {
            $this->data['produtos'] = $this->produto->get_like($this->input->post('query'));
        }
        $this->data['conteudo'] = 'busca/index';
        $this->load->view('layout/template', $this->data);
    }
}
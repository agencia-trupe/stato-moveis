<div class="conteudo busca">
	<div class="clearfix"></div>
		<h1>Resultado da busca 
		<?php if($query): ?>por "<?php echo $query ?>"
		<?php endif; ?>
	</h1>
	<div class="produtos-resultado">
		<?php if($query == NULL): ?>
		<p>Você precisa preencher o campo de busca com um termo para iniciar a pesquisa.</p>
		<?php elseif(isset($produtos)): ?>
		<?php foreach ($produtos as $produto): ?>
			<a href="<?php echo site_url('produtos/detalhe/' . $produto->id) ?>" class="produto-busca-wrapper">
				<span class="produto-busca-image-wrapper">
					<img src="<?php echo base_url('assets/img/produtos/lista/' . $produto->imagem) ?>" alt="">
				</span>
				<span class="clearfix"></span>
				<span class="produto-busca-titulo">
					<?php echo $produto->titulo ?>
				</span>
				<span class="clearfix"></span>
				<span class="produto-busca-resumo">
					<?php echo $produto->resumo ?>
				</span>
			</a>
		<?php endforeach ?>
		<?php else: ?>
			<p>Nenhum resultado encontrado.</p>
		<?php endif; ?>
	</div>
</div>
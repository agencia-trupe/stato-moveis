<div class="conteudo contato">
    <div class="inner">
        <div class="clearfix"></div>
        <h1>Contato</h1>
        <div class="boxes-wrapper">
            <div class="contato-box">
                <h2>Mande sua mensagem</h2>
                <?php echo form_open('', 'id="contato-form"'); ?>
                        <?php echo form_input(array('name'=>'nome','value'=>'','class'=>'nome nome_contato textbox', 'placeholder'=>'nome', 'title'=>'Nome'))?>
                        <?php echo form_input(array('name'=>'email','value'=>'','class'=>'email email_contato textbox', 'placeholder'=>'email', 'title'=>'Email'))?>
                        <?php echo form_input(array('name'=>'telefone','value'=>'','class'=>'telefone telefone_contato textbox', 'placeholder'=>'telefone', 'title'=>'Telefone'))?>
                        <?php echo form_label('Mensagem'); ?>
                        <div class="linha"></div>
                        <?php echo form_textarea(array('name'=>'mensagem','value'=>'','class'=>'mensagem mensagem_contato textbox', 'title'=>'mensagem'))?>
                        <input type="submit" name="submit" value="enviar" id="contato-submit">
                        <?php echo form_close("\n")?>
            </div>
            <div class="contato-box segundo">
                <h2>Trabalhe conosco</h2>
                <?php echo form_open_multipart('', 'id="trabalhe"'); ?>
                        <?php echo form_input(array('name'=>'nome','value'=>'','class'=>'nome_trabalhe', 'placeholder'=>'nome', 'title'=>'Nome'))?>
                        <?php echo form_input(array('name'=>'email','value'=>'','class'=>'email_trabalhe', 'placeholder'=>'email', 'title'=>'Email'))?>
                        <?php echo form_input(array('name'=>'telefone','value'=>'','class'=>'telefone_trabalhe', 'placeholder'=>'telefone', 'title'=>'telefone'))?>
                         <div id="yourBtn" >anexar currículo</div>
                         <input id="realupload" name="realupload" class="realupload" type="file" />
                        <input type="submit" name="submit" value="enviar" id="submit-trabalhe">
                        <?php echo form_close("\n")?>
            </div>
            <div class="contato-box terceiro">
                <h2>Newsletter</h2>
                <?php echo form_open('', 'id="cadastro-form"'); ?>
                        <?php echo form_input(array('name'=>'nome','value'=>'','class'=>'nome nome_cadastro textbox', 'placeholder'=>'nome', 'title'=>'Nome'))?>
                        <?php echo form_input(array('name'=>'email','value'=>'','class'=>'email email_cadastro textbox', 'placeholder'=>'email', 'title'=>'Email'))?>
                        <input type="submit" name="submit" value="enviar" id="submit-newsletter">
                <?php echo form_close(); ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="contato-info">
            <ul>
                <li class="twitter">
                </li>
                <li class="facebook">
                    <a href="<?php echo $showroom->facebook ?>" class="facebook">statomoveis</a>
                </li>
                <li class="telefone">
                    <?php echo str_replace('-', ' ', str_replace(')', '</span>', 
                        str_replace('(', '<span class="ddd">', $showroom->tel))) ?>
                </li>
                <li class="email"><a href="mailto://<?php echo $showroom->email ?>"><?php echo $showroom->email ?></a></li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
       <div class="span11">
            <legend>
                Mensagens - Fale Conosco 
            </legend>
        </div>
    </div>
    <div class="row-fluid">
    <div class="span11">
    <table class="table">
        <thead>
            <tr>
                <th>Data</th><th>IP</th><th>Nome</th><th>Email</th><th><i class="icon-cog"></i></th>
            </tr>
        </thead>
        <?php if ($mensagens): ?>
            <?php foreach ($mensagens as $mensagem): ?>
                <tr id="mensagem_<?php echo $mensagem->id ?>">
                    <td>
                        <?php echo date('d/m/y', $mensagem->created) . ' às ' . date('H:i', $mensagem->created) . 'h'?>
                        <?php if(!$mensagem->lida): ?>
                            <span class="label label-warning">não lida</span>
                        <?php else: ?>
                            <span class="label label-success">lida</span>
                        <?php endif; ?>
                        </td>
                    <td><?php echo long2ip($mensagem->ip) ?></td>
                    <td><?php echo $mensagem->nome ?></td>
                    <td><?php echo $mensagem->email ?></td>
                    <td>
                        <a class="btn btn-info btn-mini msg-show" href="<?php echo site_url('painel/mensagens/detalhe/' . $mensagem->id) ?>">Exibir</a>
                        <a class="btn btn-danger btn-mini" href="<?php echo site_url('painel/mensagens/apagar/' . $mensagem->id) ?>">Apagar</a>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php endif ?>
    </table>
    </div>
    </div>
</div><!--/span-->
<ul class="contato">
    <li>
        <div class="icone icone-telefone"></div>
        <span><a class="telefone-footer" href="tel:<?=$contato->ddd . $contato->telefone; ?>"><small><?=$contato->ddd; ?></small> <?=$contato->telefone; ?></a></span>
    </li>
    <li>
        <div class="icone icone-email"></div>
        <span><a style="text-decoration:none; color:whitesmoke;" href="mailto:<?=$contato->email; ?>"><?=$contato->email; ?></a></span>
    </li>
    </ul>
    <ul class="endereco">
        <li><span><?=$contato->endereco; ?>, <?=$contato->bairro; ?></span></li>
        <li><span><?=$contato->cidade; ?>, <?=$contato->uf; ?> - CEP <a class="cep-footer" href="#"><?=$contato->cep; ?></a></span></li>
    </ul>
    <div class="clearfix"></div>
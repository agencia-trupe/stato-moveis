<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
       <div class="span11">
            <legend>
                Mensagem - Fale Conosco
                <a href="<?php echo site_url('painel/mensagens/apagar/' . $mensagem->id) ?>" class="btn btn-danger btn-mini msg-unread">apagar</a>
            </legend>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span11">
            <table class="table table-hover">
            <tr>
                <td class="span3"><b>Data</b></td>
                <td><?php echo date('d/m/y', $mensagem->created) . ' às ' . date('H:i', $mensagem->created) . 'h'?></td>
            </tr>
            <tr>
                <td><b>IP</b></td>
                <td><?php echo long2ip($mensagem->ip) ?></td>
            </tr>
            <tr>
                <td><b>Nome</b></td>
                <td><?php echo $mensagem->nome ?></td>
            </tr>
            <tr>
                <td><b>Email</b></td>
                <td><?php echo $mensagem->email ?></td>
            </tr>
            <tr>
                <td><b>Telefone</b></td>
                <td><?php echo $mensagem->telefone ?></td>
            </tr>
            <tr>
                <td><b>Mensagem</b></td>
                <td><?php echo $mensagem->mensagem ?></td>
            </tr>
    </table>
        </div>
    </div>
    
</div><!--/span-->
<?php
class Admin_mensagens extends MX_Controller
{
    var $data;
    public function __construct()
    {
        parent::__construct();
        $this->data['module'] = 'contato';
        $this->load->model('contato/mensagem');
    }
    public function index()
    {
        $this->lista();
    }

    public function lista()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['mensagens'] = $this->mensagem->fetch_all();
            $this->data['conteudo'] = 'contato/admin_lista_mensagens';
            $this->load->view('start/template', $this->data);
        }
    }

    public function detalhe($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if($this->input->post('unread'))
            {
                $this->data['mensagem'] = $this->mensagem->get_conteudo($this->input->post('id'), 'id');
                try 
                {
                    $this->mensagem->marcar('nao_lida', $id);
                } 
                catch (Exception $e) 
                {
                    echo $e->getMessage() . "\n";
                }
            }
            else
            {
                $this->data['mensagem'] = $this->mensagem->get_conteudo($id, 'id');
                if($this->data['mensagem']->lida == FALSE)
                {
                    try 
                    {
                        $this->mensagem->marcar('lida', $id);
                    } 
                    catch (Exception $e) 
                    {
                        echo $e->getMessage() . "\n";
                    }
                }
            }
            $this->data['conteudo'] = 'contato/admin_detalhe_mensagens';
            $this->load->view('start/template', $this->data);
        }
    }

    public function apagar($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $apaga = $this->mensagem->apaga($id);
            if($apaga)
            {
                $this->session->set_flashdata('success', 'Registro removido com sucesso');
                redirect('painel/mensagens');
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível remover o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/mensagens/');
            }
        }
    }

    private function _marcar($acao, $id)
    {
        if($acao == 'lida')
        {
             if( !$this->mensagem->marcar_lida($id) )
                throw new Exception("Erro ao marcar como lida", 1);
            return TRUE;
        }
       
        return TRUE;
    }
}
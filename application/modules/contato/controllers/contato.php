<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Contato
* 
* @author Nilton de Freitas - Trupe 
* @link http://trupe.net
*
*/
Class Contato extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('contato/endereco');
    }
    var $data;
    /**
     * Exibe o formulário de contato
     */
    public function index()
    {
        $seo = array(
            'title' => 'Contato',
            'description' => 'Entre em contato com a Stato Móveis'
            );
        $this->load->library('seo', $seo);
        $this->data = array(
            'titulo' => 'Contato',
            'page_title' => 'Contato',
            'page_description' => 'Uma maneira rápida de solicitar informações
            ou visita na sua empresa',
            'pagina' => 'contato',
            'conteudo' => 'contato/index'
                );
        $this->load->model('showrooms/showroom');
        $this->data['showroom'] = $this->showroom->get_conteudo(1);
        $this->load->view('layout/template', $this->data);
    }

    function endereco($tipo = NULL)
    {
        $this->load->model('endereco');
        $data['endereco'] = $this->endereco->get_endereco();
        if($tipo == 'header')
        {
            $this->load->view('contato/endereco_header', $data);
        }
        elseif($tipo == 'contato')
        {
            $this->load->view('contato/endereco_contato', $data);
        }
        elseif($tipo == 'mail')
        {
            $this->load->view('contato/endereco_mail', $data);
        }
    }

    function parcial()
    {
        $this->load->model('contato/endereco');
        $data['contato'] = $this->endereco->get_endereco();
        $this->load->view('contato/parcial', $data);
    }
    
    function sociais()
    {
         $this->data['contato'] = $this->endereco->get_info();
         $this->load->view('contato/sociais', $this->data);
    }

    function ajax_check() 
    {
        $this->load->library('form_validation');
        $this->load->library('typography');

        if($this->input->post('ajax') == '1') 
        {
            $this->form_validation->set_error_delimiters('','');
            if($this->form_validation->run('contato') == FALSE) 
            {
                    echo validation_errors();
            } 
            else 
            {
                try {
                    $this->_salva_mensagem($this->input->post());
                } catch (Exception $e) {
                    echo $e->getMessage() . "\n";
                }
                $this->load->library('MY_PHPMailer');
                $mail = new PHPMailer();
                // Define os dados do servidor e tipo de conexão
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->IsSMTP(); // Define que a mensagem será SMTP
                //$mail->Host = "localhost"; // Endereço do servidor SMTP (caso queira utilizar a autenticação, utilize o host smtp.seudomínio.com.br)
                $mail->SMTPAuth = true; // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
                $mail->Username = 'noreply@stato.com.br'; // Usuário do servidor SMTP (endereço de email)
                $mail->Password = 'n0replyst'; // Senha do servidor SMTP (senha do email usado)
                
                $mail->Host = 'smtp.stato.com.br';
                $mail->Port = '587'; 
                // Define o remetente
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->From = "noreply@stato.com.br"; // Seu e-mail
                $mail->Sender = "noreply@stato.com.br"; // Seu e-mail
                $mail->FromName = $this->input->post('nome'); // Seu nome
                 
                // Define os destinatário(s)
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->AddAddress('contato@stato.com.br', 'Contato Stato');
                //$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
                $mail->AddBCC('nilton@trupe.net', 'Nilton - Trupe'); // Cópia Oculta
                 
                // Define os dados técnicos da Mensagem
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
                //$mail->CharSet = 'iso-8859-1'; // Charset da mensagem (opcional)
                    
                // Define a mensagem (Texto e Assunto)
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->Subject  = '=?UTF-8?B?'.base64_encode("Contato do Site Stato Móveis").'?='; // Assunto da mensagem
                
                $data['dados'] = array(
                                    'nome' => $this->input->post('nome'),
                                    'email' => $this->input->post('email'),
                                    'telefone' => $this->input->post('telefone'),
                                    'mensagem' => $this->typography->auto_typography($this->input->post('mensagem')),
                            );
                $email_view = $this->load->view('contato/email', $data, TRUE);

                $mail->Body = $email_view;


                if(!$mail->Send()) {
                  echo "Erro: " . $mail->ErrorInfo;
                } else {
                  echo "Mensagem enviada com sucesso!";
                }
            }
        }
    }

    private function _salva_mensagem($dados)
    {
        //adiciona a data/hora de processamento e o ip
        $dados['created'] = $_SERVER['REQUEST_TIME'];
        $dados['ip'] = ip2long($_SERVER['REMOTE_ADDR']);
        $this->load->model('contato/mensagem');

        if( !$this->mensagem->insert($dados) )
        {
            throw new Exception("Erro ao salvar mensagem no banco de dados", 1);
        }
    }
}


<?php

class Mensagem extends Datamapper
{
	var $table = 'mensagens';

	public function fetch_all()
	{
		$m = new Mensagem();
		$m->order_by('created', 'DESC')->get();

		$result = array();
		foreach($m->all as $mensagem)
		{
			$result[] = $mensagem;
		}
		if(count($result))
			return $result;
		return false;
	}

	public function insert($dados)
	{
		$mensagem = new Mensagem();

		foreach($dados as $key => $value)
		{
			$mensagem->$key = $value;
		}

		if($mensagem->save()) 
			return TRUE;
		return false;
	}

	public function get_conteudo($value, $key)
	{
		$mensagem = new Mensagem();
		$mensagem->where($key, $value)->get(1);
		if( !$mensagem->exists() )
			return false;
		return $mensagem;
	}

	public function marcar($acao, $id)
	{
		$mensagem = new Mensagem();
		$mensagem->where('id', $id);
		if($acao == 'lida')
		{
			if( !$mensagem->update(array('lida'=>TRUE)) )
				throw new Exception("Erro ao marcar como lida", 1);
			return TRUE;
		}
		else
		{
			if( !$mensagem->update(array('lida'=>FALSE)) )
				throw new Exception("Erro ao marcar como não lida", 1);
			return TRUE;
		}
	}

	public function apaga($id)
	{
		$mensagem = new Mensagem();
		$mensagem->where('id', $id)->get(1);
		if( !$mensagem->delete() )
			return FALSE;
		return TRUE;
	}
}
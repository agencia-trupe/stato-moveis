<div class="row-fluid">
    <div class="span9">
        <?php if($this->session->flashdata('error') != NULL): ?>
        <div class="alert alert-error">
            <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php endif; ?>
    <?php if(isset($error)): ?>
        <div class="alert alert-error">
            <?php echo $error['error']; ?>
        </div>
    <?php endif; ?>
    <legend><?=( $acao == 'editar' ) ? 'Editar' : 'Cadastrar'; ?> Projeto</legend>
    <!--Tipo de Form-->
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'painel/produtos/processa';
                    break;
                
                default:
                    $action = 'painel/produtos/processa_cadastro';
                    break;
            }
    ?>
    <!--fim de Tipo de Form-->
    <!--Form open-->
    <?=form_open_multipart($action); ?>
    <!--Id do objeto-->
    <?php if($acao == 'editar'): ?>
    <input type="hidden" name="id" value="<?=$produto->id; ?>" class="id" />
    <?php endif; ?>
    <!--fim de Id do objeto-->
    <!--Título-->
    <?=form_label('Título'); ?>
    <?=form_input(array(
        'name' => 'titulo',
        'value' => set_value('titulo', ( $acao == 'editar' ) ? $produto->titulo : '')
    )); ?>
    <?=form_error('titulo'); ?>
    <!--fim de Título-->
    <div class="clearfix"></div>
    <!--Categoria-->
    <?php echo form_label('Categoria') ?>
    <select name="categoria_id" class="categoria-select">
        <?php foreach ($categorias as $categoria): ?>
            <option value="<?php echo $categoria->id ?>" <?php echo ($acao == 'editar' && $produto->categoria->id == $categoria->id) ? 'selected' : '' ?>><?php echo $categoria->titulo ?></option>
        <?php endforeach ?>
    </select>
    <!--fim de Categoria-->
    <div class="clearfix"></div>
    <!--Subcategoria-->
    <?php echo form_label('Subcategoria') ?>
    <select name="subcategoria_id" class="subcategoria-select">
        <?php if ($acao=='editar'): ?>
            <?php foreach ($produto->categoria->subcategoria as $subcategoria): ?>
                <option value="<?php echo $subcategoria->id ?>" <?php echo ($produto->subcategoria->id == $subcategoria->id) ? 'selected' : '' ?>><?php echo $subcategoria->titulo ?></option>
            <?php endforeach ?>
        <?php else: ?>
            <?php foreach ($categorias[0]->subcategoria as $subcategoria): ?>
                <option value="<?php echo $subcategoria->id ?>"><?php echo $subcategoria->titulo ?></option>
            <?php endforeach ?>
        <?php endif ?>
    </select>
    <!--fim de Subcategoria-->
    <div class="clearfix"></div>
    <!--Resumo-->
    <?=form_label('Resumo'); ?>
    <?=form_textarea(array(
        'name' => 'resumo',
        'value' => set_value('resumo', ( $acao == 'editar' ) ? $produto->resumo : ''),
        'class' => 'span8'
    )); ?>
    <?=form_error('resumo'); ?>
    <!--fim de Resumo-->
    <!--Descrição-->
    <?=form_label('Descrição'); ?>
    <?=form_textarea(array(
        'name' => 'descricao',
        'value' => set_value('descricao', ( $acao == 'editar' ) ? $produto->descricao : ''),
        'class' => 'tinymce span8'
    )); ?>
    <?=form_error('descricao'); ?>
    <!--fim de Descrição-->
    <br>
    <?php if ($acao == 'editar') : ?>
    <img src="<?php echo base_url(); ?>assets/img/produtos/thumbs/<?php echo $produto->imagem; ?>" alt="" >
    <?php endif; ?>
    <div class="control-group">
            <label class="control-label" for="imagem">Foto de capa</label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem')); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
    <div class="clearfix"></div>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('produtos/admin_produtos/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
    <div class="clearfix"></div>
    <?php if ( $acao == 'editar' ): ?>
    <legend>Fotos do produto 
        
        <a href="#" class="<?=($fotos == NULL) ? 'invisible' : ''?> btn btn-mini btn-info btn-reordenar">
            <i class="icon-retweet icon-white"></i> Reordenar
        </a>
        <a href="#" class="btn btn-mini btn-info btn-adicionar-foto">
            <i class="icon-picture icon-white"></i> Adicionar Foto
        </a>
    </legend>
    <div class="row">
        <div class="form-adicionar-foto well span4 hide">
            <?=form_open('','id="produtos-upload"'); ?>
            <?=form_upload('produto-foto-upload', '', 'id="produto-foto-upload"'); ?>
            <input type="submit" class="btn btn-mini btn-success btn-adicionar-foto-upload"
            value="Fazer upload" >  
            <a href="#" class="btn btn-mini btn-danger btn-adicionar-foto-cancela">
            <i class="icon-remove-sign icon-white"></i> Cancelar</a>
            <?=form_close(); ?>
        </div>
    </div> 
    <div class="fotos-lista">
        <div class="alert alert-info hide produtos-interna-mensagem">
                <span>Fotos reordenadas com sucesso</span>
                <a class="close" data-dismiss="alert" href="#">&times;</a>
        </div>
        <br><br>
        <ul id="produto-images" class="ui-sortable" style="list-style-type:none; padding:0">
        <?php foreach ($fotos as $foto): ?>
            <li class="produto-foto" id="foto_<?=$foto->id; ?>">
                <img width="121" height="121" style="margin-bottom:10px;" src="<?=base_url('assets/img/produtos/thumbs/' . $foto->imagem); ?>" alt="">
                <a href="#" data-id="<?=$foto->id; ?>" data-module="produtos" class="btn btn-delete btn-mini btn-danger"><i class="icon-trash icon-white"></i></a>
            </li>
        <?php endforeach; ?>
        </ul>
        <div class="clearfix"></div>
    </div>
    <?php else: ?>
    <div class="alert alert-info">
        <span>Para adicionar fotos ao produto, salve-o e utilize a opção <em>editar</em></span>
    </div>
    <?php endif; ?>
    </div>
</div>
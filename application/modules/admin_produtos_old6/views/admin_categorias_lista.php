<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="span11">
            <legend>Categorias 
                <?php echo anchor('painel/produtos/categorias/cadastrar', 'Novo', 'class="btn btn-info btn-mini"'); ?>  
                <a href="#" class="ordenar-categorias btn btn-mini btn-info">ordenar categorias</a>
                <a href="#" class="salvar-ordem-categorias hide btn btn-mini btn-warning">salvar ordem</a>
            </legend>
            <div class="alert alert-info hide categorias-mensagem">
                <span>Para ordenar, clique no nome da categoria e arraste até a posição desejada</span>
                <a class="close" data-dismiss="alert" href="#">&times;</a>
            </div>
        </div>
    </div>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Título</th><th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php if ($categorias): ?>
                <?php foreach ($categorias as $categoria): ?>
                    <tr id="categoria_<?=$categoria->id; ?>">
                        <td><?php echo $categoria->titulo ?></td>
                        <td>
                            <a class="btn btn-success btn-mini" href="<?php echo site_url('painel/produtos/categorias/lista_sub/' . $categoria->id) ?>">Listar Subcategorias</a>
                            <a class="btn btn-info btn-mini" href="<?php echo site_url('painel/produtos/categorias/editar/' . $categoria->id) ?>">Editar</a>
                            <a class="btn btn-danger btn-mini" href="<?php echo site_url('painel/produtos/categorias/apagar/' .$categoria->id) ?>">Apagar</a>
                        </td>
                    </tr>
                <?php endforeach ?>
            <?php endif ?>
        </tbody>
    </table>
</div><!--/span-->
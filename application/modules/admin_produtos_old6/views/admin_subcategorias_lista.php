<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="span11">
            <legend>Subcategorias de <?php echo $categoria ?>
                <?php echo anchor('painel/produtos/categorias/', 'Voltar', 'class="btn btn-info btn-mini"'); ?>  
                <a href="#" class="ordenar-sub2categorias btn btn-mini btn-info">ordenar subcategorias</a>
                <a href="#" class="salvar-ordem-sub2categorias hide btn btn-mini btn-warning">salvar ordem</a>
            </legend>
            <div class="alert alert-info hide sub2categorias-mensagem">
                <span>Para ordenar, clique no nome da subcategoria e arraste até a posição desejada</span>
                <a class="close" data-dismiss="alert" href="#">&times;</a>
            </div>
        </div>
    </div>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Título</th><th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php if ($subcategorias): ?>
                <?php foreach ($subcategorias as $subcategoria): ?>
                    <tr id="subcategoria_<?=$subcategoria->id; ?>">
                        <td><?php echo $subcategoria->titulo ?></td>
                        <td>
                            <a class="btn btn-success btn-mini" href="<?php echo site_url('painel/produtos/lista/' . $subcategoria->id) ?>">Listar Produtos</a>
                        </td>
                    </tr>
                <?php endforeach ?>
            <?php endif ?>
        </tbody>
    </table>
</div><!--/span-->
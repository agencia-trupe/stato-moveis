<?php
class Admin_categorias extends MX_Controller
{
    var $data;
    public function __construct()
    {
        parent::__construct();
        $this->data['module'] = 'produtos';
        $this->load->model('produtos/categoria');
        $this->load->model('produtos/subcategoria');
        $this->load->model('produtos/produto');
    }
    public function index()
    {
        $this->lista();
    }

    public function lista()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['categorias'] = $this->categoria->fetch_all(TRUE);
            $this->data['conteudo'] = 'admin_produtos/admin_categorias_lista';
            $this->load->view('start/template', $this->data);
        }
    }

    public function lista_sub($categoria_id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['subcategorias'] = $this->subcategoria->get_all_by_categoria($categoria_id);
            $this->data['categoria'] = $this->data['subcategorias'][0]->categoria->titulo;
            $this->data['conteudo'] = 'admin_produtos/admin_subcategorias_lista';
            $this->load->view('start/template', $this->data);
        }
    }

    public function editar($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'editar';
            $this->data['categoria'] = $this->categoria->get_conteudo($id);
            $this->data['subcategorias'] = $this->subcategoria->get_all_by_categoria($id);
            $this->data['conteudo'] = 'admin_produtos/admin_categorias_edita';
            $this->load->view('start/template', $this->data);
        }
    }


    public function adicionar_subcategoria()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
            {
                $titulo = $this->input->post('subcategoria_titulo');
                $categoria_id = $this->input->post('categoria_id');

                $insert = $this->subcategoria->insert($titulo, $categoria_id);
                if($insert)
                {
                    $result['status'] = 'success';
                    $result['id'] = $insert->id;
                }
                else
                {
                    $result['status'] = 'error';
                    $result['message'] = 'Erro ao atualizar subcategoria';
                }
                echo json_encode($result);
            }
            else
            {
                redirect();
            }
        }
    }

    public function editar_subcategoria()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
            {
                $dados['id'] = $this->input->post('subcategoria_id');
                $dados['titulo'] = $this->input->post('subcategoria_titulo');
                if($this->subcategoria->change($dados))
                {
                    $result['status'] = 'success';
                }
                else
                {
                    $result['status'] = 'error';
                    $result['message'] = 'Erro ao atualizar subcategoria';
                }
                echo json_encode($result);
            }
            else
            {
                redirect();
            }
        }
    }

    public function apagar_subcategoria()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
            {
                $id = $this->input->post('subcategoria_id');
                if($this->subcategoria->remove($id))
                {
                    $result['status'] = 'success';
                }
                else
                {
                    $result['status'] = 'error';
                    $result['message'] = 'Erro ao atualizar subcategoria';
                }
                echo json_encode($result);
            }
            else
            {
                redirect();
            }
        }
    }

    public function cadastrar()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'cadastrar';
            $this->data['conteudo'] = 'admin_produtos/admin_categorias_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function processa_cadastro()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('categorias'))
            {
                $this->data['acao'] = 'cadastrar';
                $this->data['conteudo'] = 'categorias/admin_edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }
                $post['slug'] = get_slug($post['titulo']);

                /*if(strlen($_FILES["imagem"]["name"])>0)
                {
                    
                    $config['upload_path'] = './assets/img/categorias/capas';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '8000';
                    $config['max_width']  = '6000';
                    $config['max_height']  = '4000';

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('imagem'))
                    {

                            $this->data['error'] = array('error' => $this->upload->display_errors());
                            $this->data['categoria'] = $this->categoria->get_conteudo($id, $this->input->post('id'));
                            $this->data['conteudo'] = 'categorias/admin_edita';
                            $this->load->view('start/template', $this->data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

                        if($this->image_moo->load($file_uploaded)->resize_crop(188,328)->save($new_file,true))
                        {
                            $post['capa'] = $upload_data['file_name'];
                        }
                        else
                        {
                            $post['capa'] = NULL;
                        }
                    }
                }*/
                if($this->categoria->insert($post))
                {
                    $this->session->set_flashdata('success', 'Registro adicionado com sucesso');
                    redirect('painel/produtos/categorias');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível adicionar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/produtos/categorias/cadastrar');
                }
            }
        }
    }

    public function apagar($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $apaga = $this->categoria->apaga($id);
            if($apaga)
            {
                $this->session->set_flashdata('success', 'Registro removido com sucesso');
                redirect('painel/produtos/categorias/');
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível remover o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/produtos/categorias/');
            }
        }
    }

    public function processa()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('categorias'))
            {
                $this->data['acao'] = 'editar';
                $this->data['categoria'] = $this->categoria->get_conteudo($this->input->post('id'));
                $this->data['fotos'] = NULL;
                $this->data['conteudo'] = 'admin_produtos/admin_categorias_edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }

                $post['slug'] = get_slug($post['titulo']);

                /*if(strlen($_FILES["imagem"]["name"])>0)
                {
                    
                    $config['upload_path'] = './assets/img/categorias/capas';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '8000';
                    $config['max_width']  = '6000';
                    $config['max_height']  = '4000';

                    $this->load->library('upload', $config);

                    if ( !$this->upload->do_upload('imagem'))
                    {

                            $this->data['acao'] = 'editar';
                            $this->data['categoria'] = $this->categoria->get_conteudo($this->input->post('id'));
                            $this->data['fotos'] = $this->foto->get_categoria($this->input->post('id'));
                            $this->data['conteudo'] = 'teste';
                            $this->load->view('teste', $this->data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

                        if($this->image_moo->load($file_uploaded)->resize_crop(188,328)->save($new_file,true))
                        {
                            $post['capa'] = $upload_data['file_name'];
                        }
                        else
                        {
                            $post['capa'] = NULL;
                        }
                    }
                }*/
                if($this->categoria->change($post))
                {
                    $this->session->set_flashdata('success', 'Registro alterado com sucesso');
                    redirect('painel/produtos/categorias');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível alterar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/produtos/categorias/edita/' . $post['id']);
                }
            }
        }
    }

    public function sort_categorias()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Concept - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $itens = $this->input->post('categoria');
            if ($itens)
            {
                $ordenar = $this->categoria->ordena($itens);
                if($ordenar)
                {
                    echo 'Ordenado';
                }
                else
                {
                    echo 'Erro!';
                }
                /*foreach($items as $key => $value) 
                {           
                    // Use whatever SQL interface you're using to do
                    // something like this:
                    // UPDATE image_table SET sort_order = $key WHERE image_id = $value
                }*/
            } 
            else 
            {
              echo 'Erro!';
            }
        }
    }
}
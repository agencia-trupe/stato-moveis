<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    
    <hr>
    <div class="row-fluid">
       <div class="span11">
            <legend>
                Cases 
                <?php echo anchor('painel/cases/cadastra', 'Novo', 'class="btn btn-info btn-mini"'); ?>
                <a href="#" class="ordenar-cases btn btn-mini btn-info">ordenar cases</a>
                <a href="#" class="salvar-ordem-cases hide btn btn-mini btn-warning">salvar ordem</a>
            </legend>
            <div class="alert alert-info hide cases-mensagem">
                <span>Para ordenar, clique no título do case e arraste até a posição desejada</span>
                <a class="close" data-dismiss="alert" href="#">&times;</a>
            </div>
        </div>
    </div>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Título</th><th>Ações</th>
            </tr>
        </thead>
        <?php if ($cases): ?>
            <?php foreach ($cases as $case): ?>
                <tr id="case_<?php echo $case->id ?>">
                    <td><?php echo $case->titulo ?></td>
                    <td>
                        <a class="btn btn-info btn-mini" href="<?php echo site_url('painel/cases/editar/' . $case->id) ?>">Editar</a>
                        <a class="btn btn-danger btn-mini" href="<?php echo site_url('painel/cases/apagar/' .$case->id) ?>">Apagar</a>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php endif ?>
    </table>
    <div class="clearfix"></div>
</div><!--/span-->
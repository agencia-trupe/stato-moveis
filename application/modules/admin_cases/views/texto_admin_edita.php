<div class="span12">
	<div class="row-fluid">
    <legend>Cases - Texto superior</legend>
    <div class="case-texto well">
        <div class="case-texto-content">
            <?php echo $case_texto ?>
        </div>
        <div class="case-texto-edit" style="display:none;">
            <textarea name="case-texto" id="case-texto-edit" class="tinymce span9" cols="30" rows="10">
                <?php echo $case_texto ?>
            </textarea>
        </div>
    </div>
    <a href="#" class="btn btn-info btn-small case-texto-editor">Editar</a>
    <a href="#" style="display:none;" class="btn btn-success btn-small case-texto-salvar">Salvar</a>
    <div class="clearfix"></div>
</div>
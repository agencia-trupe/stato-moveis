<div class="row-fluid">
    <div class="span9">
        <?php if($this->session->flashdata('error') != NULL): ?>
        <div class="alert alert-error">
            <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php endif; ?>
    <?php if(isset($error)): ?>
        <div class="alert alert-error">
            <?php echo $error['error']; ?>
        </div>
    <?php endif; ?>
    <legend><?=( $acao == 'editar' ) ? 'Editar' : 'Cadastrar'; ?> Case</legend>
    <!--Tipo de Form-->
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'painel/cases/processa';
                    break;
                
                default:
                    $action = 'painel/cases/processa_cadastro';
                    break;
            }
    ?>
    <!--fim de Tipo de Form-->
    <!--Form open-->
    <?=form_open_multipart($action); ?>
    <!--Id do objeto-->
    <?php if($acao == 'editar'): ?>
    <input type="hidden" name="id" value="<?=$case->id; ?>" class="id" />
    <?php endif; ?>
    <!--fim de Id do objeto-->
    <!--Título-->
    <?=form_label('Título'); ?>
    <?=form_input(array(
        'name' => 'titulo',
        'value' => set_value('titulo', ( $acao == 'editar' ) ? $case->titulo : '')
    )); ?>
    <?=form_error('titulo'); ?>
    <!--fim de Título-->
    <div class="clearfix"></div>
    <!--Descrição-->
    <?=form_label('Descrição'); ?>
    <?=form_textarea(array(
        'name' => 'descricao',
        'value' => set_value('descricao', ( $acao == 'editar' ) ? $case->descricao : ''),
        'class' => 'tinymce span8'
    )); ?>
    <?=form_error('descricao'); ?>
    <!--fim de Descrição-->
    <br>
    <?php if ($acao == 'editar') : ?>
    <img src="<?php echo base_url(); ?>assets/img/cases/thumbs/<?php echo $case->imagem; ?>" alt="" >
    <?php endif; ?>
    <div class="control-group">
            <label class="control-label" for="imagem">Foto de capa</label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem')); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
    <div class="clearfix"></div>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('cases/admin_cases/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
    <div class="clearfix"></div>
    <?php if ( $acao == 'editar' ): ?>
    <legend>Fotos do case 
        
        <a href="#" class="<?=($fotos == NULL) ? 'invisible' : ''?> btn btn-mini btn-info btn-reordenar">
            <i class="icon-retweet icon-white"></i> Reordenar
        </a>
        <a href="#" class="btn btn-mini btn-info btn-adicionar-foto">
            <i class="icon-picture icon-white"></i> Adicionar Foto
        </a>
    </legend>
    <div class="row">
        <div class="form-adicionar-foto well span4 invisible">
            <?=form_open('','id="cases-upload"'); ?>
            <?=form_upload('caso-foto-upload', '', 'id="caso-foto-upload"'); ?>
            <input type="submit" class="btn btn-mini btn-success btn-adicionar-foto-upload"
            value="Fazer upload" >  
            <a href="#" class="btn btn-mini btn-danger btn-adicionar-foto-cancela">
            <i class="icon-remove-sign icon-white"></i> Cancelar</a>
        </div>
    </div> 
    <div class="fotos-lista">
        <ul id="case-images" class="ui-sortable" style="list-style-type:none; padding:0">
        <?php foreach ($case->cfoto as $foto): ?>
            <li class="case-foto" id="foto_<?=$foto->id; ?>">
                <img width="121" height="121" style="margin-bottom:10px;" src="<?=base_url('assets/img/cases/thumbs/' . $foto->imagem); ?>" alt="">
                <a href="#" data-module="cases" data-id="<?=$foto->id; ?>" class="btn btn-delete btn-mini btn-danger"><i class="icon-trash icon-white"></i></a>
            </li>
        <?php endforeach; ?>
        </ul>
        <div class="clearfix"></div>
    </div>
    <?php else: ?>
    <div class="alert alert-info">
        <span>Para adicionar fotos ao case, salve-o e utilize a opção <em>editar</em></span>
    </div>
    <?php endif; ?>
    </div>
</div>
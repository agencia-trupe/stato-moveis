<?php
class Admin_cases extends MX_Controller
{
    var $data;
    public function __construct()
    {
        parent::__construct();
        $this->data['module'] = 'cases';
        $this->load->model('cases/caso');
        $this->load->model('cases/cfoto');
    }
    public function index()
    {
        $this->lista();
    }

    public function lista()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->load->model('paginas/pagina');
            $case_texto = $this->pagina->get_conteudo('cases', 'slug');
            $this->data['case_texto'] = $case_texto->texto;
            $this->data['cases'] = $this->caso->fetch_all();
            $this->data['conteudo'] = 'admin_cases/admin_lista';
            $this->load->view('start/template', $this->data);
        }
    }

    public function texto()
    {
        $this->load->model('paginas/pagina');
        $case_texto = $this->pagina->get_conteudo('cases', 'slug');
        $this->data['case_texto'] = $case_texto->texto;
        $this->data['conteudo'] = 'admin_cases/texto_admin_edita';
        $this->load->view('start/template', $this->data);
    }

    public function editar($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'editar';
            $this->data['case'] = $this->caso->get_by_id($id);
            $this->data['fotos'] = NULL;
            $this->data['conteudo'] = 'admin_cases/admin_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function cadastrar()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'cadastrar';
            $this->data['conteudo'] = 'admin_cases/admin_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function processa_cadastro()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('cases'))
            {
                $this->data['acao'] = 'cadastrar';
                $this->data['conteudo'] = 'admin_cases/admin_edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }
                if(strlen($_FILES["imagem"]["name"])>0)
                {
                    
                    $config['upload_path'] = './assets/img/cases';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '8000';
                    $config['max_width']  = '6000';
                    $config['max_height']  = '4000';

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('imagem'))
                    {

                            $this->data['error'] = array('error' => $this->upload->display_errors());
                            $this->data['case'] = $this->caso->get_by_id($id, $this->input->post('id'));
                            $this->data['categorias'] = $this->categoria->fetch_all();
                            $this->data['conteudo'] = 'admin_cases/admin_edita';
                            $this->load->view('start/template', $this->data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
                        $lista = $upload_data['file_path'] . './lista/' . $upload_data['file_name'];
                        $thumb = $upload_data['file_path'] . './thumbs/' . $upload_data['file_name'];

                        if(
                            $this->image_moo->load($file_uploaded)
                                            ->resize(770,500,FALSE)
                                            ->save($new_file,true)
                            && 
                            $this->image_moo->load($file_uploaded)
                                            ->resize_crop(290,166)
                                            ->save($lista,true)
                            && 
                            $this->image_moo->load($file_uploaded)
                                            ->resize_crop(110,56)
                                            ->save($thumb,true)
                        )
                        {
                            $post['imagem'] = $upload_data['file_name'];
                        }
                    }
                }
                if($this->caso->insert($post))
                {
                    $this->session->set_flashdata('success', 'Registro adicionado com sucesso');
                    redirect('painel/cases');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível adicionar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/cases/');
                }
            }
        }
    }

    public function sort_fotos()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $itens = $this->input->post('foto');
            if ($itens)
            {
                $ordenar = $this->cfoto->ordena($itens);
                if($ordenar)
                {
                    echo 'Ordenado';
                }
                else
                {
                    echo 'Erro!';
                }
                /*foreach($items as $key => $value) 
                {           
                    // Use whatever SQL interface you're using to do
                    // something like this:
                    // UPDATE image_table SET sort_order = $key WHERE image_id = $value
                }*/
            } 
            else 
            {
              echo 'Erro!';
            }
        }
    }

    public function apagar($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $apaga = $this->caso->apaga($id);
            if($apaga)
            {
                $this->session->set_flashdata('success', 'Registro removido com sucesso');
                redirect('painel/cases');
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível remover o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/cases/');
            }
        }
    }

    public function deleta_foto()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if($this->input->post('ajax') == 1)
            $deleta = $this->cfoto->deleta_foto($this->input->post('foto_id'));
            if($deleta)
            {
                $response['status'] = 'success';
                $response['msg'] = 'Foto apagada com sucesso.';
            }
            else
            {
                $response['status'] = 'error';
                $response['msg'] = 'Erro ao apagar, por favor, 
                tente novamente';
            }
            echo json_encode($response);
        }
    }

    public function adiciona_foto()
    {

         $this->load->library('form_validation');

         $status = "";
         $msg = "";
         $file_element_name = 'caso-foto-upload';
        
         $config['upload_path'] = './assets/img/cases';
         $config['allowed_types'] = 'jpg|png';
         $config['max_size']  = '8000';
         $config['encrypt_name'] = FALSE;
         $this->load->library('upload', $config);
         if (!$this->upload->do_upload($file_element_name))
         {
             $status = 'error';
             $msg = $this->upload->display_errors('', '');
             $id = '';
             $nome = '';
         }
         else
         {
              $this->load->library('image_moo');
             //Is only one file uploaded so it ok to use it with $uploader_response[0].
             $upload_data = $this->upload->data();
             $file_uploaded = $upload_data['full_path'];
             $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
             $thumb = $upload_data['file_path'] . './thumbs/' . $upload_data['file_name'];

             if(
                 $this->image_moo->load($file_uploaded)
                                 ->resize(770,500,FALSE)
                                 ->save($new_file,true)
                 &&
                 $this->image_moo->load($file_uploaded)
                                 ->resize_crop(110,56)
                                 ->save($thumb,true)
             )
             {
                 $imagem = $upload_data['file_name'];
                     $data = array(
                     'imagem' => $imagem,
                     'caso_id' => $this->input->post('caso_id'),
                     'titulo' => 'Teste'
                     );
                 $adiciona = $this->cfoto->insert($data);
                 if($adiciona)
                 {
                     $status = "success";
                     $msg = "Foto adicionada com sucesso.";
                     $id = $adiciona;
                     $nome = $data['imagem'];
                 }
                 else
                 {
                     unlink($data['file_data']['full_path']);
                     $status = "error";
                     $msg = "Houve um erro ao processar sua requisição. 
                             Por vavor, tente novamente, ou entre 
                             em contato com o suporte.";
                     $id = '';
                     $nome = '';
                 }
             }
             else
             {
                 $status = 'error';
                 $msg = $this->image_moo->display_errors();
                 $id = '';
                 $nome = '';
             }
         }   
         @unlink($_FILES[$file_element_name]);
         echo json_encode(array(
                                 'status' => $status, 
                                 'msg' => $msg, 
                                 'id' => $id, 'nome' => $nome)
                         );
    }

    public function processa()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('cases'))
            {
                $this->data['acao'] = 'editar';
                $this->data['case'] = $this->caso
                                    ->get_by_id($this->input->post('id'));
                $this->data['fotos'] = NULL;
                $this->data['conteudo'] = 'admin_cases/admin_edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }
                if(strlen($_FILES["imagem"]["name"])>0)
                {
                    
                    $config['upload_path'] = './assets/img/cases';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '8000';
                    $config['max_width']  = '6000';
                    $config['max_height']  = '4000';

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('imagem'))
                    {
                        $this->data['error'] = array(
                                'error' => $this->upload->display_errors());
                        $this->data['case'] = $this->caso
                            ->get_by_id($id, $this->input->post('id'));
                        $this->data['categorias'] = $this->categoria
                            ->fetch_all();
                        $this->data['conteudo'] = 'admin_cases/admin_edita';
                        $this->load->view('start/template', $this->data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
                        $lista = $upload_data['file_path'] . './lista/' . $upload_data['file_name'];
                        $thumb = $upload_data['file_path'] . './thumbs/' . $upload_data['file_name'];

                        if(
                            $this->image_moo->load($file_uploaded)
                                            ->resize(770,500,FALSE)
                                            ->save($new_file,true)
                            &&
                            $this->image_moo->load($file_uploaded)
                                            ->resize_crop(290,166)
                                            ->save($lista,true)
                            &&
                            $this->image_moo->load($file_uploaded)
                                            ->resize_crop(110,56)
                                            ->save($thumb,true)
                        )
                        {
                            $post['imagem'] = $upload_data['file_name'];
                        }
                    }
                }
                if($this->caso->change($post))
                {
                    $this->session->set_flashdata('success', 'Registro alterado com sucesso');
                    redirect('painel/cases');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível alterar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/cases/edita/' . $post['id']);
                }
            }
        }
    }

    public function sort_cases()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Concept - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $itens = $this->input->post('cas');
            if ($itens)
            {
                $ordenar = $this->caso->ordena($itens);
                if($ordenar)
                {
                    echo 'Ordenado';
                }
                else
                {
                    echo 'Erro!';
                }
                /*foreach($items as $key => $value) 
                {           
                    // Use whatever SQL interface you're using to do
                    // something like this:
                    // UPDATE image_table SET sort_order = $key WHERE image_id = $value
                }*/
            } 
            else 
            {
              echo 'Erro!';
            }
        }
    }

    public function altera_texto()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Concept - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if( ! $this->input->is_ajax_request() )
                redirect();

            $this->load->model('paginas/pagina');
            $dados['slug'] = 'cases';
            $dados['texto'] = $this->input->post('texto');
            if( ! $this->pagina->change_by_slug($dados) ) 
            {
                echo 'error!';
            }
            else
            {
                echo   'success!';
            }
        }
    }

}
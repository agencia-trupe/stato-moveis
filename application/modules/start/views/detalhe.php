<div class="conteudo cases">
	<div class="inner">
		<div class="clearfix"></div>
		<div class="conteudo-case">
			<div class="sidebar">
				<h1>Cases</h1>
				<nav>
					<ul>
						<?php foreach ($cases as $nav): ?>
							<li><a <?php echo ($nav->id == $case->id) ? 'class="active"' : '' ?> href="<?php echo site_url('cases/detalhe/' . $nav->id) ?>"><?php echo $nav->titulo ?></a></li>
						<?php endforeach ?>
					</ul>
				</nav>	
			</div>
			<div class="case-wrapper">
				<h2><?php echo $case->titulo ?></h2>
				<div class="case-image-wrapper">
					<div class="case-slider">
						<div class="case-slide">
							<img src="<?php echo base_url('assets/img/cases/' . $case->imagem) ?>" alt="$case->titulo">
						</div>
						<?php foreach ($fotos as $foto): ?>
							<div class="case-slide">
								<img src="<?php echo base_url('assets/img/cases/' . $foto->imagem) ?>" alt="$case->titulo">
							</div>
						<?php endforeach ?>
					</div>
				</div>
				<?php print_r($image_count) ?>
				<?php print_r(count($fotos)) ?>
				<div class="case-thumbs">
					<a data-slide-index="0" href="#"><img src="<?php echo base_url('assets/img/cases/thumbs/' . $case->imagem) ?>" alt="<?php echo $case->titulo ?>"></a></li>
					<?php for ($i=1; $i < $image_count + 1; $i++): ?>
						<a  data-slide-index="<?php echo $i ?>" href="#"><img src="<?php echo base_url('assets/img/cases/thumbs/' . $fotos[$i - 1]->imagem) ?>" alt="<?php echo $case->titulo ?>"></a></li>
					<?php endfor ?>
					<div class="clearfix"></div>
				</div>
				<div class="case-description">
					<?php echo $case->descricao ?>
				</div>
				<div class="voltar-wrapper">
					<a href="<?php echo site_url('cases') ?>" class="case-voltar">voltar</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
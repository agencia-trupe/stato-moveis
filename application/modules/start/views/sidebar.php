<div id="sidebar">
    <?php echo form_open('busca/publico/resultado'); ?>
    <?php echo form_input('valor', set_value('valor', 'Pesquisar')); ?>
    <?php echo form_hidden('source', 'home'); ?>
    <input type="submit" class="int-button-search" value="">
    </form>
    <ul>
        <li><a href="<?php echo base_url() . 'nossa-historia'; ?>">Nossa História</a></li>
        <li  id="servicos">
            <a href="<?php echo base_url() . 'nossos-servicos'; ?>">Nossos Serviços</a>
            <ul>
                <li><a href="<?php echo base_url() . 'servicos/cursos'; ?>">Cursos</a></li>
                <li><a href="<?php echo base_url() . 'servicos/cursos-vip'; ?>">Cursos Vip</a></li>
                <li><a href="<?php echo base_url() . 'servicos/palestras'; ?>">Palestras</a></li>
                <li><a href="<?php echo base_url() . 'servicos/workshops'; ?>">Workshop</a></li>
                <li><a href="<?php echo base_url() . 'servicos/coaching'; ?>">Coaching</a></li>

            </ul>
        </li>
        <li><a href="<?php echo base_url() . 'paginas/nossos-compromissos'; ?>">Nossos Compromissos</a></li>
        <li><a href="<?php echo base_url(); ?>">Calendário</a></li>
        <li><a href="<?php echo base_url() . 'paginas/fotos'; ?>">Galeria</a></li>
        <li><a href="<?php echo base_url() . 'paginas/trabalhe-conosco'; ?>">Quero Palestrar</a></li>
    </ul>
    <ul id="social">
        <li><a target="_blank" href="https://www.facebook.com/pages/Instituto-Interagir/189966974467990" id="facebook"></a></li>
        <li><a target="_blank" href="http://twitter.com/_interagir" id="twitter"></a></li>
        <li><a target="_blank" href="http://www.linkedin.com/company/instituto-interagir/" id="linkedin"></a></li>
    </ul>
</div>


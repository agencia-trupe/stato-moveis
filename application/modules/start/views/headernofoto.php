<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title><?php echo $title; ?></title>
  <meta name="description" content="">
  <!-- Mobile viewport optimized: h5bp.com/viewport -->
  <meta name="viewport" content="width=device-width">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/interagir-base.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css">
  

  <?php 
  if(isset($css_files)): ?>
<?php foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
<?php endforeach; ?>
    <?php endif; ?> 


  <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->

  <!-- All JavaScript at the bottom, except this Modernizr build.
       Modernizr enables HTML5 elements & feature detects for optimal performance.
       Create your own custom Modernizr build: www.modernizr.com/download/ -->
  <script src="<?php base_url(); ?>assets/js/libs/modernizr-2.5.3.min.js"></script>
</head>
<body>
  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
    <div id="container">
        <header>
            <div id="logo-info">
            <a href="#" title="Stato Móveis" id="logo"></a>
            <div id="header-info">
                <p>Uma empresa do</p>
                <a href="http://grupodsrh.com.br/" title="Grupo DSRH®">
                </a>
            </div>
            </div>
            <div id="divider"></div>
            <div id="myCarousel" class="carousel slide">
            <div class="carousel-inner">
              <div class="item active">
                  <img src="<?php echo base_url(); ?>assets/img/slides/slide1.jpg" alt="">
              </div>
              <div class="item">
                <img src="<?php echo base_url(); ?>assets/img/slides/slide2.jpg" alt="">
                
              </div>
              <a href="" class="item">
                  <img src="<?php echo base_url(); ?>assets/img/slides/slide3.jpg" alt="">
                
              </a>
            </div>
            
          </div>
            	 <div class="clearfix"></div>
        </header>
        
    
                 
		<div id="corpo">
                    <div id="anteEsquerda">
				<ul><li></li></ul>
			</div>

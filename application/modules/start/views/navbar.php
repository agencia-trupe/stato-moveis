<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#"><? $this->seo->site_name(); ?></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li 
                  class="<?php if($module === 'perfil')
                  {
                      echo 'active';
                  }
                  ?>"
              ><a href="<?php echo base_url();?>gperfil">Perfil</a></li>
              <li 
                  class="<?php if($module === 'conquistas')
                  {
                      echo 'active';
                  }
                  ?>"
              >
              <a href="<?php echo base_url(); ?>gconquistas">Conquistas</a></li>
              <li 
                  class="<?php if($module === 'agenda')
                  {
                      echo 'active';
                  }
                  ?>"
               ><a href="<?php echo base_url(); ?>geventos">Agenda</a>
               <li 
                  class="<?php if($module === 'fotos')
                  {
                      echo 'active';
                  }
                  ?>"
               >
                  <a href="<?php echo base_url(); ?>gfotos">Fotos</a></li>
               <li 
                  class="<?php if($module === 'torcida')
                  {
                      echo 'active';
                  }
                  ?>"
              >
               <a href="<?php echo base_url(); ?>gtorcida/">Torcida</a></li>
              <li 
                  class="<?php if($module === 'imprensa')
                  {
                      echo 'active';
                  }
                  ?>"><a href="<?php echo base_url(); ?>gimprensa">Imprensa</a></li>
            </ul>
            <ul class="nav pull-right">
                <?php if ($this->tank_auth->is_logged_in()): ?>
                  <li id="user-menu" class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <?php
                          $username = $this->tank_auth->get_username();
                          echo 'Logado como '.$username;
                      ?>
                    <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><?php echo anchor('logout', ' logout'); ?></li>
                    </ul>
                  </li>
                  <?php endif; ?>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
<div id="accordion">
    <?php foreach ( $servicos as $servico ): ?>
        <a href="#" class="header">
            <div class="item-icone"></div>
            <h1><?=$servico->titulo; ?></h1>
        </a>
        <div class="servicos-texto">
            <?=$servico->descricao; ?>
            <div class="clearfix"></div>
        </div>
    <?php endforeach; ?>
</div>


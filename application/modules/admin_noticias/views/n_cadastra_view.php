<div class="span12">

<legend><?php echo $acao == 'editar' ? 'Editar noticia' : 'Nova noticia' ;?></legend>
    <?php echo isset($error) ? $error['error'] : ''; ?>
    <?php

    
    echo form_open_multipart(($acao == 'editar') ? 'painel/noticias/atualiza' : 'painel/noticias/salva', 'class="well"'); 
    ?>

    <div class="row-fluid">
      <?php if($acao == 'edita')
      {
        echo form_hidden('acao', 'edita');
      }
      else{
        echo form_hidden('acao', 'cadastra');
      }
      ?>  
      </div>
    <?php if($acao == 'editar') : ?>
    <?php echo
    form_hidden('id', set_value('id', $noticia->id));
    ?>
  <?php endif; ?>
    <div class="row-fluid">
        <div class="span9">
            <label for="">Título</label>
            <?php echo form_input(array('name'=>'titulo', 'id' => 'titulo', 'value'=>set_value('titulo', $acao == 'editar' ? $noticia->noticia_titulo : ''), 'class' => 'span12',)); ?>
            <?php echo form_error('titulo'); ?>
        </div>
    </div>

  <div class="row-fluid">
      <div class="clearfix"></div>
      <div class="control-group">
            <label class="control-label" for="conteudo">Conteúdo</label>
            <div class="controls">
              <?php echo form_textarea('conteudo', set_value('conteudo', $acao == 'editar' ? $noticia->noticia_conteudo : ''), 'class="tinymce span11"'); ?>
              <span class="help-inline"><?php echo form_error('conteudo'); ?></span>
            </div>
      </div>
      <div class="control-group">
            <label class="control-label" for="data_publicacao">Data de publicação</label>
            <div class="controls">
              <?php echo form_input('data_publicacao', set_value('data_publicacao', $acao == 'editar' ? date('d/m/Y', $noticia->noticia_data_publicacao) : ''), 'id="datepicker"'); ?>
              <span class="help-inline"><?php echo form_error('data_publicacao'); ?></span>
            </div>
      </div>
     <?php if($acao == 'editar'): ?>
     <?php if($noticia->noticia_imagem):?>
     <img src="<?php echo base_url(); ?>assets/img/noticias/<?php echo $noticia->noticia_imagem; ?>" alt="<?php echo $noticia->noticia_titulo; ?>" >
     <?php endif; ?>
     <?php endif; ?>
     <div class="control-group">
            <label class="control-label" for="imagem"><?php echo ($acao == 'editar' ? 'Adicionar / Alterar Imagem' : 'Imagem'); ?></label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem'), 'id="datepicker"'); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
  </div>
  <?php echo form_submit('submit', ($acao == 'editar') ? 'Salvar' : 'Cadastrar', 'class="btn btn-primary"'); ?>
  <?php echo form_close(); ?> 
</div>
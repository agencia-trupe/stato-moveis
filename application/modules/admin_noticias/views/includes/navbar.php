<ul class="menu hor">
    <li id="menuleft" class="gradiente ativo">
        
    </li>
    <li class="gradiente inativo">
        <a href="<?php echo base_url();?>atleta">
            <h1>O atleta</h1>
            <span>Perfil / Bio / Conquistas</span>
        </a>
    </li>
    <li class="gradiente inativo">
        <a href="<?php echo base_url();?>noticias">
            <h1>Notícias</h1>
            <span>Novidades do Rugby</span>
        </a>
    </li>
    <li class="gradiente inativo">
        <a href="<?php echo base_url();?>fotos">
            <h1>Fotos</h1>
            <span>Jogos / Daniel Gregg</span>
        </a>
    </li>
    <li class="gradiente inativo">
        <a href="<?php echo base_url();?>eventos">
            <h1>Eventos</h1>
            <span>Agenda / Resultados</span>
        </a>
    </li>
    <li class="gradiente inativo">
        <a href="<?php echo base_url();?>contato">
            <h1>Contato</h1>
            <span>Recados / Contato</span>
        </a>
    </li>
    <li class="gradiente ativo">
        <a id="fbtop" href="#"></a>
        <a id="twtop" href="#"></a>
    </li>
    <li id="menuright" class="gradiente inativo">
        
    </li>
</ul>
<div class="clearfix"></div>
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_noticias extends MX_Controller
{
    var $title;

    function __construct()
    {
        parent::__construct();
        $this->title = 'Transpety &middot; Novidades';
    }

    function index(){

        $this->lista();
    }
    /**
    *Lista todos os noticias cadastrados atualmente
    *
    * @return [type] [description]
    */
    function lista()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = $this->title;
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
          {
            $this->load->library('pagination');
            $this->load->library('table');
            $pagination_config = array(
                        'base_url'       => site_url() . 'admin_noticias/lista/',
                        'total_rows'     => $this->db->get('noticias')->num_rows(),
                        'per_page'       => 30,
                        'num_links'      => 5,
                        'next_link'      => 'próximo',
                        'prev_link'      => 'anterior',
                        'first_link'     => FALSE,
                        'last_link'      => FALSE, 
                        'full_tag_open'  => '<div class="pagination center"><ul>',
                        'full_tag_close' => '</ul></div>',
                        'cur_tag_open'   => '<li class="active"><a href="#">',
                        'cur_tag_close'  => '</a></li>',
                        'num_tag_open'   => '<li>',
                        'num_tag_close'  => '</li>',
                        'next_tag_open'   => '<li>',
                        'next_tag_close'  => '</li>',
                        'prev_tag_open'   => '<li>',
                        'prev_tag_close'  => '</li>',
                );
                $this->pagination->initialize($pagination_config);
                //Obtendo resultados no banco
                $this->load->model('noticias/noticia');
                $data['result'] = $this->noticia->
                get_all_all($pagination_config['per_page'], $this->uri->
                    segment(3));

                $data['title'] = $this->title;
                $data['module'] = 'noticias';
                $data['main_content'] = 'n_lista_view';
                $this->load->view('includes/template', $data);
          }
          else
          {
          Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de 
            acesso area administrativa sem privilégios');
          $this->session->set_flashdata('error', 'Erro de permissão. 
                                                Você precisa ser 
                                                administrador para realizar essa ação');
          redirect();

          } 
        }
    }
    /**
     * Mostra a página de edição de um noticia cujo id foi passado como 
     * parâmetro.
     *
     * @param  [int] $id [description]
     * @return [mixed]     [description]
     */
    function editar($id)
    {
        /**
         * Verifica se o usuário está logado para então prosseguir ou não.
         */
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Transpety - Painel de Controle';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            //Verifica se o usuário tem nível de acesso permitido
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                $id = $this->uri->segment(4);
                if(!$id)
                {
                    $this->session->set_flashdata('error', 'A ação não pode ser
                    realizada, tente novamente ou entre em contato com o suporte');
                    redirect('painel/noticias/lista');
                }
                else
                {
                    $this->load->model('noticias/noticia');
                    $data['module'] = 'noticias';
                    $data['title'] = $this->title;

                    if($this->noticia->get_noticia($id))
                    {
                        $data['noticia'] = $this->noticia->get_noticia($id);
                        $data['acao'] = 'editar';
                        $data['main_content'] = 'n_cadastra_view';
                        $this->load->view('includes/template', $data);
                    }
                    else
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                        redirect('noticias/lista');
                    }
                }
            }
            else
            {
                Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
                redirect();
            }
        }
    }

    function cadastra()
    {

        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Transpety - Painel de Controle';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
              if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
              {
                  $data['title'] = $this->title;
                  $data['module'] = 'noticias';
                  $data['acao'] = 'cadastra';
                  $data['main_content'] = 'n_cadastra_view';
                  $this->load->view('includes/template', $data);
              }
              else
              {
              Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
              $this->session->set_flashdata('error', 'Erro de permissão.
                                                    Você precisa ser administrador para realizar essa ação');
              redirect();

              }
        }
    }

    function salva(){

        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Transpety - Painel de Controle';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            $config = array(
                array(
                    'field' => 'titulo',
                    'label' => 'titulo',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'conteudo',
                    'label' => 'conteúdo',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'data_publicacao',
                    'label' => 'data_publicacao',
                    'rules' => 'required',
                ),
            );
            $this->load->library('form_validation');
            $this->form_validation->set_rules($config);
            $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');
            $data['acao'] = $this->input->post('acao');

            if($this->form_validation->run() == FALSE ){
                  $data['title'] = $this->title;
                  $data['module'] = 'noticias';
                  $data['main_content'] = 'n_cadastra_view';
                  $this->load->view('includes/template', $data);
            }
            else
            {   //Verifica se foi feito o upload de uma imagem
                if(strlen($_FILES["imagem"]["name"])>0)
                {
                    $config['upload_path'] = './assets/img/noticias/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '2000';
                    $config['max_width']  = '1600';
                    $config['max_height']  = '1200';

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('imagem'))
                    {
                            $data['error'] = array('error' => $this->upload->display_errors());

                            $data['title'] = $this->title;
                            $data['module'] = 'noticias';
                            $data['main_content'] = 'n_cadastra_view';
                            $this->load->view('includes/template', $data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
                        $thumb = $upload_data['file_path'] . './thumbs/' . $upload_data['file_name'];

                        if(
                            $this->image_moo->load($file_uploaded)
                                ->resize(600,600,$pad=FALSE)
                                ->save($new_file,true)
                            
                            &&

                            $this->image_moo->load($new_file)
                                 ->resize_crop(200,120)
                                 ->save($thumb, true)
                            )
                        {
                             $this->load->model('noticias/noticia');

                            //prepara o array com os dados para enviar ao model
                            $dados = array(
                                    'imagem' => $upload_data['file_name'],
                                    'titulo' => $this->input->post('titulo'),
                                    'conteudo' => $this->input->post('conteudo'),
                                    'data_publicacao' => $this->input->post('data_publicacao'),
                                );

                            if( ! $this->noticia->cadastra_imagem($dados))
                            {
                                $this->session->set_flashdata('error', 'A ação não pode ser
                                realizada, tente novamente ou entre em contato com o suporte');
                                redirect('painel/noticias/cadastra');
                            }
                            else
                            {
                                $this->session->set_flashdata('success', 'Notícia cadastrada
                                com sucesso!');
                                redirect('painel/noticias/lista');
                            }
                        }
                        else
                        {
                            $this->session->set_flashdata('error', 'A ação não pode ser
                            realizada, tente novamente ou entre em contato com o suporte');
                            redirect('painel/noticias/cadastra');
                        }
                    }
                }
                else
                {
                    $this->load->model('noticias/noticia');

                    //prepara o array com os dados para enviar ao model
                    $dados = array(
                            'titulo' => $this->input->post('titulo'),
                            'conteudo' => $this->input->post('conteudo'),
                            'data_publicacao' => $this->input->post('data_publicacao'),
                        );

                    if( ! $this->noticia->cadastra($dados))
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                        redirect('painel/noticias/cadastra');
                    }
                    else
                    {
                        $this->session->set_flashdata('success', 'Notícia cadastrada
                        com sucesso!');
                        redirect('painel/noticias/lista');
                    }
                }
            }
        }
    }

    function atualiza()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = $this->title;
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                $config = array(
                    array(
                    'field' => 'titulo',
                    'label' => 'titulo',
                    'rules' => 'required',
                    ),
                    array(
                        'field' => 'conteudo',
                        'label' => 'conteúdo',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'data_publicacao',
                        'label' => 'data_publicacao',
                        'rules' => 'required',
                    ),
                );
                $this->load->library('form_validation');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');

                if($this->form_validation->run() == FALSE )
                {
                    $id  = $this->input->post('id');
                    $this->load->model('noticias/noticia');
                    $data['module'] = 'noticias';
                    $data['title'] = $this->title;
                    $data['noticia'] = $this->noticia->get_noticia($id);
                    $data['acao'] = 'editar';
                    $data['main_content'] = 'n_cadastra_view';
                    $this->load->view('includes/template', $data);
                }
                else
                {
                    //verifica se foi postada uma imagem
                    if(strlen($_FILES["imagem"]["name"])>0)
                    {
                        $config['upload_path'] = './assets/img/noticias/';
                        $config['allowed_types'] = 'gif|jpg|png';
                        $config['max_size'] = '2000';
                        $config['max_width']  = '1600';
                        $config['max_height']  = '1200';

                        $this->load->library('upload', $config);

                        if ( ! $this->upload->do_upload('imagem'))
                        {
                                $data['error'] = array('error' => $this->upload->display_errors());

                                $id  = $this->input->post('id');
                                $this->load->model('noticias/noticia');
                                $data['module'] = 'noticias';
                                $data['title'] = $this->title;
                                $data['noticia'] = $this->noticia->get_noticia($id);
                                $data['acao'] = 'editar';
                                $data['main_content'] = 'n_cadastra_view';
                                $this->load->view('includes/template', $data);
                        }
                        else
                        {
                            $this->load->library('image_moo');
                            //Is only one file uploaded so it ok to use it with $uploader_response[0].
                            $upload_data = $this->upload->data();
                            $file_uploaded = $upload_data['full_path'];
                            $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
                            $thumb = $upload_data['file_path'] . './thumbs/' . $upload_data['file_name'];

                            if(
                                $this->image_moo->load($file_uploaded)
                                    ->resize(600,600,$pad=FALSE)
                                    ->save($new_file,true)
                                
                                &&

                                $this->image_moo->load($new_file)
                                     ->resize_crop(200,120)
                                     ->save($thumb, true)
                                )
                            {
                                 $this->load->model('noticias/noticia');

                                //prepara o array com os dados para enviar ao model
                                $dados = array(
                                        'id'     => $this->input->post('id'),
                                        'imagem' => $upload_data['file_name'],
                                        'titulo' => $this->input->post('titulo'),
                                        'conteudo' => $this->input->post('conteudo'),
                                        'data_publicacao' => $this->input->post('data_publicacao'),
                                    );

                                if( ! $this->noticia->atualiza_imagem($dados))
                                {
                                    $this->session->set_flashdata('error', 'A ação não pode ser
                                    realizada, tente novamente ou entre em contato com o suporte');
                                    redirect('painel/noticias/cadastra');
                                }
                                else
                                {
                                    $this->session->set_flashdata('success', 'Noticia atualizada
                                    com sucesso!');
                                    redirect('painel/noticias/lista');
                                }
                            }
                            else
                            {
                                $this->session->set_flashdata('error', 'A ação não pode ser
                                realizada, tente novamente ou entre em contato com o suporte');
                                redirect('painel/noticias/cadastra');
                            }
                        }
                    }
                    else
                    {

                        $this->load->model('noticias/noticia');

                        //prepara o array com os dados para enviar ao model
                        $dados = array(
                                    'id' => $this->input->post('id'),
                                    'titulo' => $this->input->post('titulo'),
                                    'conteudo' => $this->input->post('conteudo'),
                                    'data_publicacao' => $this->input->post('data_publicacao'),
                            );


                            if( ! $this->noticia->atualiza($dados))
                            {
                                $this->session->set_flashdata('error', 'A ação não pode ser
                                realizada, tente novamente ou entre em contato com o suporte');
                                redirect('painel/noticias/atualiza/' . $this->input->post('id'));
                            }
                            else
                            {
                                $this->session->set_flashdata('success', 'Noticia atualizada
                                com sucesso!');
                                redirect('painel/noticias/lista');
                            }
                    }
                }

              }
              else
              {
              Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
              $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
              redirect();

              }
        }
    }

    function apaga($id)
    {
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Transpety - Painel de Controle';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                $id = $this->uri->segment(4);
                if(!$id)
                {
                $this->session->set_flashdata('error', 'A ação não pode ser
                    realizada, tente novamente ou entre em contato com o suporte');
                redirect('painel/noticias/lista');
                }
                else
                {
                    $this->load->model('noticias/noticia');
                    if($this->noticia->delete_noticia($id))
                    {
                        $this->session->set_flashdata('success', 'Registro apagado
                        com sucesso');
                         redirect('painel/noticias/lista');
                    }
                     else
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                         realizada, tente novamente ou entre em contado com o suporte');
                        redirect('painel/noticias/lista');
                    }
                }
            }
            else
            {
                Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
                redirect();

            }
        }

    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
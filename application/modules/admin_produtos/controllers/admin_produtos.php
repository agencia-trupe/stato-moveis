<?php
class Admin_produtos extends MX_Controller
{
    var $data;
    public function __construct()
    {
        parent::__construct();
        $this->data['module'] = 'produtos';
        $this->load->model('produtos/produto');
        $this->load->model('produtos/categoria');
        $this->load->model('produtos/subcategoria');
        $this->load->model('produtos/foto');
    }
    public function index()
    {
        $this->lista();
    }

    public function lista($subcategoria)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {   
            $subcategoria_result = $this->subcategoria->get_conteudo($subcategoria);
            $this->data['subcategoria'] = $subcategoria_result->titulo;
            $categoria = $this->categoria->get_conteudo($subcategoria_result->categoria_id);
            $this->data['categoria'] = $categoria->titulo;
            $this->data['produtos'] = $this->produto->fetch_all($subcategoria, TRUE);
            $this->data['conteudo'] = 'admin_produtos/admin_lista';
            $this->load->view('start/template', $this->data);
        }
    }

    public function editar($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'editar';
            $this->data['produto'] = $this->produto->get_conteudo($id);
            $this->data['categorias'] = $this->categoria->fetch_all();
            $this->data['fotos'] = $this->foto->get_by_produto($id);
            $this->data['conteudo'] = 'admin_produtos/admin_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function cadastrar($subcategoria_id = NULL)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if($subcategoria_id){
                $this->data['subcategoria_id'] = $subcategoria_id;
            }
            $this->data['acao'] = 'cadastrar';
            $this->data['categorias'] = $this->categoria->fetch_all();
            $this->data['conteudo'] = 'admin_produtos/admin_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function processa_cadastro()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('produtos'))
            {
                $this->data['acao'] = 'cadastrar';
                $this->data['categorias'] = $this->categoria->fetch_all();
                $this->data['conteudo'] = 'admin_produtos/admin_edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }
                if(strlen($_FILES["imagem"]["name"])>0)
                {
                    
                    $config['upload_path'] = './assets/img/produtos';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '8000';
                    $config['max_width']  = '6000';
                    $config['max_height']  = '4000';

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('imagem'))
                    {

                            $this->data['error'] = array('error' => $this->upload->display_errors());
                            $this->data['produto'] = $this->produto->get_conteudo($id, $this->input->post('id'));
                            $this->data['categorias'] = $this->categoria->fetch_all();
                            $this->data['conteudo'] = 'admin_produtos/admin_edita';
                            $this->load->view('start/template', $this->data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './ampliadas/' . $upload_data['file_name'];
                        $lista = $upload_data['file_path'] . './lista/' . $upload_data['file_name'];
                        $thumb = $upload_data['file_path'] . './thumbs/' . $upload_data['file_name'];

                        $categoria = $this->categoria->get_slug($post['categoria_id']);
                        if($categoria !== 'assentos')
                        {
                            $lista_size['width'] = 360;
                            $lista_size['height'] = 167;
                            $thumb_size['width'] = 110;
                            $thumb_size['height'] = 56;
                            $thumb_process = $this->image_moo->load($file_uploaded)
                                 ->resize_crop($thumb_size['width'],$thumb_size['height'])
                                 ->save($thumb,true);
                        } else {
                            $lista_size['width'] = 230;
                            $lista_size['height'] = 300;
                            $thumb_size['width'] = 110;
                            $thumb_size['height'] = 110;
                            $thumb_process = $this->image_moo->load($file_uploaded)
                                 ->resize($thumb_size['width'],$thumb_size['height'],false)
                                 ->save($thumb,true);
                        }
                        if(
                            $this->image_moo->load($file_uploaded)
                                            ->resize(770,500,FALSE)
                                            ->save($new_file,true)
                            && 
                            $this->image_moo->load($file_uploaded)
                                            ->resize_crop($lista_size['width'],$lista_size['height'])
                                            ->save($lista,true)
                            &&
                            $thumb_process
                        )
                        {
                            $post['imagem'] = $upload_data['file_name'];
                        }
                    }
                }
                if($this->produto->insert($post))
                {
                    $this->session->set_flashdata('success', 'Registro adicionado com sucesso');
                    redirect('painel/produtos/lista/' . $post['subcategoria_id']);
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível adicionar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/produtos/lista/' . $post['subcategoria_id']);
                }
            }
        }
    }

    public function sort_fotos()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $itens = $this->input->post('foto');
            if ($itens)
            {
                $ordenar = $this->foto->ordena($itens);
                if($ordenar)
                {
                    echo 'Ordenado';
                }
                else
                {
                    echo 'Erro!';
                }
                /*foreach($items as $key => $value) 
                {           
                    // Use whatever SQL interface you're using to do
                    // something like this:
                    // UPDATE image_table SET sort_order = $key WHERE image_id = $value
                }*/
            } 
            else 
            {
              echo 'Erro!';
            }
        }
    }

    public function apagar($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $produto = $this->produto->get_conteudo($id);
            $apaga = $this->produto->apaga($id);
            if($apaga)
            {
                $this->session->set_flashdata('success', 'Registro removido com sucesso');
                redirect('painel/produtos/lista/' . $produto->subcategoria_id);
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível remover o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/produtos/lista' . $produto->subcategoria_id );
            }
        }
    }

    public function deleta_foto()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if($this->input->post('ajax') == 1)
            $deleta = $this->foto->deleta_foto($this->input->post('foto_id'));
            if($deleta)
            {
                $response['status'] = 'success';
                $response['msg'] = 'Foto apagada com sucesso.';
            }
            else
            {
                $response['status'] = 'error';
                $response['msg'] = 'Erro ao apagar, por favor, 
                tente novamente';
            }
            echo json_encode($response);
        }
    }

    public function adiciona_foto()
    {

         $this->load->library('form_validation');

         $status = "";
         $msg = "";
         $file_element_name = 'produto-foto-upload';
        
         $config['upload_path'] = './assets/img/produtos';
         $config['allowed_types'] = 'jpg|png';
         $config['max_size']  = 1024 * 8;
         $config['encrypt_name'] = FALSE;
         $this->load->library('upload', $config);
         if (!$this->upload->do_upload($file_element_name))
         {
             $status = 'error';
             $msg = $this->upload->display_errors('', '');
             $id = '';
             $nome = '';
         }
         else
         {
              $this->load->library('image_moo');
             //Is only one file uploaded so it ok to use it with $uploader_response[0].
             $upload_data = $this->upload->data();
             $file_uploaded = $upload_data['full_path'];
             $new_file = $upload_data['file_path'] . './ampliadas/' . $upload_data['file_name'];
             $lista = $upload_data['file_path'] . './lista/' . $upload_data['file_name'];
             $thumb = $upload_data['file_path'] . './thumbs/' . $upload_data['file_name'];

             $categoria = $this->categoria->get_slug($this->input->post('categoria_id'));
             
             if($categoria !== 'assentos')
             {
                $thumb_size['width'] = 110;
                $thumb_size['height'] = 56;
                $thumb_process = $this->image_moo->load($file_uploaded)
                                 ->resize_crop($thumb_size['width'],$thumb_size['height'])
                                 ->save($thumb,true);
             } else {
                $thumb_size['width'] = 110;
                $thumb_size['height'] = 110;
                $thumb_process = $this->image_moo->load($file_uploaded)
                                 ->resize($thumb_size['width'],$thumb_size['height'],false)
                                 ->save($thumb,true);
             }


             if(
                 $this->image_moo->load($file_uploaded)
                                 ->resize(770,500,FALSE)
                                 ->save($new_file,true)
                 && 
                 $this->image_moo->load($file_uploaded)
                                 ->resize_crop(360,167)
                                 ->save($lista,true)
                 &&
                 $thumb_process
             )
             {
                 $imagem = $upload_data['file_name'];
                     $data = array(
                     'imagem' => $imagem,
                     'produto_id' => $this->input->post('produto_id'),
                     'titulo' => 'Teste'
                     );
                 $adiciona = $this->foto->insert($data);
                 if($adiciona)
                 {
                     $status = "success";
                     $msg = "Foto adicionada com sucesso.";
                     $id = $adiciona;
                     $nome = $data['imagem'];
                 }
                 else
                 {
                     unlink($data['file_data']['full_path']);
                     $status = "error";
                     $msg = "Houve um erro ao processar sua requisição. 
                             Por vavor, tente novamente, ou entre 
                             em contato com o suporte.";
                     $id = '';
                     $nome = '';
                 }
             }
             else
             {
                 $status = 'error';
                 $msg = $this->image_moo->display_errors();
                 $id = '';
                 $nome = '';
             }
         }   
         @unlink($_FILES[$file_element_name]);
         echo json_encode(array(
                                 'status' => $status, 
                                 'msg' => $msg, 
                                 'id' => $id, 'nome' => $nome)
                         );
    }

    public function processa()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('produtos'))
            {
                $this->data['acao'] = 'editar';
                $this->data['produto'] = $this->produto
                                    ->get_conteudo($this->input->post('id'));
                $this->data['fotos'] = NULL;
                $this->data['conteudo'] = 'admin_produtos/admin_edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }
                if(strlen($_FILES["imagem"]["name"])>0)
                {
                    
                    $config['upload_path'] = './assets/img/produtos';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '8000';
                    $config['max_width']  = '6000';
                    $config['max_height']  = '4000';

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('imagem'))
                    {
                        $this->data['error'] = array(
                                'error' => $this->upload->display_errors());
                        $this->data['produto'] = $this->produto
                            ->get_conteudo($id, $this->input->post('id'));
                        $this->data['categorias'] = $this->categoria
                            ->fetch_all();
                        $this->data['conteudo'] = 'admin_produtos/admin_edita';
                        $this->load->view('start/template', $this->data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './ampliadas/' . $upload_data['file_name'];
                        $lista = $upload_data['file_path'] . './lista/' . $upload_data['file_name'];
                        $thumb = $upload_data['file_path'] . './thumbs/' . $upload_data['file_name'];

                        $categoria = $this->categoria->get_slug($post['categoria_id']);
                        if($categoria !== 'assentos')
                        {
                            $lista_size['width'] = 360;
                            $lista_size['height'] = 167;
                            $thumb_size['width'] = 110;
                            $thumb_size['height'] = 56;
                            $thumb_process = $this->image_moo->load($file_uploaded)
                                 ->resize_crop($thumb_size['width'],$thumb_size['height'])
                                 ->save($thumb,true);
                        } else {
                            $lista_size['width'] = 230;
                            $lista_size['height'] = 300;
                            $thumb_size['width'] = 110;
                            $thumb_size['height'] = 110;
                            $thumb_process = $this->image_moo->load($file_uploaded)
                                 ->resize($thumb_size['width'],$thumb_size['height'],false)
                                 ->save($thumb,true);
                        }
                        if(
                            $this->image_moo->load($file_uploaded)
                                            ->resize(770,500,FALSE)
                                            ->save($new_file,true)
                            && 
                            $this->image_moo->load($file_uploaded)
                                            ->resize_crop($lista_size['width'],$lista_size['height'])
                                            ->save($lista,true)
                            &&
                            $thumb_process
                        )
                        {
                            $post['imagem'] = $upload_data['file_name'];
                        }
                    }
                }
                if($this->produto->change($post))
                {
                    $this->session->set_flashdata('success', 'Registro alterado com sucesso');
                    redirect('painel/produtos/lista/' . $post['subcategoria_id']);
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível alterar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/produtos/edita/' . $post['id']);
                }
            }
        }
    }

    public function select_subcategoria()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Stato Móveis - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
            {
                $categoria_id = $this->input->post('categoria_id');
                $subcategorias = $this->subcategoria->get_by_categoria($categoria_id);
                if($subcategorias)
                {
                    $result['status'] = 'success';
                    $result['subcategorias'] = $subcategorias;
                }
                else
                {
                    $result['status'] = 'error';
                    $result['message'] = 'Erro ao atualizar subcategoria';
                }
                echo json_encode($result);
            }
            else
            {
                redirect();
            }
        }
    }

    public function sort_produtos()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Concept - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $itens = $this->input->post('produto');
            if ($itens)
            {
                $ordenar = $this->produto->ordena($itens);
                if($ordenar)
                {
                    echo 'Ordenado';
                }
                else
                {
                    echo 'Erro!';
                }
                /*foreach($items as $key => $value) 
                {           
                    // Use whatever SQL interface you're using to do
                    // something like this:
                    // UPDATE image_table SET sort_order = $key WHERE image_id = $value
                }*/
            } 
            else 
            {
              echo 'Erro!';
            }
        }
    }

    public function sort_subcategorias()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Concept - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $itens = $this->input->post('subcategoria');
            if ($itens)
            {
                $ordenar = $this->subcategoria->ordena($itens);
                if($ordenar)
                {
                    echo 'Ordenado';
                }
                else
                {
                    echo 'Erro!';
                }
                /*foreach($items as $key => $value) 
                {           
                    // Use whatever SQL interface you're using to do
                    // something like this:
                    // UPDATE image_table SET sort_order = $key WHERE image_id = $value
                }*/
            } 
            else 
            {
              echo 'Erro!';
            }
        }
    }
}
<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="span11">
            <legend>
                <?php echo $categoria . ' &raquo; ' . $subcategoria . ' &raquo; Produtos' ?> 
                <?php echo anchor('painel/produtos/cadastra', 'Novo', 'class="btn btn-info btn-mini"'); ?>
                <a href="#" class="ordenar-produtos btn btn-mini btn-info">ordenar produtos</a>
                <a href="#" class="salvar-ordem-produtos hide btn btn-mini btn-warning">salvar ordem</a>
            </legend>
            <div class="alert alert-info hide produtos-mensagem">
                <span>Para ordenar, clique no nome da categoria e arraste até a posição desejada</span>
                <a class="close" data-dismiss="alert" href="#">&times;</a>
            </div>
        </div>
    </div>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Categoria</th><th>Produto</th><th><i class="icon-cog"></i></th>
            </tr>
        </thead>
        <?php if ($produtos): ?>
            <?php foreach ($produtos as $produto): ?>
                <tr id="produto_<?php echo $produto->id ?>">
                    <td><?php echo $produto->categoria->titulo ?></td>
                    <td><?php echo $produto->titulo ?></td>
                    <td>
                        <a class="btn btn-info btn-mini" href="<?php echo site_url('painel/produtos/editar/' . $produto->id) ?>">Editar</a>
                        <a class="btn btn-danger btn-mini" href="<?php echo site_url('painel/produtos/apagar/' .$produto->id) ?>">Apagar</a>
                    </td>
                </tr>
            <?php endforeach ?>
        

    </table>
        <?php else: ?>
        <div class="alert alert-info">Não há produtos cadastrados nessa subcategoria.</div>
    <?php endif; ?>
</div><!--/span-->
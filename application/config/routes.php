<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['perfil'] = 'paginas/view/perfil';
$route['atuacao'] = 'paginas/view/atuacao';

$route['painel/mensagens'] = 'contato/admin_mensagens';
$route['painel/mensagens/(:any)'] = 'contato/admin_mensagens/$1';
$route['painel/curriculos'] = 'uploads/admin_curriculos';
$route['painel/curriculos/(:any)'] = 'uploads/admin_curriculos/$1';
$route['painel/arquitetos'] = 'arquitetos/admin_arquitetos';
$route['painel/arquitetos/(:any)'] = 'arquitetos/admin_arquitetos/$1';

$route['espaco-arquiteto'] = 'arquitetos';
$route['artigos'] = 'noticias';
$route['artigos/(:any)'] = 'noticias/$1';
$route['artigos/post/(:any)'] = 'noticias/post/$1';
$route['newsletter'] = 'newsletters';
$route['newsletter/lista/(:num)'] = 'newsletters/lista/$1';
$route['newsletter/lista'] = 'newsletters/lista';
$route['newsletter/detalhe/(:num)'] = 'newsletters/detalhe/$1';
$route['showroom'] = 'showrooms';
$route['localizacao'] = 'showrooms';
$route['artigos/lista/(:any)'] = 'noticias/lista/$1';


$route['login']  = 'auth/login';
$route['logout'] = 'auth/logout';
$route['registrar'] = 'auth/register';

$route['painel'] = 'admin_produtos/admin_categorias';
$route['painel/paginas'] = 'paginas/admin_paginas';
$route['painel/projetos'] = 'projetos/admin_projetos';
$route['painel/projetos/(:any)'] = 'projetos/admin_projetos/$1';
$route['painel/servicos'] = 'servicos/admin_servicos';
$route['painel/servicos/(:any)'] = 'servicos/admin_servicos/$1';
$route['painel/dicas'] = 'dicas/admin_dicas';
$route['painel/dicas/(:any)'] = 'dicas/admin_dicas/$1';
$route['painel/contato'] = 'contato/admin_contatos';
$route['painel/contato/(:any)'] = 'contato/admin_contatos/$1';
$route['painel/midia'] = 'midias/admin_midias';
$route['painel/midia/(:any)'] = 'midias/admin_midias/$1';
$route['painel/slideshow'] = 'admin_slideshow';
$route['painel/slideshow/(:any)'] = 'admin_slideshow/$1';

$route['painel/noticias'] = 'admin_noticias';
$route['painel/noticias/lista'] = 'admin_noticias/lista';
$route['painel/noticias/cadastra'] = 'admin_noticias/cadastra';
$route['painel/noticias/salva'] = 'admin_noticias/salva';
$route['painel/noticias/editar/(:any)'] = 'admin_noticias/editar/$1';
$route['painel/noticias/atualiza'] = 'admin_noticias/atualiza';
$route['painel/noticias/apaga/(:any)'] = 'admin_noticias/apaga/$1';

$route['painel/produtos'] = 'admin_produtos';
$route['painel/produtos/select_subcategoria'] = 'admin_produtos/select_subcategoria';
$route['painel/produtos/lista'] = 'admin_produtos/lista';
$route['painel/produtos/lista/(:num)'] = 'admin_produtos/lista/$1';
$route['painel/produtos/cadastra'] = 'admin_produtos/cadastrar';
$route['painel/produtos/cadastra/(:any)'] = 'admin_produtos/cadastrar/$1';
$route['painel/produtos/processa_cadastro'] = 'admin_produtos/processa_cadastro';
$route['painel/produtos/editar/(:any)'] = 'admin_produtos/editar/$1';
$route['painel/produtos/processa'] = 'admin_produtos/processa';
$route['painel/produtos/apagar/(:any)'] = 'admin_produtos/apagar/$1';
$route['painel/produtos/adiciona_foto'] = 'admin_produtos/adiciona_foto';
$route['painel/produtos/deleta_foto'] = 'admin_produtos/deleta_foto';
$route['painel/produtos/sort_fotos'] = 'admin_produtos/sort_fotos';
$route['painel/produtos/sort_produtos'] = 'admin_produtos/sort_produtos';
$route['painel/produtos/sort_subcategorias'] = 'admin_produtos/sort_subcategorias';

$route['painel/cases'] = 'admin_cases';
$route['painel/cases/texto'] = 'admin_cases/texto';
$route['painel/cases/lista'] = 'admin_cases/lista';
$route['painel/cases/cadastra'] = 'admin_cases/cadastrar';
$route['painel/cases/processa_cadastro'] = 'admin_cases/processa_cadastro';
$route['painel/cases/editar/(:any)'] = 'admin_cases/editar/$1';
$route['painel/cases/processa'] = 'admin_cases/processa';
$route['painel/cases/apagar/(:any)'] = 'admin_cases/apagar/$1';
$route['painel/cases/adiciona_foto'] = 'admin_cases/adiciona_foto';
$route['painel/cases/deleta_foto'] = 'admin_cases/deleta_foto';
$route['painel/cases/sort_fotos'] = 'admin_cases/sort_fotos';
$route['painel/cases/altera_texto'] = 'admin_cases/altera_texto';

$route['painel/produtos/categorias'] = 'admin_produtos/admin_categorias';
$route['painel/produtos/categorias/(:any)'] = 'admin_produtos/admin_categorias/$1';
$route['painel/produtos/categorias/apagar/(:any)'] = 'admin_produtos/admin_categorias/apagar/$1';

$route['painel/newsletters'] = 'admin_newsletters';
$route['painel/newsletters/lista'] = 'admin_newsletters/lista';
$route['painel/newsletters/cadastra'] = 'admin_newsletters/cadastrar';
$route['painel/newsletters/processa_cadastro'] = 'admin_newsletters/processa_cadastro';
$route['painel/newsletters/editar/(:any)'] = 'admin_newsletters/editar/$1';
$route['painel/newsletters/processa'] = 'admin_newsletters/processa';
$route['painel/newsletters/apagar/(:any)'] = 'admin_newsletters/apagar/$1';

$route['painel/showroom'] = 'admin_showrooms';
$route['painel/showroom/lista'] = 'admin_showrooms/lista';
$route['painel/showroom/cadastra'] = 'admin_showrooms/cadastrar';
$route['painel/showroom/processa_cadastro'] = 'admin_showrooms/processa_cadastro';
$route['painel/showroom/editar/(:any)'] = 'admin_showrooms/editar/$1';
$route['painel/showroom/processa'] = 'admin_showrooms/processa';
$route['painel/showroom/apagar/(:any)'] = 'admin_showrooms/apagar/$1';

$route['painel/cadastros'] = 'admin_cadastros';
$route['painel/cadastros/seleciona'] = 'admin_cadastros/seleciona';


$route['404_override'] = '';





/* End of file routes.php */
/* Location: ./application/config/routes.php */
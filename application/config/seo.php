<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Detalhes SEO
|
| Configurações básicas de SEO.
|--------------------------------------------------------------------------
*/
$config['site_name'] = 'Stato Móveis';

$config['site_decription'] = 'Stato Móveis';

$config['site_keywords'] = 'Stato, Móveis';

$config['site_author'] = 'Trupe Agência Criativa - http://trupe.net';
/* End of file seo.php */
/* Location: ./application/config/seo.php */
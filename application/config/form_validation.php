<?php 

$config = array(
    'showrooms' => array(
            array(
                    'field'=>'endereco',
                    'label'=>'endereço',
                    'rules'=>'required'
                ),
            array(
                    'field'=>'bairro',
                    'label'=>'bairro',
                    'rules'=>'required'
                ),
            array(
                    'field'=>'cidade',
                    'label'=>'cidade',
                    'rules'=>'required'
                ),
            array(
                    'field'=>'uf',
                    'label'=>'uf',
                    'rules'=>'required'
                ),
            array(
                    'field'=>'tel',
                    'label'=>'tel',
                    'rules'=>'required'
                ),
            array(
                    'field'=>'email',
                    'label'=>'email',
                    'rules'=>'required|valid_email'
                ),
            array(
                    'field'=>'inicio_seg_sex',
                    'label'=>'inicio atentimento seg - sex',
                    'rules'=>'required'
                ),
            array(
                    'field'=>'fim_seg_sex',
                    'label'=>'fim de atendimento seg-sex',
                    'rules'=>'required'
                ),
            array(
                    'field'=>'inicio_sab',
                    'label'=>'inicio_sab',
                    'rules'=>'required'
                ),
            array(
                    'field'=>'fim_sab',
                    'label'=>'fim_sab',
                    'rules'=>'required'
                ),
        ),
    'arquitetos' => array(
            array(
                    'field'=>'nome',
                    'label'=>'nome',
                    'rules'=>'required'
                ),
            array(
                    'field'=>'email',
                    'label'=>'email',
                    'rules'=>'required|valid_email'
                ),
            array(
                    'field'=>'endereco',
                    'label'=>'endereco',
                    'rules'=>'required'
                ),
            array(
                    'field'=>'numero',
                    'label'=>'numero',
                    'rules'=>'required'
                ),
            array(
                    'field'=>'cep',
                    'label'=>'cep',
                    'rules'=>'required'
                ),
            array(
                    'field'=>'cidade',
                    'label'=>'cidade',
                    'rules'=>'required'
                ),
            array(
                    'field'=>'estado',
                    'label'=>'estado',
                    'rules'=>'required'
                ),
            array(
                    'field'=>'telefone',
                    'label'=>'telefone',
                    'rules'=>'required'
                ),
            array(
                    'field'=>'statocad_mensagem',
                    'label'=>'mensagem',
                    'rules'=>'required'
                ),
        ),
    'categorias' => array(
            array(
                    'field' => 'titulo',
                    'label' => 'título',
                    'rules' => 'required'
                )
        ),
    'newsletters' => array(
            array(
                    'field' => 'mes',
                    'label' => 'mês',
                    'rules' => 'required'
                ),
             array(
                    'field' => 'ano',
                    'label' => 'ano',
                    'rules' => 'required'
                ),
        ),
     'paginas' => array(
             array(
                     'field' => 'titulo',
                     'label' => 'título',
                     'rules' => 'required'
                  ),
             array(
                     'field' => 'texto',
                     'label' => 'texto',
                     'rules' => 'required'
                  ),                       
        ),
     'produtos' => array(
             array(
                     'field' => 'titulo',
                     'label' => 'título',
                     'rules' => 'required'
                  ),
             array(
                     'field' => 'descricao',
                     'label' => 'descricao',
                     'rules' => 'required'
                  ),               
        ),
     'cases' => array(
             array(
                     'field' => 'titulo',
                     'label' => 'título',
                     'rules' => 'required'
                  ),
             array(
                     'field' => 'descricao',
                     'label' => 'descricao',
                     'rules' => 'required'
                  ),               
        ),
    'servicos' => array(
             array(
                     'field' => 'titulo',
                     'label' => 'título',
                     'rules' => 'required'
                  ),
             array(
                     'field' => 'descricao',
                     'label' => 'descricao',
                     'rules' => 'required'
                  ),                       
        ),
    'dicas' => array(
            array(
                     'field' => 'titulo_nav',
                     'label' => 'título (navegação)',
                     'rules' => 'required'
                  ),
            array(
                     'field' => 'titulo_conteudo',
                     'label' => 'título (conteúdo)',
                     'rules' => 'required'
                  ),
            array(
                'field' => 'texto',
                'label' => 'texto',
                'rules' => 'required'
                ),
        ),
    'endereco' => array(
            array(
                'field' => 'endereco',
                'label' => 'endereço',
                'rules' => 'required' 
                ),
            array(
                'field' => 'bairro',
                'label' => 'bairro',
                'rules' => 'required' 
                ),
            array(
                'field' => 'cidade',
                'label' => 'cidade',
                'rules' => 'required' 
                ),
            array(
                'field' => 'uf',
                'label' => 'uf',
                'rules' => 'required|max_length[2]' 
                ),
            array(
                'field' => 'cep',
                'label' => 'cep',
                'rules' => 'required' 
                ),
            array(
                'field' => 'email',
                'label' => 'email',
                'rules' => 'required' 
                ),
            array(
                'field' => 'telefone',
                'label' => 'telefone',
                'rules' => 'required' 
                ),
            array(
                'field' => 'twitter',
                'label' => 'twitter',
                'rules' => 'required' 
                ),
            array(
                'field' => 'facebook',
                'label' => 'facebook',
                'rules' => 'required' 
                ),
        ),
        'midia' => array(
            array(
                'field' => 'titulo',
                'label' => 'título',
                'rules' => 'required'  
            ),
        ),
        'contato' => array(
            array(
                'field' => 'nome',
                'label' => 'nome',
                'rules' => 'required'
            ),
            array(
                'field' => 'email',
                'label' => 'email',
                'rules' => 'required|valid_email'
            ),
            array(
                'field' => 'mensagem',
                'label' => 'mensagem',
                'rules' => 'required|min_length[30]'
            ),
        )
     );
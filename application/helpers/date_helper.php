<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * Date helpers
 * 
 * @author      Nilton Freitas
 * @link        http://trupe.net
 */

// ------------------------------------------------------------------------

if( ! function_exists('get_long_date'))
{
	function get_long_date($time)
	{
		$date = array();
		
		$day = date('d', $time);
		
		switch (date('m',$time)) {
			case '01': $month = 'janeiro'; break;
			case '02': $month = 'fevereiro'; break;
			case '03': $month = 'março'; break;
			case '04': $month = 'abril'; break;
			case '05': $month = 'maio'; break;
			case '06': $month = 'junho'; break;
			case '07': $month = 'julho'; break;
			case '08': $month = 'agosto'; break;
			case '09': $month = 'setembro'; break;
			case '10': $month = 'outubro'; break;
			case '11': $month = 'novembro'; break;
			case '12': $month = 'dezembro'; break;
		}

		$year = date('Y', $time);

		return $day . ' de ' . $month . ' de ' . $year;
	}

	function month_name($time, $short = null, $uppercase = null)
	{
		if (strlen((string) $time ) == 10){
			$input = date('m', $time);
		}
		else
		{
			$input = $time;
		}

		
		switch ($input) {
			case '01': $month = 'janeiro'; break;
            case '02': $month = 'fevereiro'; break;
            case '03': $month = 'março'; break;
            case '04': $month = 'abril'; break;
            case '05': $month = 'maio'; break;
            case '06': $month = 'junho'; break;
            case '07': $month = 'julho'; break;
            case '08': $month = 'agosto'; break;
            case '09': $month = 'setembro'; break;
            case '10': $month = 'outubro'; break;
            case '11': $month = 'novembro'; break;
            case '12': $month = 'dezembro'; break;
		}
		if($short)
			$month = substr($month, 0, 3);
		if($uppercase)
			return strtoupper($month);

		return $month;
	}
}
//Jquery Accordion - empresas
$(".accordionLink").click(function() {
    var id = $(this).data('panel');
    $( "#accordion" ).accordion( "option", "active", id );
});

$('.empresa-slider').bxSlider({
  pager: false,
  controls: false,
  auto: true,
  mode: 'fade',
  speed: 2000
});


function getFile(){
        document.getElementById("trabalhe-input").click();
  }

$(function() {
  //Botão Upload currículo

  //Statocad Message
  $('.statocad_mensagem').autoGrowInput();

  //Produtos slider
  $('.produto-slider').bxSlider({
    pagerCustom: '.produto-thumbs',
    controls: false
  });

  $('.case-slider').bxSlider({
    pagerCustom: '.case-thumbs',
    controls: false
  });

    $('body').on('click', '.navigation-link', function(){
      var subcategoria = $(this).data('subcategoria');
      var categoria = $(this).data('categoria');
      var offset = $(this).data('offset');
    $.ajax({
      dataType: "json",
      type: 'post',
      url: $(this).attr('href'),
      success: function(data){
        $('#sub-'+ subcategoria ).fadeOut(500, function(){
          $('#sub-' + subcategoria ).empty();
          var wrapper = $('#sub-' + subcategoria );
          $('.pagination-' + subcategoria ).empty();
          $('.pagination-' + subcategoria ).append(data.pagination);
          $.each(data.produtos, function(k, v) {
            template_data = {'offset': offset, 'categoria': categoria, 'subcategoria':subcategoria, 'produto_id':v.id, 'imagem':v.imagem, 'titulo':v.titulo, 'resumo':v.resumo}
            var produto_template = ich.produto_template(template_data);
            $('#sub-' + subcategoria ).append(produto_template);
            console.log(produto_template);
          });
          $('#sub-' + subcategoria ).fadeIn(500);
        });
      }
    });
    return false;
  });
 
  $('#tabs').tabs();
  
  $("#accordion").accordion({ 
    autoHeight: false,
    collapsible: true, 
  });
  $("a[rel^='prettyPhoto']").prettyPhoto({
      allow_resize: true,
      default_width: 1000,
      default_height: 1000,
      overlay_gallery: false,
      show_title: false,
      deeplinking: false,
      markup: '<div class="pp_pic_holder"> \
            <div class="ppt">&nbsp;</div> \
            <div class="pp_content_container"> \
                <div class="pp_content"> \
                  <div class="pp_loaderIcon"></div> \
                  <div class="pp_fade"> \
                                    <a class="pp_close" href="#">Close</a> \
                    <a href="#" class="pp_expand" title="Expand the image">Expand</a> \
                    <div class="pp_hoverContainer"> \
                      <a class="pp_next" href="#">next</a> \
                      <a class="pp_previous" href="#">previous</a> \
                    </div> \
                    <div id="pp_full_res"></div> \
                    <div class="pp_details"> \
                      <div class="pp_nav"> \
                        <a href="#" class="pp_arrow_previous">Previous</a> \
                        <a href="#" class="pp_arrow_next">Next</a> \
                      </div> \
                      <div class="pp_description_wrapper"> \
                      <div class="pp_description" style="display: block"></div> \
                      </div> \
                      {pp_social} \
                    </div> \
                  </div> \
                </div> \
            </div> \
          </div> \
          <div class="pp_overlay"></div>',
    });

    // jQuery Masked Input
    $('.statocad_celular').focusout(function(){
      var phone, element;
      element = $(this);
      element.unmask();
      phone = element.val().replace(/\D/g, '');
      if(phone.length > 10) {
          element.mask("(99) 99999-999?9");
      } else {
          element.mask("(99) 9999-9999?9");
      }
  }).trigger('focusout');
    $('.statocad_telefone').mask("(99) 9999-9999");
    $('.statocad_cep').mask("99999-999");
    
});

function runSlider(){
      //altera o resultado do array retornado em serve_slides
    function returnAsset(item){
      return '/assets/img/slides/' + item;
    }
    //Retorna o link para o projeto pai do slide atual
    function getSlideLink(id){
      return location.protocol + '//' + location.hostname + '/projetos/detalhe/' + id;
    }
    //Iinicia o full screen slideshow
      $.getJSON('/home/serve_slides', function(data) {
         $.backstretch(data.map(returnAsset), {
          duration:300000000,
          fade:1500
        });
      });
      slider = $('.bxslider').bxSlider({
          'auto':true,
          'pager':false,
          'speed': 1500,
          'mode':'fade',
          'nextSelector':'#slide_right',
          'nextText':'',
          'prevSelector':'#slide_left',
          'prevText':'',
          onSlideBefore: function($slideElement, oldIndex, newIndex){
            if(( oldIndex + 1 ) < newIndex )
            {
              $("body").data("backstretch").prev();
              console.log($(this));
              slider.startAuto();
            }
            else{
              if(oldIndex < newIndex || oldIndex > (newIndex + 1)){
              $("body").data("backstretch").next();
              console.log($(this));
              slider.startAuto();
              } 
              else {
                $("body").data("backstretch").prev();
                console.log($(this));
                slider.startAuto();
              }
            }
            
          }
        });
    }
/*
$( "#accordion" ).bind( "accordionchangestart", function(event, ui) {
});
*/
//Verifica o suporte ao HTML Placeholder - home/contato
jQuery(function() {
  jQuery.support.placeholder = false;
  test = document.createElement('input');
  if('placeholder' in test) jQuery.support.placeholder = true;
});

//Hack para navegadores que não oferecem suporte ao HTML5 Placeholder - home/contato
$(function() {
  if(!$.support.placeholder) { 
    var active = document.activeElement;
    $(':text').focus(function () {
      if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
        $(this).val('').removeClass('hasPlaceholder');
      }
    }).blur(function () {
      if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
        $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
      }
    });
    $(':text').blur();
    $(active).focus();
    $('form').submit(function () {
      $(this).find('.hasPlaceholder').each(function() { $(this).val(''); });
    });
  }
});

//Prepara a requisição ajax do tipo POST para enviar os dados do formulário de
//contato - contato
$(function() {
   $('#trabalhe').submit(function(e) {
      e.preventDefault();
      $.ajaxFileUpload({
         url         : location.protocol + "//" + location.host + "/uploads/upload_file/",
         secureuri      : false,
         fileElementId  :'realupload',
         dataType    : 'json',
         data        : {
            'nome' : $('.nome_trabalhe').val(),
            'email' : $('.email_trabalhe').val(),
            'telefone' : $('.telefone_trabalhe').val(),
         },
         success  : function (data, status)
         {
            alert(data.msg);
         }
      });
      return false;
   });


 $('#contato-form').submit(function() {
    $('#message').html('Enviando...'); 
    var form_data = {
      nome : $('.nome_contato').val(),
      telefone : $('.telefone_contato').val(),
      email : $('.email_contato').val(),
      mensagem : $('.mensagem_contato').val(),
       ajax : '1'
    };
    $.ajax({
      url: location.protocol + "//" + location.host + "/contato/ajax_check",
      type: 'POST',
      async : false,
      data: form_data,
      success: function(msg) {
        alert(msg);
      }
    });
    return false;
  });

 $('#cadastro-form').submit(function() {
    var form_data = {
      nome : $('.nome_cadastro').val(),
      email : $('.email_cadastro').val(),
    };
    $.ajax({
      url: location.protocol + "//" + location.host + "/cadastros/ajax_check",
      type: 'POST',
      async : false,
      data: form_data,
      success: function(msg) {
        alert(msg);
      }
    });
    return false;
  });
 $('input[type="checkbox"]').click(function(){
  if($(this).attr('checked') == 'checked'){
    $(this).attr('checked', false);
  } else {
    $(this).attr('checked', true);
  }
 });
 //Statocad
 $('.statocad-form').submit(function() {
    $('#message').html('Enviando...'); 
    var form_data = {
      nome : $('.statocad_nome').val(),
      email : $('.statocad_email').val(),
      endereco : $('.statocad_endereco').val(),
      site : $('.statocad_empresa').val(),
      numero : $('.statocad_numero').val(),
      complemento : $('.statocad_complemento').val(),
      cep : $('.statocad_cep').val(),
      cidade : $('.statocad_cidade').val(),
      estado : $('.statocad_estado').val(),
      telefone : $('.statocad_telefone').val(),
      celular : $('.statocad_celular').val(),
      statocad_mensagem : $('.statocad_mensagem').val(),
    };
    if( $('.ferramenta_statocad').attr('checked') == 'checked') { 
      form_data.ferramenta_statocad = $('.ferramenta_statocad').val();
    };
    if( $('.catalogo_eletronico').attr('checked') == 'checked' ) { 
      form_data.catalogo_eletronico = $('.catalogo_eletronico').val();
    };
    if( $('.catalogo_impresso').attr('checked') == 'checked' ) { 
      form_data.catalogo_impresso = $('.catalogo_impresso').val();
    };
    if( $('.agendar_visita').attr('checked') == 'checked' ) { 
      form_data.agendar_visita = $('.agendar_visita').val();
    };
    $.ajax({
      url: location.protocol + "//" + location.host + "/arquitetos/form_check",
      type: 'POST',
      async : false,
      data: form_data,
      success: function(msg) {
        alert(msg);
      }
    });
    return false;
  });
}); 


//Reordenar Slides
$('.ordenar-slides').click(function(){
  $("table tbody").sortable({'disabled': false, 'delay':'100'});
  $(this).toggle();
  $('.slides-mensagem, .salvar-ordem-slides').toggle();
  return false;
});

$('.salvar-ordem-slides').click(function(){
  $.ajax({
    type: "POST",
    url: location.protocol + "//" + location.hostname + "/painel/slideshow/sort_slides/",
    data: $("table tbody").sortable("serialize"),
    success: function(status){
      $("table tbody").sortable( "option", "disabled", true );
      $('.slides-mensagem span').text('Slides ordenados com sucesso.');
      $('.salvar-ordem-slides').toggle();
      $('.ordenar-slides').toggle();
    }
  });
  return false;
});

//Reordenar Cases
$('.ordenar-cases').click(function(){
  $("table tbody").sortable({'disabled': false, 'delay':'100'});
  $(this).toggle();
  $('.cases-mensagem, .salvar-ordem-cases').toggle();
  return false;
});

$('.salvar-ordem-cases').click(function(){
  $.ajax({
    type: "POST",
    url: location.protocol + "//" + location.hostname + "/admin_cases/sort_cases/",
    data: $("table tbody").sortable("serialize"),
    success: function(status){
      $("table tbody").sortable( "option", "disabled", true );
      $('.cases-mensagem span').text('Cases ordenadas com sucesso.');
      $('.salvar-ordem-cases').toggle();
      $('.ordenar-cases').toggle();
    }
  });
  return false;
});

//Reordenar Categorias
$('.ordenar-categorias').click(function(){
  $("table tbody").sortable({'disabled': false, 'delay':'100'});
  $(this).toggle();
  $('.categorias-mensagem, .salvar-ordem-categorias').toggle();
  return false;
});

$('.salvar-ordem-categorias').click(function(){
  $.ajax({
    type: "POST",
    url: location.protocol + "//" + location.hostname + "/painel/produtos/categorias/sort_categorias/",
    data: $("table tbody").sortable("serialize"),
    success: function(status){
      $("table tbody").sortable( "option", "disabled", true );
      $('.categorias-mensagem span').text('Categorias ordenadas com sucesso.');
      $('.salvar-ordem-categorias').toggle();
      $('.ordenar-categorias').toggle();
    }
  });
  return false;
});

//Reordenar Subcategorias
$('.ordenar-sub2categorias').click(function(){
  $("table tbody").sortable({'disabled': false, 'delay':'100'});
  $(this).toggle();
  $('.sub2categorias-mensagem, .salvar-ordem-sub2categorias').toggle();
  return false;
});

$('.salvar-ordem-sub2categorias').click(function(){
  $.ajax({
    type: "POST",
    url: location.protocol + "//" + location.hostname + "/painel/produtos/sort_subcategorias/",
    data: $("table tbody").sortable("serialize"),
    success: function(status){
      $("table tbody").sortable( "option", "disabled", true );
      $('.sub2categorias-mensagem span').text('Subcategorias ordenadas com sucesso.');
      $('.salvar-ordem-sub2categorias').toggle();
      $('.ordenar-sub2categorias').toggle();
    }
  });
  return false;
});

//Reordenar Produtos
$('.ordenar-produtos').click(function(){
  $("table tbody").sortable({'disabled': false, 'delay':'100'});
  $(this).toggle();
  $('.produtos-mensagem, .salvar-ordem-produtos').toggle();
  return false;
});

$('.salvar-ordem-produtos').click(function(){
  $.ajax({
    type: "POST",
    url: location.protocol + "//" + location.hostname + "/painel/produtos/sort_produtos/",
    data: $("table tbody").sortable("serialize"),
    success: function(status){
      $("table tbody").sortable( "option", "disabled", true );
      $('.produtos-mensagem span').text('Produtos ordenados com sucesso.');
      $('.salvar-ordem-produtos').toggle();
      $('.ordenar-produtos').toggle();
    }
  });
  return false;
});

//Reordenar subcategorias
$('.ordenar-subcategorias').click(function(){
  $(".subcategorias-list").sortable({'disabled': false, 'delay':'100'});
  $(this).toggle();
  $('.subcategorias-mensagem span').text('Para ordenar, clique no nome da subcategoria e arraste até a posição desejada')
  $('.salvar-ordem-subcategorias').toggle();
  return false;
});

$('.salvar-ordem-subcategorias').click(function(){
  $.ajax({
    type: "POST",
    url: location.protocol + "//" + location.hostname + "/painel/produtos/sort_subcategorias/",
    data: $(".subcategorias-list").sortable("serialize"),
    success: function(status){
      $(".subcategorias-list").sortable( "option", "disabled", true );
      $('.subcategorias-mensagem span').text('Subcategorias ordenadas com sucesso.');
      $('.salvar-ordem-subcategorias').toggle();
      $('.ordenar-subcategorias').toggle();
    }
  });
  return false;
});



function displayAlert(data){
    var type = 'alert-' + data.status;
    $('.top-alert').hide().addClass(type).fadeIn('fast', function(){
      $('.alert-content').text(data.texto);
    });
}

//Exibe edição de subcategoria
$(document.body).on('click', '.edit-subcategoria', function(){
  $(this).fadeOut('fast', function(){
    $(this).parent().css('list-style-type','none');
    $(this).parent().find('.editar-subcategoria-wrapper').fadeIn();
  });
  return false;
});

//Edita subcategoria
$(document.body).on('click', '.update-subcategoria', function(){
    var wrapper = $(this).parent();
    var form_data = {
      subcategoria_id : wrapper.find('.subcategoria_id').val(),
      subcategoria_titulo : wrapper.find('.input-subcategoria').val()
    };
    $.ajax({
      url: location.protocol + "//" + location.host + "/painel/produtos/categorias/editar_subcategoria",
      type: 'POST',
      async : false,
      data: form_data,
      success: function(data) {
        if(data.status = 'success')
        {
          wrapper.fadeOut('fast', function(){
             $(this).parent().css('list-style-type', 'disc').find('.edit-subcategoria').fadeIn('fast', function(){
                $(this).text(form_data.subcategoria_titulo);
             });
          });
        }
        else{
          $(this).parent().find('.subcategoria_message').text(data.message);
        }
        
      }
    });
    return false;
  });

//Edita subcategoria
$(document.body).on('click', '.delete-subcategoria', function(){
    if(confirm("A remoção desse registro afetará registros relacionados a ele, em outros módulos. Deseja realmente fazer isso?")) {
        var wrapper = $(this).parent();
        var form_data = {
          subcategoria_id : wrapper.find('.subcategoria_id').val(),
        };
        $.ajax({
          url: location.protocol + "//" + location.host + "/painel/produtos/categorias/apagar_subcategoria",
          type: 'POST',
          async : false,
          data: form_data,
          success: function(data) {
            if(data.status = 'success')
            {
              wrapper.fadeOut('fast');
            }
            else{
              $(this).parent().find('.subcategoria_message').text(data.message);
            }
            
          }
        });
        return false;
    }
  });

//Cancela edição de subcategoria
$(document.body).on('click', '.cancel-edit-subcategoria', function(){
  $(this).parent().fadeOut('fast', function(){
     $(this).parent().css('list-style-type', 'disc').find('.edit-subcategoria').fadeIn();
  });
  return false;
});

//Reordenar fotos de produto
$(document.body).on('click', '.btn-reordenar', function(){
    $("#produto-images").sortable({'disabled': false, 'delay':'100'});
    $("#produto-images li").css({'cursor':'pointer'});
    $('.fotos-lista').addClass('reordena');
    $(this).removeClass('btn-info btn-reordenar')
           .addClass('btn-warning btn-salva-ordem')
           .text('Salvar ordem')
    return false;
  }).on('click', '.btn-salva-ordem', function(){
      $(this).removeClass('btn-warning btn-salva-ordem')
             .addClass('btn-info btn-reordenar')
             .text('Reordenar');
      $.ajax({
        type: "POST",
        url: location.protocol + "//" + location.hostname + "/painel/produtos/sort_fotos/",
        data: $("#produto-images").sortable("serialize"),
        success: function(status){
          $("#produto-images").sortable( "option", "disabled", true );
          $("#produto-images li").css({'cursor':'default'});
          $(".fotos-lista").removeClass('reordena');
          $('.produtos-interna-mensagem').toggle();
        }
      });
      return false;
});

//Main
$(function(){
    //Editar cases
    $('.case-texto-editor').click(function(){
        $('.case-texto-content').fadeOut('slow', function(){
            $('.case-texto-edit').fadeIn();
        });
        $('.case-texto-editor').css('display', 'none');
        $('.case-texto-salvar').show();
        return false;
    });

    $('.case-texto-salvar').click( function(){
        $.ajax({
            type: "POST",
            url: location.protocol + "//" + location.hostname + "/painel/cases/altera_texto/",
            data: {
                'texto' : $('#case-texto-edit').val()
            },
            success: function(data){
                $('.case-texto-content').html($('#case-texto-edit').val());
                $('.case-texto-edit').fadeOut('slow', function(){
                    $('.case-texto-content').fadeIn();
                })
                $('.case-texto-salvar').css('display', 'none');
                $('.case-texto-editor').show();
            }
        });
        return false;
    })


  //Newsletter cadastros datepickr
  var dates = $( "#from, #to" ).datepicker({
        dateFormat: 'dd/mm/yy',
        defaultDate: "+1w",
        numberOfMonths: 1,
        beforeShow: function( ) {
            var other = this.id == "from" ? "#to" : "#from";
            var option = this.id == "from" ? "maxDate" : "minDate";
            var selectedDate = $(other).datepicker('getDate');
            
            $(this).datepicker( "option", option, selectedDate );
        }
    }).change(function(){
        var other = this.id == "from" ? "#to" : "#from";
        
        if ( $('#from').datepicker('getDate') > $('#to').datepicker('getDate') )
            $(other).datepicker('setDate', $(this).datepicker('getDate') );
    });

  //Modifica as subcategorias de produtos baseando-se na categoria
  $('.categoria-select').change(function(){
    var categoria_id = $(this).val();
    $.ajax({
        type: "POST",
        url: location.protocol + "//" + location.hostname + "/painel/produtos/select_subcategoria/",
        data: {
          'categoria_id' : categoria_id
        },
        success: function(data){
          data = JSON.parse(data);
          $('.subcategoria-select').empty();
          $.each(data.subcategorias, function(k, v) {
            var option_template = '<option value="' + k + '">' + v.titulo + '</option>';
            $('.subcategoria-select').append(option_template)
          });
        }
    });
  });


  //Fecha os alertas do topo
  $('.top-alert').on('click', '.close', function(){
    $('.top-alert').slideUp('fast', function(){
      $(this).removeClass('alert-success alert-error');
      $('.alert-content').text('');
    });
    return false;
  });

  /**
   * Exibe o formulário de adição de fotos;
   */
  $('.btn-adicionar-foto').click(function(){
    $('.form-adicionar-foto').slideDown().removeClass('invisible');
    return false;
  });
  $('.btn-adicionar-foto-cancela').click(function(){
    $('.form-adicionar-foto').slideUp();
    return false;
  });

  /**
   * Exclui uma foto relacionada a um produto
   */
  function deletaFoto(target){
    var foto = target.parent();
    var module = target.data('module');
    
    $.ajax({
        type: "POST",
        url: location.protocol + "//" + location.hostname + "/painel/" + module + "/deleta_foto/",
        data: {
          'ajax' : 1,
          'foto_id' : target.data('id')
        },
        success: function(msg){
          obj = JSON.parse(msg);
          var alerta = {'status':obj.status, 'texto':obj.msg }
          if(obj.status == "success"){
            $(foto).hide();
            displayAlert(alerta);
          } else {
            displayAlert(alerta);
          }
        }
    });
  }

  $(document.body).on('click', '.btn-delete', function(){
    deletaFoto($(this));
    return false;
  });
  /*$(document.body).on('click', 'btn-delete', function(){
    
    return false;
  });*/
  function displayReordena()
  {
    if($('.btn-reordenar').hasClass('invisible'))
    {
      $('.btn-reordenar').removeClass('invisible');
    }
  }

  $('#produtos-upload').submit(function(e) {
      e.preventDefault();
      $.ajaxFileUpload({
         url         : location.protocol + "//" + location.hostname + "/painel/produtos/adiciona_foto/",
         secureuri      : false,
         fileElementId  : 'produto-foto-upload',
         dataType    : 'json',
         data        : {
            'ajax' : 1,
            'produto_id' : $('.id').val(),
             'categoria_id' : $('.categoria-select').val()
         },
         success  : function (data, status)
         {
            var alerta = {'status':data.status, 'texto' : data.msg}
            if(data.status == 'error')
            {
              displayAlert(alerta);
            }
            else
            {
              var foto_template = ich.foto_template(data);
              displayReordena();
              $('#produto-images').append(foto_template);
              displayAlert(alerta);
            }
         }
      });
      return false;
   });

  //Cases Upload
  $('#cases-upload').submit(function(e) {
      e.preventDefault();
      $.ajaxFileUpload({
         url         : location.protocol + "//" + location.hostname + "/painel/cases/adiciona_foto/",
         secureuri      : false,
         fileElementId  : 'caso-foto-upload',
         dataType    : 'json',
         data        : {
            'ajax' : 1,
            'caso_id' : $('.id').val()
         },
         success  : function (data, status)
         {
            var alerta = {'status':data.status, 'texto' : data.msg}
            if(data.status == 'error')
            {
              displayAlert(alerta);
            }
            else
            {
              var case_foto_template = ich.case_foto_template(data);
              displayReordena();
              $('#case-images').append(case_foto_template);
              displayAlert(alerta);
            }
         }
      });
      return false;
   });

  //Adiciona uma nova subcategoria
  $('.add-subcategoria').click(function(){
    var form_data = {
      subcategoria_titulo : $('.subcategoria').val(),
      categoria_id: $('.id').val()
    };
    if(form_data.subcategoria_titulo !== '')
    {
       $.ajax({
        url: location.protocol + "//" + location.host + "/painel/produtos/categorias/adicionar_subcategoria",
        type: 'POST',
        async : false,
        data: form_data,
        success: function(data) {
          if(data.status = 'success')
          {
            data = JSON.parse(data);
            var template_data = {'titulo': form_data.subcategoria_titulo, 'id':data.id}
            var subcategoria_template = ich.subcategoria_template(template_data);
            $('.subcategorias-list').append(subcategoria_template);
            $('.subcategoria').val('');
          }
          else{
            $('.subcategoria_message').text(data.message);
          }
          
        }
      });
    }
    return false;
  });
});